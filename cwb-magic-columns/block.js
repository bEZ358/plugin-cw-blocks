( function( blocks, editor, i18n, element, components, _, blockEditor, cwb ) {
	const __ = i18n.__;
	const el = element.createElement;
	const InnerBlocks = blockEditor.InnerBlocks;
	const useBlockProps = blockEditor.useBlockProps;
	const InspectorControls = blockEditor.InspectorControls;

	const {
	} = components;

	const {
		cwbGlobals,
		cwbComponents
	} = cwb;

	const {
	} = cwbGlobals;

	const {
		cwbAttributes
	} = cwbComponents;


	// @ref links below
	// https://mediaron.com/how-to-create-a-repeater-field6-in-gutenberg/
	// https://reactnative.dev/docs/state

	blocks.registerBlockType( 'cwb/magic-columns', {


		// Block ASSETS
		//-----------------------------

		title: __( 'Magic Columns', 'CWB' ),
		icon: 'admin-page', // see: /wp-includes/css/dashicons.css
		category: 'common',
		attributes: {
			anchor: {
				type: 'string',
				default: false,
			},
			cssClasses: {
				type: 'array',
				default: ['my-0'],//['w-100', 'd-flex', 'm-0', 'align-items-center', 'bg-cover']
			},


			// Common Attribute Sets
			//-----------------------------
			...cwbAttributes.__blockColors,
			...cwbAttributes.__bgImage,
			// ...cwbAttributes.__blockBorder,
			...cwbAttributes.__bgOverlayColors,
			overlayBlur: {
				type: 'integer',
				default: 0,
			},
			reverseOnMobile: {
				type: 'boolean',
				default: false
			}

		},
		example: {
			attributes: {
				// title: __( 'Chocolate Chip Cookies', 'gutenberg-examples' ),
				// mediaID: 1,
				// mediaURL:
				// 	'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/2ChocolateChipCookies.jpg/320px-2ChocolateChipCookies.jpg',
			},
		},



		// Block FUNCTIONS
		//-----------------------------

		edit: function( props ) {
			let attributes = props.attributes;
			let blockStyle = {
				width: '100%',
				margin: '0px auto',
				padding: '0px',
				border: '1px dotted rgba(0,0,0,0.2)',
				color: attributes.textColor ? attributes.textColor : null,
				background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : attributes.bgColorOutput,
			};
			let reverseOnMobile		= __get(attributes.reverseOnMobile, false);
			let rowCss = 'row magic-columns mx-0';
			if(reverseOnMobile){
				rowCss += ' magic-mobile-reverse';
			}


			// RETURN: Simple-Repeater-Block (BACK-end)
			//================================
			return el( 'div', useBlockProps( {
					className: attributes.cssClasses,
					style: blockStyle,
				} ),


				el( 'div', {
						className: rowCss,
					},


					el(InnerBlocks, {
						allowedBlocks : [
							'cwb/magic-column',
						],
						template : [
							[ 'cwb/magic-column', { isLeftColumn: true, containerClasses: 'p-4 pl-magic-0'}],
							[ 'cwb/magic-column', { isLeftColumn: false, containerClasses: 'p-4 pr-magic-0'}],
						],
						// templateLock: 'insert',
						renderAppender : false//allowMoreItems ? null : false
					}),

				),

				// InspectorControls
				//================================
				el( InspectorControls, {
						key: 'controls' },
					// Panel : Layout Settings
					//-----------------------------
					el( 'div', {
							style: {
								padding: '0 20px'
							}
						},
						el( TextControl,
							{
								label: 'Block Anchor',
								value: typeof attributes.anchor === 'string' ? attributes.anchor : null,
								onChange: function(value){
									props.setAttributes({
										anchor: limitForHtmlId(value)
									})
								},
							}
						),

						el(ToggleControl, {
							label: 'Reverse on mobile',
							onChange: function (value) {
								props.setAttributes({
									reverseOnMobile: value,
								});
								attributes = props.attributes;
							},
							checked: attributes.reverseOnMobile,
							// help: focusItem.btn ? 'Click the slide button to edit it' : null,
						}),
					),


					// Panel : Block Colors
					//-----------------------------
					el( BlockColors, props),

					// Panel : Background Image
					//-----------------------------
					el( BgImagePanel, {
						props : props,
						blockStyle : blockStyle
					}),
				)
			)
		},

		save: function( props ) {

			let attributes = props.attributes;
			let rowCss = 'row magic-columns mx-0 flex-md-row';
			let blockStyle = {
				margin: '0px',
				padding: '0px',
				position: 'relative',
				width: '100%',
				color: attributes.textColor ? attributes.textColor : null,
				// background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : attributes.bgColorOutput,
				background: attributes.bgColorOutput, // <-- lazy-load version
			};
			let colCss = [
				...attributes.cssClasses
			];
			if(attributes.mediaURL ) colCss.push('cw-lazy-bg');
			if(attributes.bgCover ) colCss.push('bg-cover');


			let reverseOnMobile		= __get(attributes.reverseOnMobile, false);
			rowCss += reverseOnMobile ? ' flex-column-reverse' : ' flex-column';


			// RETURN: Block (FRONT-end)
			//================================
			return el( 'div', useBlockProps.save({
					id: typeof attributes.anchor === 'string' ? attributes.anchor : null,
					className: colCss.join(' '),
					style: blockStyle, // todo not working : atts do not appear on front-end
					['data-bg']:  attributes.mediaURL !== '' ? attributes.mediaURL : null
				}),


				el( 'div', {
						className: rowCss,
					},

					el( InnerBlocks.Content )
				)
			);
		},
	} );
}(
	window.wp.blocks,
	window.wp.editor,
	window.wp.i18n,
	window.wp.element,
	window.wp.components,
	window._,
	window.wp.blockEditor,
	window.cwb
) );
