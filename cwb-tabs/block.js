
( function( blocks, editor, i18n, element, components, _, blockEditor, cwb ) {
	const __ = i18n.__;
	const el = element.createElement;
	const InnerBlocks = blockEditor.InnerBlocks;
	const useBlockProps = blockEditor.useBlockProps;
	const InspectorControls = blockEditor.InspectorControls;
	const useSelect = wp.data.useSelect;

	const {
	} = components;

	const {
		cwbGlobals,
		cwbComponents
	} = cwb;

	const {
	} = cwbGlobals;

	const {
		cwbAttributes
	} = cwbComponents;

	// const useInnerBlocksProps = blockEditor.useInnerBlocksProps;


	blocks.registerBlockType( 'cwb/tabs', {


		// Block ASSETS
		//-----------------------------

		title: __( 'Tabs', 'CWB' ),
		icon: 'admin-page', // see: /wp-includes/css/dashicons.css
		category: 'common',
		attributes: {
			anchor: {
				type: 'string',
				default: false,
			},
			cssClasses: {
				type: 'array',
				default: ['d-flex', 'flex-wrap'],//['w-100', 'd-flex', 'm-0', 'align-items-center', 'bg-cover']
			},

			// tabList: {

			// Common Attribute Sets
			//-----------------------------
			...cwbAttributes.__blockColors,
			...cwbAttributes.__bgImage,
			// ...cwbAttributes.__blockBorder,
			...cwbAttributes.__bgOverlayColors,
			overlayBlur: {
				type: 'integer',
				default: 0,
			},


			// Repeater Block Settings
			//-----------------------------
			tabItemBlocks: {
				type : 'array',
				default: []
			},
		},
		example: {
			attributes: {
				// title: __( 'Chocolate Chip Cookies', 'gutenberg-examples' ),
				// mediaID: 1,
				// mediaURL:
				// 	'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/2ChocolateChipCookies.jpg/320px-2ChocolateChipCookies.jpg',
			},
		},


		// Block FUNCTIONS
		//-----------------------------

		edit: function( props ) {

			const tabItemBlocks = useSelect( ( select ) => select( 'core/block-editor' ).getBlock( props.clientId ).innerBlocks );
			const tabItemCount = tabItemBlocks.length;

			let attributes = props.attributes;
			let tabItemLimit = 5;
			let optionCount = 0;
			let allowMoreItems = tabItemCount < tabItemLimit;
			let blockStyle = {
				width: 'auto',
				margin: '0px auto',
				padding: '0px',
				background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` :
					attributes.bgColorOutput !== ''  ? attributes.bgColorOutput : null,
				color: attributes.textColor ? attributes.textColor : null,
			};


			// required for save function
			props.setAttributes({
				tabItemBlocks : tabItemBlocks
			});
			attributes = props.attributes;


			// Function to update child block attributes
			function updateChildAttributes(newAttributes, clientId) {
				wp.data.dispatch('core/block-editor').updateBlockAttributes(clientId, newAttributes)
			}

			for(let i = 0; i < tabItemBlocks.length; i++){
				if(i === 0 ){
					updateChildAttributes({
						isFirstTab: true
					}, tabItemBlocks[i].clientId);
				}
			}






			// Navigate/Set current tab via state
			//-----------------------------
			const [currentTab, setCurrentTab] = useState(1);






			// RETURN:
			//================================
			return el( 'div', useBlockProps( {
					className: [
						...attributes.cssClasses,
						'cwb-editor-outline',
					].join(' '),
					// style: blockStyle
				} ),




				// Tabs Menu
				//================================
				el( 'ul', { className: 'cw-tabs-menu nav nav-tabs'},
					tabItemBlocks.map((tab) => {

						optionCount++;
						let itemKey = optionCount;

						return el('li', {
							className: 'nav-item',
							['data-key']: itemKey,
							onClick: function(){
								setCurrentTab(itemKey)
							}
						}, el('a', {
							className: itemKey === parseInt(currentTab) ? 'nav-link active' : 'nav-link'
							}, tab.attributes.tabName)
						)
					})
				),



				// Tabs Container
				//================================
				el( 'div',  useBlockProps( {
					className: [
						// ...attributes.cssClasses,
						'cw-tabs-container w-100'
					].join(' '),
					['data-show']: currentTab,
					style: blockStyle
				}) ,

					el(CwbBgEffect, {
						bgOverlay : attributes.overlayColorOutput,
						backdropFilter: attributes.overlayBlur ?
							'blur('+attributes.overlayBlur+'px)' : null
					}),

					el(InnerBlocks, {
						allowedBlocks: [
							'cwb/tab',
						],
						template: [
							['cwb/tab', {
								template: [
									['outermost/icon-block',{
										"icon": "",
										"iconName": "wordpress-siteLogo",
										"itemsJustification": "center",
										// "iconColor": "secondary_l1",
										// "iconColorValue": "#28aaef",
										"width": "100px",
										"className": "my-0 py-0"
									}],
									// ['core/image', {
									// 	url: 'http://localhost/Clickwurx/Blank_333/wp-content/uploads/2023/12/icon-placeholder-80.png'
									// }],
									['core/heading', {
										content: 'Veni',
										level: 3
									}],
									['core/paragraph'],
								]
							}],
						],
					}),
				),



				// InspectorControls
				//================================
				el( InspectorControls, {
					key: 'controls' },

					el( 'div', {
							style: {
								margin: '0 0 20px',
								padding: '0 20px'
							},
						},
						el( TextControl,
							{
								label: 'Block Anchor',
								value: typeof attributes.anchor === 'string' ? attributes.anchor : null,
								onChange: function(value){
									props.setAttributes({
										anchor: limitForHtmlId(value)
									})
								},
							}
						),

						// el( SelectControl, {
						// 	key: 'multiple',
						// 	label: __('Select Repeater Item Type'),
						// 	value: attributes.repeaterItemType,
						// 	className: ['mt-3'],
						// 	onChange: function(value){
						// 		props.setAttributes({
						// 			repeaterItemType : value
						// 		});
						// 		attributes = props.attributes;
						// 	},
						// 	options: [
						// 		{ value: 'repeater-item--basic', label: 'Basic Repeater Item' },
						// 		// { value: 'repeater-item--slide', label: 'Slide (Basic)' },
						// 		{ value: 'repeater-item--itb', label: 'Image Text Button (ITB)' }
						// 	]
						// }),
					),


					// Panel : Block Colors
					//-----------------------------
					el( BlockColors, props),

					// Panel : BgEffects
					//-----------------------------
					el( BgEffects, props)
				)
			);
		},



		save: function( props ) {
			let attributes = props.attributes;
			let blockStyle = {
				width: 'auto',
				margin: '0px auto',
				padding: '0px',
				position: 'relative',
				background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` :
					attributes.bgColorOutput !== ''  ? attributes.bgColorOutput : null,
				color: attributes.textColor ? attributes.textColor : null,
			};



			let optionCount = 0;

			// RETURN: Layout-Block (FRONT-end)
			//================================
			return el( 'div', useBlockProps.save( {
					id: typeof attributes.anchor === 'string' ? attributes.anchor : null,
					className: attributes.cssClasses.join(' '),
					style: blockStyle
				} ),



				el( 'ul', { className: 'cw-tabs-menu nav nav-tabs'},
					attributes.tabItemBlocks.map((tab) => {
						optionCount++;
						let itemKey = optionCount;

						// Option UI element
						return el( 'li', { className : 'nav-item'},
							el( 'a', {
								className: itemKey === 1 ? 'active nav-link' : 'nav-link',
								href: "#tab-"+tab.attributes.anchor,
								['data-toggle']: 'tab',
								role: 'option'
								}, tab.attributes.tabName
							)
						)
					})
				),

				// todo:
				// 	~ responsivetabs (no-js?):
				// 		. https://gist.github.com/howbizarre/e9e5cc2aa301c1d3ed7f5bf5e82cb1bf
				// 		. https://openam.github.io/bootstrap-responsive-tabs/
				// 	≈ tab overflow: need JS to listen to overflow

				el( 'div', { className: 'cw-tabs-container w-100 tab-content position-relative'},
					el(CwbBgEffect, {
						bgOverlay : attributes.overlayColorOutput,
						backdropFilter: attributes.overlayBlur ?
							'blur('+attributes.overlayBlur+'px)' : null
					}),

					el( InnerBlocks.Content ),
				)
			);
		},
	} );
}(
	window.wp.blocks,
	window.wp.editor,
	window.wp.i18n,
	window.wp.element,
	window.wp.components,
	window._,
	window.wp.blockEditor,
	window.cwb
) );
