( function( blocks, editor, i18n, element, components, _, blockEditor, cwb ) {
	const __ = i18n.__;
	const el = element.createElement;
	const useBlockProps = blockEditor.useBlockProps;
	const InnerBlocks = blockEditor.InnerBlocks;
	const InspectorControls = blockEditor.InspectorControls;
	const MediaPlaceholder = blockEditor.MediaPlaceholder;
		// ^ https://github.com/WordPress/gutenberg/blob/trunk/packages/block-editor/src/components/media-placeholder/README.md

	const {
	} = components;

	const {
		cwbGlobals,
		cwbComponents
	} = cwb;

	const {
		screenSizeTabs,
		defaultScreenSizeObj
	} = cwbGlobals;

	const {
		cwbAttributes,
		CwbVideo
	} = cwbComponents;



	blocks.registerBlockType( 'cwb/video-background', {


		// Block ASSETS
		//-----------------------------

		title: __( 'CWB: Video Background', 'CWB' ),
		icon: 'format-video',
		category: 'common',
		attributes: {
			anchor: {
				type: 'string',
				default: false,
			},
			videoId: {
				type: 'id',
				default: ''
			},
			cssClasses: {
				type: 'array',
				default: ['p-0', 'my-0', '_trace-green'],//['w-100', 'd-flex', 'm-0', 'align-items-center', 'bg-cover']
			},
			vidSrc: {
				type: 'object',
				default: ''
			},


			innerTemplate: {
				type: 'array',
				default: [
					[
						'cwb/layout-block', // Outer block name
						{}, // Outer block attributes
						[ // Inner blocks for 'cwb/layout-block'
							[
								'cwb/content-wrap', // Inner block name
								{ // Inner block attributes
									blockHeight: {
										sm: '',
										md: '',
										lg: '',
										xl: '',
										base: 'vh-75-min'
									}
								}
							]
						]
					]
				]
			},


			// Common Attribute Sets
			//-----------------------------
			...cwbAttributes.__blockColors,
			...cwbAttributes.__bgImage,
			// ...cwbAttributes.__blockBorder,
		},
		example: {
			attributes: {
				title: __( 'Chocolate Chip Cookies', 'gutenberg-examples' ),
				mediaID: 1,
				mediaURL:
					'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/2ChocolateChipCookies.jpg/320px-2ChocolateChipCookies.jpg',
			},
		},


		// Block FUNCTIONS
		//-----------------------------

		edit: function( props ) {
			let attributes = props.attributes;
			let blockStyle = {
				// background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : attributes.bgColorOutput,
				width: '100%',
				margin: '0px auto',
				padding: '0px',
				color: attributes.textColor ? attributes.textColor : null,
			};
			attributes.vidSrc = attributes.vidSrc === '' ? false : attributes.vidSrc;

			// Check if the block already has saved content
			const hasInnerBlocks = useSelect(
				(select) => !!select('core/block-editor').getBlocks(props.clientId).length,
				[props.clientId]
			);


			// RETURN: SandBawx Block Editor
			//================================
			return el( 'div', useBlockProps( {
					className: attributes.cssClasses,
					style: blockStyle } ),

					el('div', { className: 'position-relative overflow-hidden'},


					el( MediaUpload, {
						onSelect: function(media){
							// console.log('media', media);
							// todo DRY
							props.setAttributes({
								vidSrc : media
							});
							attributes = props.attributes
						},
						allowedTypes: 'video',
						value: attributes.vidSrc,

						// todo DRY: MediaEditHover
						render: function( obj ) {
							// return el('div', { className: '_position-relative _vh-90'},

							return	attributes.vidSrc ? el('div', {},
									el(CwbVideo, {
										className: ['video-background'],
										vidSrc : attributes.vidSrc.url,
									}),
									el('div', {className:'display-on-parent-hover text-white',
											style: {
												position: 'absolute',
												width: '100%',
												height: '100%',
												background: "rgba(0,0,0,0.5)",
												top: 0,
												alignItems: 'center',
												justifyContent: 'center',
												flexDirection: 'column'
											},
										},
										el('p', { className: 'mb-0'}, 'file: ',
											el('strong', {}, attributes.vidSrc.filename)
										),
										el('p', { className: ''}, "Video player is disabled in editor mode"),

										el('div', { className: 'text-white',
												style: {
													border: "1px solid rgba(255,255,255,0.5)",
													background: "rgba(0,0,0,0.5)",
												}},

											el(components.Button,{
													onClick: function(){
														// todo DRY
														props.setAttributes({
															vidSrc : false,
														});
														attributes = props.attributes
													},
												},
												el( Icon, { icon: 'trash', style: {
														color: 'white'
													} }),
											)
										)
									),
								) :null//,

								// : el(MediaPlaceholder,
								// 	{
								// 		labels: {
								// 			title: 'Add / Upload a Video'
								// 		},
								// 		icon: el(Icon, {icon: 'format-video', style: {margin: '0 20px 0 0'}}),
								// 		// dropZoneUIOnly: true,
								// 		allowedTypes: 'video',
								// 		onSelect: function(media){
								// 			// console.log('MediaPlaceholder', media);
								// 			props.setAttributes({
								// 				vidSrc : media
								// 			});
								// 			attributes = props.attributes
								// 		},
								// 	}
								// ),
							// )
						}
					}),



					el(InnerBlocks, {
						allowedBlocks : ['cwb/layout-block','cwb/content-wrap'],
						template: attributes.innerTemplate,
						// templateLock: 'insert' // Change to 'all' or 'insert' if you want to lock the structure
					}),
				),


				// InspectorControls
				//================================
				el( InspectorControls, { key: 'controls' },
					el('div', {className: '', style: {padding: '0 20px'}},
						el( TextControl,
							{
								label: 'Block Anchor',
								value: typeof attributes.anchor === 'string' ? attributes.anchor : null,
								onChange: function(value){
									props.setAttributes({
										anchor: limitForHtmlId(value)
									})
								},
							}
						),


						el( MediaUpload, {
							onSelect: function(media){
								console.log(media); // todo
								props.setAttributes({
									vidSrc : media
								});
								attributes = props.attributes
							},
							allowedTypes: 'video',
							value: attributes.vidSrc,
							render: function( obj ) {
								return el('div', { className: '_text-white',
										style: {
											// border: "1px solid rgba(255,255,255,0.5)",
											// background: "rgba(0,0,0,0.5)",
											marginBottom: '20px'
										}},

									el('div', {},

										el(TextControl, {
											label : 'Video File URL',
											help : '',
											value : attributes.vidSrc.url,
											onChange :function(value){
												let filename = attributes.vidSrc.filename;

												if(value !== '') {
													props.setAttributes({
														vidSrc: {
															url: value,
															filename: value !== filename ?
																value !== '' ? 'external link' : 'none'
																: filename
														}
													});
													attributes = props.attributes
												}
												else {
													props.setAttributes({
														vidSrc: false
													});
													attributes = props.attributes
												}
											}
										}),

										attributes.vidSrc ?
										el('p', { className: 'mb-0'}, 'file: ',
											el('strong', {}, attributes.vidSrc.filename)
										) : null

									),

									attributes.vidSrc ?
										el(components.Button,{
												className: 'components-button is-tertiary',
												onClick: function(){
													props.setAttributes({
														vidSrc : false,
													});
													attributes = props.attributes
												},
											},
											el( Icon, { icon: 'trash', style: {
													// color: 'white'
												} }),
										)
										: el(components.Button,{
											onClick: obj.open,
											className: 'components-button is-tertiary'
										},
										'Video Library ',
										el( Icon, { icon: 'format-video', style: {
												marginLeft: '10px'
												// color: 'white'
											} }),
										),
								)
							}
						}),
					)
				)
			);
		},

		save: function( props ) {

			let attributes = props.attributes;
			let blockStyle = {
				background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : attributes.bgColorOutput,
				margin: '0px',
				padding: '0px',
				width: '100%',
				color: attributes.textColor ? attributes.textColor : null,
			};


			// RETURN: SandBawx-Block (FRONT-end)
			//================================
			return el('div', { className: 'position-relative _vh-90'},
				// el('div', { className: 'video-background'},
					el(CwbVideo, {
						className: ['video-background'],
						id: typeof attributes.anchor === 'string' ? attributes.anchor : null,
						vidSrc : attributes.vidSrc.url,
					}),
				// ),
				el( InnerBlocks.Content )
			);
		},
	} );
}(
	window.wp.blocks,
	window.wp.editor,
	window.wp.i18n,
	window.wp.element,
	window.wp.components,
	window._,
	window.wp.blockEditor,
	window.cwb
) );
