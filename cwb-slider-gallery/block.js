( function( blocks, editor, i18n, element, components, _, blockEditor, cwb ) {
	const __ = i18n.__;
	const el = element.createElement;
	const InnerBlocks = blockEditor.InnerBlocks;
	const useBlockProps = blockEditor.useBlockProps;
	const InspectorControls = blockEditor.InspectorControls;
	const MediaPlaceholder = blockEditor.MediaPlaceholder;
	const { Fragment } = element;

	// const {
	// 	ToggleControl,
	// 	Panel,
	// 	PanelBody,
	// 	PanelRow
	// } = components;

	const {
		cwbGlobals,
		cwbComponents
	} = cwb;

	const {
		cwbAttributes
	} = cwbComponents;


	/**
	 * Distribute (Images) to Columns
	 * --------------------------------
	 * @param items
	 * @returns {{rightCol: Array, leftCol: Array}}
	 *
	 * - Splits images into two even columns - sequentially, based on
	 *   image height - first filling up the left column, then the right
	 */
	const distributeToCols = function(items){

		let leftCol = [];
		let rightCol = [];
		let heightTotal = 0;
		let leftColHTotal = 0;
		for( let i =0 ; i < items.length; i++){
			heightTotal += items[i].sizes.medium.height;
		}


		for( let x=0; x < items.length; x++){

			let thisHeight = items[x].sizes.medium.height;
			let nextHeight = x+1 < items.length ? items[x+1].sizes.medium.height : 0;
			let rightColHTotal = heightTotal - leftColHTotal;
			let newLeftColHeight = leftColHTotal + thisHeight;

			// PLACE images into LEFT col
			//----------------------------------
			if(
				// - left column image.height sum should always be more than right
				(leftColHTotal < heightTotal/2
					// && keep left & right col height as even as possible;
					&& thisHeight < (rightColHTotal - newLeftColHeight))

				// - avoid gallery array order vs visual order mismatch
			 	|| (leftColHTotal < heightTotal/2
					// - account for case when the NEXT image.height is smaller
					&& thisHeight > nextHeight)
			){
				leftCol.push(items[x]);
				leftColHTotal = newLeftColHeight;
			}

			// PLACE images into RIGHT col
			//----------------------------------
			else {
				rightCol.push(items[x]);
			}
		}


		return {
			leftCol : leftCol,
			rightCol : rightCol
		}
	};

	/**
	 * Column Image
	 * --------------------------------
	 * @param image
	 * @param colSide
	 * @param colImageCount
	 * @param imageEl
	 * @returns {*}
	 *
	 * - set an image wrapper with padding based on image order and column side (left v. right)
	 * - using .d-inline-block and <br> because images are being alight via text-align css prop
	 */
	const columnImage = function(image, colSide, colImageCount, imageEl){


		// Build image padding prefix
		// - the goal of the padding is to create visual variety
		//   in images sizes regardless if two items that are
		//   next to each other are the same size.
		let offSet 	= colSide === 'right';
		let mod3 	= (colImageCount + offSet) % 3;
		let padPref = colSide === 'right' ? 'pr-' :'pl-';
		let colPad 	= mod3 > 0 ? padPref+ (mod3 + 3) : null;


		return el(Fragment, {},
			el('div', { className: 'd-inline-block w-100 '+colPad,
				style : {
					maxWidth: '100%',
				}},
				imageEl
			),
			el('br')
		);
	};


	blocks.registerBlockType( 'cwb/slider-gallery', {


		// Block ASSETS
		//-----------------------------

		title: __( 'CWB: Slider Gallery', 'CWB' ),
		icon: 'slides', // see: /wp-includes/css/dashicons.css
		category: 'common',
		attributes: {
			sliderId: {
				type: 'number',
				default: false
			},
			cssClasses: {
				type: 'array',
				default: ['p-0', 'my-0']
			},

			items: {
				type: 'array',
				default: ''
			},
		},
		example: {
			attributes: {
				title: __( 'Chocolate Chip Cookies', 'gutenberg-examples' ),
				// mediaID: 1,
				// mediaURL:
				// 	'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/2ChocolateChipCookies.jpg/320px-2ChocolateChipCookies.jpg',
			},
		},


		// Block FUNCTIONS
		//-----------------------------

		edit: function( props ) {
			let attributes = props.attributes;
			let blockStyle = {
				width: '100%',
				margin: '0px auto',
				padding: '0px',
				color: attributes.textColor ? attributes.textColor : null,
			};

			// Set Slider ID upon Block instantiation
			if (!attributes.sliderId){
				props.setAttributes({
					sliderId: Math.floor(Math.random() * 999999)
				});
				attributes = props.attributes
			}

			// Build Image ID array for MediaUpload value
			let imageIds = [];
			for(let i=0; i<attributes.items.length;i++){
				imageIds.push(attributes.items[i].id);
			};


			/**
			 * Gallery Image Editor
			 * @param image
			 * @param props
			 * @param mediaUploadObj
			 * @returns {*}
			 *
			 * requires: CwGalleryImageEditor
			 */
			const galleryImageComponent = function(image, props, mediaUploadObj){
				// console.log('block/galleryImageComponent', image.title);
				return el( 'div',{ className : 'text-center'},
					el(CwGalleryImageEditor, {
						...props,
						image: image,
						imgSize : 'large',
						mediaUploadObj: mediaUploadObj
					})
				);
			};

			// Navigate/Set current slide via state
			const [currentItem, setCurrentItem] = useState(0);
			const prevSlide = function(){
				let prev = currentItem > 0 ? currentItem - 1 : 0;
				setCurrentItem(prev)
			};
			const nextSlide = function(){
				let next = currentItem+1 < attributes.items.length ? currentItem + 1 : 0;
				setCurrentItem(next)
			};


			// RETURN: Gallery Block Editor
			//================================
			return el( 'div', useBlockProps( {
					className: attributes.cssClasses,
					style: blockStyle } ),


					// Gallery Editor Block
					//================================
					el( MediaUpload, {
						onSelect: function (media) {
							props.setAttributes({
								items : media
							});
							attributes = props.attributes;
						},
						allowedTypes: 'image',
						value: imageIds, // array of ids for MediaUpload (not array of img objects like MediaPlaceholder)
						gallery: true,
						multiple: true,
						render: function (obj) {

							return attributes.items.length ?

							el(SliderEditBlock, {
								items: attributes.items,
								currentKey : currentItem,
								onPrev : prevSlide,
								onNext : nextSlide,
								onOptionClick : function(itemKey){
									setCurrentItem(itemKey)
								},
							}, (slide) => {
								return galleryImageComponent(slide, props, obj);
							}):

							el(MediaPlaceholder, {
								labels: { title: 'Gallery Images' },
								icon: el(Icon, {icon: 'format-image', style: {margin: '0 20px 0 0'}}),
								value: attributes.items,
								multiple: true,
								onSelect: function (items) {
									props.setAttributes({ items : items });
									attributes = props.attributes;
								},
							})
						}
					}),


					// InspectorControls
					//================================
					el( InspectorControls, { key: 'controls' },
				)
			);
		},

		save: function( props ) {

			let attributes = props.attributes;
			let blockStyle = {
				margin: '0px',
				padding: '0px',
				width: '100%',
				color: attributes.textColor ? attributes.textColor : null,
			};



			// RETURN: Slider-Gallery-Block (FRONT-end)
			//================================
			return el( 'div', useBlockProps.save( {
					className: attributes.cssClasses.join(' '),
					style: blockStyle
				} ),


				el(CwImageSlider, {
					images: attributes.items,
					sliderId: typeof attributes.sliderId !== 'undefined' ?
						attributes.sliderId : null,
					lazyLoad: true
				})
			);
		},
	} );
}(
	window.wp.blocks,
	window.wp.editor,
	window.wp.i18n,
	window.wp.element,
	window.wp.components,
	window._,
	window.wp.blockEditor,
	window.cwb
) );
