( function( blocks, editor, i18n, element, components, _, blockEditor, cwb ) {
	const __ = i18n.__;
	const el = element.createElement;
	const InnerBlocks = blockEditor.InnerBlocks;
	const useBlockProps = blockEditor.useBlockProps;
	const InspectorControls = blockEditor.InspectorControls;

	const {
	} = components;

	const {
		cwbGlobals,
		cwbComponents
	} = cwb;

	const {
	} = cwbGlobals;

	const {
		cwbAttributes
	} = cwbComponents;


	// @ref links below
	// https://mediaron.com/how-to-create-a-repeater-field6-in-gutenberg/
	// https://reactnative.dev/docs/state

	blocks.registerBlockType( 'cwb/repeater-item--modal-iframe', {


		// Block ASSETS
		//-----------------------------

		title: __( 'Repeater Item (Modal iFrame)', 'CWB' ),
		icon: 'admin-page', // see: /wp-includes/css/dashicons.css
		category: 'common',
		attributes: {
			cssClasses: {
				type: 'array',
				default: ['tile'],//['w-100', 'd-flex', 'm-0', 'align-items-center', 'bg-cover']
			},

			// videoId: {
			// 	type: 'id',
			// 	default: ''
			// },
			iFrameSrc: {
				type: 'string',
				default: false
			},

			// triggerImage: {
			// 	type: 'string',
			// 	default: ''
			// },
			// triggerImageID: {
			// 	type: 'number',
			// },

			triggerImgObj: {
				type: 'object',
				default: false
			},



			gridCssClasses: {
				type : 'string',
				default: ''
			},

			gapPaddingCss: {
				type : 'string',
				default: ''
			},

			// Common Attribute Sets
			//-----------------------------
			// ...cwbAttributes.__blockColors,
			// ...cwbAttributes.__bgImage,
			// ...cwbAttributes.__blockBorder,
			...cwbAttributes.__bgOverlayColors,
			// overlayBlur: {
			// 	type: 'integer',
			// 	default: 0,
			// },



			modalHeading: {
				type : 'string',
				default: 'iFrame modal'
			},

		},
		example: {
			attributes: {
				// title: __( 'Chocolate Chip Cookies', 'gutenberg-examples' ),
				// mediaID: 1,
				// mediaURL:
				// 	'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/2ChocolateChipCookies.jpg/320px-2ChocolateChipCookies.jpg',
			},
		},



		// Block FUNCTIONS
		//-----------------------------

		edit: function( props ) {
			let attributes = props.attributes;
			// let vAlignCss = [...attributes.vAlignCss];
			// let hAlignCss = [...attributes.hAlignCss];
			let blockStyle = {
				width: '100%',
				margin: '0px auto',
				padding: '0px',
				border: '1px dotted rgba(0,0,0,0.2)',
				// color: attributes.textColor ? attributes.textColor : null,
				// background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : attributes.bgColorOutput,
				position: 'relative'
			};

			attributes.vidSrc = attributes.vidSrc === '' ? false : attributes.vidSrc;




			// Get & Set Dynamic Parent Block Attributes
			//-----------------------------
			let parentAtts = {...cwbAttributes.globalBlockAtts[props.clientId]};
			props.setAttributes({
				gridCssClasses : parentAtts.gridCssClasses,
				gapPaddingCss : parentAtts.gapPaddingCss
			});
			attributes = props.attributes;




			// Set Grid & TileGap Classes
			//-----------------------------
			let gridCssClasses = __get(parentAtts.gridCssClasses, attributes.gridCssClasses);
			let gapPaddingCss = __get(parentAtts.gapPaddingCss, attributes.gapPaddingCss);
			let itemCss = [
				gridCssClasses,
				gapPaddingCss,
				'cwb-editor-outline',
			].join(' ');



			const setContainerClasses = function( value ) {
				props.setAttributes( { containerClasses: value } );
				attributes = props.attributes;
			};


			// // for ResponsiveTabs
			// //-----------------------------
			// const [layoutTab, setLayoutTab] = useState(false);





			// RETURN: Repeater Item
			//================================
			return el( 'div', {
					className: itemCss,
					style: blockStyle
				} , el( 'div', useBlockProps( {
					className: attributes.cssClasses,
					// style: blockStyle
				} ),

				// attributes.bgEffect ?
				el(CwbBgEffect, {
					bgOverlay : attributes.overlayColorOutput,
					backdropFilter: attributes.overlayBlur ?
						'blur('+attributes.overlayBlur+'px)' : null
				}),
				// : null,


				el('div', {
						className: [
							attributes.containerClasses,
							// ...attributes.vAlignCss,
							// 'd-flex',
							// 'slide-flex'
						].join(' ')
					},
					el('div', {
							className: [
								// ...attributes.hAlignCss,
								'tile-wrap w-100'
							].join(' ')
						},

						el('div', { className: 'position-relative'},

							//SliderModalTriggerEditor onEditClick()
							attributes.triggerImgObj ?
								el('div', {
										// className: 'd-inline-block'
									},
									el(BlockMediaEditor, {
										label: 'Select a Video Image',
										imageSize: 'mobile-crop',
										image: attributes.triggerImgObj,
										onSelect: function (value) {

											props.setAttributes({
												triggerImgObj: value
											})
										},
										onRemove: function () {
											props.setAttributes({
												triggerImgObj: false
											})
										}
									})
								) : el('div', {className: 'trace-green p-4'})
						)
					),

					el('p', {className: 'mt-3'}, attributes.modalHeading)
				),


				// InspectorControls
				//================================
				el( InspectorControls, {
						key: 'controls' },


					el('div', {
							className: 'px-3'
						},
						el(TextControl, {
							label : 'iFrame URL',
							help : '',
							value : attributes.iFrameSrc,
							onChange :function(value){
								// let filename = attributes.vidSrc.filename;

								if(value !== '') {
									props.setAttributes({
										iFrameSrc: value
									});
									attributes = props.attributes
								}
								else {
									props.setAttributes({
										iFrameSrc: false
									});
									attributes = props.attributes
								}
							}
						}),


						el(TextControl, {
							label : 'Video Title (Modal Heading)',
							help : '',
							value : attributes.modalHeading,
							onChange :function(value){
								// let filename = attributes.vidSrc.filename;

								if(value !== '') {
									props.setAttributes({
										modalHeading: value
									});
									attributes = props.attributes
								}
								else {
									props.setAttributes({
										modalHeading: false
									});
									attributes = props.attributes
								}
							}
						}),
						// ),


						el(InspectorMediaInput,{
							label: 'Choose Video Image',
							currentValue: attributes.triggerImgObj,
							onSelect: function(value){
								props.setAttributes({
									triggerImgObj : value
								})
							},
							onRemove: function(){
								props.setAttributes({
									triggerImgObj : false
								})
							},
						})


						// // Panel : Block Colors
						// //-----------------------------
						// el( BlockColors, props),
						//
						// // Panel : Background Image
						// //-----------------------------
						// el( BgImagePanel, {
						// 	props : props,
						// 	blockStyle : blockStyle
						// }),
						//
						// // Panel : BgEffects
						// //-----------------------------
						// el( BgEffects, props),
					)
				)
				)
			);
		},

		save: function( props ) {
			let attributes = props.attributes;
			// let includeAltText = attributes.includeAriaLabel && attributes.mediaAlt.length;
			let blockStyle = {
				// background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : attributes.bgColorOutput,
				margin: '0px',
				padding: '0px',
				width: '100%',
				// color: attributes.textColor ? attributes.textColor : null,
				// background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : attributes.bgColorOutput,
				position:'relative'
				// color: attributes.textColor ? attributes.textColor : null,
			};



			// Set Grid & TileGap CSS
			//-----------------------------
			let itemCss = [
				attributes.gridCssClasses,
				attributes.gapPaddingCss,
			];


			// Repeater Item CSS
			//-----------------------------
			let tileCss = [
				...attributes.cssClasses,
				// 'container',
				'position-relative'
			];


			// RETURN: Layout-Block (FRONT-end)
			//================================
			return el( 'div', {
					className: itemCss.join(' '),
					style: blockStyle
				} ,
				el( 'div', {},


					// attributes.bgEffect ?
					el(CwbBgEffect, {
						bgOverlay : attributes.overlayColorOutput,
						// backdropFilter: attributes.overlayBlur ?
						// 	'blur('+attributes.overlayBlur+'px)' : null
					}),
					// : null,

					el('div', useBlockProps.save( {
							className: [
								...tileCss,
								'd-flex',
							].join(' ')
						}),

						el(CwIFrameTrigger, {
							image : attributes.triggerImgObj,
							iFrameSrc : attributes.iFrameSrc,
							modalId : 'iframe-modal',
							modalHeading: attributes.modalHeading,
							imageIndex : 0,
							singleImage : false,
							captionOnHover : false,
							lazyLoad : true,
							extraWrapClass: 'anim-on-scroll anim-b-in ',
							imgSize : 'mobile-crop'
							// id: typeof attributes.anchor === 'string' ? attributes.anchor : null,
							// vidSrc : attributes.vidSrc.url,
							// loop: false
						})
						// )
					),

					el('p', {className: 'mt-3 anim-on-scroll anim-alpha-in'}, attributes.modalHeading)
				)
			);
		},
	} );
}(
	window.wp.blocks,
	window.wp.editor,
	window.wp.i18n,
	window.wp.element,
	window.wp.components,
	window._,
	window.wp.blockEditor,
	window.cwb
) );
