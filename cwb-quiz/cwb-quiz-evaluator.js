

jQuery(function($) {
    $(document).ready(function ($) {
        let cwbQuizes = $('.cwb-quiz');

        // LOOP Quizzes
        /*-------------------------------------*/
        for(let i = 0; i < cwbQuizes.length; i++) {

            let quiz = $(cwbQuizes[i]),
                submit = $(quiz.find('.quiz-submit')),
                reset = $(quiz.find('.quiz-reset'));


            // Reset Quiz Answers
            /*-------------------------------------*/
            function resetQuiz(target) {

                let targetQuiz = $(target),
                    questions = $(targetQuiz.find('.quiz-question'));


                // LOOP Questions
                for (let q = 0; q < questions.length; q++) {

                    let question = $(questions[q]);
                    question
                        .removeClass('correct')
                        .removeClass('wrong');
                }
            }

            reset.click(function () {
                let target = $(this).attr('data-target');
                resetQuiz(target);

                // Reset Select inputs
                $($(target).find('option[value="none-selected"]')).prop('selected', true);
            });


            // Evaluate Quiz Answers
            /*-------------------------------------*/

            submit.click(function () {

                let target = $(this).attr('data-target'),
                    targetQuiz = $(target),
                    questions = $(targetQuiz.find('.quiz-question'));

                resetQuiz(target);


                // LOOP Questions
                /*-------------------------------------*/
                for (let q = 0; q < questions.length; q++) {
                    let question = $(questions[q]),
                        answerIsCorrect = false,
                        options = $(question.find('.option'));


                    // Validate RADIO Input
                    /*-------------------------------------*/

                    // - Find selected options
                    for (let o = 0; o < options.length; o++) {
                        let option = $(options[o]),
                            input = $(option.find('input')),
                            label = $(option.find('label')),
                            isChecked = input.prop('checked'),
                            isCorrect = input.attr('value') === '1';

                        // - if only ONE correct answer
                        if (isChecked && isCorrect) {
                            answerIsCorrect = true;
                            // break;
                        }
                        ;

                        // - if there are MANY answers...to_do
                    }


                    // Validate SELECT Input
                    /*-------------------------------------*/
                    let select = $(question.find('select')),
                        selectOptions = $(select.find('option'));

                    // - Find selected options
                    for (let y = 0; y < selectOptions.length; y++) {
                        let selectOption = $(selectOptions[y]),
                            isCorrectOption = selectOption.attr('value') === '1';
                        if (selectOption.prop('selected')) {

                            // Answer is correct IF the selected option is flagged as correct [value=1]
                            if (isCorrectOption) {
                                answerIsCorrect = true
                            }
                        }
                    }


                    // Mark Quiz-Question
                    /*-------------------------------------*/
                    if (answerIsCorrect) {
                        question.addClass('correct');
                    } else {
                        question.addClass('wrong');
                    }
                }
            });
        }
    });
});
