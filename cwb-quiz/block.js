( function( blocks, editor, i18n, element, components, _, blockEditor, cwb ) {
	const __ = i18n.__;
	const el = element.createElement;
	const InnerBlocks = blockEditor.InnerBlocks;
	const useBlockProps = blockEditor.useBlockProps;
	const InspectorControls = blockEditor.InspectorControls;
	const MediaPlaceholder = blockEditor.MediaPlaceholder;
	const { Fragment } = element;

	const {
	// 	ToggleControl,
	} = components;

	const {
		cwbGlobals,
		cwbComponents
	} = cwb;

	const {
		screenSizeTabs,
		defaultScreenSizeObj
	} = cwbGlobals;

	const {
		cwbAttributes
	} = cwbComponents;



	blocks.registerBlockType( 'cwb/quiz', {


		// Block ASSETS
		//-----------------------------

		title: __( 'CWB: Quiz', 'CWB' ),
		icon: 'media-code', // see: /wp-includes/css/dashicons.css
		category: 'common',
		attributes: {
			quizId: {
				type: 'string',
				default: false
			},
			cssClasses: {
				type: 'array',
				default: ['p-0', 'my-0', 'cwb-quiz'],//['w-100', 'd-flex', 'm-0', 'align-items-center', 'bg-cover']
			},


			contentTemplate : {
				type: 'array',
				default: [
					[ 'core/heading', { level: 3, className: 'h2', content: 'POP QUIZ! - h3.h2' } ],
					[ 'core/paragraph', { content: 'Lorem ipsum dolor sit amet labore cras venenatis.' } ],
					[ 'core/columns', {}, [
						[ 'core/column', {}, [
								['core/paragraph', { 'placeholder': 'Column 1 content' }]
							]
						],
						[ 'core/column', {}, [
								['core/paragraph', { 'placeholder': 'Column 2 content' }]
							]
						]
					]]
				]
			},
		},
		// example: {
		// 	attributes: {
		// 		title: __( 'Chocolate Chip Cookies', 'gutenberg-examples' ),
		// 		mediaID: 1,
		// 		mediaURL:
		// 			'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/2ChocolateChipCookies.jpg/320px-2ChocolateChipCookies.jpg',
		// 	},
		// },


		// Block FUNCTIONS
		/*--------------------------------------*/

		edit: function( props ) {
			let attributes = props.attributes;
			let blockStyle = {
				width: '100%',
				margin: '0px auto',
				padding: '0px',
			};


			// GENERATE Quiz ID on creation
			/*--------------------------------------*/
			if (!attributes.quizId){
				props.setAttributes({
					quizId: 'qz'+Math.floor(Math.random() * 999999)
				});
				attributes = props.attributes
			}



			// RETURN: CWB-Quiz Block Editor
			/*--------------------------------------*/
			return el( 'div', useBlockProps( {
					id: 'cwb-quiz-'+attributes.quizId,
					className: attributes.cssClasses,
					style: blockStyle }
				),

				// Inner Blocks
				/*--------------------------------------*/
				el( InnerBlocks, {
					template: attributes.contentTemplate,
					// // todo not working?
					// allowedBlocks : [
					// 	'core/paragraph',
					// 	'core/heading',
					// 	'core/columns',
					// 	'core/list',
					// 	'cwb/layout-block',
					// 	'cwb/content-wrap',
					// 	'cwb/quiz-question',
					// ]
				}),



				// Quiz Buttons
				/*--------------------------------------*/
				el('div', { className: 'text-center' },

					// SUBMIT Button
					//-----------------
					el('button', { className: 'quiz-submit btn btn-primary' },
						'Submit'),

					// RESET Button
					//-----------------
					el('button', { className: 'quiz-reset btn btn-alt' },
						'Reset'),
				),



				// InspectorControls
				/*--------------------------------------*/
				el( InspectorControls, { key: 'controls' },
					el('div', {className: '', style: {padding: '0 20px'}},
					)
				)
			);
		},

		save: function( props ) {

			let attributes = props.attributes;
			let blockStyle = {
				margin: '0px',
				padding: '0px',
				width: '100%',
			};


			// RETURN: CWB-Quiz Block (FRONT-end)
			/*--------------------------------------*/
			return el( 'div', useBlockProps.save( {
					id: 'cwb-quiz-'+attributes.quizId,
					className: attributes.cssClasses.join(' '),
					style: blockStyle
				} ),

				// Inner Blocks
				/*--------------------------------------*/
				el( InnerBlocks.Content ),


				// Quiz Buttons
				/*--------------------------------------*/
				el('div', { className: 'text-center' },

					// SUBMIT Button
					//-----------------
					el('button', { id: 'quiz-submit-'+attributes.quizId,
							className: 'quiz-submit btn btn-primary',
							['data-target']: '#cwb-quiz-'+attributes.quizId
						},
						'Submit'),

					// RESET Button
					//-----------------
					el('button', { id: 'quiz-reset-'+attributes.quizId,
							className: 'quiz-reset btn btn-alt',
							['data-target']: '#cwb-quiz-'+attributes.quizId
						},
						'Reset'),
				)
			);
		},
	} );
}(
	window.wp.blocks,
	window.wp.editor,
	window.wp.i18n,
	window.wp.element,
	window.wp.components,
	window._,
	window.wp.blockEditor,
	window.cwb
) );
