<?php

/**
 * Plugin Name: Gutenberg Examples Recipe Card
 * Plugin URI: https://github.com/WordPress/gutenberg-examples
 * Description: This is a plugin demonstrating how to register new blocks for the Gutenberg editor.
 * Version: 1.1.0
 * Author: the Gutenberg Team
 *
 * @package gutenberg-examples
 */

defined( 'ABSPATH' ) || exit;

/**
 * Registers all block assets so that they can be enqueued through Gutenberg in
 * the corresponding context.
 *
 * Passes translations to JavaScript. <-- todo
 */
function register_block__cwb_quiz() {

	if ( ! function_exists( 'register_block_type' ) ) {
		// Gutenberg is not active.
		return;
	}
	// Register the block by passing the location of block.json to register_block_type.
	register_block_type( __DIR__ );

}
add_action( 'init', 'register_block__cwb_quiz' );





if ( ! function_exists( 'enqueue_cwb_quiz_evaluator' ) ) {

    /**
     * Enqueue THEME FRONT-END CONFIGS
     *
     * - enqueue_parent_theme_front_end_scripts() MUST be active in parent theme
     * - cannot check if parent theme function exists because child theme functions get called first
     */
    function enqueue_cwb_quiz_evaluator(){
        global $cw;
        global $child_dir_uri;
        $version = null;//$cw->theme->script_version;

        $dependencies = array('jquery');
        $file_inFooter = true;

        wp_register_script( 'cwb-quiz-evaluator', '/wp-content/plugins/cw-blocks/cwb-quiz/cwb-quiz-evaluator.js', $dependencies, $version, $file_inFooter );
        wp_enqueue_script('cwb-quiz-evaluator');
    }
    add_action( 'wp_enqueue_scripts', 'enqueue_cwb_quiz_evaluator', 99 );
}

