/*
 * name: CWB Components
 * handle: 'cwb-components'
 *
 * Must Run After 'cwb-globals' && Before 'cwb'
 */


// IMPORT Dependencies
//=================================

Component = window.React.Component;

const __ 			= window.wp.i18n.__;
const cwbGlobals 	= window.cwbGlobals;
const element 		= window.wp.element;
const components 	= window.wp.components;
const blockEditor 	= window.wp.blockEditor;


const useSelect = wp.data.useSelect;
const MediaUpload 		= blockEditor.MediaUpload;
const ContrastChecker 	= blockEditor.ContrastChecker;
const RichText 			= blockEditor.RichText;
const MediaPlaceholder 	= blockEditor.MediaPlaceholder;
const LinkControl 		= blockEditor.__experimentalLinkControl;

const el 			= element.createElement;
const { Fragment } 	= element;
const useState 		= element.useState;
const useEffect 	= element.useEffect;
const RawHTML 		= element.RawHTML;
// const PanelColorSettings = blockEditor.PanelColorSettings;
// const PanelBody = components.PanelBody;
const {
	Dashicon,
	Toolbar,
	ToolbarButton,
	TextControl,
	// TextareaControl,
	// NumberControl,
	ColorPicker,
	ColorPalette,
	ColorIndicator,
	GradientPicker,
	RangeControl,
	Icon,
	SelectControl,
	TabPanel,
	ToggleControl,
	Panel,
	PanelBody,
	PanelRow,
	Popover } = components;

const CheckboxControl 			= components.CheckboxControl;
const NumberControl 			= components.__experimentalNumberControl;
const ToggleGroupControl 		= components.__experimentalToggleGroupControl;
const ToggleGroupControlOption 	= components.__experimentalToggleGroupControlOption;
const AlignmentMatrixToolbar 	= components.__experimentalAlignmentMatrixControl;
// const RichText = blockEditor.RichText;

const {
	screenSizeTabs,
	defaultScreenSizeObj
} = cwbGlobals;


// Debugging
//=================================


/**
 * Get Parent Function
 * @returns {*}
 *
 * - required by debug()
 */
const getParentFunction = function() {
	try {
		throw new Error();
	} catch (e) {
		// matches this function, the caller and the parent
		const allMatches = e.stack.match(/(\w+)@|at (\w+) \(/g);
		// match parent function name
		const parentMatches = allMatches[2].match(/(\w+)@|at (\w+) \(/);
		// return only name
		return parentMatches[1] || parentMatches[2];
	}
};

/**
 * Debug
 *
 * - outputs in conosle:  name of caller function & arguments
 */
const debug = function(){

	// let err = getErrorObject();
	// let caller_line = err.stack.split("\n")[4];
	// let index = caller_line.indexOf("at ");
	let caller = getParentFunction();

	console.log(caller, ...arguments);
};


/* Utilities */
/* ================================= */


/**
 * __Exists
 * @param variable
 * @returns {boolean}
 * @private
 *
 * - simplifying the following pattern:
 *   	typeof variable !== 'undefined'
 */
const __exists = function(variable){

	return typeof variable !== 'undefined'
};

/**
 * __Get
 * @param variable
 * @param failVal
 * @returns {boolean}
 * @private
 *
 * - Tests variable via __exists
 * - returns failVal or false if test fails
 */
const __get = function(variable, failVal){
	return __exists(variable) ? variable :
		__exists(failVal) ? failVal :
			false;
};

/**
 * Limit (string) for HTML id (attribute)
 * @param str
 * @returns {*}
 */
const limitForHtmlId = function(str) {
	// A regex that allows only characters valid in an HTML id:
	// letters, digits, underscores, hyphens, colons, and periods.
	return str.replace(/[^a-zA-Z0-9\-_:.]/g, '');
};

/**
 * Set Button CSS Classes
 * @param btnObj (obj)
 * @param className (arr)
 * @returns {arr}
 *
 * - btnType: [primary | secondary | alt]
 * - btnSize: [sm | lg]
 * - hoverScale: bool
 */
const setBtnCssClasses = function(btnObj, className){

	let newCssClasses = className.split(' ');
	arrRemoveStrings(newCssClasses, [
		'btn',
	]);
	newCssClasses.push('btn');



	// Btn Type
	//==============
	arrRemoveStrings(newCssClasses, [
		'btn-primary',
		'btn-secondary',
		'btn-tertiary',
		'btn-alt',
		'btn-alt-lite'
	]);
	if(btnObj.btnType !== 'default'){
		newCssClasses.push('btn-'+btnObj.btnType);
	}


	// Btn Size
	//==============
	arrRemoveStrings(newCssClasses, [
		'btn-sm',
		'btn-lg'
	]);
	if(btnObj.btnSize !== 'default'){
		newCssClasses.push('btn-'+btnObj.btnSize);
	}


	// Hover Scale
	//==============
	arrRemoveStrings(newCssClasses, [
		'btn-hover-scale'
	]);
	if(btnObj.hoverScale){
		newCssClasses.push('btn-hover-scale');
	}

	// console.log('newCssClasses', newCssClasses);

	return  newCssClasses;//.join(' ');
};

/**
 * Get Tab Screen Size
 * @param size
 * @returns {string}
 */
const getTabScreenSize = function (size){

	switch(size){
		case 'base': return 'Base';
			break;
		case 'sm': return 'Mobile';
			break;
		case 'md': return 'Tablet';
			break;
		case 'lg': return 'Desktop';
			break;
		case 'xl': return 'Desktop XL';
			break;
	}
};

/**
 * Search and Replace (via) Subsrting (in) Array
 * @param substr
 * @param arr
 * @param replace
 * @returns {Array}
 */
const searchAndReplaceSubstrArr = function(substr, arr, replace){

	let subStrFound = false;
	let replaceFound = false;
	replace = replace !== '' ? replace : false;
	arr = typeof arr !== 'undefined' ? arr : [];

	for (let i = 0; i < arr.length; i++){
		if (arr[i].includes(substr) !== false){

			if(replace === arr[i]) replaceFound = true;
			subStrFound = true;
			arr.splice(i, 1);
			if(replace) arr.push(replace);
			break;
		}
	}
	if(!subStrFound && replace !== false && !replaceFound){
		arr.push(replace);
	}

	return arr;
};

/**
 * Has Responsive Substring
 * - checks input string for bootstrap css screen size substrings
 *
 * @param str
 * @returns boolean
 */
const hasResponsiveSubStr = function(str) {

	return str.includes('-sm') ||
		str.includes('-md') ||
		str.includes('-lg') ||
		str.includes('-xl');
};

/**
 * Search and Replace Main Bootstrap CSS
 * - based on: searchAndReplaceSubstrArr()
 * - accounts for exceptions like vh-33 vs vh-md-33
 *
 * @param substr
 * @param classArr
 * @param replace
 * @returns {Array}
 */
const searchAndReplaceBootstrapCss = function(substr, classArr, replace){

	// console.log('searchAndReplaceBootstrapCss', substr, classArr, replace);

	let subStrFound = false;
	let replaceFound = false;
	let responsive_substr = hasResponsiveSubStr(substr);
	replace = replace !== '' ? replace : false;
	classArr = typeof classArr !== 'undefined' ? classArr : [];

	for (let i = 0; i < classArr.length; i++){
		if (classArr[i].includes(substr) !== false){

			// Detect if replace string already exists in classes array
			if(replace === classArr[i]) replaceFound = true;

			// Detect responsive substring
			let responsive_css = false;
			if( hasResponsiveSubStr( classArr[i]) ){
				responsive_css = true;
			}

			// Conditionally replace class string in classes array
			// - Only replace responsive class strings if input substr is also responsive
			// - Replace base class strings if input string is the base (without the addition substring denoting screen size)
			if(!responsive_substr && !responsive_css ||
				responsive_substr && responsive_css ){

				// Flag Find
				subStrFound = true;

				// Remove Found Class
				classArr.splice(i, 1);

				// Update classes if replace string is passed
				if(replace) classArr.push(replace);
				break;
			}
		}
	}

	// Update classes if no string found while a replace string is specified
	if(!subStrFound && !replaceFound){
		if(replace) classArr.push(replace);
	}



	return classArr;
};



/**
 * Set Responsive Classes
 *	- receives input from block editor
 *	- outputs an updated array for css classes
 *
 * @param data
 * @param attrStr
 * @param substr
 * @param classArr
 * @param attributes
 * @param props
 */
const setResponsiveClasses = function(data, attrStr, substr, classArr, attributes, props){

	let updatedAttr = attributes[attrStr] !== '' ? attributes[attrStr] : {};
	let updateVal = JSON.parse(data);
	updatedAttr[updateVal[0]] = updateVal[1] ? updateVal[1] : false;

	for(const screenSize in updatedAttr){

		let responsiveSubStr = screenSize === 'base' ? substr : substr + screenSize;

		// classArr = searchAndReplaceSubstrArr(substr + screenSize, classArr , updatedAttr[screenSize] );
		classArr = searchAndReplaceBootstrapCss(responsiveSubStr, classArr , updatedAttr[screenSize] );
		props.setAttributes( { cssClasses: classArr } );
	}

	// NOTE: logic below is updating but NOT saving attributes.blockHeight.
		// not sure why or how to troubleshoot
		// the feature works as expected but the settings are not being reflected
	// PATCHED: Set Default as empty string (due to bug in gutenberg)
	// - @ref: https://github.com/WordPress/gutenberg/issues/
	props.setAttributes( { [attrStr]: updatedAttr } );
	attributes = props.attributes;












	// //DEBUG
	// //------------------------------------------
	// console.log('setResponsiveClasses:', attrStr, updatedAttr);
	// console.log('- '+attrStr+':',attributes[attrStr]);
	// console.log('- cssClasses: ',attributes.cssClasses);

	// ASSUMES
	//------------------------------------------
	// attributes[attrStr] = {
	// 	// xs: false,
	// 		sm: '',
	// 		md: '',
	// 		lg: '',
	// 		xl: '',
	// }

	// Requires
	//------------------------------------------
	// bootstrap (or similar) css framework
};

// TODO (^):
//  - What are the conditions for using one over the other?
//  - is there a way we can use one, and depreciate teh other?
const genResponsiveCss = function(data, attrStr, substr, classArr, attributes, props) {

	let updatedAttr = attributes[attrStr] !== '' ? attributes[attrStr] : {};
	let updateVal = JSON.parse(data);
	updatedAttr[updateVal[0]] = updateVal[1] ? updateVal[1] : false;

	for (const screenSize in updatedAttr) {

		let responsiveSubStr = screenSize === 'base' ? substr : substr + screenSize;

		// classArr = searchAndReplaceSubstrArr(substr + screenSize, classArr , updatedAttr[screenSize] );
		classArr = searchAndReplaceBootstrapCss(responsiveSubStr, classArr, updatedAttr[screenSize]);
		// props.setAttributes({cssClasses: classArr});
	}

	// NOTE: logic below is updating but NOT saving attributes.blockHeight.
	// not sure why or how to troubleshoot
	// the feature works as expected but the settings are not being reflected
	// PATCHED: Set Default as empty string (due to bug in gutenberg)
	// - @ref: https://github.com/WordPress/gutenberg/issues/
	props.setAttributes({[attrStr]: updatedAttr});
	attributes = props.attributes;
};



/**
 * Array Remove Strings
 * 	- removes a set of string, from a larger set of strings
 *
 * @param arr
 * @param strings_arr
 */
const arrRemoveStrings = function(arr, strings_arr){

	for(let i = 0; i < strings_arr.length; i++ ){

		if(arr.indexOf(strings_arr[i]) > -1)
			arr.splice(arr.indexOf(strings_arr[i]), 1);
	}
};

/**
 * Generate Responsive CSS Classes
 * @param classObj
 * @returns {string}
 */
const genResponsiveClasses = function(classObj){
	let newCssClasses = [];
	for(const screenSize in classObj){
		if(classObj[screenSize] !== '') newCssClasses.push(classObj[screenSize]);
	}
	return newCssClasses.join(' ');
};

/**
 * Generate Tile Gap CSS
 * @param titleGapObj
 * @returns {{gapMarginCss: string, gapPaddingCss: string}}
 *
 * - Prep CSS class arrays
 * - Loop titleGapObj
 * 		- Get gapValue
 * 		- Update CSS class arrays
 * - Return margin & padding CSS strings
 */
const genTileGapCss = function(tileGapObj){

	// Prep CSS class arrays
	let gapMarginCss = [];
	let gapPaddingCss = [];

	// Loop titleGapObj
	for(const screenSize in tileGapObj){

		// Get gapValue
		let gapValue = tileGapObj[screenSize];

		// Update CSS class arrays
		if(gapValue !== '' && gapValue !== false) {
			let marginPrefix = screenSize !== 'base' ? 'mx-'+screenSize+'-n' : 'mx-n';
			gapMarginCss.push(marginPrefix + gapValue);

			let paddingPrefix = screenSize !== 'base' ? 'p-'+screenSize+'-' : 'p-';
			gapPaddingCss.push(paddingPrefix + gapValue);
		}
	}
	// console.log(gapMarginCss, gapPaddingCss);

	// Return margin & padding CSS strings
	return {
		gapMarginCss : gapMarginCss.join(' '),
		gapPaddingCss : gapPaddingCss.join(' '),
	}
};

/**
 * Set Default Screen Size Obj
 * @param props
 * @param attr
 *
 * Set a default Responsive CSS class obj a responsive CSS block attribute
 */
const setDefaultScreenSizeObj = function(props, attr){

	let attributes = props.attributes;
	if(attributes[attr] === ''){
		props.setAttributes({
			[attr] : {...defaultScreenSizeObj}//.key.toString()
		});
		attributes = props.attributes;
	}
};

/**
 * Get Image Source
 * @param imgObj
 * @param size
 * @returns {*}
 *
 * Retrieves image source from REST API response generated by add_featured_media_obj_to_rest()
 */
const getImageSource = function(imgObj, size){

	// Default to 'gallery' size
	size = typeof size !== 'undefined' ? size : 'gallery';

	return imgObj && typeof imgObj.sizes[size] !== 'undefined' ?
		// use specified size if available
		typeof imgObj.sizes[size].url !== 'undefined' ?
			imgObj.sizes[size].url : imgObj.sizes[size]

		// else use medium size if available
		: typeof imgObj.sizes.medium !== 'undefined' ?
			typeof imgObj.sizes.medium.url !== 'undefined' ?
				imgObj.sizes.medium.url : imgObj.sizes.medium

			// else use original file
			: imgObj.file;
	// ^ this is the part that requires add_featured_media_obj_to_rest()
	// ~ todo modify so it doesn't break if used imgObj comes from '/wp/v2/media' REST API request
};

/**
 * Get Image Alt (Text)
 * @param imgObj
 * @returns {string}
 */
const getImageAlt = function(imgObj){
	// console.log('imgObj:', imgObj);
	let alt = imgObj.alt !== '' ? imgObj.alt : false;
	alt = !alt && imgObj.title !== '' ? imgObj.title : alt;
	alt = !alt && typeof imgObj.alt_text !== 'undefined' ? imgObj.alt_text : alt;
	alt = !alt && typeof imgObj.filename !== 'undefined' ? imgObj.filename.split('.')[0] : alt;

	// Use URL to get File name as last resort
	if(!alt && typeof imgObj.url !== 'undefined'){
		let parts = imgObj.url.split('/');
		let filename = parts[parts.length-1];
		alt = !alt && typeof filename !== 'undefined' ? filename.split('.')[0] : alt;
	}


	return alt;
};

// todo move to core?
// need this for cases such as: when post.excerpt.raw is empty post.excerpt.rendered is  not
const removeTags = function (str) {
	if ((str===null) || (str===''))
		return false;
	else
		str = str.toString();

	// Regular expression to identify HTML tags in
	// the input string. Replacing the identified
	// HTML tag with a null string.
	return str.replace( /(<([^>]+)>)/ig, '');
};

const allowTagsSimpleText = function(htmlString) {
	// Define the allowed tags using regular expressions
	var allowedTagsRegex = /<(\/?(p|strong|i|em))[^>]*>/gi;

	// Replace all HTML tags except the allowed ones
	var cleanedString = htmlString.replace(/<[^>]+>/g, function(match) {
		// Check if the tag is allowed or not
		return match.match(allowedTagsRegex) ? match : '';
	});

	return cleanedString;
}

function toTitleCase(str) {
	return str.replace(
		/\w\S*/g,
		function(txt) {
			return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
		}
	);
}


/* Components */
/* ================================= */


/* COLOR Component & its Extensions */
/*-------------------------------*/

/**
 * Color Component
 * --------------------------
 * Uses Dynamic Block Attributes
 * Functions only
 * Used by:
 * - BGPopover
 * - TextColorPopover
 * - TextColorPopover
 */
class ColorComponent extends Component {

	init( props ){
		this.props = props.props;
	}

	setColor(color, attribute){

		let props = __get(this.props.blockProps, this.props);
		let attributes = props.attributes;

		props.setAttributes({
			[attribute]: color,
		});
		attributes = props.attributes;
	}

	rgbaColorStyle(color){

		let c = color.rgb;
		return `rgba(${c.r},${c.g},${c.b},${c.a})`;
	}

	setCustomColor(color, attribute){

		let c = color.rgb;
		const colStr = `rgba(${c.r},${c.g},${c.b},${c.a})`;

		this.setColor(colStr, attribute);
	}

	clearColor(attribute){


		let self = this;
		let props = __get(this.props.blockProps, this.props);
		let attributes = props.attributes;

		props.setAttributes({
			[attribute]: false,
		});
		attributes = props.attributes;
	}


	// render() {
	//
	// 	let self = this;
	// 	let props = this.props;
	// 	let attributes = this.props.attributes;
	// 	// let newCssClasses 	= [...attributes.cssClasses];
	//
	//
	// 	return 'test'
	// }
}

/**
 * Color Popover
 * --------------------------
 * Popover Containing a Color Pick and Color Palette
 * Intended for Extension By TextColorPopover, BorderColorPopover, etc...
 *
 * Required Params:
 * - targetAttr: string
 * - popoverHeader: string
 * - popoverToggle: boolean
 */
class ColorPopover extends ColorComponent {

	init(props) {
		this.props = props.props;
	}

	onTogglePopover(){
		let props = this.props;
		if (__exists(props.onTogglePopover))
			props.onTogglePopover();
	}

	// TODO do this via state
	togglePopover(popoverToggle) {

		// //old way
		// let props = __get(this.props.blockProps, this.props);
		// let attributes = props.attributes;
		// // popoverToggle = attributes.popoverToggle;
		// props.setAttributes({
		// 	[popoverToggle]: !attributes[popoverToggle]
		// });


		// new way
		let self = this;
		self.onTogglePopover();
	}

	return(params) {

		let self = this;
		let props = __get(this.props.blockProps, this.props);
		let attributes = props.attributes;

		let targetAttr = params.targetAttr;
		let popoverHeader = params.popoverHeader;
		let popoverToggle = params.popoverToggle;

		return el(Popover,{
				position: 'top left',
				className: 'popover-plus',
				// NOTE: this breaks when the gradient angle ui gets clicked on
				// onFocusOutside : toggleBgPopover
			},
			el('div',{ className:'content-wrap' },


				// Close Button
				//-----------------------------
				el(components.Button,{
						className : 'close-btn',
						onClick: function(){
							self.togglePopover(popoverToggle);
						},
					},
					el( Icon, { icon: 'no-alt' }),
				),

				el('div', {},
					el('p', {
						className: 'f-w-600'
					}, popoverHeader),

					// ColorPalette
					//-----------------------------
					el( PanelBody, {
							title: 'Preset Colors',
							className: cwbGlobals.innerPanelClasses,
							initialOpen: !0//false
						},
						el( ColorPalette, {
								// - https://developer.wordpress.org/block-editor/reference-guides/components/color-picker/
								// - https://studiopress.blog/theme-color-palette/
								// NOTE: this should match BRAND COLOURS in mod-theme-design-base.css
								color: attributes[targetAttr],
								colors: cwbGlobals.customColorPalette,
								// onChange: setCustomColor,
								clearable: false,
								disableCustomColors: true,
								onChange: function(value){
									self.setColor(value, targetAttr);
								}
							}
						),
					),


					// ColorPicker
					//-----------------------------
					// - https://developer.wordpress.org/block-editor/reference-guides/components/color-picker/
					// - https://studiopress.blog/theme-color-palette/
					el( PanelBody, {
							title: 'Custom Color',
							className: cwbGlobals.innerPanelClasses,
							initialOpen: false
						},
						el( ColorPicker, {
								color: attributes[targetAttr],
								onChangeComplete: function(value){
									self.setCustomColor(value, targetAttr);
								}
							}
						),
					),


					// Button : Clear Color (conditional)
					//----------------------------------------
					attributes[targetAttr] ?
						el( components.Button, {
								className: 'button button-large cursor-pointer',
								style: {margin:'20px 0 20px'},
								onClick: function() {
									self.clearColor(targetAttr)
								},
								// isTertiary: !0,
								// isLink: !0,
								// isDestructive: !0,
							},
							__( 'Clear Color', 'cwb' )
						) : null,
				)
			),
		)
	}
}


class SingleColorPopover extends ColorPopover {

	init(props) {
		this.props = props.props;
	}

	render() {

		let self = this;

		console.log('self.props', self.props);

		return self.return(self.props.params);
	}
}


/**
 * Text Color Popover
 * --------------------------
 * Required Block Attributes:
 * - textColor: string
 */
class TextColorPopover extends ColorPopover {

	init(props) {
		this.props = props.props;
	}

	render() {

		let self = this;

		return self.return({
			targetAttr 		: 'textColor',
			popoverHeader 	: 'Text Color',
			// popoverToggle 	: 'textColorPopover',
		});
	}
}

/**
 * Border Color Popover
 * --------------------------
 * Required Block Attributes:
 * - borderColor: string
 */
class BorderColorPopover extends ColorPopover {

	init(props) {
		this.props = props.props;
	}

	render() {

		let self = this;

		return self.return({
			targetAttr 		: 'borderColor',
			popoverHeader 	: 'Border Color',
			// popoverToggle 	: 'borderColorPopover',
		});
	}
}

/**
 * Text Color Popover
 * --------------------------
 * Required Block Attributes:
 * - textColor: string
 */
class TextHoverColorPopover extends ColorPopover {

	init(props) {
		this.props = props.props;
	}

	render() {

		let self = this;

		return self.return({
			targetAttr 		: 'hoverTextColor',
			popoverHeader 	: 'Text Hover Color',
			// popoverToggle 	: 'hoverTextColorPopover',
		});
	}
}

/**
 * Border Color Popover
 * --------------------------
 * Required Block Attributes:
 * - borderColor: string
 */
class BorderHoverColorPopover extends ColorPopover {

	init(props) {
		this.props = props.props;
	}

	render() {

		let self = this;

		return self.return({
			targetAttr 		: 'hoverBorderColor',
			popoverHeader 	: 'Border Hover Color',
			// popoverToggle 	: 'hoverBorderColorPopover',
		});
	}
}

/**
 * BG Popover
 * --------------------------
 * BG Options
 * - solid + gradient !!
 * * alpha on solid & gradients
 *
 * Required Block Attributes:
 * - bgColorOutput: string
 * - bgColor: string
 * - bgUseGradient: boolean
 * - bgGradient: string
 */
class BGPopover extends ColorComponent {

	init( props ){
		this.props = props.props;
	}

	onTogglePopover(){
		let props = this.props;
		if (__exists(props.onTogglePopover))
			props.onTogglePopover();
	}

	setBg(newAtts){

		let props = __get(this.props.blockProps, this.props);
		let attributes = props.attributes;

		// const bgUseGradient = '';
		newAtts = typeof newAtts === 'object' ? newAtts : false;
		const bgColor = newAtts && typeof newAtts.bgColor !== 'undefined' ?
			newAtts.bgColor : attributes.bgColor;
		const bgUseGradient = newAtts && typeof newAtts.bgUseGradient !== 'undefined' ?
			newAtts.bgUseGradient : attributes.bgUseGradient;
		const bgGradient = newAtts && typeof newAtts.bgGradient !== 'undefined' ?
			newAtts.bgGradient : attributes.bgGradient;

		// console.log('newAtts:', newAtts);
		// console.log('bgGradient:', bgGradient);
		// console.log('bgUseGradient:', bgUseGradient);
		// console.log('bgColor:', bgColor);
		const output = bgGradient && bgUseGradient ?
			`${bgGradient}` : bgColor ?
				`${bgColor}` : null;

		return  output;

	}

	clearGradient() {

		let self = this;
		let props = __get(this.props.blockProps, this.props);
		let attributes = props.attributes;

		props.setAttributes({
			bgGradient: null,
			bgColorOutput: self.setBg({
				bgGradient: null
			})
		});
		attributes = props.attributes;
	}

	setGradientToggle(value) {

		let self = this;
		let props = __get(this.props.blockProps, this.props);
		let attributes = props.attributes;

		props.setAttributes({
			bgUseGradient: value,
			bgColorOutput: self.setBg({
				bgUseGradient: value,
			})
		});
		attributes = props.attributes;
	}

	setGradientColor(value) {

		let self = this;
		let props = __get(this.props.blockProps, this.props);
		let attributes = props.attributes;

		props.setAttributes({
			bgGradient: value,
			bgColorOutput: self.setBg({
				bgGradient: value
			})
		});
		attributes = props.attributes;
	}

	render() {

		let self = this;
		let props = __get(this.props.blockProps, this.props);
		let attributes = props.attributes;


		// // TODO do this via state
		// const toggleBgPopover = function(){
		// 	props.setAttributes({
		// 		// todo: depreciate this attr
		// 		bgPopover: !attributes.bgPopover
		// 	});
		// };


		return el(Popover,{
				position: 'top left',
				className: 'popover-plus',
				// NOTE: this breaks when the gradient angle ui gets clicked on
				// onFocusOutside : toggleBgPopover
			},
			el('div',{ className:'content-wrap' },


				// Close Button
				//-----------------------------
				el(components.Button,{
						className : 'close-btn',
						// onClick: onTogglePopover,
						onClick: function() {
							// use this to manage popover display condition
							self.onTogglePopover();
						}
					},
					el( Icon, { icon: 'no-alt' }),
				),

				el('p', {
					className: 'f-w-600'
				}, 'Background Color'),


				// Toggle: Solid BG vs Gradient
				//-----------------------------
				el( ToggleGroupControl, {
						label : "Color Type",
						style: { marginBottom: '20px' },
						value : attributes.bgUseGradient,
						// onChange : setGradientToggle,
						onChange: function(value) {
							self.setGradientToggle(value);
						}
					},
					el(ToggleGroupControlOption,{
						label : 'Solid',
						value : false
					}),
					el(ToggleGroupControlOption,{
						label : 'Gradient',
						value : true}
					),
				),

				!attributes.bgUseGradient ?

					el('div', {},

						// ColorPalette
						//-----------------------------
						el( PanelBody, {
								title: 'Preset Colors',
								className: cwbGlobals.innerPanelClasses,
								initialOpen: !0//false
							},
							el( ColorPalette, {
									// - https://developer.wordpress.org/block-editor/reference-guides/components/color-picker/
									// - https://studiopress.blog/theme-color-palette/
									// NOTE: this should match BRAND COLOURS in mod-theme-design-base.css
									// TODO: PHP presets (gradients too?) https://richtabor.com/block-editor-gradients/
									color: attributes.bgColor,
									colors: cwbGlobals.customColorPalette,
									// onChange: setCustomColor,
									clearable: false,
									disableCustomColors: true,
									// onChange: setPaletteColor,
									onChange: function(value) {
										// self.setPaletteColor(value);
										self.setColor(value, 'bgColor');
										self.setColor(value, 'bgColorOutput');
									}
								}
							),
						),


						// ColorPicker
						//-----------------------------
						// - https://developer.wordpress.org/block-editor/reference-guides/components/color-picker/
						// - https://studiopress.blog/theme-color-palette/
						el( PanelBody, {
								title: 'Custom Color',
								className: cwbGlobals.innerPanelClasses,
								initialOpen: false
							},
							el( ColorPicker, {
									color: attributes.bgColor,
									// onChangeComplete: setCustomColor,
									onChangeComplete: function(value) {
										// self.setCustomColor(value);

										const colStr = self.rgbaColorStyle(value);
										const bgColorOutput = self.setBg({
											bgColor: colStr,
										});

										self.setColor(colStr, 'bgColor');
										self.setColor(bgColorOutput, 'bgColorOutput');

									}
								}
							),
						),


						// Button : Clear Bg Color (conditional)
						//----------------------------------------
						attributes.bgColor ?
							el( components.Button, {
									className: 'button button-large',
									style: {margin:'20px 0 20px'},
									onClick: function() {
										self.clearColor('bgColor');
										self.clearColor('bgColorOutput');
									},
									// isTertiary: !0,
									// isLink: !0,
									// isDestructive: !0,
								},
								__( 'Clear Background Color', 'cwb' )
							) : null,
					) :

					el('div', {},

						// GradientPicker
						//-----------------------------
						el(GradientPicker, {
							value : attributes.bgGradient,
							gradients: cwbGlobals.customGradientPresets,
							// onChange : setGradientColor,
							onChange: function(value) {
								self.setGradientColor(value);
							},
							clearable : false
						}),


						// Button : Clear Bg Gradient (conditional)
						//----------------------------------------
						attributes.bgGradient ?
							el( components.Button, {
									className: 'button button-large',
									style: {margin:'20px 0 20px'},
									// onClick: clearGradient,
									onClick: function() {
										self.clearGradient();
									}
								},
								__( 'Clear Background Gradient', 'cwb' )
							) : null,
					),
			),
		)
	}
}

/**
 * BG Effects (Popover)
 *
 * todo: move popover toggle state logic to BGPopover
 * 		and depreciate this
 * 		currently only used in BgEffects (panel)
 */
class BGEffectsPopover extends ColorComponent {

	init( props ){
		this.props = props.props;
		// this.state = {
		// 	showPopover: false
		// };
	}

	onTogglePopover(){
		let props = this.props;
		if (__exists(props.onTogglePopover))
			props.onTogglePopover();
	}

	setOverlayBg(newAtts){

		// let props = this.props;
		let attributes = this.props.attributes;

		// const bgUseGradient = '';
		newAtts = typeof newAtts === 'object' ? newAtts : false;
		const overlayColor = newAtts && __exists(newAtts.overlayColor) ?
			newAtts.overlayColor : attributes.overlayColor;
		const overlayUseGradient = newAtts && __exists(newAtts.overlayUseGradient) ?
			newAtts.overlayUseGradient : attributes.overlayUseGradient;
		const overlayGradient = newAtts && __exists(newAtts.overlayGradient) ?
			newAtts.overlayGradient : attributes.overlayGradient;

		return overlayGradient && overlayUseGradient ?
			`${overlayGradient}` : overlayColor ?
				`${overlayColor}` : null;

	}

	clearGradient() {

		let self = this;
		let props = this.props;
		let attributes = this.props.attributes;

		props.setAttributes({
			overlayGradient: null,
			overlayColorOutput: self.setOverlayBg({
				overlayGradient: null
			})
		});
		attributes = props.attributes;
	}

	setGradientToggle(value) {

		let self = this;
		let props = this.props;
		let attributes = this.props.attributes;

		props.setAttributes({
			overlayUseGradient: value,
			overlayColorOutput: self.setOverlayBg({
				overlayUseGradient: value,
			})
		});
		attributes = props.attributes;
	}

	setGradientColor(value) {

		let self = this;
		let props = this.props;
		let attributes = this.props.attributes;

		props.setAttributes({
			overlayGradient: value,
			overlayColorOutput: self.setOverlayBg({
				overlayGradient: value
			})
		});
		attributes = props.attributes;
	}

	render() {

		let self = this;
		let props = this.props;
		let attributes = props.attributes;


		return el(Popover,{
				position: 'top left',
				className: 'popover-plus',
				// NOTE: this breaks when the gradient angle ui gets clicked on
				// onFocusOutside : toggleBgPopover
			},
			el('div',{ className:'content-wrap' },


				// Close Button
				//-----------------------------
				el(components.Button,{
						className : 'close-btn',
						// onClick: onTogglePopover,
						onClick: function() {
							// use this to manage popover display condition
							self.onTogglePopover();
						}
					},
					el( Icon, { icon: 'no-alt' }),
				),

				el('p', {
					className: 'f-w-600'
				}, 'Overlay Color'),


				// Toggle: Solid Overlay vs Gradient
				//-----------------------------
				el( ToggleGroupControl, {
						label : "Color Type",
						style: { marginBottom: '20px' },
						value : attributes.overlayUseGradient,
						// onChange : setGradientToggle,
						onChange: function(value) {
							self.setGradientToggle(value);
						}
					},
					el(ToggleGroupControlOption,{
						label : 'Solid',
						value : false
					}),
					el(ToggleGroupControlOption,{
						label : 'Gradient',
						value : true}
					),
				),

				!attributes.overlayUseGradient ?

					el('div', {},

						// ColorPalette
						//-----------------------------
						el( PanelBody, {
								title: 'Preset Colors',
								className: cwbGlobals.innerPanelClasses,
								initialOpen: !0//false
							},
							el( ColorPalette, {
									// - https://developer.wordpress.org/block-editor/reference-guides/components/color-picker/
									// - https://studiopress.blog/theme-color-palette/
									// NOTE: this should match BRAND COLOURS in mod-theme-design-base.css
									// TODO: PHP presets (gradients too?) https://richtabor.com/block-editor-gradients/
									color: attributes.overlayColor,
									colors: cwbGlobals.customColorPalette,
									clearable: false,
									disableCustomColors: true,
									onChange: function(value) {
										self.setColor(value, 'overlayColor');
										self.setColor(value, 'overlayColorOutput');
									}
								}
							),
						),


						// ColorPicker
						//-----------------------------
						// - https://developer.wordpress.org/block-editor/reference-guides/components/color-picker/
						// - https://studiopress.blog/theme-color-palette/
						el( PanelBody, {
								title: 'Custom Color',
								className: cwbGlobals.innerPanelClasses,
								initialOpen: false
							},
							el( ColorPicker, {
									color: attributes.overlayColor,
									onChangeComplete: function(value) {

										const colStr = self.rgbaColorStyle(value);
										const overlayColorOutput = self.setOverlayBg({
											overlayColor: colStr,
										});

										self.setColor(colStr, 'overlayColor');
										self.setColor(overlayColorOutput, 'overlayColorOutput');

									}
								}
							),
						),


						// Button : Clear Overlay Color (conditional)
						//----------------------------------------
						attributes.overlayColor ?
							el( components.Button, {
									className: 'button button-large',
									style: {margin:'20px 0 20px'},
									onClick: function() {
										self.clearColor('overlayColor');
										self.clearColor('overlayColorOutput');
									},
									// isTertiary: !0,
									// isLink: !0,
									// isDestructive: !0,
								},
								__( 'Clear Overlay Color', 'cwb' )
							) : null,
					) :

					el('div', {},

						// GradientPicker
						//-----------------------------
						el(GradientPicker, {
							value : attributes.overlayGradient,
							gradients: cwbGlobals.customGradientPresets,
							onChange: function(value) {
								self.setGradientColor(value);
							},
							clearable : false
						}),


						// Button : Clear Overlay Gradient (conditional)
						//----------------------------------------
						attributes.overlayGradient ?
							el( components.Button, {
									className: 'button button-large',
									style: {margin:'20px 0 20px'},
									onClick: function() {
										self.clearGradient();
									}
								},
								__( 'Clear Overlay Gradient', 'cwb' )
							) : null,
					),
			),
		)
	}
}




/* INSPECTOR Panels */
/*-------------------------------*/

/**
 * Color Indicator & Text
 * --------------------------
 * Props:
 * - color: string
 * - text: string
 */
class ColorIndicatorAndText extends Component {

	init( props ){
		this.props = props.props;
	}

	render() {

		let props = this.props;

		return el( 'div', {
				style: {
					padding: '5px 0px 10px',
					cursor: 'pointer',
					// background: '#fcfcfc'
				},
				onClick: props.onClick},

			el( ColorIndicator, { className: 'm-0', colorValue: props.color}),
			el('span', { style: { marginLeft: '10px'}}, props.text)
		)
	}
}

/**
 * Block Border Panel
 * --------------------------
 * Required Block Attributes:
 * - borderColor: string
 * - borderWidth: string
 */
class BlockBorderPanel extends Component {

	init( props ){
		this.props = props.props;
	}

	render() {

		let props = this.props;
		let attributes = this.props.attributes;
		// let newCssClasses = [...attributes.cssClasses];

		// TODO do this via state
		const toggleBorderColorPopover = function(){
			props.setAttributes({
				borderColorPopover: !attributes.borderColorPopover
			});
		};

		const setBorderWidth = function(value) {
			props.setAttributes({
				borderWidth: value
			});
			attributes = props.attributes;
		};

		return el( PanelBody, {
				title : 'Block Border',
				initialOpen: true
			},


			// Preview & Popover Trigger
			//-----------------------------
			el(ColorIndicatorAndText, {
				color: attributes.borderColor,
				text: 'Border Color',
				onClick: toggleBorderColorPopover,
			}),

			el(NumberControl, {
				label: __('Border Width'),
				value: attributes.borderWidth,
				// isShiftStepEnabled: true,
				// shiftStep : 1,
				max: 10,
				min: 0,
				onChange: setBorderWidth, // tdod: save() returns 'undefined'
			}),


			// Border Color Popover
			//-----------------------------
			attributes.borderColorPopover ?
				el(BorderColorPopover, props) : null

		)
	}
}

/**
 * Block Colors (Panel)
 * --------------------------
 * Text & background color setting + Contrast Checker
 *
 * Required Block Attributes:
 * - bgColorOutput: string
 * - bgColor: string
 * - bgUseGradient: boolean
 * - bgGradient: string
 * - textColor: string
 */
class BlockColors extends Component {

	constructor(props) {
		super(props);
		this.state = {
			showTextColorPopover: false,
			showBgPopover: false
		};
	}

	init( props ){
		this.props = props.props;
	}

	toggleBgPopover() {
		// console.log('showPopover', this.state);
		this.setState({ showBgPopover: !this.state.showBgPopover });
	}

	toggleTextColorPopover() {
		// console.log('showPopover', this.state);
		this.setState({ showTextColorPopover: !this.state.showTextColorPopover });
	}

	render() {

		let self = this;
		let props = this.props;
		let attributes = this.props.attributes;
		let newCssClasses 	= [...attributes.cssClasses];
		let showTextColors =  __exists(attributes.textColor) ;//typeof props.showTextColors !== 'undefined' ? props.showTextColors : true;
		let showBgColors =  __exists(attributes.bgColorOutput) ;//typeof props.showBgColors !== 'undefined' ? props.showBgColors : true;
		let showTextWhite =  __exists(attributes.textWhite) ;
		let initialOpen = __get(props.initialOpen, false);


		return el( PanelBody, {
				title: 'Block Colors',
				initialOpen: initialOpen//!0
			},

			// Preview & Popover Trigger
			//-----------------------------
			showBgColors && (showTextColors || showTextWhite) ? el('div', {
					className: 'transparency-grid',
				},
				el( 'div', {
						className: 'px-3 py-1 w-100 d-inline-flex' + (attributes.textWhite ? ' text-white' : ''),//'editor-post-featured-image__toggle edit-bg-color',
						style:{
							background: attributes.bgColorOutput,
							fontWeight: 400,
							fontSize: '14px'
						},
					},
					el( 'span', { style: { color: attributes.textColor }}, __( 'Sample Text', 'cwb' ))
				)
			): null,
			showBgColors ? el(ColorIndicatorAndText, {
				color: attributes.bgColorOutput,
				text: 'Background Color',
				//onClick: toggleBgPopover,
				onClick: function() {
					self.toggleBgPopover();
				}
			}) : null,
			showTextColors ? el(ColorIndicatorAndText, {
				color: attributes.textColor,
				text: 'Text Color',
				// onClick: toggleBTextColorPopover
				onClick: function() {
					self.toggleTextColorPopover();
				}
			}) : null,


			// Toggle : Light Text
			//-----------------------------
			showTextWhite ? el( 'div', { style: { padding: '5px 0 0' } },
				el( ToggleControl,
					{
						label: 'Use Light Text',
						onChange: function(value){
							let classArr = searchAndReplaceSubstrArr('text-white', newCssClasses , value ? 'text-white' : '' );
							props.setAttributes( {
								cssClasses: classArr,
								textWhite : value
							} );
							attributes = props.attributes;
						},
						checked: props.attributes.textWhite,
					}
				)
			) : null,


			// Contrast Checker
			// ~ todo: default font color detection
			// ≈ todo: font-size detection
			//-----------------------------
			showBgColors && (showTextColors || showTextWhite) ? el(ContrastChecker,{
					backgroundColor: attributes.bgColor,
					textColor: showTextWhite ? "#ffffff" : attributes.textColor,
					fontSize: 16,
				}
			): null, // https://wp-gb.com/contrastchecker/


			// Background Color Popover
			//-----------------------------
			this.state.showBgPopover ?
				el(BGPopover, {
					blockProps: props,
					onTogglePopover: function(){
						self.toggleBgPopover();
					}
				}) : null,


			// Text Color Popover
			//-----------------------------
			this.state.showTextColorPopover ?
				el(TextColorPopover, {
					blockProps: props,
					onTogglePopover: function(){
						self.toggleTextColorPopover();
					}
				}) : null,
		);
	}
}

/**
 * BG Image Panel
 * --------------------------
 * Required block attributes:
 * - bgCover: 'boolean', default: true // if true add 'bg-cover to cssClasses defaults
 * - mediaURL: 'string'
 * - mediaID: 'number',
 *
 * TODO lazy load?
 */
class BgImagePanel extends Component {

	// init( data ){}

	render() {

		let self            = this;
		let props           = this.props.props;
		let attributes      = this.props.props.attributes;
		let newCssClasses 	= [...attributes.cssClasses];
		let blockStyle      = this.props.blockStyle;

		const onSelectImage = function( media ) {
			blockStyle.backgroundImage = 'url(\''+attributes.mediaURL+'\');';
			// console.log(media);
			return props.setAttributes( {
				mediaURL: media.url,
				mediaID: media.id,
				mediaAlt: media.alt ? media.alt : '',
			} );
		};
		const removeMedia = function() {
			props.setAttributes({
				mediaID: false,
				mediaURL: ''
			});
			attributes = props.attributes;
			// self.onUpdate();
		};
		const toggleBgCover = function(value) {
			let classArr = searchAndReplaceSubstrArr('bg-cover', newCssClasses , value ? 'bg-cover' : '' );
			props.setAttributes( { cssClasses: classArr } );
			props.setAttributes( { bgCover: value } );
			attributes = props.attributes;
			// self.onUpdate();
		};
		const toggleAriaLabel = function( value ) {
			props.setAttributes( { includeAriaLabel: value } );
			attributes = props.attributes;
		};


		// Panel : Background Image
		//-----------------------------
		return el( PanelBody, {
				title: 'Background Image',
				initialOpen: typeof attributes.mediaID !== 'undefined'
			},

			// MediaUpload
			//-----------------------------
			// el( MediaUploadCheck, {}, todo: remove this if nothing is breaking
			el( MediaUpload, {
					// extra links: https://www.liip.ch/en/blog/add-an-image-selector-to-a-gutenberg-block
					onSelect: onSelectImage,
					allowedTypes: 'image',
					value: attributes.mediaID,
					render: function( obj ) {
						return ! attributes.mediaID ?

							// UI: Choose Image (conditional)
							//-----------------------------
							el( 'div', null,
								el( components.Button, {
										className: 'editor-post-featured-image__toggle',
										style: {margin:'20px 0'},
										onClick: obj.open,
									},
									__( 'Choose Image', 'gutenberg-examples' )
								)
							) :

							// UI: Manage BG Image (conditional)
							//-----------------------------
							el( 'div', null,

								// UI: Preview / Replace Image
								//-----------------------------
								el( 'div', {
										className: 'image-button cursor-pointer',
										style: {margin:'20px 0'},
										onClick: obj.open,
									},
									el( 'img', {
										src: attributes.mediaURL }
									)
								),

								// UI: Remove Image
								//-----------------------------
								el( components.Button, {
										className: 'button-large',
										style: {margin:'0 0 20px'},
										onClick: removeMedia,
										isTertiary: !0,
										// isLink: !0,
										isDestructive: !0,
									},
									__( 'Remove Image', 'gutenberg-examples' )
								),

								// UI: BG Cover Toggle
								//-----------------------------
								el( ToggleControl, {
										// className: 'button button-large',
										label: __('Background-Cover'),
										help: __('Stretch image to cover background'),
										style: {margin:'0 0 20px'},
										checked: attributes.bgCover,
										onChange: toggleBgCover
									}
								),

								// UI: BG ARIA Toggle
								//-----------------------------
								el( ToggleControl,
									{
										label: 'Include ARIA Label',
										onChange: toggleAriaLabel,
										checked: props.attributes.includeAriaLabel,
									}
								)
							)
					}
				}
			)
		);
	}
}

/**
 * Hover Colors
 * --------------------------
 * Text, background, and border color settings + Contrast Checker
 *
 * Required Block Attributes:
 * - bgColorOutput: string
 * - bgColor: string
 * - bgUseGradient: boolean
 * - bgGradient: string
 * - textColor: string
 * - borderColor: string
 */
class HoverColors extends Component {

	init( props ){
		this.props = props.props;
	}

	render() {

		let props = this.props;
		let attributes = this.props.attributes;
		let newCssClasses 	= [...attributes.cssClasses];

		// TODO do this via state
		const toggleBgPopover = function(){
			props.setAttributes({
				bgPopover: !attributes.bgPopover
			});
		};

		// TODO do this via state
		const toggleBTextColorPopover = function () {
			props.setAttributes({
				hoverTextColorPopover: !attributes.hoverTextColorPopover
			});
		};

		// TODO do this via state
		const toggleBorderColorPopover = function(){
			props.setAttributes({
				hoverBorderColorPopover: !attributes.hoverBorderColorPopover
			});
		};

		return el( PanelBody, {
				title: 'Hover Colors',
				initialOpen: false//!0
			},

			// Preview & Popover Trigger
			// todo: dry
			//-----------------------------
			el('div', {
					className: 'transparency-grid',
				},
				el( 'div', {
						className: 'px-3 py-1 w-100 d-inline-flex',//'editor-post-featured-image__toggle edit-bg-color',
						style:{
							background: attributes.hoverBgColorOutput,
							fontWeight: 400,
							fontSize: '14px'
						},
					},
					el( 'span', { style: { color: attributes.hoverTextColor }}, __( 'Sample Text', 'cwb' ))
				)
			),
			el(ColorIndicatorAndText, {
				color: attributes.hoverBgColorOutput,
				text: 'Background Color',
				// onClick: toggleBgPopover,
				// onClick: function() {
				// 	self.toggleBgPopover();
				// }
			}),
			el(ColorIndicatorAndText, {
				color: attributes.hoverTextColor,
				text: 'Text Color (on hover)',
				onClick: toggleBTextColorPopover
			}),
			el(ColorIndicatorAndText, {
				color: attributes.hoverBorderColor,
				text: 'Border Color (on hover)',
				onClick: toggleBorderColorPopover,
			}),


			// Contrast Checker
			// ~ todo: default font color detection
			// ≈ todo: font-size detection
			//-----------------------------
			el(ContrastChecker,{
					backgroundColor: attributes.bgColor,
					textColor: attributes.textColor,
					fontSize: 16,
				}
			), // https://wp-gb.com/contrastchecker/


			// // Background Color Popover
			// //-----------------------------
			// attributes.bgPopover ?
			// 	el(BGPopover, props) : null,


			// Text Color Popover
			//-----------------------------
			attributes.hoverTextColorPopover ?
				el(TextHoverColorPopover, props) : null,


			// Border Color Popover
			//-----------------------------
			attributes.hoverBorderColorPopover ?
				el(BorderHoverColorPopover, props) : null
		);
	}
}

/**
 * BG Effects (Panel)
 * --------------------------
 * - Bg Overlay Color: BGEffectsPopover
 * - todo: (^) Bg Overlay Color: BGPopover
 * - BG Overlay Blur Effect: RangeControl
 */
class BgEffects extends Component {

	constructor(props) {
		super(props);
		this.state = {
			showPopover: false
		};
	}

	togglePopover() {
		// console.log('showPopover', this.state);
		this.setState({ showPopover: !this.state.showPopover });
	}

	render() {
		let self = this; // Preserve 'this' context
		let props = this.props;
		let attributes = this.props.attributes;
		let blur = __get(attributes.overlayBlur, 0);
		let initialOpen = __get(props.initialOpen, false);


		return el( PanelBody, {
				title: 'BG Effects',
				initialOpen: initialOpen //!0
			},
			__exists(attributes.overlayColorOutput) ?
				el(ColorIndicatorAndText, {
					...props,
					color: attributes.overlayColorOutput,
					text: 'Overlay Color',
					onClick: function() {
						self.togglePopover();
					}
				}) : null,

			// Background Color Popover
			//-----------------------------
			this.state.showPopover ?
				el(BGEffectsPopover, {
					...props,
					onTogglePopover: function(){
						self.togglePopover();
					}
				}) : null,


			__exists(attributes.overlayBlur) ?
				el(RangeControl, {
					value: blur,
					label: 'Background Blur',
					step: 0.1,
					min: 0,
					max: 16,
					// trackColor: 'blue',
					onChange: function(value){
						props.setAttributes({
							overlayBlur: value
						});
						attributes = props.attributes;
					}
				}) : null

			// __exists(attributes.overlayContrast)
			// __exists(attributes.overlayHue)
		);
	}
}




/* Responsive Tabs */
/*-------------------------------*/

/**
 * Responsive Tabs
 * ------------------------------------
 * @prop image
 * @prop modalId
 *
 * requires:
 * - cwbGlobals.screenSizeTabs
 *
 * Inspector tabs(via TabPanel) to facilitate responsive block edits
 * - tab option sample: { name: 'lg', className: 'tab-lg', title: 'lg' }
 */
class ResponsiveTabs extends Component {

	init(props) {
		this.props = props.props;
	}

	render() {

		let props = this.props;

		return el('div',{ style: {
					marginTop: '10px',
					border: '1px solid #ddd'
				}},
			el( TabPanel, {
					// label: ,
					key: 'multiple',
					activeClass: 'is-active',
					orientation: 'horizontal',
					tabs: screenSizeTabs,
				},
				( tab ) => {

					return el('div', {
							style: {
								padding: '20px 10px',
								background: '#f8f8f8'
							}
						},
						// el('p', {}, //'Responsive Wrap Settings: ',
						// el('strong', {},
						// 	`${getTabScreenSize(tab.name)}`
						// ),
						// ),
						props.children(tab)
					)
				}
			)
		)
	}

// // Instantiation Pattern
// //---------------------------
// el(ResponsiveTabs, props,
// 	(tab) => {
// 		props = {
// 			tab:{...tab},
// 			...props
// 		};
// 		return el(LayoutSettings, props);
// 	}
// ),
}

/**
 * Layout Settings Tabs (for Repeaters)
 * ------------------------------------
 * @prop image
 * @prop modalId
 *
 * requires:
 * - ResponsiveTabs
 *
 * usage:
 * - use inside RepeaterControls
 *
 * todo: remove comments once LayoutSettingsTab is built
 */
class RepeaterLayoutSettingsTab extends Component {

	init(props) {
		this.props = props.props;
	}

	render() {

		let self = this;
		let props = this.props;
		let attributes = this.props.attributes;
		let tab = props.tab;

		let focusItem = attributes.items[ attributes.focusItem !== null ? attributes.focusItem : 0 ];
		// console.log(focusItem.cssClasses);
		// let newCssItemClasses 	= typeof focusItem.cssClasses !== 'undefined' ? [...focusItem.cssClasses] : [];


		// ~todo remove below comments once LayoutSettingsTab is built
		// Block Height
		//-----------------------------
		// const genBlockHeightOptions = function(tab){
		//
		// 	// let prefix = tab.name+'-';
		// 	let prefix = tab.name === 'base' ? '' : tab.name+'-';
		//
		// 	return [
		// 		{ value: JSON.stringify([tab.name, 0]), label: tab.name === 'base' ? 'default' : 'inherit from smaller' },
		// 		{ value: JSON.stringify([tab.name, 'vh-'+prefix+'25-min']), label: tab.name+'-25' },
		// 		{ value: JSON.stringify([tab.name, 'vh-'+prefix+'33-min']), label: tab.name+'-33' },
		// 		{ value: JSON.stringify([tab.name, 'vh-'+prefix+'50-min']), label: tab.name+'-50' },
		// 		{ value: JSON.stringify([tab.name, 'vh-'+prefix+'66-min']), label: tab.name+'-66' },
		// 		{ value: JSON.stringify([tab.name, 'vh-'+prefix+'75-min']), label: tab.name+'-75' },
		// 		{ value: JSON.stringify([tab.name, 'vh-'+prefix+'100-min']), label: tab.name+'-100' },
		// 	]
		// };

		// const setBlockHeight = function(value) {
		// 	setResponsiveClasses(value, 'blockHeight','vh-', newCssClasses, attributes, props );
		// };

		// const setVAlign = function(value) {
		// 	setResponsiveClasses(value, 'vAlign','align-items-', newCssClasses, attributes, props );
		// };

		// const setContainerClasses = function( value ) {
		// 	props.setAttributes( { containerClasses: value } );
		// };


		const updateRepeaterItems = function(newArr){
			props.setAttributes({
				items : newArr
			});
			attributes = props.attributes;
		};

		const setItemAttr = function(value, focusItem, property){

			let newItemsArr = [...attributes.items];

			// update item
			newItemsArr[focusItem.key][property] = value;

			//
			// update block
			updateRepeaterItems(newItemsArr);
		};

		const setResponsiveAttr = function(value, propName){

			// Parse SelectControl JSON string value
			let responsiveCssObj = {...focusItem[propName]};
			let [screenSize,
				responsiveCssClass] = JSON.parse(value);

			// Update Item Attribute
			responsiveCssObj[screenSize] = responsiveCssClass;
			setItemAttr(responsiveCssObj, focusItem, propName);
		};

		const genVAlignOptions = function(tab){

			// let prefix = tab.name+'-';
			let prefix = tab.name === 'base' ? '' : tab.name+'-';

			return [
				{ value: JSON.stringify([tab.name, tab.name === 'base' ? 'align-items-center' : '']), label: tab.name === 'base' ? 'default' : 'inherit from smaller' },
				{ value: JSON.stringify([tab.name, 'align-items-'+prefix+'start']), label: 'Align Top' },
				{ value: JSON.stringify([tab.name, 'align-items-'+prefix+'center']), label: 'Align Middle' },
				{ value: JSON.stringify([tab.name, 'align-items-'+prefix+'end']), label: 'Align Bottom' },
			]
		};

		const genHAlignOptions = function(tab){

			// let prefix = tab.name+'-';
			let prefix = tab.name === 'base' ? '' : tab.name+'-';

			return [
				{ value: JSON.stringify([tab.name, tab.name === 'base' ? 'text-center' : '']), label: tab.name === 'base' ? 'default' : 'inherit from smaller' },
				{ value: JSON.stringify([tab.name, 'text-'+prefix+'left']), label: 'Align Left' },
				{ value: JSON.stringify([tab.name, 'text-'+prefix+'center']), label: 'Align Center' },
				{ value: JSON.stringify([tab.name, 'text-'+prefix+'right']), label: 'Align Right' },
			]
		};


		// Layout Settings
		//-----------------------------
		return el( 'div', { style: {
					padding: '0 10px 20px',
					background: '#f8f8f8'
				}},
			el('p',{},'Responsive Wrap Settings: ',
				el('br', {}),
				el('strong',{},
					`${ getTabScreenSize(tab.name) }`
				)
			),

			// Vertical Alignment
			//-----------------------------
			el( SelectControl, {
				key: 'multiple',
				label: __('vertical alignment'),
				value: JSON.stringify([tab.name, focusItem.vAlign[tab.name]]),
				className: ['mt-3'],
				onChange: function(value){
					setResponsiveAttr(value, 'vAlign');
				},
				options: genVAlignOptions(tab)
			}),

			// Horizontal Alignment
			//-----------------------------
			el( SelectControl, {
				key: 'multiple',
				label: __('horizontal alignment'),
				value: JSON.stringify([tab.name, focusItem.hAlign[tab.name]]),
				className: ['mt-3'],
				onChange: function(value){
					setResponsiveAttr(value, 'hAlign');
				},
				options: genHAlignOptions(tab)
			}),


			// Alignment Matrix
			// - potential alternative to above alignment SelectControls
			// - paused for now: currently no way of accounting for the "inherit from small" option
			//-----------------------------
			// el( AlignmentMatrixToolbar, {
			// 	value: null,//'center center',
			// 	label: __('Content alignment'),
			// 	onChange: function(value){
			// 		console.log('BlockAlignmentMatrixToolbar', value);
			// 		// setResponsiveItemClasses(value,'vAlign','align-items-', newCssItemClasses, attributes, props);
			// 	},
			// })

			// ~todo remove below comments once LayoutSettingsTab is built
			// Content Height
			//-----------------------------
			// el( SelectControl, {
			// 	key: 'multiple',
			// 	label: __('min-height'),
			// 	value: JSON.stringify([tab.name, attributes.blockHeight[tab.name]]),
			// 	className: ['mt-3'],
			// 	// onChange: setBlockHeight,
			// 	options: genBlockHeightOptions(tab)
			// }),
		);
	}
}

/**
 * Block Height Tab (old repeater style)
 * ------------------------------------
 * @prop props (block props)
 * @prop tab
 *
 * requires:
 * - ResponsiveTabs
 *
 * usage:
 *
 *
 * - todo: try to render via e(BlockLayoutSettingsTab, {allowedSettings['BlockHeight']})
 * 		- then substitute BlockHeightTab with e(BlockLayoutSettingsTab, {allowedSettings['BlockHeight']}) in block logic (usually sliders)
 * 		- eventually depreciate	BlockHeightTab
 *
 * ~ todo: see if Pre-Saving object is still required for the components (remove comment if not)
 */
class BlockHeightTab extends Component {

	init(props) {
		this.props = props.props;
	}

	render() {

		let self = this;
		let props = this.props;
		let attributes = this.props.attributes;
		let tab = props.tab;


		let focusItem =  typeof attributes.items !== 'undefined' ?
			attributes.items[ attributes.focusItem !== null ? attributes.focusItem : 0 ] : false;
		// console.log('BlockHeightTab | focusItem', focusItem);
		let newCssClasses 	= focusItem ? [...focusItem.cssClasses] : [...attributes.cssClasses];



		// Pre-Save Object type Attributes (due to bug in gutenberg)
		// - @ref: https://github.com/WordPress/gutenberg/issues/37967
		//-----------------------------
		// if(attributes.blockHeight === '') {
		// 	props.setAttributes( { blockHeight:{} } );
		// 	props.setAttributes( { blockHeight: JSON.parse(JSON.stringify(defaultScreenSizeObj)) } )
		// }


		// Block Height
		//-----------------------------
		const genBlockHeightOptions = function(tab){

			// let prefix = tab.name+'-';
			let prefix = tab.name === 'base' ? '' : tab.name+'-';

			return [
				{ value: JSON.stringify([tab.name, 0]), label: tab.name === 'base' ? 'default' : 'inherit from smaller' },
				{ value: JSON.stringify([tab.name, 'vh-'+prefix+'25-min']), label: tab.name+'-25' },
				{ value: JSON.stringify([tab.name, 'vh-'+prefix+'33-min']), label: tab.name+'-33' },
				{ value: JSON.stringify([tab.name, 'vh-'+prefix+'50-min']), label: tab.name+'-50' },
				{ value: JSON.stringify([tab.name, 'vh-'+prefix+'66-min']), label: tab.name+'-66' },
				{ value: JSON.stringify([tab.name, 'vh-'+prefix+'75-min']), label: tab.name+'-75' },
				{ value: JSON.stringify([tab.name, 'vh-'+prefix+'100-min']), label: tab.name+'-100' },
			]
		};

		const setBlockHeight = function(value) {

			// console.log('setBlockHeight', value);

			setResponsiveClasses(value, 'blockHeight','vh-', newCssClasses, attributes, props );
		};



		// Block Height Settings
		//-----------------------------
		return el( 'div', { style: {
					padding: '0 10px 20px',
					background: '#f8f8f8'
				}},
			el('p',{},'Responsive Wrap Settings: ',
				el('br', {}),
				el('strong',{},
					`${ getTabScreenSize(tab.name) }`
				)
			),

			// Content Height
			//-----------------------------
			el( SelectControl, {
				key: 'multiple',
				label: __('min-height'),
				value: JSON.stringify([tab.name, attributes.blockHeight[tab.name]]),
				className: ['mt-3'],
				onChange: setBlockHeight,
				options: genBlockHeightOptions(tab)
			}),
		)
	}
}

/**
 * Layout Settings Tab (core component)
 *
 * - Generates options for: blockHeight, vAlign, & hAlign
 * - onUpdateLayout: for custom logic from block-level
 *
 * - extended by: BlockLayoutSettingsTab
 * - todo: check if states work (specifically: blockHeight)
 * ------------------------------------
 */
class LayoutSettingsTab extends Component {

	//todo: check if states work (specifically: blockHeight); they might be getting overridden when component is extend
	init(props) {
		this.props = props.props;
		this.state = {
			blockHeight: attributes.blockHeight,
			vAlign: attributes.vAlign,
			hAlign: attributes.hAlign,
		};
	}

	genBlockHeightOptions(tab){

		// let prefix = tab.name+'-';
		let prefix = tab.name === 'base' ? '' : tab.name+'-';

		return [
			{ value: JSON.stringify([tab.name, 0]), label: tab.name === 'base' ? 'default' : 'inherit from smaller' },
			{ value: JSON.stringify([tab.name, 'vh-'+prefix+'25-min']), label: tab.name+'-25' },
			{ value: JSON.stringify([tab.name, 'vh-'+prefix+'33-min']), label: tab.name+'-33' },
			{ value: JSON.stringify([tab.name, 'vh-'+prefix+'50-min']), label: tab.name+'-50' },
			{ value: JSON.stringify([tab.name, 'vh-'+prefix+'66-min']), label: tab.name+'-66' },
			{ value: JSON.stringify([tab.name, 'vh-'+prefix+'75-min']), label: tab.name+'-75' },
			{ value: JSON.stringify([tab.name, 'vh-'+prefix+'80-min']), label: tab.name+'-80' },
			{ value: JSON.stringify([tab.name, 'vh-'+prefix+'85-min']), label: tab.name+'-85' },
			{ value: JSON.stringify([tab.name, 'vh-'+prefix+'90-min']), label: tab.name+'-90' },
			{ value: JSON.stringify([tab.name, 'vh-'+prefix+'100-min']), label: tab.name+'-100' },
		]
	}

	genVAlignOptions(tab) {

		// let prefix = tab.name+'-';
		let prefix = tab.name === 'base' ? '' : tab.name + '-';

		return [
			{
				value: JSON.stringify([tab.name, tab.name === 'base' ? 'align-items-center' : '']),
				label: tab.name === 'base' ? 'default' : 'inherit from smaller'
			},
			{value: JSON.stringify([tab.name, 'align-items-' + prefix + 'start']), label: 'Align Top'},
			{value: JSON.stringify([tab.name, 'align-items-' + prefix + 'center']), label: 'Align Middle'},
			{value: JSON.stringify([tab.name, 'align-items-' + prefix + 'end']), label: 'Align Bottom'},
		]
	}

	genHAlignOptions(tab){

		// let prefix = tab.name+'-';
		let prefix = tab.name === 'base' ? '' : tab.name+'-';

		return [
			{ value: JSON.stringify([tab.name, tab.name === 'base' ? 'text-center' : '']), label: tab.name === 'base' ? 'default' : 'inherit from smaller' },
			{ value: JSON.stringify([tab.name, 'text-'+prefix+'left']), label: 'Align Left' },
			{ value: JSON.stringify([tab.name, 'text-'+prefix+'center']), label: 'Align Center' },
			{ value: JSON.stringify([tab.name, 'text-'+prefix+'right']), label: 'Align Right' },
		]
	}

	onUpdateLayout(data, attrStr){
		let props = this.props;
		if (__exists(props.onUpdateLayout))
			props.onUpdateLayout(data, attrStr);
	}
}

/**
 * Block Layout Settings Tabs
 *
 * TODO: updateLayoutAttribute() works but DOES NOT MAKE ANY SENSE
 * - todo: allowedSettings
 * ------------------------------------
 */
class BlockLayoutSettingsTab extends LayoutSettingsTab {

	init(props) {
		this.props = props.props;
	}

	// TODO: updateLayoutAttribute() works but DOES NOT MAKE ANY SENSE
	updateLayoutAttribute(data, attrStr, attributes) {

		let self = this;
		let props = this.props;
		let attribute = attributes[attrStr];
		let classArr = [...attributes.cssClasses];
		let updatedAttr = attribute !== '' ? attribute : {};
		let updateVal = JSON.parse(data);
		updatedAttr[updateVal[0]] = updateVal[1] ? updateVal[1] : false;

		// console.log('updateLayoutAttribute', attrStr, classArr, updatedAttr);

		// TODO WTF why does this fix the problem (while below does nothing at all)?
		// is it because updating the cssClasses attribute, even with the same value, forces the editor content to update?
		// - does that mean
		props.setAttributes( { cssClasses: classArr } );
		//
		// TODO WTF ...does nothing at all?
		// - while above "'updatedAttr[updateVal[0]] =..." line seems to be enough to ste the attribute?
		props.setAttributes( { [attrStr]: updatedAttr } );
		//
		//
		self.onUpdateLayout(data, attrStr);


		attributes = props.attributes;


		// // Update Item Attribute
		// this.onUpdateLayout(attrStr, updatedAttr);
	}

	render() {

		let self = this;
		let props = this.props;
		let attributes = this.props.attributes;
		let tab = props.tab;
		let blockHeight = __exists(attributes.blockHeight);
		let vAlign = __exists(attributes.vAlign);
		let hAlign = __exists(attributes.hAlign);

		// Layout Settings
		//-----------------------------
		return el( 'div', { style: {
					padding: '0 10px 20px',
					background: '#f8f8f8'
				}},
			el('p',{},'Responsive Wrap Settings: ',
				el('br', {}),
				el('strong',{},
					// getTabScreenSize(tab.name), // todo: try this (don't remember why the `${}` is needed)
					`${ getTabScreenSize(tab.name) }`
				)
			),


			// Content Height
			//-----------------------------
			blockHeight ?
			el( SelectControl, {
				key: 'multiple',
				className: ['mt-3'],

				label: __('min-height'),
				value: JSON.stringify([tab.name, attributes.blockHeight[tab.name]]),
				onChange: function(value){
					self.updateLayoutAttribute(value, 'blockHeight', attributes);
				},
				options: self.genBlockHeightOptions(tab)
			}): el('div'),


			// Vertical Alignment
			//-----------------------------
			vAlign ?
				el( SelectControl, {
				key: 'multiple',
				className: ['mt-3'],

				label: __('vertical alignment'),
				value: JSON.stringify([tab.name, attributes.vAlign[tab.name]]),
				onChange: function(value){
					self.updateLayoutAttribute(value, 'vAlign', attributes);
				},
				options: self.genVAlignOptions(tab)
			}): el('div'),


			// Horizontal Alignment
			//-----------------------------
			hAlign ?
				el( SelectControl, {
				key: 'multiple',
				className: ['mt-3'],

				label: __('horizontal alignment'),
				value: JSON.stringify([tab.name, attributes.hAlign[tab.name]]),
				onChange: function(value){
					self.updateLayoutAttribute(value, 'hAlign', attributes);
				},
				options: self.genHAlignOptions(tab)
			}): el('div'),

			props.children

			// Alignment Matrix
			// - potential alternative to above alignment SelectControls
			// - paused for now: currently no way of accounting for the "inherit from small" option
			//-----------------------------
			// el( AlignmentMatrixToolbar, {
			// 	value: null,//'center center',
			// 	label: __('Content alignment'),
			// 	onChange: function(value){
			// 		console.log('BlockAlignmentMatrixToolbar', value);
			// 		// setResponsiveItemClasses(value,'vAlign','align-items-', newCssItemClasses, attributes, props);
			// 	},
			// })
		);
	}
}

/**
 * Grid Settings tab
 *
 * - Generates options for: grid, tileGap, & gridAlign
 * - onUpdateLayout: for custom logic from block-level
 * ------------------------------------
 */
class GridSettingsTab extends Component {

	init(props) {
		this.props = props.props;
	}

	onUpdateLayout(data, attrStr){
		let props = this.props;
		if (__exists(props.onUpdateLayout))
			props.onUpdateLayout(data, attrStr);
	}

	genGridOptions(tab){

		// let prefix = tab.name+'-';
		let prefix = tab.name === 'base' ? '' : tab.name+'-';

		return [
			{ value: JSON.stringify([tab.name, 0]), label: tab.name === 'base' ? 'default' : 'inherit from smaller' },
			{ value: JSON.stringify([tab.name, 'width-'+prefix+'1']), label: tab.name+'-1' },
			{ value: JSON.stringify([tab.name, 'width-'+prefix+'2']), label: tab.name+'-2' },
			{ value: JSON.stringify([tab.name, 'width-'+prefix+'3']), label: tab.name+'-3' },
			{ value: JSON.stringify([tab.name, 'width-'+prefix+'4']), label: tab.name+'-4' },
			{ value: JSON.stringify([tab.name, 'width-'+prefix+'5']), label: tab.name+'-5' },
			{ value: JSON.stringify([tab.name, 'width-'+prefix+'6']), label: tab.name+'-6' },
			{ value: JSON.stringify([tab.name, 'width-'+prefix+'7']), label: tab.name+'-7' },
			{ value: JSON.stringify([tab.name, 'width-'+prefix+'8']), label: tab.name+'-8' },
			{ value: JSON.stringify([tab.name, 'width-'+prefix+'9']), label: tab.name+'-9' },
			// { value: JSON.stringify([tab.name, 'width-'+prefix+'10']), label: tab.name+'-10' },
			// { value: JSON.stringify([tab.name, 'width-'+prefix+'11']), label: tab.name+'-11' },
			// { value: JSON.stringify([tab.name, 'width-'+prefix+'12']), label: tab.name+'-12' },
		]
	}

	genGapOptions(tab){

		// let prefix = tab.name+'-';
		let prefix = tab.name === 'base' ? '' : tab.name+'-';

		return [
			{ value: JSON.stringify([tab.name, 0]), label: tab.name === 'base' ? 'default' : 'inherit from smaller' },
			{ value: JSON.stringify([tab.name, '1']), label: tab.name+'-1' },
			{ value: JSON.stringify([tab.name, '2']), label: tab.name+'-2' },
			{ value: JSON.stringify([tab.name, '3']), label: tab.name+'-3' },
			{ value: JSON.stringify([tab.name, '4']), label: tab.name+'-4' },
			{ value: JSON.stringify([tab.name, '5']), label: tab.name+'-5' },
			// { value: JSON.stringify([tab.name, 'width-'+prefix+'10']), label: tab.name+'-10' },
			// { value: JSON.stringify([tab.name, 'width-'+prefix+'11']), label: tab.name+'-11' },
			// { value: JSON.stringify([tab.name, 'width-'+prefix+'12']), label: tab.name+'-12' },
		]
	}

	genGridAlignOptions(tab){

		// let prefix = tab.name+'-';
		let prefix = tab.name === 'base' ? '' : tab.name+'-';

		return [
			{ value: JSON.stringify([tab.name, 0]), label: tab.name === 'base' ? 'default' : 'inherit from smaller' },
			{ value: JSON.stringify([tab.name, 'justify-content-'+prefix+'start']), label: tab.name+'-left' },
			{ value: JSON.stringify([tab.name, 'justify-content-'+prefix+'center']), label: tab.name+'-center' },
			{ value: JSON.stringify([tab.name, 'justify-content-'+prefix+'end']), label: tab.name+'-right' },
			// { value: JSON.stringify([tab.name, 'width-'+prefix+'10']), label: tab.name+'-10' },
			// { value: JSON.stringify([tab.name, 'width-'+prefix+'11']), label: tab.name+'-11' },
			// { value: JSON.stringify([tab.name, 'width-'+prefix+'12']), label: tab.name+'-12' },
		]
	}


	// TODO: updateLayoutAttribute() works but DOES NOT MAKE ANY SENSE
	updateLayoutAttribute(data, attrStr, attributes) {

		let self = this;
		let props = this.props;
		let attribute = attributes[attrStr];
		let classArr = [...attributes.cssClasses];
		let updatedAttr = attribute !== '' ? attribute : {};
		let updateVal = JSON.parse(data);
		updatedAttr[updateVal[0]] = updateVal[1] ? updateVal[1] : false;



		// TODO WTF why does this fix the problem (while below does nothing at all)?
		// is it because updating the cssClasses attribute, even with the same value, forces the editor content to update?
		// - does that mean
		props.setAttributes({cssClasses: classArr});

		// TODO WTF ...does nothing at all?
		// - while above "'updatedAttr[updateVal[0]] =..." line seems to be enough to set the attribute?
		props.setAttributes({[attrStr]: updatedAttr});

		self.onUpdateLayout(data, attrStr);


		attributes = props.attributes;
		console.log('[updateLayoutAttribute]', updateVal, attributes[attrStr]);


		// // Update Item Attribute
		// this.onUpdateLayout(attrStr, updatedAttr);
	}

	render() {

		let self = this;
		let props = this.props;
		let attributes = this.props.attributes;
		let tab = props.tab;
		let allowedGridConfigs = typeof attributes.allowedGridConfigs !== 'undefined' ?
			attributes.allowedGridConfigs : ['tile-width', 'tile-gap'];


		// Layout Settings
		//-----------------------------
		return el('div', {
				style: {
					padding: '0 10px 20px',
					background: '#f8f8f8'
				}
			},
			el('p', {}, 'Responsive Wrap Settings: ',
				el('br', {}),
				el('strong', {},
					// getTabScreenSize(tab.name), // todo: try this (don't remember why the `${}` is needed)
					`${getTabScreenSize(tab.name)}`
				)
			),

			// Tile Width
			//-----------------------------
			allowedGridConfigs.includes('tile-width') ? el(SelectControl, {
				key: 'multiple',
				className: ['mt-3'],

				label: __('Tile Width'),
				value: JSON.stringify([tab.name, attributes.grid[tab.name]]),
				onChange: function (value) {
					self.updateLayoutAttribute(value, 'grid', attributes);
				},
				options: self.genGridOptions(tab)
			}) : null,

			// Tile Gap
			//-----------------------------
			allowedGridConfigs.includes('tile-gap') ? el(SelectControl, {
				key: 'multiple',
				className: ['mt-3'],

				label: __('Tile Gap'),
				value: JSON.stringify([tab.name, attributes.tileGap[tab.name]]),
				onChange: function (value) {
					self.updateLayoutAttribute(value, 'tileGap', attributes);
				},
				options: self.genGapOptions(tab)
			}) : null,

			// Grid Align
			//-----------------------------
			allowedGridConfigs.includes('grid-align') ? el(SelectControl, {
				key: 'multiple',
				className: ['mt-3'],

				label: __('Align Grid'),
				value: JSON.stringify([tab.name, attributes.gridAlign[tab.name]]),
				onChange: function (value) {
					self.updateLayoutAttribute(value, 'gridAlign', attributes);
				},
				options: self.genGridAlignOptions(tab)
			}) : null,
		);
	}
}




/* Background Effects */
/*-------------------------------*/

/**
 * CWB BG Effect
 * --------------------------
 * @prop bgOverlay (string)
 * @prop backdropFilter (string)
 *
 * Basically CwbSlideTint on steroids
 *
 * Usage:
 * - Use in the front-end Slider markup & back-end slider editor
 */
class CwbBgEffect extends Component {

	init(props) {
		this.props = props.props;
	}

	render() {

		let props = this.props;
		let bgOverlay = __get(props.bgOverlay, false);
		let backdropFilter = __get(props.backdropFilter, false);

		return el( 'div' , {
			style : {
				background: bgOverlay ? bgOverlay : null,
				backdropFilter: backdropFilter ? backdropFilter : null,
				top: 0,
				left: 0,
				width: '100%',
				height: '100%',
				position: 'absolute'
			}
		})
	}
}




/* REPEATER Components (& functions) */
// todo depreciate?
//  - replaced by: 'cwb/repeater'
// 	- used in:
//		- 'cwb/repeater-block' ... and alike
//		- StudioV custom Blocks
//		- DP&O: home, team
//		- MA: contributors, one welcome sub-page, quiz
//		- bezwurx: CaseStudies
//		- 'cwb/accordion-block'
//		- 'cwb/testimonial-grid'
//		- 'cwb/content-grid'
//		- 'cwb/content-quiz'
// 	- [the ISSUE]: default items from new repeater blocks do not save properly
//		- [workaround]: creat new items manually and delete default ones
// 	- [possible FIX]: not default items
/*-------------------------------*/

// - repeater: Dev Utils
const updateItemModel = function (item, model) {

	let newItem = {...item};

	// Update BtnObj model
	if(typeof item.btnObj !== 'undefined'
		&& typeof model.btnObj !== 'undefined'){

		if( Object.keys(model.btnObj).length < Object.keys(item.btnObj).length){
			let btnObjKeys = Object.keys(item.btnObj).sort();
			let modelBtnObjKeys = Object.keys(model.btnObj).sort();

			for(let y = 0; y < btnObjKeys.length; y++){
				if( !modelBtnObjKeys.includes(btnObjKeys[y]) ){
					delete model.btnObj[btnObjKeys[y]];
				}
			}
		}
		else {
			item.btnObj = {...model.btnObj, ...item.btnObj}; // slide btnObj should overwrite model values
		}
	}

	// Update Slide Obj model
	if (Object.keys(model).length < Object.keys(item).length){
		let itemKeys = Object.keys(item).sort();
		let modelKeys = Object.keys(model).sort();

		for(let x = 0; x < itemKeys.length; x++){
			if( !modelKeys.includes(itemKeys[x]) ){
				delete newItem[itemKeys[x]];
			}
		}
	}
	else {

		newItem = {...model, ...item}; // slide should overwrite model values
	}
	return newItem;
};

/**
 * Fix Focus Item
 *
 * @param focusItem
 * @param attributes
 * @returns {*}
 *
 * Fixes repeater bug where
 * - using the delete function from the item toolbar (not the inspector),
 * - caused the focusItem block attribute to remain the key of deleted item
 *
 * - this was only really detectable while focused on the last item in the array
 *
 * used on the BLOCK level rather then the component level
 */
const fixFocusItem = function (focusItem, attributes){
	if(typeof focusItem === 'undefined'){

		let lastItemIdx = attributes.items.length - 1;

		focusItem = attributes.items[lastItemIdx];
	}
	return focusItem;
};

/**
 * Repeater Component
 * --------------------------
 * The base extended by all other repeater components
 *
 * Required Block Attributes:
 * - items: array
 * 		- item: object
 * 			- key: string (int)
 * 			- itemText: string
 * - focusItem: string
 */
class RepeaterComponent extends Component {

	state = {
		focusItemKey: 0
	};

	init(props) {
		this.props = props.props;
	}

	updateRepeater(newArr, focusItemKey) {

		let props = this.props;
		let blockProps = typeof props.blockProps !== 'undefined' ? props.blockProps : props;
		let attributes = typeof props.blockProps !== 'undefined' ? props.blockProps.attributes : props.attributes;

		// reset item keys
		for(let i = 0; i < newArr.length; i++){
			newArr[i].key = i;
		}
		// update block
		blockProps.setAttributes({
			items : newArr,
			// focusItem : focusItemKey.toString()
		});
		attributes = blockProps.attributes;

		this.updateFocus(focusItemKey.toString())
	}

	addItem() {

		let self = this;
		let props = this.props;
		let attributes = typeof props.blockProps !== 'undefined' ? props.blockProps.attributes : props.attributes;
		let newArr = [...attributes.items];
		let newKey = newArr.length;

		let itemObjModel = props.itemObjModel;
		let defaultNewObj = {key: newKey, itemText: 'item ' + (newKey+1)};
		let itemObj = typeof itemObjModel !== 'undefined' ?
			{...itemObjModel,...defaultNewObj} : defaultNewObj;

		console.log('addItem', itemObj);

		// add item
		newArr.push(itemObj);
		self.updateRepeater(newArr, newArr.length-1);
	}

	removeItem(focusItemKey) {

		let self = this;
		let props = this.props;
		let attributes = typeof props.blockProps !== 'undefined' ? props.blockProps.attributes : props.attributes;
		let newArr = [...attributes.items];

		newArr.splice(focusItemKey,1);
		self.updateRepeater(newArr, '0');
	}

	moveUp(focusItemKey){

		let self = this;
		let props = this.props;
		let attributes = typeof props.blockProps !== 'undefined' ? props.blockProps.attributes : props.attributes;
		let newArr = [...attributes.items];

		focusItemKey = parseInt(focusItemKey);
		let pulled = newArr.splice(focusItemKey, 1);
		let targetIndex = focusItemKey - 1 >= 0 ? focusItemKey - 1 : 0;


		newArr.splice(targetIndex, 0, pulled[0]);
		self.updateRepeater(newArr, targetIndex);
	};

	moveDown(focusItemKey){

		let self = this;
		let props = this.props;
		let attributes = typeof props.blockProps !== 'undefined' ? props.blockProps.attributes : props.attributes;
		let newArr = [...attributes.items];

		focusItemKey = parseInt(focusItemKey);
		let pulled = newArr.splice(focusItemKey, 1);
		let targetIndex = focusItemKey + 1 <= newArr.length ? focusItemKey + 1 : newArr.length;


		newArr.splice(targetIndex, 0, pulled[0]);
		self.updateRepeater(newArr, targetIndex);
	};

	updateFocus(item){
		let self = this;
		let props = this.props;
		let blockProps = typeof props.blockProps !== 'undefined' ? props.blockProps : props;
		let attributes = typeof props.blockProps !== 'undefined' ? props.blockProps.attributes : props.attributes;

		blockProps.setAttributes({
			focusItem : item//.key.toString()
		});
		attributes = blockProps.attributes;

		// // todo: trying to sync block state focusItemState with RepeaterComponent state focusItemKey
		// // ...not quite there yet...
		// this.setState({
		// 	focusItemKey : item
		// }, function(){
		// 	console.log('RepeaterComponent', 'focusItemKey:', self.state.focusItemKey, 'focusItemState:', props.focusItemState, 'focusItem:', attributes.focusItem);
		// });
	};
}

/**
 * Repeater Toolbar
 * --------------------------
 * @prop props
 *
 * Remove or Re-Order repeater Item
 * Should be used inside an item element, when that item is in focus,
 * 	either in the block editor or the inspector controls
 *
 * usage:
 * - Can be used inside a block editor OR inspector panel for a Repeater Item
 *
 * Required Block Attributes:
 * - items: array
 * 		- item: object
 * 			- key: string (int)
 * 			- itemText: string
 * - focusItem: string
 */
class RepeaterToolbar extends RepeaterComponent {

	init(props) {
		this.props = props.props;
	}

	render() {
		let self = this;
		let props = this.props;
		let attributes = typeof props.blockProps !== 'undefined' ? props.blockProps.attributes : props.attributes;
		let focusItemKey = typeof attributes !== 'undefined' ? attributes.focusItem : 0;
		let hideUiMove = typeof props.hideUiMove === 'boolean' ? props.hideUiMove : false;
		// let attributes = typeof props.blockProps === 'boolean' ? props.blockProps : true;


		return el( Toolbar,{
				style: {
					padding: 0,
					border: '1px solid #bbb',
				}
			},
			!hideUiMove ? el( ToolbarButton,{
					onClick: function(){
						self.moveUp(focusItemKey);
					}
				}, el( Dashicon, { icon : 'arrow-up-alt2'} ),
			) : null,
			!hideUiMove ? el( ToolbarButton,{
					onClick: function(){
						self.moveDown(focusItemKey);
					}
				}, el( Dashicon, { icon : 'arrow-down-alt2'} ),
			) : null,
			el( ToolbarButton,{
					onClick	: function(){
						self.removeItem(focusItemKey);
					}
				}, el( Dashicon, { icon : 'trash'} )
			)
		);
	}
}

/**
 * Repeater Add Item UI
 * --------------------------
 * @prop props (block props: used by RepeaterComponent function)
 *
 * usage:
 * - To be used inside a block editor for a Repeater Item
 *
 * Required Block Attributes:
 * - items: array
 * 		- item: object
 * - focusItem: string (int)
 */
class RepeaterAddItemUI extends RepeaterComponent {

	init(props) {
		this.props = props.props;
	}

	render(){
		let self = this;
		// let props = this.props;
		// let attributes = props.attributes;

		return el( 'div', {
				style: {padding:'0 0 8px 10px'}},
			el( components.Button, {
				className: 'button button-small cursor-pointer',
				onClick: function(){
					self.addItem()
				}
			}, 'Add Item' )
		)
	}
}

/**
 * Repeater Controls
 * --------------------------
 * (Inspector Controls Panel)
 * Add, Remove, Navigate or Re-Order repeater Items
 * Uses only the first two child components - one before the Toolbar, one after
 *  this is intended to keep the inspector RepeaterToolbar instantiation in one place
 *  (inside RepeaterControls) while allowing for inspector layout customization
 *
 * Required Block Attributes:
 * - items: array
 * 		- item: object
 * 			- key: string (int)
 * 			- itemText: string
 * - focusItem: string
 */
class RepeaterControls extends RepeaterComponent {

	init(props) {
		this.props = props.props;
	}

	render() {
		let self = this;
		let props = this.props;
		let attributes = typeof props.blockProps !== 'undefined' ? props.blockProps.attributes : props.attributes;
		let hideUiMove = typeof props.hideUiMove === 'boolean' ? props.hideUiMove : false;

		// Selector Options
		//------------------
		let ops = [];
		attributes.items.map((item) => {
			ops.push({
				value : item.key,
				label : 'item '+item.key+': '+item.itemText.replace(/<[^>]+>/g, '')
			});
		});


		// RepeaterControls
		//------------------
		return el('div', props,
			el('div', {className: 'repeater-controls'},


					el( 'div', {
						style: {
							padding: '0 20px',
							display: 'flex',
							alignItems: 'end'
						}
					},

						// RepeaterControls : Item Selector
						//------------------------------------------------------
						// ops.length ?
						el( 'div', {className: ['w-100'],},
							el( SelectControl, {
								key: 'multiple',
								label: __('items:'),
								value: attributes.focusItem,
								className: ['mt-1'],
								onChange: function(value){
									self.updateFocus(value);
								},
								options: ops
							}),
						),
						// ): null,


						// RepeaterControls : Add Item Btn
						//------------------------------------------------------
						el( 'div', {
							style: {padding:'0 0 8px 10px'}},
							el( components.Button, {
								className: 'button button-small cursor-pointer',
								onClick: function(){
									self.addItem()
								}
							}, 'Add Item' )
						)
				),




				attributes.focusItem ? el( 'div', {
						style: {
							position: 'relative'
						}
					},
					el( 'div', {
							style: {
								marginTop: '20px',
								padding: '10px 0 20px',
								borderTop: '1px solid #ddd',
								borderBottom: '1px solid #ddd',
								background: '#FAFAFA'
							}
						},


						// RepeaterControls : RepeaterToolbar
						//------------------------------------------------------
						el( 'div', {
								style: {
									padding: '10px 20px'
								}
							},
							el(RepeaterToolbar, {hideUiMove: hideUiMove,...props})
						),

						// props.children,
						// todo: trying to sync block state focusItemState with RepeaterComponent state focusItemKey
						// props.children(this.state.focusItemKey),
						props.children,

						// // RepeaterControls : first child props.children[0]
						// //------------------------------------------------------
						// Array.isArray(props.children) ? props.children[0](this.state.focusItemState) : props.children(this.state.focusItemState),
						//
						//
						// // RepeaterControls : second child props.children[1]
						// //------------------------------------------------------
						// Array.isArray(props.children) ? props.children[1](this.state.focusItemState) : null,
					)
				) : null,
			)
		);
	}
}

/**
 * Repeater Button Component
 * --------------------------
 * The base extended by all other repeater button components
 *
 * Required Block Attributes:
 * - items: array
 * 		- item: object
 */
class RepeaterBtnComponent extends Component {

	state = {
		btnPopover: false,
		btnDisabled: false
	};

	init(props) {
		this.props = props.props;
	}

	showBtnPopover(){
		// let self = this;
		let state = this.state;
		if(!state.btnDisabled)
			this.setState({ btnPopover: true }, () => {
				// console.log('showBtnPopover');
			});
	};

	hideBtnPopover(){
		let self = this;
		this.setState({ btnDisabled: true });
		this.setState({ btnPopover: false });
		setTimeout(function(){
			self.setState({ btnDisabled: false });
		}, 200);
	};

	onClick(value) {
		let props = this.props;
		props.onClick(value); }

	setItemBtnAtts(value, focusItem, btnProperty, btnObjName){

		if(typeof btnObjName === 'undefined') btnObjName = 'btnObj';
		let props = this.props.blockProps;
		let attributes = props.attributes;
		let newArr = [...attributes.items];

		// update item

		// TODO: for some reason this updates the button properties of
		//  ALL items in a repeater, not just the focus item. This only happens when
		//  a repeater block is first created, before it gets saved. After
		//  updating then refreshing the editor page, everything works as expected
		//
		// if(btnProperty === 'text'){
		// 	console.log('updateText1',
		// 		// attributes,
		// 		focusItem.itemText, focusItem.key,
		// 		newArr[0][btnObjName].text,
		// 		newArr[1][btnObjName].text,
		// 		newArr[2][btnObjName].text,
		// 		// newArr[focusItem.key][btnObjName][btnProperty]
		// 	);
		// }
		newArr[focusItem.key][btnObjName][btnProperty] = value;
		// todo NOTE: tried below and it also resulted in every button changing
		// newArr[0][btnObjName][btnProperty] = value;
		//
		// if(btnProperty === 'text'){
		// 	console.log('updateText2',
		// 		// attributes,
		// 		focusItem.itemText, focusItem.key,
		// 		newArr[0][btnObjName].text,
		// 		newArr[1][btnObjName].text,
		// 		newArr[2][btnObjName].text,
		// 		// // newArr[focusItem.key][btnObjName][btnProperty]
		// 	);
		// }

		//
		// update block
		props.setAttributes({
			items : newArr
		});
		attributes = props.attributes;


		// if(btnProperty === 'btnType')
		// 	console.log('setItemBtnAtts', value, focusItem[btnObjName], btnProperty, btnObjName, attributes.items[focusItem.key][btnObjName].btnType);
	};

	// render() {
	//
	// 	let self = this;
	// 	let props = this.props;
	// 	let attributes = this.props.attributes;
	// 	// let newCssClasses 	= [...attributes.cssClasses];
	//
	//
	// 	return 'test'
	// }
}

/**
 * Repeater Button
 * --------------------------
 * @prop props (block props)
 * @prop item
 *
 * usage:
 * - To be used inside a block editor for a Repeater Item
 *
 * Required Block Attributes:
 * - items: array
 * 		- item: object
 * - focusItem: string (int)
 */
class RepeaterBtn extends RepeaterBtnComponent {

	init(props) {
		this.props = props.props;
	}

	onClick(value) {
		let props = this.props;
		if(typeof this.props.onClick !== 'undefined')
			props.onClick(value);
	}

	render() {

		let self = this;
		let props = this.props.blockProps;
		let item = this.props.item;
		let btnObjName = typeof this.props.btnObjName !== 'undefined' ? this.props.btnObjName : 'btnObj';
		let btnType = typeof item[btnObjName].btnType !== 'undefined' ? 'btn-'+item[btnObjName].btnType : 'btn-primary';
		let btnSize = typeof item[btnObjName].btnSize !== 'undefined' ? 'btn-'+item[btnObjName].btnSize : '';
		let buttonText = item[btnObjName].text;
		let className = [
			'd-inline-block',
			'position-relative',
			'text-center',
		];

		// console.log(btnType);
		if(btnType !== 'btn-text'){
			className.push('btn');
			className.push(btnType);
			className.push(btnSize);
			className.push('m-1');
		}


		// let attributes = props.attributes;
		// let focusItem = attributes.items[ attributes.focusItem !== null ? attributes.focusItem : 0 ];
		// console.log('RepeaterBtn : focusItem', attributes.focusItem, props);


		// console.log(btnType, btnType !== 'btn-text' ? 'button' : 'a');

		// The Repeater Button
		//--------------------------------------
		return el( btnType !== 'btn-text' ? 'button' : 'a', {
				className: className.join(' '),
				['type']: 'button',
				onClick: function(){
					self.showBtnPopover();
					self.onClick()
				},
			},

			// Item Button Text
			//--------------------------------------
			buttonText,

			// Item Button Editor (POPOVER)
			//--------------------------------------
			self.state.btnPopover ?

				el(Popover,{
						position: 'top left',
						className: 'popover-plus btn-editor',
						onFocusOutside : function(){ self.hideBtnPopover() }
					},
					el('div',{ className:'content-wrap' },

						// Close Popover Button
						//-----------------------------
						el(components.Button,{
								className : 'close-btn',
								onClick: function(e){
									e.stopPropagation();
									self.hideBtnPopover()
								},
							},
							el( Icon, { icon: 'no-alt' }),
						),

						// Button Editor
						//-----------------------------
						el('div', {},
							el(RepeaterBtnEditor, {
								blockProps: props,
								item: item,
								btnObjName : btnObjName
							})
						)
					)
				)
				: null,


			// el( TextControl,{
			// 	value: focusItem.btnObj.text,
			// 	onChange: function(value){
			// 		// updateText(value, item);
			// 	},
			// 	// onFocus: function(){
			// 	// 	updateFocus(item.key.toString());
			// 	// }
			// }),
		)
	}
}

/**
 * Repeater Button Editor
 * --------------------------
 * @prop props (block props)
 *
 * usage:
 * - To be used in the inspector or inside a popover
 *
 * Required Block Attributes:
 * - items: array
 * 		- item: object
 * - focusItem: string (int)
 */
class RepeaterBtnEditor extends RepeaterBtnComponent {

	init(props) {
		// this.props = props.props.blockProps;
		this.props = props.props;
	}

	render() {

		let self = this;
		let item = this.props.item;
		let props = this.props.blockProps;
		let attributes = props.attributes;
		let btnObjName = typeof this.props.btnObjName !== 'undefined' ? this.props.btnObjName : 'btnObj';


		// console.log(props);

		// console.log('RepeaterBtnEditor',
		// 	btnObjName,
		// 	item.itemText,
		// 	item[btnObjName]
		// );


		return el( 'div', {//PanelBody, {
				// title: 'Button Meta',
				// className: cwbGlobals.innerPanelClasses,
				// initialOpen: !0//false
			},
			el(CwbButtonEditor,{
				isPopup : true,
				btnObj : {
					btnType		 	: item[btnObjName].btnType,
					btnFunction 	: item[btnObjName].btnFunction,
					text			: item[btnObjName].text,
					url 			: item[btnObjName].url,
					newTab 			: item[btnObjName].newTab,
					target			: item[btnObjName].target,
					dataToggle		: item[btnObjName].dataToggle,
					dataShortcode	: item[btnObjName].dataShortcode,
					dataModalSize	: item[btnObjName].dataModalSize,
					dataModalHeading: item[btnObjName].dataModalHeading,

					dataSrc: item.btnObj.dataSrc, // todo remember to update repeater-btn objects
					postObj: item.btnObj.postObj
				},
				// allowedBtnTypes : ['link'],

				setButtonFunction : function(value){
					self.setItemBtnAtts(value, item, 'btnFunction', btnObjName);
					attributes = props.attributes
				},
				updateText : function(value){

					// console.log('updateText',
					// 	// attributes,
					// 	item.itemText,
					// 	item.key,
					// 	props.attributes.items,
					// 	// item[btnObjName]
					// );
					self.setItemBtnAtts(value, item, 'text', btnObjName);
					attributes = props.attributes;
				},
				updateUrl : function(value){
					self.setItemBtnAtts(value, item, 'url', btnObjName);
					attributes = props.attributes
				},
				updateNewTab : function(value){
					let canUseNewTab = item.btnObj.btnFunction === 'link' || item.btnObj.btnFunction === 'wp_link';

					self.setItemBtnAtts(canUseNewTab ? value : false,  item, 'newTab', btnObjName);
					attributes = props.attributes
				},
				updateDataToggle : function(value){
					let isModalTrigger = item.btnObj.btnFunction === 'iframe'
						|| item.btnObj.btnFunction === 'modal_trigger'
						|| item.btnObj.btnFunction === 'shortcode_modal_trigger';
					self.setItemBtnAtts(isModalTrigger ? 'modal' : null, item, 'dataToggle', btnObjName);
					attributes = props.attributes
				},
				updateDataTarget : function(value){
					let isModalTrigger = item.btnObj.btnFunction === 'modal_trigger';
					self.setItemBtnAtts(isModalTrigger ? value : null, item, 'target', btnObjName);
					attributes = props.attributes
				},


				updateDataSrc : function(value){
					self.setItemBtnAtts(value, item, 'dataSrc', btnObjName);
					attributes = props.attributes
				},
				updateShortcode : function(value){
					self.setItemBtnAtts(value, item, 'dataShortcode', btnObjName);
					attributes = props.attributes
				},
				updateModalSize : function(value){
					self.setItemBtnAtts(value, item, 'dataModalSize', btnObjName);
					attributes = props.attributes
				},
				updateModalHeading : function(value){
					self.setItemBtnAtts(value, item, 'dataModalHeading', btnObjName);
					attributes = props.attributes
				},
				// updatePostObj : function(value){
				// 	self.setItemBtnAtts(value, item, 'postObj', btnObjName);
				// 	attributes = props.attributes
				// },


				updateBtnType : function(value){
					self.setItemBtnAtts(value, item, 'btnType', btnObjName);
					attributes = props.attributes;


				},
				updateBtnSize : function(value){
					self.setItemBtnAtts(value, item, 'btnSize', btnObjName);
					attributes = props.attributes;


					// console.log('RepeaterBtnEditor-2',
					// 	// attributes,
					// 	props.attributes.items[item.key].btnType,
					// 	item[btnObjName].btnType
					// );
				},
				updateHoverScale : function(value){
					self.setItemBtnAtts(value, item, 'btnScale', btnObjName);
					attributes = props.attributes
				},
			}),
		)
	}
}

/**
 * Repeater Media Upload
 * --------------------------
 * @prop props (block props)
 * @prop item
 * @prop itemProp
 *
 * usage:
 * - Media Upload Inspector Component for Repeater items
 *
 * Required Block Attributes:
 * - items: array
 * 		- item: object
 *
 * ~	todo create and extend ItemComponent
 */
class RepeaterMediaUpload extends Component {


	init(props) {
		this.props = props.props;
	}

	render() {

		let self = this.props;
		let props = this.props.blockProps;
		let item = this.props.item;
		let attributes = props.attributes;
		let itemProp = this.props.itemProp;
		// let onSelect = this.props.itemProp;

		// ~ todo create and extend ItemComponent
		const updateRepeaterItems = function(newArr){
			props.setAttributes({
				items : newArr
			});
			attributes = props.attributes;
		};
		const setItemAttr = function(value, focusItem, property){

			let newArr = [...attributes.items];

			// update item
			newArr[focusItem.key][property] = value;

			// update block
			updateRepeaterItems(newArr);
		};

		const onSelect = function(media){

			console.log(self);
			self.onSelect(media);
		};

		return el('div', {},
			el( MediaUpload, {
				label: 'Top Image',
				onSelect: function(media){
					onSelect(media)
				},
				allowedTypes: 'image',
				value: item[itemProp].url, // todo: note itemProp = mediaObj
				render: function( obj ) {
					// console.log('debug: MediaUpload render', item, item[itemProp]);
					return !item[itemProp] ?

						// UI: Choose Image (conditional)
						//-----------------------------
						el( 'div', null,
							el( components.Button, {
									className: 'editor-post-featured-image__toggle',
									style: {margin:'10px 0'},
									onClick: obj.open,
								},
								__( 'Choose Image', 'CWB' )
							)
						) :

						// UI: Manage BG Image (conditional)
						//-----------------------------
						el( 'div', null,

							// UI: Preview / Replace Image
							//-----------------------------
							el( 'div', {
									className: 'image-button cursor-pointer',
									style: {margin:'10px 0'},
									onClick: obj.open,
								},
								el( 'img', {
									src: item[itemProp].url }
								)
							),

							// UI: Remove Image
							//-----------------------------
							el( components.Button, {
									className: 'button-large',
									style: {margin:'0 0 20px'},
									onClick: function(){
										setItemAttr(false, item, itemProp);
									},
									isTertiary: !0,
									// isLink: !0,
									isDestructive: !0,
								},
								__( 'Remove Image', 'gutenberg-examples' )
							),
						)
				}
			}));
	}
}

/**
 * Repeater WP Content Popover
 * ------------------------------------
 * @prop props (block props)
 * @prop selectedPost (retrieve via useSelect() on the block-level)
 * @prop focusItem (generate on the block-level)
 *
 * - Dynamically populate repeater item content via LinkControl component
 *
 * usage:
 * - use in block edit()
 * - use state management of the block level to toggle popover
 * - generate & store more complete dynamic data (than what LinkControl outputs)
 *   on the block-level via useSelect()
 *
 * requires:
 * 	- block props to edit images array
 * 	- selectedPost to trigger populating of post content
 */
class RepeaterWpContentPopover extends Component {

	init(props) {
		this.props = props.props;
	}

	/**
	 * Toggle Popover
	 * ------------------------------------
	 * 	- toggle popover by managing state on the block level
	 * 	- managing from block level because popover also needs to
	 * 	  get toggled by other UI elements in the block editor
	 */
	togglePopover() {
		let props = this.props;
		props.togglePopover();

		// /* usage: Toggle popover via useState(): */
		// /* ------------------------------------- */
		// - toggle popover by managing state on the block level
		// - managing from block level because popover also needs to
		//   get toggled by other UI elements in the block editor
		//
		// const [wpContentPopover, setWpContentPopover] = useState(false);
		// const toggleWpContentPopover = function(){ setWpContentPopover( !wpContentPopover )};
		// el (RepeaterWpContentPopover, {
		// 		togglePopover: toggleWpContentPopover,
		// 		...
	}

	render(){

		// /* Block-level usage (do not uncomment) */
		// Use below pattern on the block-level to generate
		// more complete dynamic content via useSelect()
		//
		// // Dynamic WP Content
		// //--------------------------------------
		// // * these cannot be inside functions, event handlers, or conditional statements
		// // 	- ref: https://reactjs.org/docs/hooks-rules.html
		// //  - dynamic content gets loaded into FOCUS item once select is called
		// //  - use selectors to get that information from the store
		//
		// // get Post title & thumbnail id
		// const selectedPost = useSelect((select) => {
		// 	return wp.data.select('core').getEntityRecord('postType','post', focusItem.postObj.id);
		// });
		// if(selectedPost){
		// 	focusItem.postObj = {
		// 		...focusItem.postObj,
		// 		title: selectedPost.title.raw,
		// 		featured_media: selectedPost.featured_media,
		// 		link: selectedPost.link
		// 	};
		// }
		//
		// // get post thumbnail
		// const slideImage = useSelect((select) => {
		// 	return wp.data.select('core').getEntityRecord('postType','attachment', focusItem.postObj.featured_media);
		// });
		// if(slideImage){
		// 	focusItem.postObj = {
		// 		...focusItem.postObj,
		// 		image: slideImage
		// 	};
		// }


		let self = this;
		let props = this.props;
		let selectedPost = props.selectedPost; // retrieve via useSelect() on the block-level
		let focusItem = props.focusItem; // generate on the block-level
		let attributes = props.attributes;
		let value = focusItem.postObj;
		let postType  = typeof props.postType !== 'undefined' ? props.postType : false;

		const updateRepeaterItems = function(newArr){
			props.setAttributes({
				items : newArr
			});
			attributes = props.attributes;

		};

		// - loads only the LinkControl output
		// - post link and featured image get loaded to FOCUS item
		//   after item selection via LinkControl
		const updatePostObj = function(linkObj, item){

			// debug(getParentFunction(), !!selectedPost, item.postObj.type, linkObj.type);

			let newArr = [...attributes.items];

			// update item
			newArr[item.key].postObj = linkObj;

			// update block
			updateRepeaterItems(newArr);

		};

		// - use dynamically updated focus item to populate Slide content with Post data
		// - caveat may need to be clicked twice (or just wait longer after selection before clicking)
		const usePostContent = function(focusItem){

			let newArr = [...attributes.items];
			let postImg = focusItem.postObj.image;

			newArr[focusItem.key] = {
				...newArr[focusItem.key],
				itemText: focusItem.postObj.title,
				bgID: focusItem.postObj.featured_media,
				bgURL: postImg ? postImg.guid.raw : false
			};
			newArr[focusItem.key].btnObj.text = 'View Post';
			newArr[focusItem.key].btnObj.url = focusItem.postObj.link;


			// debug(getParentFunction(), !!selectedPost, focusItem.postObj.type);

			// update block
			if (selectedPost)
				updateRepeaterItems(newArr);
		};



		return el(Popover, {
				// position: 'top left',
				className: 'popover-plus get-use-post-content',
				onFocusOutside: function(){
					self.togglePopover()
				}
			},
			el('div', {className: 'content-wrap'},


				// Close Button
				//-----------------------------
				el(components.Button, {
						className: 'close-btn',
						onClick: function(){
							self.togglePopover()
						},
					},
					el(Icon, {icon: 'no-alt'}),
				),

				el('div', {},
					el('p', {
						className: 'f-w-600'
					}, postType ? 'Search '+toTitleCase(postType)+' Content' : 'Search WP Content'),


					el('div', {
							className: 'cwb-input',
						},

						el(LinkControl, {
							searchInputPlaceholder: "Search here...",
							value: value,//focusItem.postObj,
							onChange: function (linkObj) {
								updatePostObj(linkObj, focusItem)
							},
							settings : [], // disable "open in new tab" toggle
							suggestionsQuery : postType ? {type: 'post', subtype: postType} : null,
							// withCreateSuggestion : true // https://wordpress.stackexchange.com/questions/399076/gutenberg-linkcontrol-suggestionsquery-not-working
						}),

						el(components.Button, {
							className: 'button button-large cursor-pointer mt-4',
							onClick: function(){
								usePostContent(focusItem)
							}
						}, postType ? 'Use '+toTitleCase(postType)+' Content' : 'Use WP Content'),
					),
				)
			)
		)
	}
}

/**
 * Simple Repeater (Editor Block)
 * --------------------------
 * Displays simple repeater content in the block editor
 * Edit, Remove, or Re-Order repeater items directly form the block editor
 * Repeater Item content is determined by the component's children
 *
 * Required Block Attributes:
 * - items: array
 * 		- item: object
 * - focusItem: string
 */
class SimpleRepeaterBlock extends RepeaterComponent {

	init(props) {
		this.props = props.props;
	}

	render() {
		let self = this;
		let props = this.props;
		let blockProps = typeof props.blockProps !== 'undefined' ? props.blockProps : props;
		let attributes = blockProps.attributes;
		let toolBarCss = typeof props.toolBarCss !== 'undefined' ? props.toolBarCss : '';
		let className = typeof props.className !== 'undefined' ? props.className : [];
		// let colClasses = typeof props.colClasses !== 'undefined' ? props.colClasses : [];
		let itemObjModel = typeof props.itemObjModel !== 'undefined' ? props.itemObjModel : '';
		let hideUiMove = typeof props.hideUiMove === 'boolean' ? props.hideUiMove : false;

		return el('div', {className: Array.isArray(className) ? className.join(' ') : className},
			attributes.items.map((item) => {

				// todo focusItem via state b/c this ui stays even after you click away
				let toolbar = item.key.toString() === attributes.focusItem ?
					el( 'div',{
							className : toolBarCss,
							style: {
								padding: 0,
								border: '1px solid #bbb',
								position: 'absolute',
								// top: '-50px',
								top: '0',
								right: '0',
								zIndex: 100
							}
						},
						el(RepeaterToolbar, {
							itemObjModel : itemObjModel,
							// ...props,
							blockProps: blockProps,
							hideUiMove: hideUiMove
						}),

					) : null;

				// Repeater Item Markup
				return props.children(item, toolbar)

			}),
			// el('div', {className: colClasses.join(' ')+' p-4'}, // todo matchItemStyleOnAdd
			// 	el(RepeaterAddItemUI, props)
			el('div',{ className: 'w-100 text-right'},
				el(RepeaterAddItemUI, {
					itemObjModel: itemObjModel,
					// ...props,
					blockProps: blockProps
				})
			)
		)
	}
}

/**
 * Repeater Slider (Editor Block) todo use SliderEditBlock for interface
 * --------------------------
 * Navigate, Edit, Remove, or Re-Order slider content directly form the block editor
 * Slide(item) content is determined by the component's children
 *
 * Required Block Attributes:
 * - items: array
 * 		- item: object
 * 			- key: string (int)
 * 			- itemText: string
 * - focusItem: string
 *
 * ! this is located under REPEATER (even though it has slider in the name) because
 *   it extends the RepeaterComponent to enable the editing of slider content.
 *
 * use:
 * - intended for use only in a Repeater Context
 * - use in block edit()
 * - for block save() use: CwbSlider
 */
class RepeaterSliderEditBlock extends RepeaterComponent {

	init(props) {
		this.props = props.props;
	}

	render() {

		let self = this;
		let props = this.props;
		let attributes = typeof props.blockProps !== 'undefined' ? props.blockProps.attributes : props.attributes;
		let focusItem = attributes.items[ attributes.focusItem !== null ? attributes.focusItem : 0 ];


		return el( 'div', {
				className: 'cwb-slider',
				// dataControlKey: 'category-post-slider'
			},

			el( 'div', { className: 'slider-wrap'},

				el( 'div', { className: 'slides'},

					el( 'div', { className: 'd-flex align-items-center'},



						// SLIDER Nav Controls
						//========================

						el( 'div', {
							className: 'slide-prev slider-arrow',
							style: {
								position: 'absolute'
							},
							onClick: function(){

								let size = attributes.items.length;
								let target = focusItem.key > 0 ? focusItem.key - 1 : size - 1;
								self.updateFocus(target.toString());
							}
						}),

						el( 'div', {
							className: 'slide-next slider-arrow',
							style: {
								position: 'absolute'
							},
							onClick: function(){

								let size = attributes.items.length;
								let target = focusItem.key < size - 1 ? focusItem.key + 1 : 0;
								self.updateFocus(target.toString());
							}
						}),

						el( 'div', {className: 'slider-options',},
							el( 'div', {className: 'slider-options-inner',},
								attributes.items.map((item) => {
									return el( 'div', {
											className: item.key.toString() === attributes.focusItem ? 'slider-option active' : 'slider-option',
											['data-key']: item.key,
											onClick: function(){
												self.updateFocus(item.key.toString());
											}
										},
									)
								})
							)
						),



						// SLIDES
						//========================

						attributes.items.map((item) => {
							return item.key.toString() === attributes.focusItem ?

								props.children(item) : // pass item obj to child slide element
								null;
						})
					)
				)
			)
		)


		// Below was worth a shot but could get the slide content to appear
		// return el(SliderEditBlock, {
		// 		items: attributes.items,
		// 		currentKey: focusItem.key,
		// 		onPrev: function(){
		//
		// 			let size = attributes.items.length;
		// 			let target = focusItem.key > 0 ? focusItem.key - 1 : size - 1;
		// 			self.updateFocus(target.toString());
		// 		},
		// 		onNext: function(){
		//
		// 			let size = attributes.items.length;
		// 			let target = focusItem.key < size - 1 ? focusItem.key + 1 : 0;
		// 			self.updateFocus(target.toString());
		// 		},
		// 		onOptionClick: function(itemKey){
		// 			self.updateFocus(itemKey.toString());
		// 		}
		// 	},
		// 	(slide) => {
		// 		// console.log('RepeaterSliderEditBlock', slide);
		//
		// 		// item.key.toString() === attributes.focusItem ?
		// 		props.children(slide) //: // pass item obj to child slide element
		// 		// null
		// 	}
		// )
	}
}




/* Button */
/*-------------------------------*/

/**
 * CWB CwbButtonEditor
 * ------------------------------------
 * @prop btnObj
 * @prop allowedBtnTypes
 * @prop setButtonFunction(value)
 * @prop updateText(value)
 * @prop updateUrl(value)
 * @prop updateNewTab(value)
 * @prop updateDataToggle(value)
 * @prop updateDataTarget(value)
 * @prop updateDataSrc(value)
 * @prop updateShortcode(value)
 * @prop updateModalSize(value)
 * @prop updateModalHeading(value)
 * ~@prop updatePostObj(value)
 *
 * -
 *
 * use:
 * - use in block edit()
 * - for block save() use: CwbButton
 *
 * requires:
 * - TextControl (InspectorTextInput)
 * - SelectControl
 * - ToggleControl
 *
 *
 * TODO: if is_function()
 */
class CwbButtonEditor extends Component {
	constructor(props) {
		super(props);
		this.state = {
			posts: [],
		};
	}

	init(props) {
		this.props = props.props;
	}

	setButtonFunction(value) {
		let props = this.props;
		props.setButtonFunction(value); }

	updateText(value) {
		let props = this.props;
		props.updateText(value); }

	updateUrl(value) {
		let props = this.props;
		props.updateUrl(value); }

	updateNewTab(value) {
		let props = this.props;
		props.updateNewTab(value); }




	updateDataToggle(value) {
		let props = this.props;
		props.updateDataToggle(value); }

	updateDataTarget(value) {
		let props = this.props;
		props.updateDataTarget(value); }

	updateDataSrc(value) {
		let props = this.props;
		props.updateDataSrc(value); }

	updateShortcode(value) {
		let props = this.props;
		props.updateShortcode(value); }

	updateModalSize(value) {
		let props = this.props;
		props.updateModalSize(value); }

	updateModalHeading(value) {
		let props = this.props;
		props.updateModalHeading(value); }



	updatePostObj(value) {
		let props = this.props;
		props.updatePostObj(value); }

	updateBtnType(value) {
		let props = this.props;
		props.updateBtnType(value); }

	updateBtnSize(value) {
		let props = this.props;
		props.updateBtnSize(value); }

	updateHoverScale(value) {
		let props = this.props;
		props.updateHoverScale(value); }


	async componentDidMount() {

		let origin = window.location.origin;
		let pathname = window.location.pathname;


		// Check for popup CPT via WP REST API
		let fetchPath = origin + pathname.replace('wp-admin/post.php', '/wp-json/wp/v2/popup');
		try {
			const response = await fetch(fetchPath);
			if (response.ok) {
				const fetchedPosts = await response.json();
				this.setState({ posts: fetchedPosts });
				// console.log('fetchedPosts:', fetchedPosts);
			} else {
				// console.error('Error fetching posts:', response.statusText, response);
			}
		} catch (error) {
			// console.error('Error fetching posts:', error);
		}
	}

	render(){
		let self = this;
		let props = this.props;
		let btnObj = props.btnObj;
		let isPopup = typeof props.isPopup !== 'undefined' ? props.isPopup : false;
		let allowedBtnTypes = typeof props.allowedBtnTypes !== 'undefined' ? props.allowedBtnTypes : [
			'link', 'modal_trigger', 'iframe', 'shortcode_modal_trigger', 'text'
		];


		let btnFunctionOptions = [
			{ value: 'link', label: 'Link' },
			{ value: 'modal_trigger', label: 'Modal Trigger' },
			{ value: 'iframe', label: 'iFrame Trigger' },
			{ value: 'shortcode_modal_trigger', label: 'Shortcode Modal Trigger' }, // <-- requires ez-core v3.0.12
			{ value: 'wp_link', label: 'WP Link' } // disabled until we can set allowed btn options externally
		];

		// ADD popup_cpt to btn-functions
		// - todo: if (popup cpt exists) ; current done on block level using getEntityRecords
		if( typeof this.state.posts !== 'undefined' && this.state.posts.length ) {

			if(typeof props.allowedBtnTypes !== 'undefined' && props.allowedBtnTypes.includes('popup_cpt')){

				allowedBtnTypes.push('popup_cpt');
				btnFunctionOptions.push({ value: 'popup_cpt', label: 'Popup CPT' });
			}
		}


		let selectOptions = [];
		for (let x = 0; x < btnFunctionOptions.length; x++){
			if(allowedBtnTypes.includes(btnFunctionOptions[x].value)){
				selectOptions.push(btnFunctionOptions[x])
			}
		}



		// const [layoutTab, setLayoutTab] = useState(false);
		// const [sliderHeightTab, setSliderHeightTab] = useState(false);



		return el( 'div', {},

			isPopup ?
				el('p', { className: 'f-w-600 p-2 mb-0 f-sz-16'}, 'Button Editor')
			: null,


			// todo : tabs vs panels? (rquires states)
			// el(ResponsiveTabs, props,
			// 	(tab) => {
			// 		setSliderHeightTab(tab);
			// 		// ^ fixes something (not sure what yet) to allow below to work
			// 		props = {
			// 			// tab:{...layoutTab},
			// 			// ^ this would make more sense (maybe?) but somehow below gets fixed after
			// 			//   setLayoutTab(tab) is called... don't understand why
			// 			tab:{...tab},
			// 			...props
			// 		};
			// 		// console.log('BtnMetaTab.ResponsiveTabs | tab', tab);
			//
			// 		// console.log('props.tab.name', props.tab.name);
			// 		return el(BtnMetaTab, props);
			// 	}
			// ),




			// Panel : Button Style
			//-----------------------------
			el( PanelBody, {
					title: 'Button Styles',
					initialOpen: false
				},

				el( SelectControl, {
					key: 'multiple',
					label: __('Button Type'),
					value: btnObj.btnType,
					className: ['mt-3'],
					onChange: function(value){ self.updateBtnType(value) },
					options: [
						// { value: '', label: tab.name === 'base' ? 'default' : 'inherit from smaller' },
						{ value: 'primary', label: 'primary' },// 			{ value: 'default', label: 'Default' },
						{ value: 'secondary', label: 'secondary' },
						{ value: 'tertiary', label: 'tertiary' },
						{ value: 'alt', label: 'alt' },
						{ value: 'alt-lite', label: 'alt-lite' },
						{ value: 'text', label: 'text' },
						// { value: 'alt-secondary', label: 'alt-secondary' }, <--- TODO
					]
				}),

				el( SelectControl, {
					key: 'multiple',
					label: __('Button Size'),
					value: btnObj.btnSize,
					className: ['mt-3'],
					onChange: function(value){ self.updateBtnSize(value) },
					options: [
						// { value: '', label: tab.name === 'base' ? 'default' : 'inherit from smaller' },
						{ value: 'sm', label: 'Small' },
						{ value: 'default', label: 'Default' },
						{ value: 'lg', label: 'Large' },
					]
				}),


				el('p',{}, 'Button Animations'),
				el( CheckboxControl, {
					// 	key: 'multiple',
					label: __('Grow on Hover'),
					checked: btnObj.hoverScale,
					onChange: function(value){ self.updateHoverScale(value) },
				}),
			),



			// Panel : Button Meta
			//-----------------------------
			el( PanelBody, {
					title: 'Button Meta',
					initialOpen: false//!0
				},


				allowedBtnTypes.length > 1 ? el( SelectControl, {
					key: 'multiple',
					label: __( 'Button Function'),
					value: btnObj.btnFunction,
					onChange: function(value){
						self.setButtonFunction(value);
						self.updateDataToggle(value);
					},
					options: selectOptions
				}) : null,



				el( 'div', { style: {
							padding: '20px 10px',
							background: '#f8f8f8'
						}
					},
					el(InspectorTextInput, {
						label: __('Button Text'),
						currentValue: btnObj.text,
						onChange: function(value){ self.updateText(value) },
					}),


					// Basic Link Button Meta
					//-----------------------------
					btnObj.btnFunction === 'link' ?
						el( 'div', {},
							el(InspectorTextInput, {...props,
								label: __('Button Attribute: url'),
								currentValue: btnObj.url,
								onChange: function(value){
									self.updateUrl(value)
								},
							}),
							el( ToggleControl,
								{
									label: 'Open in new tab',
									onChange: function(value){ self.updateNewTab(value) },
									checked: btnObj.newTab,
								}
							)
						): null,




					// Modal Button Meta
					//-----------------------------
					btnObj.btnFunction === 'iframe' ||
					btnObj.btnFunction === 'modal_trigger' ||
					btnObj.btnFunction === 'popup_cpt' ||
					btnObj.btnFunction === 'shortcode_modal_trigger' ?
						el( 'div', {},
							el(InspectorTextInput, {
								label: __('Popup Heading'),
								currentValue: btnObj.dataModalHeading,
								onChange: function(value){ self.updateModalHeading(value) },
							}),
							el( SelectControl, {
								className: ['mt-3'],
								key: 'multiple',
								label: __('Modal Size'),
								currentValue: btnObj.dataModalSize,
								onChange: function(value){ self.updateModalSize(value) },
								options: [
									// { value: '', label: tab.name === 'base' ? 'default' : 'inherit from smaller' },
									{ value: 'default', label: 'Default' },
									{ value: 'lg', label: 'Large' },
									{ value: 'xl', label: 'Extra Large' },
									{ value: 'fs', label: 'Full Screen' },
								]
							}),
						): null,



					// iframe Modal Source
					//-----------------------------
					btnObj.btnFunction === 'iframe' ?
						el( 'div', {},
							el(InspectorTextInput, {
								label: __('iFrame Source (data-src)'),
								currentValue: btnObj.dataSrc,
								onChange: function(value){ self.updateDataSrc(value) },
							}),
						): null,



					// iframe Modal Trigger Meta
					//-----------------------------
					btnObj.btnFunction === 'modal_trigger' ?
						el( 'div', {},
							el(InspectorTextInput, {
								label: __('Target Modal'),
								currentValue: btnObj.target,
								onChange: function(value){ self.updateDataTarget(value) },
							}),
						): null,



					// Shortcode Modal Trigger Meta
					//-----------------------------
					btnObj.btnFunction === 'shortcode_modal_trigger' ?
						el( 'div', {},
							el(InspectorTextInput, {
								label: __('WP Shortcode'),
								currentValue: btnObj.dataShortcode,
								onChange: function(value){

									// ~todo : shortcodes don't work if quotes are used (see: wp_shortcode_via_ajax wp ajax call in php)
									self.updateShortcode(value) },
							}),
						): null,



					// WP Link Meta
					//-----------------------------
					btnObj.btnFunction === 'wp_link' ?

						el( 'div', {
								className: 'cwb-input',
								style: {
									padding: '5px 0 0',
									background: '#f8f8f8'
								}
							},

							el( LinkControl, {
								searchInputPlaceholder : "Search here...",
								value: btnObj.postObj,
								onChange: function(value){
									self.updatePostObj(value) },

								// suggestionsQuery : null
								// withCreateSuggestion : true // https://wordpress.stackexchange.com/questions/399076/gutenberg-linkcontrol-suggestionsquery-not-working
							})

						) : null,

					btnObj.btnFunction === 'popup_cpt' ?
						el( 'div', {
								className: 'cwb-input hide-new-tab-toggle',
								style: {
									padding: '5px 0 0',
									background: '#f8f8f8'
								}
							},
							el(LinkControl, {
								searchInputPlaceholder : "Search here...",
								value: btnObj.postObj,
								suggestionsQuery : {
									type: 'post',
									subtype: 'popup',
								},
								onChange: function(value){
									self.updatePostObj(value);
								},
							})
						)
						: null
				)
			),
		);
	}
}

/**
 * CWB Button
 * ------------------------------------
 * @prop btnObj
 *
 * - types supported (btnObj.btnFunction)
 * 	- iframe
 * 	- modal_trigger
 * 	- shortcode_modal_trigger
 * 	- link
 *
 * use:
 * - use in block save()
 */
class CwbButton extends Component {

	init(props) {
		this.props = props.props;
	}

	render() {

		let props = this.props;
		let btnObj = props.btnObj;

		let btnElTag = 'div';//'button';//'a';
		let btnProps = { ['type']: 'button' };
		let extraBtnCss = typeof props.extraBtnCss !== 'undefined' ?
			props.extraBtnCss : [];
		let btnSize = typeof props.btnSize !== 'undefined' ? props.btnSize : '';
		let btnType = typeof props.btnType !== 'undefined' ? 'btn-'+props.btnType : 'btn-primary';
		let dataId = typeof btnObj.dataId !== 'undefined' ? btnObj.dataId : false;


		let btnCss = [
			// 'btn',
			// btnType,
			'clickable',
			'text-center',
			...extraBtnCss
		];
		btnCss = setBtnCssClasses(btnObj, btnCss.join(' '));

		if(btnType !== 'btn-text'){
			// btnElTag = 'button';
			btnCss.push('btn');
			btnCss.push('text-center');
			btnCss.push(btnType);
			btnCss.push(btnSize);
			btnProps.role = 'button';
		}
		if(btnType === 'btn-text'){
			btnCss.push('no-btn-style');
			// btnCss.push('text-underline');
			btnCss.push('read-more');
			btnProps.tabindex = '0';
			btnProps.role = 'button';

			// if(!btnObj.btnFunction === 'link' && !btnObj.btnFunction === 'wp_link'){
			// 	btnProps.role = 'button';
			// }
			//
			// if(!btnObj.btnFunction === 'link' && !btnObj.btnFunction === 'wp_link'){
			// 	btnProps.role = 'link';
			// }
		}

		// Conditional Block Properties
		// - based on attributes.btnFunction value
		//-----------------------------------------



		btnProps['tabindex'] = '0';

		// basic link button conditionals
		if(btnObj.btnFunction === 'link') {
			btnElTag = 'a';
			btnProps.href = __get(btnObj.href, btnObj.url);
			btnProps.target = btnObj.newTab ? '_blank' : null;
			// need to add rel="noopener" below otherwise breaks button block in editor
			btnProps.rel = btnObj.newTab ? 'noopener' : null;
			btnProps['aria-label'] = btnObj.newTab ? 'External Link' : null;
		}


		// WP link button conditionals
		if(btnObj.btnFunction === 'wp_link') {
			btnObj.url = btnObj.postObj !== null && btnObj.postObj !== '' ? btnObj.postObj.url : null;
			btnElTag = 'a';
			if(typeof btnObj.postObj !== 'undefined' && btnObj.postObj !== null){
				let opensInNewTab = btnObj.postObj.opensInNewTab;
				opensInNewTab = typeof opensInNewTab !== 'undefined' ? opensInNewTab : false;
				btnProps.target = opensInNewTab ? '_blank' : null;
				// need to add rel="noopener" below otherwise breaks button block in editor
				btnProps.rel = opensInNewTab ? 'noopener' : null;
			};
			btnProps.href = btnObj.url;
		}


		// modal trigger (common) conditionals
		if(	btnObj.btnFunction === 'iframe' ||
			btnObj.btnFunction === 'popup_cpt' ||
			btnObj.btnFunction === 'modal_trigger' ||
			btnObj.btnFunction === 'shortcode_modal_trigger' ) {

			btnProps['data-toggle'] = 'modal';
			// below requires ez-core v3.0.12
			btnProps['data-modal-size'] = btnObj.dataModalSize !== '' ? btnObj.dataModalSize : null;
			btnProps['data-modal-heading'] = btnObj.dataModalHeading !== '' ? btnObj.dataModalHeading : null;


			btnProps['aria-haspopup'] = 'true';
			btnProps['aria-expanded'] = 'false';
			if(dataId !== false){
				btnProps['aria-controls'] = dataId;
			}
		}

		// iframe trigger conditionals
		if(btnObj.btnFunction === 'iframe'){
			btnElTag = 'div';
			btnCss.push('iframe-trigger');
			btnCss.push('d-inline-block');
			btnProps['data-target'] = '#iframe-modal';
			btnProps['aria-controls'] = '#iframe-modal';
			btnProps['data-src'] = btnObj.dataSrc !== '' ? btnObj.dataSrc : null;
			// btnProps['aria-label'] = 'opens popup'
		}
		// modal trigger conditionals
		if(btnObj.btnFunction === 'modal_trigger'){


			btnProps['data-target'] = btnObj.target;
			btnProps['aria-controls'] = btnObj.target;
			if(dataId !== false){

				btnProps['data-id'] = dataId;
			}
		}
		// shortcode modal conditionals
		if(btnObj.btnFunction === 'shortcode_modal_trigger'){
			btnCss.push('shortcode-modal-trigger');
			btnProps['data-target'] = '#shortcode-ajax-modal';
			btnProps['aria-controls'] = '#shortcode-ajax-modal';
			btnProps['data-wp-ajax'] = JSON.stringify({
				action: 'wp_shortcode_via_ajax',
				ajax_wp_shortcode: encodeURI(btnObj.dataShortcode)
			});
			// below requires ez-core v3.0.12
			// todo depreciate this (recover btn block on DP0, MiskoAki, StudioV)
			btnProps['data-shortcode'] = btnObj.dataShortcode !== '' ? encodeURI(btnObj.dataShortcode) : null;
		}
		// popup_cpt conditionals
		// - requires custom-post-types/wp-cpt-popup/wp-cpt-popup.php (child theme)
		// - requires control-modal/control-wp-content-modal.php (core)
		if(btnObj.btnFunction === 'popup_cpt'){
			btnCss.push('wcm-trigger');
			btnProps['data-target'] = '#wp-content-modal';
			btnProps['aria-controls'] = '#wp-content-modal';
			btnProps['data-wp-ajax'] = JSON.stringify({
				action : 'get_wcm_content',
				ajax_wp_post_id : btnObj.postObj.id

			});
			// todo depreciate this (recover btn block on DP0, MiskoAki, StudioV)
			btnProps['data-post-id'] = btnObj.postObj.id;
		}

		return el( btnElTag, {
				...btnProps,
				className: btnCss.join(' '),
				// ['data-update'] : 23061500,
			}, btnObj.text
		)
	}
}

/**
 * CWB Editable Button
 *
 * - for use in block editor
 */
class CwbEditableButton extends CwbButtonEditor {

	state = {
		btnPopover: false,
		btnDisabled: false
	};

	init(props) {
		this.props = props.props;
	}

	showBtnPopover(){
		// let self = this;
		let state = this.state;
		if(!state.btnDisabled)
			this.setState({ btnPopover: true }, () => {
				// console.log('showBtnPopover');
			});
	};

	hideBtnPopover(){
		let self = this;
		this.setState({ btnDisabled: true });
		this.setState({ btnPopover: false });
		setTimeout(function(){
			self.setState({ btnDisabled: false });
		}, 200);
	};

	// onClick(value) {
	// 	let props = this.props;
	// 	props.onClick(value); }
	//
	// setItemBtnAtts(value, focusItem, btnProperty, btnObjName){
	//
	// 	if(typeof btnObjName === 'undefined') btnObjName = 'btnObj';
	// 	let props = this.props.blockProps;
	// 	let attributes = props.attributes;
	// 	let newArr = [...attributes.items];
	//
	// 	// update item
	// 	newArr[focusItem.key][btnObjName][btnProperty] = value;
	//
	//
	//
	// 	//
	// 	// update block
	// 	props.setAttributes({
	// 		items : newArr
	// 	});
	// 	attributes = props.attributes;
	// };

	render(){

		let self = this;
		let props = this.props;
		let btnObj = props.btnObj;
		let className = typeof props.className !== 'undefined' ? props.className : '';
		let allowedBtnTypes = typeof props.allowedBtnTypes !== 'undefined' ? props.allowedBtnTypes : ['link'];

		className = setBtnCssClasses(btnObj, className);





		return el(Fragment,{},
			el('button', {
				className: className.join(' '),
				['type']: 'button',
				onClick: function(){
					self.showBtnPopover();
					// self.onClick()
				},
			}, btnObj.text,

			self.state.btnPopover ?
				el(Popover,{
						position: 'top left',
						className: 'popover-plus btn-editor',
						onFocusOutside : function(){ self.hideBtnPopover() }
					},
					el('div',{ className:'content-wrap' },

						// Close Popover Button
						//-----------------------------
						el(components.Button,{
								className : 'close-btn',
								onClick: function(e){
									e.stopPropagation();
									self.hideBtnPopover()
								},
							},
							el( Icon, { icon: 'no-alt' }),
						),

						// Button Editor
						//-----------------------------
						el('div', {},


							el( 'div', {//PanelBody, {
									// title: 'Button Meta',
									// className: cwbGlobals.innerPanelClasses,
									// initialOpen: !0//false
								},
								el('h6',{className: 'p-2 mb-0'}, 'Button Editor'),
								el(CwbButtonEditor,{
									allowedBtnTypes  : allowedBtnTypes,
									btnObj : {
										text			: btnObj.text,
										url 			: btnObj.href,
										btnFunction 	: btnObj.btnFunction,
										target			: btnObj.target, // todo should be dataTarget because thats how CwbButton uses it
										newTab 			: btnObj.newTab,
										// dataToggle 	: btnObj.dataToggle,
										dataShortcode 	: btnObj.dataShortcode,
										dataModalHeading: btnObj.dataModalHeading,
										dataModalSize 	: btnObj.dataModalSize,
										dataSrc 		: btnObj.dataSrc,
										postObj 		: btnObj.postObj,
										btnType 		: btnObj.btnType,
										btnSize 		: btnObj.btnSize,
										hoverScale 		: btnObj.hoverScale,
									},

									// setButtonFunction : function(value){
									// 	self.setButtonFunction(value);
									// },
									updateText : function(value){
										self.updateText(value);
									},
									updateUrl : function(value){
										self.updateUrl(value);
									},
									updateNewTab : function(value){
										self.updateNewTab(value);
									},
									setButtonFunction : function(value){
										self.setButtonFunction(value);
									},

									updateDataSrc : function(value){
										self.updateDataSrc(value);
									},
									updateDataTarget : function(value){
										self.updateDataTarget(value);
									},
									updateShortcode : function(value){
										self.updateShortcode(value);
									},
									updateModalHeading : function(value){
										self.updateModalHeading(value);
									},
									updateModalSize : function(value){
										self.updateModalSize(value);
									},

									updateBtnType : function(value){
										self.updateBtnType(value);
									},
									updateBtnSize : function(value){
										self.updateBtnSize(value);
									},
									updateHoverScale : function(value){
										self.updateHoverScale(value);
									},
								}),
							)
						)
					)
				): null
			)
		)
	}
}



// // Dynamic WP Content
// 	//--------------------------------------
// const popupCptExist = useSelect((select) => {
// 		return wp.data.select('core').getEntityRecords('postType', 'blah');
// 	});

// console.log('popupCptExist', popupCptExist);
// useEffect(() => {
// 	if(popupCptExist){
//
// 		console.log('popupCptExist!!');
// 		// props.setAttributes({
// 		// 	records: records
// 		// });
// 		// attributes = props.attributes;
// 	}
// }, [popupCptExist]);





/* BOOTSTRAP Components */
/*-------------------------------*/

/**
 * Bootstrap Modal
 * ------------------------------------
 * @prop modalId
 *
 * requires:
 * 	- JQuery
 * 	- Bootstrap
 * 	- control-modal/init.php (core theme)
 *
 * Wraps child element in a Bootstrap modal
 * - modal markup based on ui_modal() in : control-modal / init.php
 */
class BootstrapModal extends Component {

	init(props) {
		this.props = props.props;
	}

	render(){
		let props = this.props;
		let modalId = props.modalId;
		let modalHeading = typeof props.modalHeading !== 'undefined' ? props.modalHeading : false;
		// todo headerClassName
		let modalSize = typeof props.modalSize !== 'undefined' ? props.modalSize : 'xl';

		return el('div', { className: 'modal', id: modalId,
				role : "dialog",
				['aria-modal'] : true,
				['aria-label'] : modalHeading ? modalHeading : null},
			el('div', { className: 'modal-dialogue modal-'+modalSize+' h-100 d-flex flex-column justify-content-center my-0 mx-auto py-3' },
				el('div', { className: 'modal-content  form-shadow-wrap'},

					el('div', { className: 'modal-header p-0'},

						el('div', { className: 'container'},

							el('div', { className: 'row'},

								el('div', { className: 'col d-flex align-items-center'},

									// todo headerClassName
									el('h2', { className: 'h4'}, modalHeading ? modalHeading  : null)
								),
								el('div', { className: 'col d-flex align-items-center max-w-100  justify-content-end'},

									el('button', { className: 'close py-3 b-l-grey-25 icon--close',
										type: 'button',
										['data-dismiss'] : 'modal',
										['aria-label'] : 'close-popup'
									})
								)
							)
						)
					),
					el('div', { className: 'responsive-scrolling-modal-content'},
						el('div', { className: 'modal-body p-3'},

							props.children
							// modalContent
						)
					)
				)
			)
		)
	}
}




/* SLIDER Components */
/*-------------------------------*/

/**
 * CWB Slider
 * --------------------------
 * Front-end Slider markup
 * Slide(item) content is determined by the component's children
 *
 * Required Block Attributes:
 * - items: array
 * 		- item: object
 * - focusItem: string
 */
class CwbSlider extends Component {

	init(props) {
		this.props = props.props;
	}

	render() {

		let props = this.props;
		let attributes = this.props.attributes;
		let items = typeof props.items !== 'undefined'
			? props.items : attributes.items; // ~todo comment
		let sliderId = typeof props.sliderId !== 'undefined'
			? props.sliderId : attributes.sliderId; // ~todo comment
		let sliderOptMarkup = typeof props.sliderOptMarkup !== 'undefined'
			? props.sliderOptMarkup : false; // ~todo comment
		let useStickyBuffer = typeof props.useStickyBuffer !== 'undefined'
			? props.useStickyBuffer : false; // ~todo comment
		let headerIsOpaque = typeof props.headerIsOpaque !== 'undefined'
			? props.headerIsOpaque : false; // ~todo comment
		// let slideCaptionMarkup = typeof props.slideCaptionMarkup !== 'undefined'
		// 	? props.slideCaptionMarkup : false;
		// let slideCaptions = typeof props.slideCaptions !== 'undefined'
		// 	? props.slideCaptions : false;


		return el( 'div', {
				// ['data-bez']: 'bez', // toggle this to force a block markup refresh
				className: 'slider hero-slider',
				['data-control-key']: 'hero-slider',
				['data-control-index']: sliderId,
				['data-slide-count']: items.length
			},

			// useStickyBuffer && headerIsOpaque ? el( 'div', { className: 'sticky-buffer-here' }) : null,

			el( 'div', { className: 'slides-wrap mb-0'},

				el( 'div', { className: 'slides'},
					//  el( 'div', { className: 'slider-arrows', style: {zIndex: 1}},
					// below overrides core slider styles; consider updating core (php) slider with a similar
					el( 'div', { className: 'slider-arrows d-flex flex-column align-items-center w-100', style: {zIndex: 1}},

						el( 'div', { className: useStickyBuffer && !headerIsOpaque ? 'sticky-buffer-here' : ''}),

						// below overrides core slider styles; consider updating core (php) slider with a similar
						el( 'div', { className : 'd-flex align-items-center w-100',
								style : { height: '100%' }},

							// el( 'div', { className: 'slider-arrows-inner',
							// below overrides core slider styles; consider updating core (php) slider with a similar
							el( 'div', { className: 'slider-arrows-inner d-block w-100',
									style : { height: 'auto' }},

								el( 'div', { className: 'slide-prev slider-arrow',
									tabindex: 0,
									['aria-label']: 'View Previous Slide',
									role: 'button'
								}),
								el( 'div', { className: 'slide-next slider-arrow',
									tabindex: 0,
									['aria-label']: 'View Next Slide',
									role: 'button'
								}),
							),
						)
					),

					el( 'div', { className: 'fade-wrapper',
						role: 'list'},


						// SLIDES
						items.length ? items.map((item) => {
							return props.children(item);
						}): null
					)
				),

                el( 'div', { className: 'slider-nav ez-row   opacity-0 anim-opacity',
						role: 'listbox',
						['aria-label']: 'slider'
					},
					el( 'div', { className: 'slider-nav-inner ez-col'},
						el( 'div', { className: 'ez-row'},
							el( 'div', { className: 'slide-prev slider-arrow ez-col',
								tabindex: 0,
								['aria-label']: 'View Previous Slide',
								role: 'button'
							}),
							el( 'div', { className: 'slider-options ez-col'},

								// // // Slide captions
								// // // todo: infrastructure set up; not tested; not sure if feature needed; captions can simply be inside the slide markup
								// slideCaptions ?
								// 	items.map((item) => {
								//
								// 		// console.log(item);
								//
								// 		return item.caption !== '' ?
								// 			slideCaptionMarkup ?
								// 				slideCaptionMarkup(item) : el( 'div', {
								// 					className: 'slider-caption',
								// 					['data-id']: item.key+1
								// 				}, el('p', {},'test'))
								// 		: null
								//
								// 		// return item.caption !== '' ? el( 'div', {
								// 		// 		className: 'slider-caption',
								// 		// 		['data-id']: item.key+1
								// 		// 	},
								// 		// 	el('p', {},'test')
								// 		// ) : null
								//
								// 		// Option UI element
								// 		// return slideCaptionMarkup ? slideCaptionMarkup(item) : null
								//
								// 	})
								// : null,


								// Slide options
								el( 'div', { className: 'margin-wrap'},
									items.length ? items.map((item) => {

										// Option UI element
										return sliderOptMarkup ? sliderOptMarkup(item) : el( 'div', {
											className: 'slider-option',
											['data-id']: item.key+1,
											role: 'option'
										})
									}): null
								),
							),
							el( 'div', { className: 'slide-next slider-arrow ez-col',
								tabindex: 0,
								['aria-label']: 'View Next Slide',
								role: 'button'
							})
						)
					)
				)
			)
		);
	}
}

/**
 * CW Image Slider Option
 * ------------------------------------
 * @prop image (obj)
 * @prop images (array)
 *
 * generates option index required for slider by comparing image object with all image objs in images array
 *
 * usage:
 * - used by CwImageSlider
 * - intended for use with CwbSlider
 */
class CwImageSliderOption extends Component {

	init(props) {
		this.props = props.props;
	}

	render() {
		let props = this.props;
		let image = props.image;
		let images = props.images;

		// Generate Option index
		let imageIndex = 0;
		for(let i=0; i < images.length; i++){
			if(images[i] === image){
				imageIndex = i+1;
				break;
			}
		}
		// build slider option element
		return el( 'div', {
			className: 'slider-option',
			tabindex: 0,
			['aria-label']: 'View Slide '+(imageIndex+1),
			role: 'button',
			['data-id']: imageIndex
		})
	}
}

/**
 * CW Image Slider
 * ------------------------------------
 * @prop images
 * @prop
 *
 * requires:
 * 	- CwbSlider
 * 	- CwImageSliderOption
 * 	- control-slider/init.php (core theme)
 *
 * markup based on cw_slider() in control-slider/init.php (core theme)
 */
class CwImageSlider extends Component {

	init(props) {
		this.props = props.props;
	}

	render() {
		let props = this.props;
		let images = props.images;
		let imgSize = typeof props.imgSize !== 'undefined' ? props.imgSize : 'gallery';
		let sliderId = props.sliderId;
		let imageIndex = 0;
		let lazy_load = typeof props.lazyLoad !== 'undefined' ?
			props.lazyLoad : false;
		let useShadow = typeof props.useShadow !== 'undefined' ?
			props.useShadow : false;
		// let sliderCaptionMarkup = typeof props.sliderCaptionMarkup !== 'undefined' ?
		// 	props.sliderCaptionMarkup : false;
		let slideCaptions = typeof props.slideCaptions !== 'undefined' ?
			props.slideCaptions : false;



		return el(CwbSlider, {
				items: images,
				sliderId: sliderId,
				sliderOptMarkup: function(item){
					return el( CwImageSliderOption, {...props,
						image: item,
						images: images
					})
				},
				// sliderCaptionMarkup: function(item){
				// 	return el( CwImageSliderOption, {...props,
				// 		image: item,
				// 		images: images
				// 	})
				// },
				//
				// slideCaptions : slideCaptions,

				// sliderCaptionMarkup: function(item){
				// 	console.log('sliderCaptionMarkup', sliderCaptionMarkup);
				// 	if(!sliderCaptionMarkup) return;
				//
				// 	return sliderCaptionMarkup(item);
				// },
			},
			(item) => {
				imageIndex++;

				let image = item;
				let alt = getImageAlt(image);
				let class_name = lazy_load ? 'slide cw-lazy-load text-unset': 'slide text-unset';

				let sizedImg = typeof image.sizes[imgSize] !== 'undefined' ?
					typeof image.sizes.large !== 'undefined' ? image.sizes.large :
						typeof image.sizes.medium !== 'undefined' ?
							image.sizes.medium : image.sizes.thumbnail : image;


				let useDropShadow = useShadow && !image.url.endsWith(".png");
				// useShadow = !image.url.endsWith(".png");

				// show first image by default
				class_name = imageIndex === 1 ? class_name + " show" : class_name;

				// The SLIDE
				//-----------------
				// console.log(image);
				return el( 'div', { className: class_name,
						// ['data-update']: '230529',
						['data-id']: imageIndex, // slider relies on a 1,2,3,4,5 index sequence; image.id won't work
					},
					el('img', {
							className: useDropShadow ? 'shadow-wrap-3' : null,
							src: lazy_load ? null : sizedImg.url,
							alt: alt && image.caption === '' ? alt : '',
							['data-src']: sizedImg.url
						}
					),
					image.caption !== '' ? el('p', {className: 'image-caption'}, image.caption) : null
				)
			}
		)
	}
}

/**
 * CwbBlockSlider  (new repeater style)
 * --------------------------
 * - Slider Built to support/display InnerBlocks content on the front-end
 * - CwbSlider cannot do this
 *
 * supports InnerBlocks
 * used by: everything else
 */
class CwbBlockSlider extends Component {

	init(props) {
		this.props = props.props;
	}

	render() {

		let props = this.props;
		let attributes = this.props.attributes;
		let repItemBlocks = __get(attributes.repItemBlocks, []);
		let useStickyBuffer = __get(attributes.useStickyBuffer, false);
		let heightCss = __get(attributes.heightCss, []);
		let sliderId = __get(attributes.sliderId, false);
		// let sliderOptMarkup = typeof props.sliderOptMarkup !== 'undefined'
		// 	? props.sliderOptMarkup : false; // ~todo comment











		// TODO pass through CwbBlockSlider props and make below default
		//  - dont forget data-control-key
		let sliderClass = [
			'slider',
			'hero-slider',
			...heightCss,
		];

		if(useStickyBuffer) sliderClass.push('sticky-buffer-parent');


		let itemCount = 0;



		// console.log('attributes.cssClasses', attributes.cssClasses);


		return el( 'div', {
				'id': 'hero-slider-'+sliderId,
				className: sliderClass.join(' '),
				['data-control-key']: 'hero-slider',
				['data-video-slider']: attributes.hasVideo,
				['data-control-index']: sliderId // TODO
			},

			// useStickyBuffer && headerIsOpaque ? el( 'div', { className: 'sticky-buffer-here' }) : null,

			el( 'div', { className: 'slides-wrap mb-0'},

				el( 'div', { className: 'slides'},
					//  el( 'div', { className: 'slider-arrows', style: {zIndex: 1}},
					// below overrides core slider styles; consider updating core (php) slider with a similar
					el( 'div', { className: 'slider-arrows d-flex flex-column _align-items-center w-100', style: {zIndex: 1}},

						el( 'div', { className: useStickyBuffer ? 'sticky-buffer-here' : ''}),

						// below overrides core slider styles; consider updating core (php) slider with a similar
						el( 'div', { className : 'd-flex align-items-center w-100',
								style : { height: '100%' }},

							// el( 'div', { className: 'slider-arrows-inner',
							// below overrides core slider styles; consider updating core (php) slider with a similar
							el( 'div', { className: 'slider-arrows-inner d-block w-100',
									style : { height: 'auto' }},

								el( 'div', { className: 'slide-prev slider-arrow',
									tabindex: 0,
									['aria-label']: 'View Previous Slide',
									role: 'button'
								}),
								el( 'div', { className: 'slide-next slider-arrow',
									tabindex: 0,
									['aria-label']: 'View Next Slide',
									role: 'button'
								}),
							),
						)
					),

					el( 'div', { className: 'fade-wrapper',
							role: 'list'},


						props.children
					)
				),

				el( 'div', { className: 'slider-nav ez-row   opacity-0 anim-opacity',
						role: 'listbox',
						['aria-label']: 'slider'
					},
					el( 'div', { className: 'slider-nav-inner ez-col'},
						el( 'div', { className: 'ez-row'},
							el( 'div', { className: 'slide-prev slider-arrow ez-col',
								tabindex: 0,
								['aria-label']: 'View Previous Slide',
								role: 'button'
							}),
							el( 'div', { className: 'slider-options ez-col'},

								// // // Slide captions
								// // // todo: infrastructure set up; not tested; not sure if feature needed; captions can simply be inside the slide markup
								// slideCaptions ?
								// 	items.map((item) => {
								//
								// 		// console.log(item);
								//
								// 		return item.caption !== '' ?
								// 			slideCaptionMarkup ?
								// 				slideCaptionMarkup(item) : el( 'div', {
								// 					className: 'slider-caption',
								// 					['data-id']: item.key+1
								// 				}, el('p', {},'test'))
								// 		: null
								//
								// 		// return item.caption !== '' ? el( 'div', {
								// 		// 		className: 'slider-caption',
								// 		// 		['data-id']: item.key+1
								// 		// 	},
								// 		// 	el('p', {},'test')
								// 		// ) : null
								//
								// 		// Option UI element
								// 		// return slideCaptionMarkup ? slideCaptionMarkup(item) : null
								//
								// 	})
								// : null,


								// Slide options
								el( 'div', { className: 'margin-wrap'},
									repItemBlocks.map((item) => {

										itemCount++;

										// Option UI element
										return el( 'div', {
											className: 'slider-option',
											['data-id']: itemCount,
											role: 'option'
										})
									})
								),
							),
							el( 'div', { className: 'slide-next slider-arrow ez-col',
								tabindex: 0,
								['aria-label']: 'View Next Slide',
								role: 'button'
							})
						)
					)
				)
			)
		);
	}
}

// todo: consider depreciating (below)
//  - used by: StudioV; DP&O (& nothing else going forward)

/**
 * Get BG Obj (old repeater style)
 * @param objVar
 * @returns {*}
 *
 * - currently used to configure conditional lazyLoad on element using an image as a BG
 * - attempted to use this for alt text but logic is incomplete
 * 		- flagged but abandoning due to eventual depreciation of
 * 		  cwb-slider-block, which was based on the old repeater style
 * - only used by: CwbSlide
 */
const getBgObj = function (objVar){

	if( typeof objVar === 'undefined') return false;

	return typeof objVar === 'object' && !Array.isArray(objVar) && objVar !== null ? {
		type : typeof objVar.type !== 'undefined' ? objVar.type : false,
		value : typeof objVar.value !== 'undefined' ? objVar.value : false,
		lazyLoad : typeof objVar.lazyLoad !== 'undefined' ? objVar.lazyLoad : false
	} : false;
};

/**
 * Slider Edit Block (old repeater style)
 * --------------------------
 * @prop items (array)
 * @prop currentKey (int)
 *
 * A slider navigation component for the block editor
 *
 * usage:
 * - used for displaying and navigating slides in the block editor
 * - used by: StudioV; DP&O (by nothing else going forward)
 */
class SliderEditBlock extends Component {

	init(props) {
		this.props = props.props;
	}

	onPrev() {
		let props = this.props;
		props.onPrev();
	}

	onNext() {
		let props = this.props;
		props.onNext();
	}

	onOptionClick(itemKey) {
		let props = this.props;
		props.onOptionClick(itemKey);
	}

	render() {

		let self = this;
		let props = this.props;
		let currentKey = props.currentKey;
		let items = props.items;
		let imageCount = 0;
		let optionCount = 0;
		// console.log(props);
		// let focusItem = attributes.items[ attributes.focusItem !== null ? attributes.focusItem : 0 ];

		return el( 'div', {
				className: 'cwb-slider',
				// dataControlKey: 'category-post-slider'
			},

			el( 'div', { className: 'slider-wrap'},

				el( 'div', { className: 'slides'},

					el( 'div', { className: 'd-flex align-items-center'},



						// SLIDER Nav Controls
						//========================

						el( 'div', {
							className: 'slide-prev slider-arrow',
							style: {
								position: 'absolute'
							},
							onClick: function(){
								self.onPrev();
							}
						}),

						el( 'div', {
							className: 'slide-next slider-arrow',
							style: {
								position: 'absolute'
							},
							onClick: function(){
								self.onNext();
							}
						}),

						el( 'div', {className: 'slider-options',},
							el( 'div', {className: 'slider-options-inner',},
								items.map((item) => {

									optionCount++;
									let itemKey = optionCount - 1;

									return el( 'div', {
											className: itemKey === currentKey ? 'slider-option active' : 'slider-option',
											['data-key']: itemKey,
											onClick: function(){
												self.onOptionClick(itemKey)
											}
										},
									)
								})
							)
						),



						// SLIDES
						//========================

						items.map((slide) => {

							imageCount++;
							let imageKey = imageCount - 1;
							// if(imageKey === currentKey)
							// 	console.log('slide', imageKey , currentKey);

							return imageKey === currentKey ?
								props.children(slide) : // pass item obj to child slide element
								null;
						})
					)
				)
			)
		)
	}
}

/**
 * CWB Slide Tint (old repeater style)
 * --------------------------
 * @prop tintValue
 *
 * Usage:
 * - Use in the front-end Slider markup & back-end slider editor
 * - currently only used by CwbSlide (front-end) & cwb-slider-block (back-end)
 * - used by: StudioV; DP&O (by nothing else going forward)
 *
 * todo: depreciate, used in: 'cwb/slider-block'
 */
class CwbSlideTint extends Component {

	init(props) {
		this.props = props.props;
	}

	render() {

		let props = this.props;
		let tintValue = typeof props.tintValue !== 'undefined' ? props.tintValue : 0;

		return el( 'div' , {
			style : {
				background: tintValue ? 'rgba(0,0,0,'+ tintValue/10 +')' : null,
				top: 0,
				left: 0,
				width: '100%',
				height: '100%',
				position: 'absolute'
			}
		})
	}
}

/**
 * CWB Slide (old repeater style)
 * --------------------------
 * @prop item (object)
 * @prop slideHeight
 * @prop useStickyBuffer (boolean)
 *
 * Front-end Slide markup
 * - slide content wrap that allows height, v-align, and h-align css classes to customize the layout
 *
 * Slide content is determined by the component's children
 *
 * Usage:
 * - Use in the front-end Slider markup
 * - used by: StudioV; DP&O (by nothing else going forward)
 *
 * Requires:
 * - CwbSlideTint
 */
class CwbSlide extends Component {

	init(props) {
		this.props = props.props;
	}


	render() {

		let props = this.props;
		let item = typeof props.item !== 'undefined' ? props.item : false;
		let slideHeight = typeof props.slideHeight !== 'undefined' ? props.slideHeight : '';
		let useStickyBuffer = typeof props.useStickyBuffer !== 'undefined' ? props.useStickyBuffer : false;

		if(!item) return;

		let includeAltText = typeof item.bgObj !== 'undefined' ? typeof item.bgObj.alt !== 'undefined' && item.bgObj.alt : false;
		// (^) item.bgObj.alt never actually gets created so includeAltText will always be false
		// 		-
		let class_name = 'slide py-2 bg-cover';

		class_name = item.key === 0 ? class_name + " show" : class_name;
		let vAlignCssClasses = genResponsiveClasses(item.vAlign);
		let hAlignCssClasses = genResponsiveClasses(item.hAlign);
		let sliderHCssClasses = genResponsiveClasses(slideHeight);



		// Lazy BG conditional logic
		//-----------------------------------------
		// // TODO pull out of the class and use this in magic cols
		// const getBgObj = function (objVar){
		//
		// 	if( typeof objVar === 'undefined') return false;
		//
		// 	return typeof objVar === 'object' ? {
		// 		type : typeof objVar.type !== 'undefined' ? objVar.type : false,
		// 		value : typeof objVar.value !== 'undefined' ? objVar.value : false,
		// 		lazyLoad : typeof objVar.lazyLoad !== 'undefined' ? objVar.lazyLoad : false
		// 	} : false;
		// };
		let slideBgObj 	= getBgObj( item.bgObj );
		let slideLazyBg = slideBgObj && slideBgObj.type === 'image' && slideBgObj.lazyLoad;

		class_name 		+= slideLazyBg ? ' cw-lazy-bg' : '';
		class_name 		+= typeof item.cssClasses !== 'undefined' ? ' '+item.cssClasses : '';




		// The SLIDE
		//-----------------
		return el( 'div', { role: 'listitem'},

			el( 'div', {
					className: class_name,
					style: {
						// background: '#555'
						// Simple Background
						background: item.bgURL && !slideLazyBg ? "url('"+item.bgURL+"')" : item.bgCol
					},
					['data-id']: item.key+1,
					// Lazy (Load) Background
					['data-bg']: item.bgObj && slideLazyBg ? slideBgObj.value : null,

					role: includeAltText ? 'img' : null,
					['aria-label']: includeAltText ? item.bgObj.alt : null
				},



				// Slide Tint (conditional)
				//-------------------------
				item.bgTint ? el(CwbSlideTint, { tintValue : item.bgTint }) : null,

				el( 'div', { className: 'container text-white p-3 pb-5 p-md-4 pb-md-5 p-xl-5 '+sliderHCssClasses+' d-flex flex-column h-100 '},

					useStickyBuffer ? el('div', {className:'sticky-buffer-here'}) : null,

					el( 'div', { className: 'slide-content-wrap d-flex flex-grow-1 py-4 w-100 '+vAlignCssClasses},
						el( 'div', { className: 'slide-content w-100 '+hAlignCssClasses},
							props.children
						)
					)
				)
			)
		)
	}
}




/* GALLERY Components */
/*-------------------------------*/
// gallery usage:
// - SLIDER Components
// - BOOTSTRAP Components

/**
 * Slider Modal Trigger Editor
 * ------------------------------------
 * @prop props (block props)
 * @prop image
 *
 * ~ todo: below may need to be conditional at some point
 * - limits image size by setting max width
 * - keeps vertical images proportionately sized based on height v. width ration

 * ~ todo: below: only onEditClick() works for now due to bug occurring with the MediaUpload 'value' prop
 * - externally accessible editor functions(onArrowUp, onArrowDown, onEditClick, onTrash)
 *
 * use:
 * - use in block edit()
 *
 * requires:
 * 	- block props to edit images array
 */
class SliderModalTriggerEditor extends Component {

	init(props) {
		this.props = props.props;
	}


	onArrowUp() {
		let props = this.props;
		props.onArrowUp();
	}


	onArrowDown() {
		let props = this.props;
		props.onArrowDown();
	}


	onEditClick() {
		let props = this.props;
		props.onEditClick();
	}


	onTrash() {
		let props = this.props;
		props.onTrash();
	}


	render(){
		let self = this;
		let props = this.props;
		let attributes = props.attributes;
		let image = props.image;
		let useShadow = typeof props.useShadow !== 'undefined' ?
			props.useShadow : false;

		const mediaHoverComponent = function(){

			return el('div', {className:'display-on-parent-hover text-white',
					style: {
						position: 'absolute',
						width: '100%',
						height: '100%',
						background: "rgba(0,0,0,0.5)",
						top: 0,
						alignItems: 'center',
						justifyContent: 'center',
					},
					// onClick: function(){
					// 	// updateFocus(item.key.toString());
					// }
				},

				el('div', { className: 'text-white',
						style: {
							border: "1px solid rgba(255,255,255,0.5)",
							background: "rgba(0,0,0,0.5)",
						}},
					el(components.Button,{
							onClick: function(){
								self.onEditClick();
							},
						},
						el( Icon, { icon: 'edit', style: {color: 'white'} }),
					),

					// ~todo: trash & arrow UI disabled because
					//   was not able to update the block on the
					//   editor front-end outside of the onSelect option
					// el(components.Button,{
					// 		onClick: function(){
					// 			self.onArrowUp();
					// 		},
					// 	},
					// 	el( Icon, { icon: 'arrow-up-alt2', style: {color: 'white'} }),
					// ),
					// el(components.Button,{
					// 		onClick: function(){
					// 			self.onArrowDown();
					// 		},
					// 	},
					// 	el( Icon, { icon: 'arrow-down-alt2', style: {color: 'white'} }),
					// ),
					// el(components.Button,{
					// 		onClick: function(){
					// 			self.onTrash();
					// 		},
					// 	},
					// 	el( Icon, { icon: 'trash', style: {color: 'white'} }),
					// ),
				)


				// el( MediaUpload, {
				// 	label: label,
				// 	onSelect: function(media){
				// 		setItemAttr(media, focusItem, property);
				// 	},
				// 	allowedTypes: 'image',
				// 	value: item[property].url,
				// 	render: function( obj ) {
				// 		return el('div', { className: 'text-white',
				// 				style: {
				// 					border: "1px solid rgba(255,255,255,0.5)",
				// 					background: "rgba(0,0,0,0.5)",
				// 				}},
				// 			el(components.Button,{
				// 					onClick: obj.open,
				// 				},
				// 				el( Icon, { icon: 'edit', style: {color: 'white'} }),
				// 			),
				// 			el(components.Button,{
				// 					onClick: function(){
				// 						setItemAttr(false, item, property);
				// 					},
				// 				},
				// 				el( Icon, { icon: 'trash', style: {color: 'white'} }),
				// 			)
				// 		)
				// 	}
				// }),
			)
		};

		// console.log('SliderModalTriggerEditor', image);
		let WvH = image.width/image.height;
		let imgHoverEl = mediaHoverComponent();

		let useDropShadow = useShadow && !image.url.endsWith(".png");
		// console.log('SliderModalTriggerEditor', props.useShadow, image.url, !image.url.endsWith(".png"));

		return el('div',{ className: 'position-relative d-inline-block _mb-0 ',
				style : {
					maxWidth: WvH > 1 ? '100%' : (100 * WvH) + '%'
				}},
			el('img',{ src: image.url,
				className: useDropShadow ? 'shadow-wrap-3 b-0' : 'b-0',
				// style : {
				// 	maxWidth: WvH > 1 ? '100%' : (100 * WvH) + '%'
				// }
			}),
			imgHoverEl
		)
	}
}

/**
 * CW Gallery Image
 * ------------------------------------
 * @prop image
 * @prop modalId
 * @prop imageIndex (slider relies on a 1,2,3,4,5 index sequence; image.id won't work)
 *
 *
 * - limits image size by setting max width
 * - keeps vertical images proportionately sized based on height v. width ration
 * - uses "gallery" custom wp image size if available, else uses "medium"
 *
 * use:
 * - use in block save()
 * - for block edit() use: CwGalleryImageEditor
 *
 * Deps:
 * - Bootstrap
 * - JQuery
 * - cw-silder (control-slider.js)
 * - ui-sliders.php (mod theme)
 */
class CwGalleryImage extends Component {

	init(props) {
		this.props = props.props;
	}

	render() {
		let props = this.props;
		let image = props.image;
		let modalId = props.modalId;
		let imageIndex = props.imageIndex;
		let singleImage = typeof props.singleImage !== 'undefined' ?
			props.singleImage : false;
		let captionOnHover = typeof props.captionOnHover !== 'undefined' ?
			props.captionOnHover : false;
		let alt = getImageAlt(image);

		let imgSize = typeof props.imgSize !== 'undefined' ? props.imgSize : 'gallery';
		let sizedImg = typeof image.sizes[imgSize] !== 'undefined' && !singleImage ?
			image.sizes[imgSize] : !singleImage ? typeof image.sizes.large !== 'undefined' ?
				image.sizes.large : typeof image.sizes.medium !== 'undefined' ?
					image.sizes.medium : image.sizes.thumbnail : image;


		// console.log(captionOnHover, image.caption);
		let WvH = sizedImg.width/sizedImg.height;
		let extraWrapClass = typeof props.extraWrapClass !== 'undefined' ?
			props.extraWrapClass : '';
		let lazy_load = typeof props.lazyLoad !== 'undefined' ?
			props.lazyLoad : false;
		// let extraImgClass = typeof props.extraImgClass !== 'undefined' ?
		// 	props.extraImgClass : '';


		// console.log(image.sizes, image.sizes['social-crop'], image.sizes['gallery'], sizedImg.url);
		// TODO review this regarding WCAG
		return el('figure',{
				className : 'position-relative cursor-pointer d-inline-block mb-0 image-wrapper hover-zoom-wrap _w-100 w-auto '+extraWrapClass,
				// ^ '.image-wrapper' required for opening slider modal with the chosen image showing
				tabindex: 0,
				['data-toggle'] : 'modal',
				['data-target'] : '#'+modalId,
				['data-id'] : imageIndex, // slider relies on a 1,2,3,4,5 index sequence; image.id won't work
				style : {
					// keeps vertical images proportionately sized based on height v. width ration
					maxWidth: WvH > 1 ? '100%' : (100 * WvH) + '%'
				},
				['aria-haspopup'] : 'true',
				['aria-expanded'] : 'false',
				['aria-controls'] : '#'+modalId,
				['aria-label'] : 'Open Gallery Popup - Image-'+imageIndex,
				['cancel-cw-open'] : 'true' // disable cw_modal_open() logic to avoid conflicts with open_modal_slider() animation
			},
			el('div',{ className: 'anim-on-scroll anim-alpha-in' }, // requires ez-core (wp_parent_theme) v3.0.13

				el('div',{ className: lazy_load ?  'hover-zoom cw-lazy-load': 'hover-zoom' },
					el('img',{
						src: lazy_load ? null : sizedImg.url,
						alt: alt ? alt : null,

						['data-src']: sizedImg.url,
						className: 'b-0 ',//+extraImgClass, // anim-on-scroll anim-alpha-in
					}),
				),
				el('div', {className:'display-on-parent-hover text-white cursor-pointer',
						['data-event'] : true, // for event tracking
						['data-event-val'] : 'thumbnail-'+image.id,
					},

					el('div', { className: 'text-white text-center',
						style: {
							// border: "1px solid rgba(255,255,255,0.5)",
							// background: "rgba(0,0,0,0.5)",
						}},


						captionOnHover && image.caption !== '' ?
							el('p', {className: 'image-caption d-inline-block w-100 mb-2'},
								// toTitleCase(image.caption)
								image.caption
							)
							: null,
						el('p', {className: 'd-inline-block my-0'},'View Image'),
					)
				)
			)
		)
	}
}
class CwGalleryImageAlt extends Component {

	init(props) {
		this.props = props.props;
	}

	render() {
		let props = this.props;
		let image = props.image;
		let modalId = props.modalId;
		let imageIndex = props.imageIndex;
		let singleImage = typeof props.singleImage !== 'undefined' ?
			props.singleImage : false;
		let sizedImg = typeof image.sizes.gallery !== 'undefined' && !singleImage ?
			image.sizes.gallery : !singleImage ? typeof image.sizes.medium !== 'undefined' ?
				image.sizes.medium : image.sizes.thumbnail : image;
		let WvH = sizedImg.width/sizedImg.height;
		let extraWrapClass = typeof props.extraWrapClass !== 'undefined' ?
			props.extraWrapClass : '';
		let extraImgClass = typeof props.extraImgClass !== 'undefined' ?
			props.extraImgClass : '';
		let useShadow = !sizedImg.url.endsWith(".png");

		// console.log('- CwGalleryImage: useShadow', sizedImg.url, useShadow);


		return el('figure',{
				className : 'position-relative d-inline-block mb-0 image-wrapper  cursor-pointer '+extraWrapClass,
				// ^ '.image-wrapper' required for opening slider modal with the chosen image showing
				['data-toggle'] : 'modal',
				['data-target'] : '#'+modalId,
				['data-id'] : imageIndex, // slider relies on a 1,2,3,4,5 index sequence; image.id won't work
				style : {
					// keeps vertical images proportionately sized based on height v. width ration
					maxWidth: WvH > 1 ? '100%' : (100 * WvH) + '%'
				}
			},
			el('div',{ className: 'hover-scale' },
				el('div',{ className: useShadow ?
						'position-relative anim-on-scroll anim-alpha-in shadow-wrap-3'
						: 'position-relative anim-on-scroll anim-alpha-in'
					},
					el('img',{ src: sizedImg.url,
						className: 'b-0 '+extraImgClass, // anim-on-scroll anim-alpha-in
					}),
					el('div', {className:'display-on-parent-hover text-white cursor-pointer',
							['data-event'] : true, // for event tracking
							['data-event-val'] : 'thumbnail-'+image.id,
						},

						el('div', { className: 'text-white',
							style: {
								// border: "1px solid rgba(255,255,255,0.5)",
								// background: "rgba(0,0,0,0.5)",
							}}, 'View Image',
						)
					)
				),
			)
		)
	}
}

class CwVideoTrigger extends Component {

	init(props) {
		this.props = props.props;
	}

	render() {
		let props = this.props;
		let image = props.image;
		let modalId = props.modalId;
		let vidSrc = props.vidSrc;
		let imageIndex = props.imageIndex;
		let singleImage = typeof props.singleImage !== 'undefined' ?
			props.singleImage : false;
		let captionOnHover = typeof props.captionOnHover !== 'undefined' ?
			props.captionOnHover : false;
		let alt = getImageAlt(image);


		if(!image) return;

		// console.log('TTEST', image);
		let imgSize = typeof props.imgSize !== 'undefined' ? props.imgSize : 'gallery';
		let sizedImg = typeof image.sizes[imgSize] !== 'undefined' && !singleImage ?
			image.sizes[imgSize] : !singleImage ? typeof image.sizes.large !== 'undefined' ?
				image.sizes.large : typeof image.sizes.medium !== 'undefined' ?
					image.sizes.medium : image.sizes.thumbnail : image;


		// console.log(captionOnHover, image.caption);
		let WvH = sizedImg.width/sizedImg.height;
		let extraWrapClass = typeof props.extraWrapClass !== 'undefined' ?
			props.extraWrapClass : '';
		let lazy_load = typeof props.lazyLoad !== 'undefined' ?
			props.lazyLoad : false;
		// let extraImgClass = typeof props.extraImgClass !== 'undefined' ?
		// 	props.extraImgClass : '';


		// console.log(image.sizes, image.sizes['social-crop'], image.sizes['gallery'], sizedImg.url);
		// TODO review this regarding WCAG
		return el('figure',{
				className : 'video-modal-trigger position-relative cursor-pointer d-inline-block mb-0 image-wrapper hover-zoom-wrap _w-100 w-auto '+extraWrapClass,
				// ^ '.image-wrapper' required for opening slider modal with the chosen image showing
				tabindex: 0,
				['data-src'] : vidSrc.url,
				['data-toggle'] : 'modal',
				['data-target'] : '#'+modalId,
				// ['data-id'] : imageIndex, // slider relies on a 1,2,3,4,5 index sequence; image.id won't work
				style : {
					// keeps vertical images proportionately sized based on height v. width ration
					maxWidth: WvH > 1 ? '100%' : (100 * WvH) + '%'
				},
				['aria-haspopup'] : 'true',
				['aria-expanded'] : 'false',
				['aria-controls'] : '#'+modalId,
				['aria-label'] : 'Open Video Popup - Image-',//+imageIndex, TODO
				// ['cancel-cw-open'] : 'true' // disable cw_modal_open() logic to avoid conflicts with open_modal_slider() animation
			},
			el('div',{ className: 'anim-on-scroll anim-alpha-in' }, // requires ez-core (wp_parent_theme) v3.0.13

				el('div',{ className: lazy_load ?  'hover-zoom cw-lazy-load': 'hover-zoom' },
					el('img',{
						src: lazy_load ? null : sizedImg.url,
						alt: alt ? alt : null,

						['data-src']: sizedImg.url,
						className: 'b-0 ',//+extraImgClass, // anim-on-scroll anim-alpha-in
					}),
				),
				el('div', {className:'display-on-parent-hover text-white cursor-pointer',
						['data-event'] : true, // for event tracking
						['data-event-val'] : 'thumbnail-'+image.id,
					},

					el('div', { className: 'text-white text-center',
							style: {
								// border: "1px solid rgba(255,255,255,0.5)",
								// background: "rgba(0,0,0,0.5)",
							}},


						captionOnHover && image.caption !== '' ?
							el('p', {className: 'image-caption d-inline-block w-100 mb-2'},
								// toTitleCase(image.caption)
								image.caption
							)
							: null,
						el('p', {className: 'd-inline-block my-0'},'View Video'),
					)
				)
			)
		)
	}
}


class CwIFrameTrigger extends Component {

	init(props) {
		this.props = props.props;
	}

	render() {
		let props = this.props;
		let image = props.image;
		let modalId = props.modalId;
		let iFrameSrc = props.iFrameSrc;
		let imageIndex = props.imageIndex;
		let modalHeading = typeof props.modalHeading !== 'undefined' ?
			props.modalHeading : 'iFrame Modal';
		let singleImage = typeof props.singleImage !== 'undefined' ?
			props.singleImage : false;
		let captionOnHover = typeof props.captionOnHover !== 'undefined' ?
			props.captionOnHover : false;
		let alt = getImageAlt(image);

		// console.log(image);

		if(!image) return;

		// console.log('TTEST', image);
		let imgSize = typeof props.imgSize !== 'undefined' ? props.imgSize : 'gallery';
		let sizedImg = typeof image.sizes[imgSize] !== 'undefined' && !singleImage ?
			image.sizes[imgSize] : !singleImage ? typeof image.sizes.large !== 'undefined' ?
				image.sizes.large : typeof image.sizes.medium !== 'undefined' ?
					image.sizes.medium : image.sizes.thumbnail : image;


		// console.log('imgSize', imgSize);
		let WvH = sizedImg.width/sizedImg.height;
		let extraWrapClass = typeof props.extraWrapClass !== 'undefined' ?
			props.extraWrapClass : '';
		let lazy_load = typeof props.lazyLoad !== 'undefined' ?
			props.lazyLoad : false;
		// let extraImgClass = typeof props.extraImgClass !== 'undefined' ?
		// 	props.extraImgClass : '';


		// console.log(image.sizes, image.sizes['social-crop'], image.sizes['gallery'], sizedImg.url);
		// TODO review this regarding WCAG
		return el('figure',{
				className : 'iframe-trigger position-relative cursor-pointer d-inline-block mb-0 image-wrapper hover-zoom-wrap _w-100 w-auto '+extraWrapClass,
				// ^ '.image-wrapper' required for opening slider modal with the chosen image showing
				tabindex: 0,
				['data-src'] : iFrameSrc,
				['data-modal-heading'] : modalHeading,
				['data-toggle'] : 'modal',
				['data-target'] : '#'+modalId,
				// ['data-id'] : imageIndex, // slider relies on a 1,2,3,4,5 index sequence; image.id won't work
				style : {
					// keeps vertical images proportionately sized based on height v. width ration
					maxWidth: WvH > 1 ? '100%' : (100 * WvH) + '%'
				},
				['aria-haspopup'] : 'true',
				['aria-expanded'] : 'false',
				['aria-controls'] : '#'+modalId,
				['aria-label'] : 'Open Video Popup - Image-',//+imageIndex, TODO
				// ['cancel-cw-open'] : 'true' // disable cw_modal_open() logic to avoid conflicts with open_modal_slider() animation
			},
			el('div',{ className: 'anim-on-scroll anim-alpha-in' }, // requires ez-core (wp_parent_theme) v3.0.13

				el('div',{ className: lazy_load ?  'hover-zoom cw-lazy-load': 'hover-zoom' },
					el('img',{
						src: lazy_load ? null : sizedImg.url,
						alt: alt ? alt : null,

						['data-src']: sizedImg.url,
						className: 'b-0 ',//+extraImgClass, // anim-on-scroll anim-alpha-in
					}),
				),
				// el('div', {className:'display-on-parent-hover text-white cursor-pointer',
				// 		['data-event'] : true, // for event tracking
				// 		['data-event-val'] : 'thumbnail-'+image.id,
				// 	},
				//
				// 	el('div', { className: 'text-white text-center',
				// 			style: {
				// 				// border: "1px solid rgba(255,255,255,0.5)",
				// 				// background: "rgba(0,0,0,0.5)",
				// 			}},
				//
				//
				// 		captionOnHover && image.caption !== '' ?
				// 			el('p', {className: 'image-caption d-inline-block w-100 mb-2'},
				// 				// toTitleCase(image.caption)
				// 				image.caption
				// 			)
				// 			: null,
				// 		el('p', {className: 'd-inline-block my-0'},'View Video'),
				// 	)
				// )
			)
		)
	}
}


/**
 * CW Gallery Image Editor
 * ------------------------------------
 * @prop image
 * @prop modalId
 * @prop imageIndex (slider relies on a 1,2,3,4,5 index sequence; image.id won't work)
 *
 * - limits image size by setting max width
 * - keeps vertical images proportionately sized based on height v. width ration
 *
 * use:
 * - use in block edit()
 * - for block save() use: CwGalleryImage
 *
 * requires:
 * 	- block props to edit images array
 * 	- SliderModalTriggerEditor
 */
class CwGalleryImageEditor extends Component {

	init(props) {
		this.props = props.props;
	}

	render() {
		let props = this.props;
		let image = props.image;
		let mediaUploadObj = props.mediaUploadObj;
		// let gapPaddingCss = typeof props.gapPaddingCss !== 'undefined' ?
		// 	props.gapPaddingCss : '';

		// todo getImageSize()
		let imgSize = typeof props.imgSize !== 'undefined' ? props.imgSize : 'gallery';
		let sizedImg = typeof image.sizes[imgSize] !== 'undefined' ?
			image.sizes[imgSize] : typeof image.sizes.large !== 'undefined' ?
				image.sizes.large : typeof image.sizes.medium !== 'undefined' ?
					image.sizes.medium : typeof image.sizes.thumbnail !== 'undefined' ?
						image.sizes.thumbnail : image.sizes.full;

		let useShadow = typeof props.useShadow !== 'undefined' ?
			props.useShadow : false;



		// ~todo: trash & arrow UI disabled because
		//   was not able to update the block on the
		//   editor front-end outside of the onSelect option
		// const loopAndUpdate = function(callback){
		//
		// 	let newImages = [...images];
		//
		// 	for(let x=0; x < images.length;x++){
		// 		newImages = callback(x);
		// 	}
		// 	console.log('newImages', newImages, images);
		//
		// 	props.setAttributes({
		// 		images : newImages
		// 	});
		// 	// attributes = props.attributes;
		// };

		// console.log('CwGalleryImageEditor', props.useShadow);


		return el(SliderModalTriggerEditor, {
			...props,
			image: sizedImg,
			useShadow: useShadow,
			// gapPaddingCss: gapPaddingCss,
			onEditClick: function () {
				mediaUploadObj.open();
				// console.log('onEditClick wurx');
			},

			// ~todo: trash & arrow UI disabled because
			//   was not able to update the block on the
			//   editor front-end outside of the onSelect option
			// onArrowUp: function () {
			// 	loopAndUpdate(function (x) {
			// 		// console.log('loop onTrash', x);
			// 		return images;
			// 	});
			// 	console.log('onArrowUp wurx');
			// },
			// onArrowDown: function () {
			// 	loopAndUpdate(function (x) {
			// 		// console.log('loop onTrash', x);
			// 		return images;
			// 	});
			// 	console.log('onArrowDown wurx');
			// },
			// onTrash: function () {
			// 	loopAndUpdate(function (x) {
			// 		console.log('loop onTrash', x, images[x] === image);
			// 		if (images[x] === image) {
			// 			if (x > -1) { // only splice array when item is found
			// 				images.splice(x, 1); // 2nd parameter means remove one item only
			// 			}
			// 		}
			// 		return images;
			// 	});
			// 	attributes = props.attributes;
			// 	// console.log('onTrash wurx');
			// }
		})
	}
}

/**
 * CW Image Gallery (Grid)
 * ------------------------------------
 * @prop images (array)
 *
 * - distributes image objects into two arrays via distributeToCols()
 * - distributes image elements into two columns via CwAsymmetricColumnImage
 *
 * use:
 * - use in block edit()
 * - used in CwGalleryEditor
 *
 * Deps:
 * - Bootstrap
 */
class CwGridGallery extends Component {

	init(props) {
		this.props = props.props;
	}

	imageEl(image) {
		let props = this.props;
		return props.imageEl(image);
	}

	render() {
		let self = this;
		let props = this.props;
		let images = props.images;
		let gridCssClasses = typeof props.gridCssClasses !== 'undefined' ?
			props.gridCssClasses : 'width-2 width-md-3 width-lg-4';
		let gapMarginCss = typeof props.gapMarginCss !== 'undefined' ?
			props.gapMarginCss : '';
		let gapPaddingCss = typeof props.gapPaddingCss !== 'undefined' ?
			props.gapPaddingCss : '';


		return images.length ?
			images.length > 1 ?
				el('div',{ className: 'row the-gallery '+gapMarginCss},
					el('div',{ className: 'col px-0'},
						el('div',{ className: 'container px-0'},

							images.map((image) => {

								return el('div', {
									className: 'd-inline-block gallery-item '+gridCssClasses+' '+gapPaddingCss},
									self.imageEl(image)
								);
							})
						)
					)
				) : el('div',{ className: 'the-gallery'},
				self.imageEl(images[0])
				)
			: null
	}
}

/**
 * CW Image Gallery Editor (base gallery editor)
 * ------------------------------------
 * @prop images (array)
 * @prop onSelect(images)
 * @prop onClick()
 *
 * - Multi-Select MediaUpload that displays a CwAsymmetricGallery in the block editor
 *
 * use:
 * - in block edit()
 * - for block save() use: CwAsymmetricGallery
 *
 * requires:
 * - CwGridGallery (default gallery layout) can be replaced by extending component and overriding galleryEditor()
 * - MediaPlaceholder
 * - MediaUpload
 */
class CwGalleryEditor extends Component {

	init(props) {
		this.props = props.props;
	}

	onSelect(images) {
		let props = this.props;
		props.onSelect(images)
	}

	onClick() {
		let props = this.props;
		props.onClick()
	}

	galleryEditor(obj) {
		let self = this;
		let props = this.props;
		let images = props.images;
		let useShadow = typeof props.useShadow !== 'undefined' ?
			props.useShadow : false;
		let gridCssClasses = typeof props.gridCssClasses !== 'undefined' ?
			props.gridCssClasses : 'width-2 width-md-3 width-lg-4';
		let gapMarginCss = typeof props.gapMarginCss !== 'undefined' ?
			props.gapMarginCss : '';
		let gapPaddingCss = typeof props.gapPaddingCss !== 'undefined' ?
			props.gapPaddingCss : '';


		// return (() => {
		return el(CwGridGallery, {
			images : images,
			gridCssClasses 	: gridCssClasses,
			gapMarginCss 	: gapMarginCss,
			gapPaddingCss 	: gapPaddingCss,
			imageEl : function(image){

				// console.log('image.sizes', image.sizes, obj);

				let sizedImg = typeof image.sizes.large !== 'undefined' ?
					image.sizes.large : typeof image.sizes.medium !== 'undefined' ?
						image.sizes.medium : image.sizes.thumbnail;

				return images.length > 1 ?

					el(CwGalleryImageEditor, {
						//...props,
						image : image,
						imgSize : 'mobile-crop',
						mediaUploadObj : obj,
						useShadow : useShadow
					}) :

					el(SliderModalTriggerEditor, {
						...props,
						image: sizedImg,
						useShadow : useShadow,
						onEditClick: function () {
							obj.open();
						}}
					);
			},
		})
	}

	render() {
		let self = this;
		let props = this.props;
		let images = props.images;
		let useShadow = typeof props.useShadow !== 'undefined' ?
			props.useShadow : false;

		// Build Image ID array for MediaUpload value
		let imageIds = [];
		for(let i=0; i<images.length; i++){
			imageIds.push(images[i].id);
		};



		return el( MediaUpload, {
			onSelect: function (images) {
				self.onSelect(images);
			},
			allowedTypes: 'image',
			value: imageIds, // array of ids for MediaUpload (not array of img objects like MediaPlaceholder)
			gallery: true,
			multiple: true,
			render: function (obj) {


				// ~todo: trash & arrow UI disabled because
				//   was not able to update the block on the
				//   editor front-end outside of the onSelect option
				// const loopAndUpdate = function(callback){
				//
				// 	let newImages = [...images];
				//
				// 	for(let x=0; x < images.length;x++){
				// 		newImages = callback(x);
				// 	}
				// 	console.log('newImages', newImages, images);
				//
				// 	props.setAttributes({
				// 		images : newImages
				// 	});
				// 	// attributes = props.attributes;
				// };

				let galleryEditor = self.galleryEditor(obj);


				return images.length ?


					galleryEditor :

					el('div', {className: 'px-md-4'},
						el(MediaPlaceholder, {
							labels: { title: 'Gallery Images' },
							icon: el(Icon, {icon: 'format-image', style: {margin: '0 20px 0 0'}}),
							value: images,
							multiple: true,
							onSelect: function (images) {
								self.onSelect(images);
							},
							onClick: function () {
								self.onClick();
							},
						})
					)
			}
		})
	}
}





/* ASYMMETRIC GALLERY */
/*-------------------------------*/

/**
 * Distribute (Images) to Columns
 * --------------------------------
 * @param items
 * @returns {{rightCol: Array, leftCol: Array}}
 *
 * - Splits images into two even columns - sequentially, based on
 *   image height - first filling up the left column, then the right
 */
const distributeToCols = function(items){

	let leftCol = [];
	let rightCol = [];
	let heightTotal = 0;
	let leftColHTotal = 0;
	for( let i=0 ; i < items.length; i++){
		// console.log(items[i]);

		// todo getImageSize()
		let i_size = typeof items[i].sizes.medium !== 'undefined' ? items[i].sizes.medium :
			typeof items[i].sizes.thumbnail !== 'undefined' ?  items[i].sizes.thumbnail :
				items[i].sizes.full;
		heightTotal += i_size.height;
	}


	for( let x=0; x < items.length; x++){

		// todo getImageSize()
		let x_size = typeof items[x].sizes.medium !== 'undefined' ? items[x].sizes.medium :
			typeof items[x].sizes.thumbnail !== 'undefined' ?  items[x].sizes.thumbnail :
				items[x].sizes.full;

		let thisHeight = x_size.height;
		let nextHeight = x+1 < items.length ? x_size.height : 0;
		let rightColHTotal = heightTotal - leftColHTotal;
		let newLeftColHeight = leftColHTotal + thisHeight;
		let item = {...items[x], imageIndex : x};

		// console.log('------------------------');
		// console.log('x_size', x_size);
		// console.log('heightTotal', heightTotal);
		// console.log('thisHeight', thisHeight);
		// console.log('rightColHTotal', rightColHTotal);
		// console.log('leftColHTotal', leftColHTotal);
		// console.log('newLeftColHeight', newLeftColHeight);
		// console.log('-----------');
		// console.log('add to left col? ', leftColHTotal < heightTotal/2 && thisHeight < (rightColHTotal - leftColHTotal));
		// console.log('-- leftColHTotal < heightTotal/2', leftColHTotal < heightTotal/2);
		// console.log('-- thisHeight'+thisHeight+' < (rightColHTotal'+rightColHTotal+' - newLeftColHeight'+newLeftColHeight+')', thisHeight < (rightColHTotal - newLeftColHeight));
		// console.log('-- thisHeight'+thisHeight+' < (rightColHTotal'+rightColHTotal+' - leftColHTotal'+leftColHTotal+')', thisHeight < (rightColHTotal - leftColHTotal));

		// PLACE images into LEFT col
		//----------------------------------
		if(
			// - left column image.height sum should always be more than right
			(leftColHTotal < heightTotal/2

				// && keep left & right col height as even as possible;
				// -- [note] leftColHTotal (below) used to be newLeftColHeight
				// -- changed on 2023.11.23 to fix column distribution bug noticed on bezwurx
				// -- monitor test on SV site
				&& thisHeight <= (rightColHTotal - (leftColHTotal)))

			// - avoid gallery array order vs visual order mismatch
			|| (nextHeight && leftColHTotal < heightTotal/2
				// - account for case when the NEXT image.height is smaller
				&& thisHeight > nextHeight)
		){
			leftCol.push(item);
			leftColHTotal = newLeftColHeight;
		}

		// PLACE images into RIGHT col
		//----------------------------------
		else {
			rightCol.push(item);
		}
	}
	// console.log('------------------------------------------------');


	return {
		leftCol : leftCol,
		rightCol : rightCol
	}
};

/**
 * CW Asymmetric Column Image
 * ------------------------------------
 * @prop colSide (left | right)
 * @prop colImageCount
 * @prop imageEl
 *
 * - Image Wrapper for Asymmetric Gallery
 * 	- adds extra padding (via bootstrap css)
 * 	- bootstrap classes determined by column side and image count
 * 	- the goal of the padding is to create visual variety in image
 * 	  sizes regardless if two items that are next to each other are the same size.
 *
 * use:
 * - in CwAsymmetricGallery
 *
 * Deps:
 * - Bootstrap
 */
class CwAsymmetricColumnImage extends Component {

	init(props) {
		this.props = props.props;
	}

	onSelect(images) {
		let props = this.props;
		props.onSelect(images)
	}

	render() {
		let props = this.props;
		let colSide = props.colSide ;
		let colImageCount = props.colImageCount ;
		let imageEl = props.imageEl ;



		// Build image padding prefix
		// - the goal of the padding is to create visual variety
		//   in image sizes regardless if two items that are
		//   next to each other are the same size.
		let offSet 	= colSide === 'right';
		let mod3 	= (colImageCount + offSet) % 3; // returns 0, 1 or 2
		let padPref = colSide === 'right' ? 'pr-' :'pl-';
		let colPad 	= mod3 > 0 ? padPref+ (mod3 + 3) : null;


		return el(Fragment, {},
			el('div', { className: 'd-inline-block w-100 '+colPad,
					style : {
						maxWidth: '100%',
					}},
				imageEl
			),
			el('br')
		);
	}
}

/**
 * CW Asymmetric Image Gallery
 * ------------------------------------
 * @prop images (array)
 *
 * - distributes image objects into two arrays via distributeToCols()
 * - distributes image elements into two columns via CwAsymmetricColumnImage
 *
 * use:
 * - use in block edit()
 * - used in CwAsymmetricGalleryEditor
 *
 * requires:
 * - distributeToCols()
 * - CwAsymmetricColumnImage
 *
 * Deps:
 * - Bootstrap
 */
class CwAsymmetricGallery extends Component {

	init(props) {
		this.props = props.props;
	}

	imageEl(image) {
		let props = this.props;
		return props.imageEl(image);
	}

	render() {
		let self = this;
		let props = this.props;
		let images = props.images;

		// // Build Image ID array for MediaUpload value
		// let imageIds = [];
		// for(let i=0; i<images.length; i++){
		// 	imageIds.push(images[i].id);
		// }

		let colImages = distributeToCols(images);
		let leftCol = colImages.leftCol;
		let rightCol = colImages.rightCol;
		let leftCount = 0;
		let rightCount = 0;



		return images.length ?
			images.length > 1 ?
				el('div',{ className: 'row the-gallery'},
					el('div',{ className: 'col pr-1 text-right'},
						leftCol.length ? leftCol.map((image) => {

							leftCount++;
							return el(CwAsymmetricColumnImage,{
								colSide : 'left',
								colImageCount : leftCount,
								imageEl : self.imageEl(image) //props, obj)
								// imageEl : galleryImageComponent(image, obj) //props, obj)
							});
						}) : null,
					),
					el('div',{ className: 'col pl-1 pt-5 text-left'},
						rightCol.length ? rightCol.map((image) => {

							rightCount++;
							return el(CwAsymmetricColumnImage,{
								colSide : 'right',
								colImageCount : rightCount,
								imageEl : self.imageEl(image) //props, obj)
								// imageEl : galleryImageComponent(image, obj) //props, obj)
							});
						}) : null,
					)
				) : el('div',{ className: 'the-gallery'},
					self.imageEl(images[0])
				)
			: null
	}
}

/**
 * CW Asymmetric Image Gallery Editor
 * ------------------------------------
 * @prop images (array)
 * @prop onSelect(images)
 * @prop onClick()
 *
 * - Multi-Select MediaUpload that displays a CwAsymmetricGallery in the block editor
 *
 * use:
 * - in block edit()
 * - for block save() use: CwAsymmetricGallery
 *
 * requires:
 * - CwAsymmetricGallery
 * - MediaPlaceholder
 * - MediaUpload
 */
class CwAsymmetricGalleryEditor extends CwGalleryEditor {

	galleryEditor(obj) {
		let self = this;
		let props = this.props;
		let images = props.images;
		let useShadow = typeof props.useShadow !== 'undefined' ?
			props.useShadow : false;


		// return (() => {
		return el(CwAsymmetricGallery, {
			images : images,
			imageEl : function(image){

				let sizedImg = typeof image.sizes.large !== 'undefined' ?
					image.sizes.large : typeof image.sizes.medium !== 'undefined' ?
						image.sizes.medium : image.sizes.thumbnail;

				return images.length > 1 ?

					el(CwGalleryImageEditor, {
						//...props,
						image : image,
						// imgSize : 'social-crop',
						mediaUploadObj : obj,
						useShadow : useShadow
					}) :

					el(SliderModalTriggerEditor, {
						...props,
						image: sizedImg,
						useShadow : useShadow,
						onEditClick: function () {
							obj.open();
						}}
					);
			},
		})
		// })()
	}



	// TODO: depreciated remove if bezwurx & studioverticale galleryies done break by Apr 2024

	// init(props) {
	// 	this.props = props.props;
	// }
	//
	// onSelect(images) {
	// 	let props = this.props;
	// 	props.onSelect(images)
	// }
	//
	// onClick() {
	// 	let props = this.props;
	// 	props.onClick()
	// }
	//
	// render() {
	// 	let self = this;
	// 	let props = this.props;
	// 	let images = props.images;
	// 	let useShadow = typeof props.useShadow !== 'undefined' ?
	// 		props.useShadow : false;
	//
	// 	// Build Image ID array for MediaUpload value
	// 	let imageIds = [];
	// 	for(let i=0; i<images.length; i++){
	// 		imageIds.push(images[i].id);
	// 	};
	//
	//
	//
	// 	return el( MediaUpload, {
	// 		onSelect: function (images) {
	// 			self.onSelect(images);
	// 		},
	// 		allowedTypes: 'image',
	// 		value: imageIds, // array of ids for MediaUpload (not array of img objects like MediaPlaceholder)
	// 		gallery: true,
	// 		multiple: true,
	// 		render: function (obj) {
	//
	//
	// 			// ~todo: trash & arrow UI disabled because
	// 			//   was not able to update the block on the
	// 			//   editor front-end outside of the onSelect option
	// 			// const loopAndUpdate = function(callback){
	// 			//
	// 			// 	let newImages = [...images];
	// 			//
	// 			// 	for(let x=0; x < images.length;x++){
	// 			// 		newImages = callback(x);
	// 			// 	}
	// 			// 	console.log('newImages', newImages, images);
	// 			//
	// 			// 	props.setAttributes({
	// 			// 		images : newImages
	// 			// 	});
	// 			// 	// attributes = props.attributes;
	// 			// };
	//
	//
	// 			return images.length ?
	//
	// 				el(CwAsymmetricGallery, {
	// 					images : images,
	// 					imageEl : function(image){
	//
	// 						let sizedImg = typeof image.sizes.large !== 'undefined' ?
	// 							image.sizes.large : image.sizes.medium;
	//
	// 						// console.log('CwAsymmetricGallery', props.useShadow);
	//
	// 						return images.length > 1 ?
	//
	// 							el(CwGalleryImageEditor, {
	// 								//...props,
	// 								image : image,
	// 								mediaUploadObj : obj,
	// 								useShadow : useShadow
	// 							}) :
	//
	// 							el(SliderModalTriggerEditor, {
	// 								...props,
	// 								image: sizedImg,
	// 								useShadow : useShadow,
	// 								onEditClick: function () {
	// 									obj.open();
	// 								}}
	// 							);
	// 					},
	// 				}) :
	//
	// 				el('div', {className: 'px-md-4'},
	// 					el(MediaPlaceholder, {
	// 						labels: { title: 'Gallery Images' },
	// 						icon: el(Icon, {icon: 'format-image', style: {margin: '0 20px 0 0'}}),
	// 						value: images,
	// 						multiple: true,
	// 						onSelect: function (images) {
	// 							self.onSelect(images);
	// 						},
	// 						onClick: function () {
	// 							self.onClick();
	// 						},
	// 					})
	// 				)
	// 		}
	// 	})
	// }
}





/* Experimental Stuff */
/*-------------------------------*/

// https://www.pluralsight.com/guides/just-plain-react/#module-plainreactstate
class StateChangeExperiment extends Component {

	state = {
		myInteger: 0
	};

	init(props) {
		this.props = props.props;
	}


	getRandomInteger() {
		const randomInt = Math.floor(Math.random()*100);
		// let self = this;

		// console.log('randomInt', randomInt);

		this.setState({
			myInteger: randomInt
		},() => {
			// console.log('myInteger', this.state.myInteger);
		});

	}

	render() {
		let self = this;
		let repeater = this;



		// RETURN:
		//================================
		return el( 'div', {},
			'int: '+ self.state.myInteger,

			el( 'div', {
					style: {
						padding: '20px'
					},
				},

				el( components.Button, {
						className: 'button button-large cursor-pointer',
						onClick: function(){
							repeater.getRandomInteger()
						}
					}
					, 'Random Int' )
			)
		)
	}
}

/**
 * CW Magic Columns
 * ------------------------------------
 * @prop containerWidth
 * @prop leftColWidth (25 < leftColWidth > 75)
 * @prop colHeightCssClasses
 * @prop layout
 *
 * @prop rowCss
 * @prop labelCss
 *
 * @prop labelContent
 * @prop labelContentCss
 *
 * - configurable Magic Columns Layout
 *
 * use:
 * -
 *
 * Deps:
 * - Bootstrap
 * - mod-theme-design-base.css
 *
 * TODOs:
 * 	- labelStyles (@prop for label bg color)
 * 	- labelBg Effect (@prop)
 *
 */
class MagicLabel extends Component {

	init(props) {
		this.props = props.props;
	}

	render(){
		let props = this.props;
		let containerWidth 		= __get(props.containerWidth, 1400); // 1400
		let leftColWidth		= __get(props.leftColWidth, 33); // 25 < leftColWidth > 75
		let layout 				= __get(props.layout, 'left');


		let colWidthDenominator = 100/leftColWidth ;//3;
		let widthOffset 		= (containerWidth/colWidthDenominator);
		let wideScreenOffset 	= ((containerWidth/2)-widthOffset);



		let rowCss 				= __get(props.rowCss, []);
		let labelCss 			= __get(props.labelCss, []);
		let labelContent 		= __get(props.labelContent, 'no content passed');
		let labelContentCss 	= __get(props.labelContentCss, []);


		// Prep row css classes
		rowCss.push(
			'row',
			'magic-columns',
			'w-100',
			'mx-0 ',
			'magic-label-row',
		);
		if( layout === 'left')
			rowCss.push(
				'pr-5',
			);
		if( layout === 'right')
			rowCss.push(
				'pl-5',
				'flex-row-reverse'
			);


		labelContentCss.push(
			'text-left',
		);


		// Prep Magic-Spacer
		let spacerMax = 'calc(50vw - '+(containerWidth/2)+'px)';
		// console.log('spacerMax', spacerMax);
		let magicSpacer = (() => {
			return el('div', { className: 'magic-spacer vw-100 _trace-green',
				style: {
					maxWidth: spacerMax,
					// important: 'true'
					// ['max-width']: '400px',
				}
			});
		})();




		let labelWrapCss = 'magic-label-wrap _trace-red magic-column';
		if(widthOffset < (containerWidth/2)) {
			labelWrapCss += layout === 'left' ?
				' magic-small':
				' magic-large';
		}
		if (widthOffset > (containerWidth/2)){
			labelWrapCss += layout === 'left' ?
				' magic-large' :
				' magic-small';
		}
		labelWrapCss += layout === 'left' ?
			' magic-small':
			' magic-large';

		if(layout === 'left'){
			labelWrapCss += ' text-left';
		}
		if(layout === 'right'){
			labelWrapCss += ' text-right';
		}




		return el( 'div', { className: rowCss.join(' '),
				['data-layout'] : layout },

			el( 'div', { className: labelWrapCss,
					style: {
						maxWidth: layout === 'left' ?
							'calc(50vw - '+wideScreenOffset+'px)' :
							'calc(50vw + '+wideScreenOffset+'px)',
					}},

				el( 'div', { className: 'magic-label '+labelCss.join(' ') },

					layout === 'left' ? magicSpacer : null,

					el( 'div', { className: '_magic-content '+labelContentCss.join(' '),
							style: {
								// width: layout === 'right' ? '100vw' : null,
								maxWidth: layout === 'left' ?
									widthOffset : containerWidth-widthOffset}
						},
						labelContent
					),

					layout === 'right' ? magicSpacer : null,

				)
			)
		);
	}
}

/**
 * CW Magic Columns
 * ------------------------------------
 * @prop containerWidth
 * @prop leftColWidth (25 < leftColWidth > 75)
 * @prop colHeightCssClasses
 * @prop reverseOnMobile (boolean)
 *
 * @prop leftColContent
 * @prop rightColContent
 *
 * @prop leftColBg
 * @prop rightColBg
 *
 * @prop leftColHover
 * @prop rightColHover
 *
 * - configurable Magic Columns Layout
 *
 * use:
 * -
 *
 * Deps:
 * - Bootstrap
 * - mod-theme-design-base.css
 *
 * TODOs:
 * * used in sandbox only
 * - TODO leftColWidth --> firstColWidth
 * - TODO rightColWidth --> secondColWidth
 * - todo: fallback (x4)
 *
 * ** current not used anywhere but keeping it just in case. It may come in handy later
 */
class MagicColumns extends Component {

	init(props) {
		this.props = props.props;
	}

	render(){
		let props = this.props;
		let containerWidth 		= props.containerWidth; // 1400 todo: fallback
		let leftColWidth		= props.leftColWidth; // 25 < leftColWidth > 75 todo: fallback
		let colHeightCssClasses = typeof props.colHeightCssClasses !== 'undefined' ? props.colHeightCssClasses : 'vh-50-min';
		let reverseOnMobile 	= typeof props.reverseOnMobile !== 'undefined' ? props.reverseOnMobile : false;
		let leftColContent 		= props.leftColContent; // todo: fallback
		let rightColContent 	= props.rightColContent; // todo: fallback
		let leftColBg 			= typeof props.leftColBg !== 'undefined' ? props.leftColBg : false;
		let rightColBg 			= typeof props.rightColBg !== 'undefined' ? props.rightColBg : false;
		let leftColHover 		= typeof props.leftColHover !== 'undefined' ? props.leftColHover : false;
		let rightColHover 		= typeof props.rightColHover !== 'undefined' ? props.rightColHover : false;
		let leftContentCss 		= typeof props.leftContentCss !== 'undefined' ? props.leftContentCss : false;
		let rightContentCss 	= typeof props.rightContentCss !== 'undefined' ? props.rightContentCss : false;
		let colWidthDenominator = 100/leftColWidth ;//3;
		let widthOffset 		= (containerWidth/colWidthDenominator);
		// !! TODO leftColWidth --> firstColWidth
		// !! TODO rightColWidth --> secondColWidth

		let rowCss = 'row magic-columns';
		let leftColCss 	= typeof props.leftColCss !== 'undefined' ? props.leftColCss : '';
		let rightColCss = typeof props.rightColCss !== 'undefined' ? props.rightColCss : '';
		leftColCss 		= 'col magic-column bg-cover pr-md-0 d-flex justify-content-end '+leftColCss;
		rightColCss 	= 'col magic-column bg-cover pl-md-0 '+rightColCss;


		let wideScreenOffset = ((containerWidth/2)-widthOffset);


		if(reverseOnMobile){
			rowCss += ' flex-column-reverse flex-md-row';
		}

		if(widthOffset < (containerWidth/2)) {
			leftColCss += ' magic-small';
			rightColCss += ' magic-large';
		}
		if (widthOffset > (containerWidth/2)){
			leftColCss += ' magic-large';
			rightColCss += ' magic-small';
		}





		// // Lazy BG conditional logic
		// //-----------------------------------------
		let leftColBgObj 	= getBgObj( props.leftColBg );
		let rightColBgObj 	= getBgObj( props.rightColBg );
		let leftColLazyBg 	= leftColBgObj && leftColBgObj.type === 'image' && leftColBgObj.lazyLoad;
		let rightColLazyBg 	= rightColBgObj && rightColBgObj.type === 'image' && rightColBgObj.lazyLoad;
		//
		leftColCss 		+= leftColLazyBg ? ' cw-lazy-bg' : '';
		rightColCss 	+= rightColLazyBg ? ' cw-lazy-bg' : '';




		return el( 'div', {
				className: rowCss,
			},

			el( 'div', { className: leftColCss,
					style: {
						maxWidth: 'calc(50% - '+ wideScreenOffset +'px)',

						// Simple Background : string
						// background: leftColBg && !leftColLazyBg ? leftColBg : null,
						background: leftColBg && !leftColLazyBg ? leftColBgObj ? leftColBgObj.value : leftColBg : null,
					},
					// Lazy (Load) Background : obj
					['data-bg']: leftColBg && leftColLazyBg ? leftColBgObj.value : null

				},

				//
				//-----------------------------------------
				el( 'div', { className: 'w-100 magic-content '+leftContentCss+' '+colHeightCssClasses+' d-flex align-items-center justify-content-start ',
						style: {
							maxWidth: widthOffset + 'px'
						}
					},

					//
					//-----------------------------------------
					el( 'div', { className: 'w-100 text-left _trace-blue w-100'},
						leftColContent
					)
				),

				leftColHover ? leftColHover : null
			),

			el( 'div', { className: rightColCss,
					style: {
						maxWidth: 'calc(50% + '+ wideScreenOffset +'px)',
						// Simple Background
						// background: rightColBg && !rightColLazyBg ? rightColBg : null
						background: rightColBg && !rightColLazyBg ? rightColBgObj ? rightColBgObj.value : rightColBg : null,
					},
					// Lazy (Load) Background
					['data-bg']: rightColBg && rightColLazyBg ? rightColBgObj.value : null
				},

				//
				//-----------------------------------------
				el( 'div', { className: 'w-100 magic-content '+rightContentCss+' '+colHeightCssClasses+' d-flex align-items-center justify-content-start ',
						style: {
							maxWidth: (containerWidth-widthOffset) + 'px'
						}
					},

					//
					//-----------------------------------------
					el( 'div', { className: 'w-100 text-left _trace-blue w-100'},
						rightColContent
					)
				),

				rightColHover ? rightColHover : null
			)
		);
	}
}



/* Inspector Text Fields */
/*-------------------------------*/

/**
 * Inspector Text
 * --------------------------
 * @prop props (block props)
 * @prop attName (string)
 *
 * A DRYer way to generate inspector text input fields
 *
 * usage:
 * - use inside the InspectorControls component
 * - attName determines the block attribute to be edited
 *
 * Required Block Attributes:
 * - specified via attName prop
 */
class InspectorText extends Component {

	init(props) {
		this.props = props.props;
	}

	render(){
		let props = this.props;
		let attributes = props.attributes;
		let attName = props.attName;
		let label = props.label;

		return el( TextControl, {
				label: typeof label !== 'undefined' ? label : null,
				value: attributes[attName],
				onChange: function(value){
					props.setAttributes({
						[attName] : value
					})
				},
			}
		);
	}
}

/**
 * Inspector Text Input
 * ------------------------------------
 * @prop props (blockprops)
 * @prop label
 * @prop currentValue
 * @prop onChange(value)
 *
 * -
 *
 * use:
 * - use in InspectorControls (block editor)
 *
 * requires:
 * - TextControl
 *
 * TODOs:
 * - todo: this could be redundant. consider removing (only used in CwbButtonEditor)
 */
class InspectorTextInput extends Component {

	init(props) {
		this.props = props.props;
	}

	onChange(value) {
		let props = this.props;
		props.onChange(value);
	}

	render(){
		let self = this;
		let props = this.props;
		let label = props.label;
		let currentValue = props.currentValue;
		let help = props.help;

		return el( TextControl, {
				label: typeof label !== 'undefined' ? label : null,
				value: currentValue,
				help: typeof help !== 'undefined' ? help : null,
				onChange: function(value){
					self.onChange(value)
				},
			}
		);
	}
}

/**
 * CW Rich Text Input
 * ------------------------------------
 * @prop label
 * @prop currentValue
 * @prop onChange(value)
 *
 * use:
 * - use in InspectorControls (block editor)
 * 	- when line breaks, and bold/italic options are necessary to include in the input
 *
 * requires:
 * - RichText
 */
class RichTextInput extends Component {

	init(props) {
		this.props = props.props;
	}

	onChange(value) {
		let props = this.props;
		props.onChange(value);
	}

	render() {
		let self = this;
		let props = this.props;
		let label = props.label;
		let currentValue = props.currentValue;

		return el('div',{className: 'w-100'},
			el('label', { className: 'f-sz-14 f-w-400 mt-3'}, label),
			el(RichText, {
					className: 'mt-0 mb-2 w-100',
					style: {
						padding: '5px',
						border: '1px solid #777',
						borderRadius: '2px',
					},
					// help: 'located above the heading',
					value: currentValue,
					placeholder: 'Type content here',
					onChange: function (value) {
						self.onChange(value);
					},
				}
			)
		)
	}
}





/* Inspector Media Fields */
/*-------------------------------*/

/**
 * Inspector Media Input
 * ------------------------------------
 * @prop currentValue
 * @prop onSelect(value)
 * @prop onRemove()
 *
 * use:
 * - use in InspectorControls (block editor)
 *
 * requires:
 * - MediaUpload
 */
class InspectorMediaInput extends Component {

	init(props) {
		this.props = props.props;
	}

	onSelect(value) {
		let props = this.props;
		props.onSelect(value);
	}

	onRemove() {
		let props = this.props;
		props.onRemove();
	}

	render() {
		let self = this;
		let props = this.props;
		let currentValue = props.currentValue;
		let allowedTypes = typeof props.allowedTypes !== 'undefined' ? props.allowedTypes : 'image';
		let label = typeof props.label !== 'undefined' ? props.label : 'Choose Image';

		return el( MediaUpload, {
				// extra links: https://www.liip.ch/en/blog/add-an-image-selector-to-a-gutenberg-block
				onSelect: function(value){
					self.onSelect(value);
				},
				allowedTypes: allowedTypes,
				value: currentValue,
				render: function( obj ) {
					return ! currentValue ?

						// UI: Choose Image (conditional)
						//-----------------------------
						el( 'div', null,
							el( components.Button, {
									className: 'editor-post-featured-image__toggle',
									style: {margin:'20px 0'},
									onClick: obj.open,
								},
								__( label, 'gutenberg-examples' )
							)
						) :

						// UI: Manage BG Image (conditional)
						//-----------------------------
						el( 'div', null,

							// UI: Preview / Replace Image
							//-----------------------------
							el( 'div', {
									className: 'image-button cursor-pointer',
									style: {margin:'20px 0'},
									onClick: obj.open,
								},
								el( 'img', {
									src: typeof currentValue === 'string' ? currentValue : currentValue.url }
								)
							),

							// UI: Remove Image
							//-----------------------------
							el( components.Button, {
									className: 'button-large',
									style: {margin:'0 0 20px'},
									onClick: function(){
										self.onRemove();
									},
									isTertiary: !0,
									// isLink: !0,
									isDestructive: !0,
								},
								__( 'Remove Image', 'gutenberg-examples' )
							),
						)
				}
			}
		)
	}
}





/* Block Text Editors */
/*-------------------------------*/

/**
 * Rich Text Block Input
 * ------------------------------------
 * @prop currentValue
 * @prop onChange(value)
 * @prop onClick(value) (used by repeater focus-item logic)
 *
 * - (?) TODO can this be depreciated now that the tagName property of RichText has been discovered?
 * - Allows text editing directly from the block
 *
 * use:
 * - use in block edit()
 * - .is-editable allows input markup to inherit parent styles
 * - intended for use inside p, h1, h2, h3, ...etc tags
 *
 * requires:
 * - RichText
 * - cwb-block-editor-mods.css
 */
class RichTextBlockInput extends Component {

	init(props) {
		this.props = props.props;
	}

	onChange(value) {
		let props = this.props;
		props.onChange(value);
	}

	// onClick(value) { // commented out 2022.12.26 (remove if nothing breaks)
	// 	let props = this.props;
	// 	props.onClick(value);
	// }
	onClick() {
		let props = this.props;
		if(typeof props.onClick !== 'undefined' )
			props.onClick();
	}

	render() {
		let self = this;
		let props = this.props;
		let currentValue = props.currentValue;
		let multiLine = typeof props.multiLine !== 'undefined' ? props.multiLine : true;
		let label = typeof props.label !== 'undefined' ? props.label : false;
		let style = typeof props.style !== 'undefined' ? props.style : {};


		let RichTextProps = {
			style: style,
			value: currentValue,
			className: 'is-editable',
			placeholder: 'Type content here',
			onChange: function (value) {
				self.onChange(value);
			},
			onClick: function (value) {
				self.onClick(value);
			},
		};
		if(!multiLine){
			RichTextProps = {
				...RichTextProps,

				// Prevent LINE BREAKS
				// - https://micheal.dev/blog/preventing-line-breaks-in-richtext-component/
				onReplace: function(){},
				onSplit: function(){}
			}
		}

		return label ?
			el('div', {},
				el('label', {className: 'components-base-control__label'}, label),
				el(RichText, RichTextProps)
			)
			: el(RichText, RichTextProps);
	}
}

/**
 * Block Rich Text
 * --------------------------
 * @prop props (block props)
 * @prop attName (string)
 *
 * - Simplifies RichTextBlockInput so that only block-props + the attribute name are required as input
 * 	(removes onChange & onClick as required props; for faster cleaner custom block coding
 *
 * usage:
 * - used for displaying and editing text directly in the block editor
 * - Wrap this input in a text tag: (p, h1, h2, ...)
 * 		- input takes on the styling of parent element (see: ceb-editor-mods.css)
 * 		- (!) TODO	tagName property removes the need for wrapping this element
 *
 * requires:
 * - RichTextBlockInput
 *
 * Required Block Attributes:
 * - specified via attName prop
 */
class BlockRichText extends Component {

	init(props) {
		this.props = props.props;
	}

	// onChange(value) {
	// 	let props = this.props;
	// 	props.onChange(value);
	// }

	render(){
		let props = this.props;
		let attributes = props.attributes;
		let attName = props.attName;
		let multiLine = typeof props.multiLine !== 'undefined' ? props.multiLine : true;


		return el(RichTextBlockInput, { // <-- TODO use RichText once tagName is used; depreciate: RichTextBlockInput
			// tagName: props,tabName, // <-- TODO
			currentValue: attributes[attName],
			multiLine: multiLine,
			onChange: function (value) {
				props.setAttributes({
					[attName] : value
				});
				attributes = props.attributes;
			}
		});
	}
}





/* Block Media Editors */
/*-------------------------------*/

/**
 * Media Upload Hover
 * ------------------------------------
 * @prop currentValue
 * @prop allowedTypes
 * @prop onSelect(value)
 * @prop onRemove()
 * @prop onClick() (used by repeater focus-item logic)
 *
 * - Allows input of a media file directly from the block editor
 * - only images currently supported
 *
 * use:
 * - include after image element, parent element position needs tp be relative
 * - use in MediaUploadHover
 *
 * requires:
 * - MediaUpload
 */
class MediaUploadHover extends Component {

	init(props) {
		this.props = props.props;
	}

	onSelect(value) {
		let props = this.props;
		props.onSelect(value);
	}

	onRemove() {
		let props = this.props;
		props.onRemove();
	}

	onClick() {
		let props = this.props;
		props.onClick();
	}

	render() {
		let self = this;
		let props = this.props;
		let currentValue = props.currentValue;
		let allowedTypes = props.allowedTypes;


		// todo DRY: MediaEditHover
		return el('div', {className:'display-on-parent-hover text-white',
				style: {
					position: 'absolute',
					width: '100%',
					height: '100%',
					background: "rgba(0,0,0,0.5)",
					top: 0,
					alignItems: 'center',
					justifyContent: 'center',
				},
				onClick: function(){
					console.log('component', 'onClick');
					self.onClick();
				}
			},


			el( MediaUpload, {
				onSelect: function(value){
					self.onSelect(value);
				},
				allowedTypes: typeof allowedTypes !== 'undefined' ? allowedTypes : 'image',
				value: currentValue,
				render: function( obj ) {
					return el('div', { className: 'text-white',
							style: {
								border: "1px solid rgba(255,255,255,0.5)",
								background: "rgba(0,0,0,0.5)",
							}},
						el(components.Button,{
								onClick: obj.open,
							},
							el( Icon, { icon: 'edit', style: {color: 'white'} }),
						),
						el(components.Button,{
								onClick: function(){
									console.log('component', 'onRemove');
									self.onRemove();
								},
							},
							el( Icon, { icon: 'trash', style: {color: 'white'} }),
						)
					)
				}
			}),
		)
	}
}

/**
 * Block Media Placeholder
 * ------------------------------------
 * @prop label
 * @prop currentValue
 * @prop onSelect(value)
 * @prop onClick() (used by repeater focus-item logic)
 *
 * use:
 * - inside repeater blocks
 * 	- repeater focus-item logic requires onClick()
 * -
 *
 * requires:
 * - MediaUpload
 * - MediaPlaceholder
 */
class BlockMediaPlaceholder extends Component {

	init(props) {
		this.props = props.props;
	}

	onSelect(value) {
		let props = this.props;
		props.onSelect(value);
	}

	onClick() {
		let props = this.props;
		props.onClick();
	}

	render() {
		let self = this;
		let props = this.props;
		let currentValue = props.currentValue;
		let label = props.label;

		return el('div', {
				onClick: function(){
					self.onClick();
				}
			},
			el( MediaUpload, {
				label: label,
				onSelect: function(value){
					self.onSelect(value);
				},
				allowedTypes: 'image',
				value: currentValue,
				render: function (obj) {
					return el(MediaPlaceholder,
						{
							onSelect: function(value){
								self.onSelect(value);
							},
							icon: el(Icon, {icon: 'format-image', style: {margin: '0 20px 0 0'}}),
							labels: {
								title: label
							}
						}
					)
				}
			})
		)
	}
}

/**
 * BlockMediaEditor
 * ------------------------------------
 * @prop image
 * @prop label
 * @prop imageSize
 * @prop onSelect(value)
 * @prop onRemove()
 * @prop onClick() (used by repeater focus-item logic)
 *
 * - Display and manage block media content directly from the block editor (rather than InspectorControl)
 *
 * use:
 * - in block editor
 *
 * requires:
 * - MediaUploadHover
 * - BlockMediaPlaceholder
 */
class BlockMediaEditor extends Component {

	init(props) {
		this.props = props.props;
	}

	onSelect(value) {
		let props = this.props;
		props.onSelect(value);
	}

	onRemove() {
		let props = this.props;
		props.onRemove();
	}

	onClick() {
		let props = this.props;
		if(typeof props.onClick === 'function')
			props.onClick();
	}

	render() {
		let self = this;
		let props = this.props;
		let image = props.image;
		let label = props.label;
		// console.log(image);
		let imageSize = image && typeof props.imageSize !== 'undefined' ? props.imageSize : false;
		let imageUrl = image && typeof image.sizes.medium !== 'undefined' ?
			image.sizes.medium.url : image ? image.url : '';


		imageUrl = imageSize && typeof image.sizes !== 'undefined' && typeof image.sizes[imageSize] !== 'undefined' ?
			image.sizes[imageSize].url : imageUrl;


		return image ?
			el('div', { className: 'position-relative'},
				el('img',{
						src: imageUrl,
						onClick: function(){
							self.onClick();
						}
					}
				),
				el(MediaUploadHover, {
					currentValue : image,
					onClick: function(){
						self.onClick();
					},
					onSelect: function(value){
						self.onSelect(value);
					},
					onRemove: function(){
						self.onRemove();
					},
				}),
			) :
			el(BlockMediaPlaceholder, {
				label : label,
				currentValue : imageUrl,
				onClick: function(){
					self.onClick();
				},
				onSelect: function(value){
					self.onSelect(value);
				},
			});
	}
}





/* Video Player */
/*-------------------------------*/

/**
 * CWB Video
 * ------------------------------------
 * @prop vidSrc (object)
 *
 * - Generates Video Player Markup
 *
 * use:
 * - in cw-blocks/cwb-video
 */
class CwbVideo extends Component {

	init(props) {
		this.props = props.props;
	}

	render(){
		let props = this.props;
		let vidSrc = props.vidSrc;
		let className = typeof props.className !== 'undefined' ? props.className: [];// TODO make array or string acceptable
		let id = typeof props.id === 'string' ? props.id : null;

		return el( 'video', {
				id: id,
				className: 		className.join(' '),
				style: 			{ width: '100vw', maxWidth: '100%'},
				playsinline: 	true, //typeof props.playsinline !== 'undefined' ? props.playsinline : true,
				autoplay: 		typeof props.autoplay !== 'undefined' ? props.autoplay : true,
				muted: 			typeof props.muted !== 'undefined' ? props.muted : true,
				loop: 			typeof props.loop !== 'undefined' ? props.loop : true,
				loading: 		"lazy",
				src: vidSrc
			},
			// el( 'source', {
			// 	type: 'video/mp4',
			// 	src: vidSrc
			// })
		);
	}
}
{/*<source src="http://localhost:8888/Clickwurx/lgg/video/EndlessNight_X_Teaser_20241122.mp4" type="video/mp4">*/}




// class CwbBreadcrumbs extends Component {
//
// 	init(props) {
// 		this.props = props.props;
// 	}
//
// 	render(){
//
// 		const { postId, postType, parentId } = useSelect( select => {
// 			const {
// 				getCurrentPostId,
// 				getCurrentPostType,
// 				getCurrentPostAttribute,
// 			} = select( 'core/editor' );
//
// 			return {
// 				postId: getCurrentPostId(),
// 				postType: getCurrentPostType(),
// 				parentId: getCurrentPostAttribute( 'parent' ),
// 			};
// 		}, [] );
//
// 		const { firstParentId, theParents, isResolved } = useSelect( select => {
// 			const { getEntityRecord } = select( 'core' );
//
// 			const theParents = [];
// 			// this is true if the current post has no parent, or that we've found the
// 			// top ancestor
// 			let isResolved   = ( ! parentId );
//
// 			const findFirstParentId = ( id, type ) => {
// 				const post = getEntityRecord( 'postType', type, id );
//
// 				// If the request has been resolved and the post data is good, then
// 				// we add the post to the parent pages array. Each item contains the
// 				// post ID and title. I.e. array( <ID>, <Title> )
// 				post && theParents.push( [ post.id, post.title?.rendered ] );
//
// 				// If the post is a child post, i.e. it has a parent, fetch the parent
// 				// post. So we're making a recursive function call until the very first
// 				// parent is found.
// 				if ( post && undefined !== post.parent && post.parent > 0 ) {
// 					return findFirstParentId( post.parent, type );
// 				}
//
// 				isResolved = ( !! post );
//
// 				// If we've found the top ancestor, return its ID.
// 				return post ? post.id : -1;
// 			};
//
// 			return {
// 				firstParentId: parentId ? findFirstParentId( postId, postType ) : -1,
// 				theParents,
// 				// use this instead to display the top ancestor first
// //      theParents: theParents.reverse(),
// 				isResolved,
// 			};
// 		}, [ postId, postType, parentId ] );
//
// 		console.log(theParents);
//
// 		return el( 'video', {
// 				style: 			{ width: '100vw', maxWidth: '100%'},
// 				playsinline: 	true, //typeof props.playsinline !== 'undefined' ? props.playsinline : true,
// 				autoplay: 		true, //typeof props.autoplay !== 'undefined' ? props.autoplay : true,
// 				muted: 			true, //typeof props.muted !== 'undefined' ? props.muted : true,
// 				loop: 			true, //typeof props.loop !== 'undefined' ? props.loop : true,
// 				// src: vidSrc
// 			},
// 			// el( 'source', {
// 			// 	type: 'video/mp4',
// 			// 	src: vidSrc
// 			// })
// 		);
// 	}
// }

/**
 * modBlockQueryRecords [archived; unused]
 * @param props
 * @param records
 * @param newObjProp
 * @param restRoute
 * @param recordProp
 *
 * created while trying to get featured_image data cwb-posts-block; not used any more because
 * there was a better alternative; saving code for reference
 */
function modBlockQueryRecords(props, records, newObjProp, restRoute, recordProp){

	let modifiedRecords = [];
	let attributes = props.attributes;
	let origin = window.location.origin;
	let pathname = window.location.pathname;
	let restPath = origin + pathname.replace('wp-admin/post.php', restRoute);




	props.setAttributes({
		records: modifiedRecords
	});
	attributes = props.attributes;

	for(let i = 0; i < records.length;i++) {

		let post = records[i];
		modifiedRecords.push({...post, [newObjProp]: false});

		// let testRestPath = origin + pathname.replace('wp-admin/post.php', `wp-json/wp/v2/${attributes.postType}/${post.id}`);//?_fields=acf.vid_embed_link`);/
		// console.log(post.featuredMedia_Obj);


		fetch(restPath + post[recordProp])
			.then(response => {
				if (!response.ok) {
					throw new Error(`HTTP error! Status: ${response.status}`);
				}
				return response.json();
			})
			.then(data => {
				// setPosts(data);

				console.log('data', data);

				modifiedRecords[i][newObjProp] = data;

				props.setAttributes({
					records: modifiedRecords // todo Flag attribute name (doesn't change?)
				});
				attributes = props.attributes;

				return modifiedRecords;
			})
			.catch(error => {
				// setError(error);
			});
	}
}

// // USAGE:

// let modifiedRecords = [];
// let newRecordProp = 'featured_media_obj';
// let restRoute = 'wp-json/wp/v2/media/';
// let recordProp = 'featured_media';
//
// const records = useSelect((select) => {
// 	return wp.data.select('core').getEntityRecords('postType', attributes.postType, attributes.query);
// });

// useEffect(() => {
// 	if(records){
//
// 		for(let i = 0; i < records.length;i++) {
//
// 			let post = records[i];
// 			console.log(post.acf);
// 		}
// 		modifiedRecords = modBlockQueryRecords(props, records, newRecordProp, restRoute, recordProp);
// 	}
// }, [records]);





// Depreciated
//=================================


// TODO: depreciated
class BgColorPanel extends Component {



	init( props ){
		this.props = props.props;
	}

	render() {
		// return 'CustomComponent';

		let props = this.props;
		let attributes = this.props.attributes;
		let newCssClasses 	= [...attributes.cssClasses];



		const toggleBgPopover = function(){
			props.setAttributes({
				bgPopover: !attributes.bgPopover
			});
		};


		const setBg = function(newAtts){

			// const bgUseGradient = '';
			newAtts = typeof newAtts === 'object' ? newAtts : false;
			const bgColor = newAtts && typeof newAtts.bgColor !== 'undefined' ?
				newAtts.bgColor : attributes.bgColor;
			const bgUseGradient = newAtts && typeof newAtts.bgUseGradient !== 'undefined' ?
				newAtts.bgUseGradient : attributes.bgUseGradient;
			const bgGradient = newAtts && typeof newAtts.bgGradient !== 'undefined' ?
				newAtts.bgGradient : attributes.bgGradient;

			// console.log('newAtts:', newAtts);
			// console.log('bgGradient:', bgGradient);
			// console.log('bgUseGradient:', bgUseGradient);
			// console.log('bgColor:', bgColor);
			const output = bgGradient && bgUseGradient ?
				`${bgGradient}` : bgColor ?
					`${bgColor}` : null;

			return  output;

		};
		const setCustomColor = function(color) {

			let c = color.rgb;
			const colStr = `rgba(${c.r},${c.g},${c.b},${c.a})`;

			props.setAttributes({
				bgColor: colStr,
				bgColorOutput: setBg({
					bgColor: colStr,
				})
			});
			attributes = props.attributes;
		};
		const setPaletteColor = function(color) {
			props.setAttributes({
				bgColor: color,
				bgColorOutput: setBg({
					bgColor: color
				})
			});
			attributes = props.attributes;
		};
		const cClear = function() {
			props.setAttributes({
				bgColor: false,
				bgColorOutput: setBg({
					bgColor: false
				})
			});
			attributes = props.attributes;
			console.log('cClear', attributes.bgColor, attributes.bgColorOutput);
		};
		const clearGradient = function() {
			props.setAttributes({
				bgGradient: null,
				bgColorOutput: setBg({
					bgGradient: null
				})
			});
			attributes = props.attributes;
			// console.log('clearGradient', attributes.bgGradient, attributes.bgColorOutput);
		};
		const setGradientToggle = function(value) {
			props.setAttributes({
				bgUseGradient: value,
				bgColorOutput: setBg({
					bgUseGradient: value,
				})
			});
			attributes = props.attributes;
		};
		const setGradientColor = function(value) {
			props.setAttributes({
				bgGradient: value,
				bgColorOutput: setBg({
					bgGradient: value
				})
			});
			attributes = props.attributes;
		};

		return el(PanelBody, {
				title : 'Background Color',
				initialOpen: true
			},


			// Preview & Popover Trigger
			//-----------------------------
			el('div', {
					className: 'transparency-grid',
				},
				el( components.Button, {
						className: 'editor-post-featured-image__toggle edit-bg-color',
						style:{ background: attributes.bgColorOutput},
						onClick: toggleBgPopover,
					},
					el( 'span', {}, __( 'Edit Background Color', 'cwb' ))
				)
			),




			// Toggle : Light Text
			//-----------------------------
			el( 'div', { style: { padding: '0' } },
				el( ToggleControl,
					{
						label: 'Use Light Text',
						onChange: function(value){
							let classArr = searchAndReplaceSubstrArr('text-white', newCssClasses , value ? 'text-white' : '' );
							props.setAttributes( {
								cssClasses: classArr,
								textWhite : value
							} );
							attributes = props.attributes;
						},
						checked: props.attributes.textWhite,
					}
				)
			),


			// attributes.bgPopover ?
			// 	el(BGPopover, props) : null,


			// // Bg Color Popover
			// //-----------------------------
			// attributes.bgPopover ?
			// 	el(Popover,{
			// 			position: 'top left',
			// 			className: 'popover-plus',
			// 			// TODO this breaks when the gradient angle ui gets clicked on
			// 			// onFocusOutside : toggleBgPopover
			// 		},
			// 		el('div',{ className:'content-wrap' },
			//
			//
			// 			// Close Button
			// 			//-----------------------------
			// 			el(components.Button,{
			// 					className : 'close-btn',
			// 					onClick: toggleBgPopover,
			// 				},
			// 				el( Icon, { icon: 'no-alt' }),
			// 			),
			//
			//
			// 			// Toggle: Solid BG vs Gradient
			// 			//-----------------------------
			// 			el( ToggleGroupControl, {
			// 					label : "Color Type",
			// 					style: { marginBottom: '20px' },
			// 					value : attributes.bgUseGradient,
			// 					onChange : setGradientToggle
			// 				},
			// 				el(ToggleGroupControlOption,{
			// 					label : 'Solid',
			// 					value : false
			// 				}),
			// 				el(ToggleGroupControlOption,{
			// 					label : 'Gradient',
			// 					value : true}
			// 				),
			// 			),
			//
			// 			!attributes.bgUseGradient ?
			//
			// 				el('div', {},
			//
			// 					// ColorPalette
			// 					//-----------------------------
			// 					el( PanelBody, {
			// 							title: 'Preset Colors',
			// 							className: cwbGlobals.innerPanelClasses,
			// 							initialOpen: !0//false
			// 						},
			// 						el( ColorPalette, {
			// 								// - https://developer.wordpress.org/block-editor/reference-guides/components/color-picker/
			// 								// - https://studiopress.blog/theme-color-palette/
			// 								// NOTE: this should match BRAND COLOURS in mod-theme-design-base.css
			// 								// TODO: PHP presets (gradients too?) https://richtabor.com/block-editor-gradients/
			// 								color: attributes.bgColor,
			// 								colors: cwbGlobals.customColorPalette,
			// 								// onChange: setCustomColor,
			// 								clearable: false,
			// 								disableCustomColors: true,
			// 								onChange: setPaletteColor
			// 							}
			// 						),
			// 					),
			//
			//
			// 					// ColorPicker
			// 					//-----------------------------
			// 					// - https://developer.wordpress.org/block-editor/reference-guides/components/color-picker/
			// 					// - https://studiopress.blog/theme-color-palette/
			// 					el( PanelBody, {
			// 							title: 'Custom Color',
			// 							className: cwbGlobals.innerPanelClasses,
			// 							initialOpen: false
			// 						},
			// 						el( ColorPicker, {
			// 								color: attributes.bgColor,
			// 								onChangeComplete: setCustomColor
			// 							}
			// 						),
			// 					),
			//
			//
			// 					// Button : Clear Bg Color (conditional)
			// 					//----------------------------------------
			// 					attributes.bgColor ?
			// 						el( components.Button, {
			// 								className: 'button button-large',
			// 								style: {margin:'20px 0 20px'},
			// 								onClick: cClear,
			// 								// isTertiary: !0,
			// 								// isLink: !0,
			// 								// isDestructive: !0,
			// 							},
			// 							__( 'Clear Background Color', 'cwb' )
			// 						) : null,
			// 				) :
			//
			// 				el('div', {},
			//
			// 					// GradientPicker
			// 					//-----------------------------
			// 					el(GradientPicker, {
			// 						value : attributes.bgGradient,
			// 						gradients: cwbGlobals.customGradientPresets,
			// 						onChange : setGradientColor,
			// 						clearable : false
			// 					}),
			//
			//
			// 					// Button : Clear Bg Gradient (conditional)
			// 					//----------------------------------------
			// 					attributes.bgGradient ?
			// 						el( components.Button, {
			// 								className: 'button button-large',
			// 								style: {margin:'20px 0 20px'},
			// 								onClick: clearGradient,
			// 							},
			// 							__( 'Clear Background Gradient', 'cwb' )
			// 						) : null,
			// 				),
			// 		),
			// 	) : null,

			// // https://developer.wordpress.org/block-editor/reference-guides/packages/packages-block-editor/#createcustomcolorshoc
			// el( PanelColorSettings, {
			// 	title: 'PanelColorSettings',
			// 	colorSettings: [
			// 			{
			// 				value: attributes.bgColor,
			// 				onChange: ( colorValue ) => {
			// 					props.setAttributes( { bgColor: colorValue } );
			//
			// 					console.log('colorSettings',PanelColorSettings);
			// 				},
			// 				label: __( 'Background Color' ),
			// 			},
			// 			// {
			// 			// 	value: textColor,
			// 			// 	onChange: ( colorValue ) => props.setAttributes( { textColor: colorValue } ),
			// 			// 	label: __( 'Text Color' ),
			// 			// }
			// 	]
			// }),

		);
	}
}

class _BorderColorPopover extends ColorComponent {

	init(props) {
		this.props = props.props;
	}

	render() {

		let self = this;
		let props = this.props;
		let attributes = this.props.attributes;

		const toggleBorderColorPopover = function () {
			props.setAttributes({
				borderColorPopover: !attributes.borderColorPopover
			});
		};

		return el(Popover,{
				position: 'top left',
				className: 'popover-plus',
				// headerTitle: 'Text Colors',
				// NOTE: this breaks when the gradient angle ui gets clicked on
				// onFocusOutside : toggleBgPopover
			},
			el('div',{ className:'content-wrap' },


				// Close Button
				//-----------------------------
				el(components.Button,{
						className : 'close-btn',
						onClick: toggleBorderColorPopover,
					},
					el( Icon, { icon: 'no-alt' }),
				),

				el('div', {},
					el('p', {
						className: 'f-w-600'
					}, 'Border Color'),

					// ColorPalette
					//-----------------------------
					el( PanelBody, {
							title: 'Preset Colors',
							className: cwbGlobals.innerPanelClasses,
							initialOpen: !0//false
						},
						el( ColorPalette, {
								// - https://developer.wordpress.org/block-editor/reference-guides/components/color-picker/
								// - https://studiopress.blog/theme-color-palette/
								// NOTE: this should match BRAND COLOURS in mod-theme-design-base.css
								color: attributes.borderColor,
								colors: cwbGlobals.customColorPalette,
								// onChange: setCustomColor,
								clearable: false,
								disableCustomColors: true,
								onChange: function(value){
									self.setColor(value, 'borderColor');
								}
							}
						),
					),


					// ColorPicker
					//-----------------------------
					// - https://developer.wordpress.org/block-editor/reference-guides/components/color-picker/
					// - https://studiopress.blog/theme-color-palette/
					el( PanelBody, {
							title: 'Custom Color',
							className: cwbGlobals.innerPanelClasses,
							initialOpen: false
						},
						el( ColorPicker, {
								color: attributes.borderColor,
								onChangeComplete: function(value){
									self.setCustomColor(value, 'borderColor');
								}
							}
						),
					),


					// Button : Clear Border Color (conditional)
					//----------------------------------------
					attributes.borderColor ?
						el( components.Button, {
								className: 'button button-large cursor-pointer',
								style: {margin:'20px 0 20px'},
								onClick: function() {
									self.clearColor('borderColor')
								},
								// isTertiary: !0,
								// isLink: !0,
								// isDestructive: !0,
							},
							__( 'Clear Text Color', 'cwb' )
						) : null,
				)
			),
		)
	}
}

class _TextColorPopover extends ColorComponent {

	init(props) {
		this.props = props.props;
	}

	render() {

		let self = this;
		let props = this.props;
		let attributes = this.props.attributes;

		const toggleTextColorPopover = function () {
			props.setAttributes({
				textColorPopover: !attributes.textColorPopover
			});
		};

		return el(Popover,{
				position: 'top left',
				className: 'popover-plus',
				// NOTE: this breaks when the gradient angle ui gets clicked on
				// onFocusOutside : toggleBgPopover
			},
			el('div',{ className:'content-wrap' },


				// Close Button
				//-----------------------------
				el(components.Button,{
						className : 'close-btn',
						onClick: toggleTextColorPopover,
					},
					el( Icon, { icon: 'no-alt' }),
				),

				el('div', {},
					el('p', {
						className: 'f-w-600'
					}, 'Text Color'),

					// ColorPalette
					//-----------------------------
					el( PanelBody, {
							title: 'Preset Colors',
							className: cwbGlobals.innerPanelClasses,
							initialOpen: !0//false
						},
						el( ColorPalette, {
								// - https://developer.wordpress.org/block-editor/reference-guides/components/color-picker/
								// - https://studiopress.blog/theme-color-palette/
								// NOTE: this should match BRAND COLOURS in mod-theme-design-base.css
								color: attributes.textColor,
								colors: cwbGlobals.customColorPalette,
								// onChange: setCustomColor,
								clearable: false,
								disableCustomColors: true,
								onChange: function(value){
									self.setColor(value, 'textColor');
								}
							}
						),
					),


					// ColorPicker
					//-----------------------------
					// - https://developer.wordpress.org/block-editor/reference-guides/components/color-picker/
					// - https://studiopress.blog/theme-color-palette/
					el( PanelBody, {
							title: 'Custom Color',
							className: cwbGlobals.innerPanelClasses,
							initialOpen: false
						},
						el( ColorPicker, {
								color: attributes.textColor,
								onChangeComplete: function(value){
									self.setCustomColor(value, 'textColor');
								}
							}
						),
					),


					// Button : Clear Border Color (conditional)
					//----------------------------------------
					attributes.textColor ?
						el( components.Button, {
								className: 'button button-large cursor-pointer',
								style: {margin:'20px 0 20px'},
								onClick: function() {
									self.clearColor('textColor')
								},
								// isTertiary: !0,
								// isLink: !0,
								// isDestructive: !0,
							},
							__( 'Clear Text Color', 'cwb' )
						) : null,
				)
			),
		)
	}
}


// EXPORT Components
//=================================
window.cwbComponents = {

	ColorComponent : ColorComponent,
	ColorIndicatorAndText : ColorIndicatorAndText,

	// BGPopover : BGPopover,
	// BgColorPanel : BgColorPanel, // depreciated
	BlockColors : BlockColors,
	BgImagePanel : BgImagePanel,
	BlockBorderPanel : BlockBorderPanel,
	HoverColors : HoverColors,

	RepeaterBtnEditor : RepeaterBtnEditor,
	RepeaterMediaUpload : RepeaterMediaUpload,
	RepeaterWpContentPopover : RepeaterWpContentPopover,
	BlockRichText : BlockRichText,
	InspectorText : InspectorText,
	CwbVideo : CwbVideo,
	CwbEditableButton : CwbEditableButton,

	ResponsiveTabs : ResponsiveTabs,
	RepeaterLayoutSettingsTab : RepeaterLayoutSettingsTab,

	RepeaterSliderEditBlock : RepeaterSliderEditBlock,
	CwbSlider : CwbSlider,
	CwbSlide : CwbSlide,
	CwbSlideTint : CwbSlideTint,
	BootstrapModal : BootstrapModal,
	SliderEditBlock : SliderEditBlock,
	CwGalleryImage : CwGalleryImage,
	CwGalleryImageEditor : CwGalleryImageEditor,
	CwAsymmetricGalleryEditor : CwAsymmetricGalleryEditor,

	RepeaterControls : RepeaterControls,
	RepeaterToolbar : RepeaterToolbar,
	StateChangeExperiment : StateChangeExperiment,
	RepeaterBtn : RepeaterBtn,

	cwbAttributes : {

		__blockColors : {
			bgColorOutput: {
				type: 'string',
				default: ''
			},
			bgColor: {
				type: 'string',
			},
			bgUseGradient: {
				type: 'boolean',
				default: false
			},
			bgGradient : {
				type: 'string',
				// default: 'linear-gradient(135deg,rgb(6,147,227) 4%,rgb(155,81,224) 100%)'
			},
			textWhite: {
				type: 'boolean',
				default: false
			},
			textColor : {
				type: 'string',
				default: ''
			},
		},
		__blockBorder : {
			borderColor: {
				type: 'string',
			},
			borderWidth: {
				type: 'string', // use 'string' type for now; 'number' is encountering bugs via ave()
				default: '1',
			},
		},
		__hoverColors : {
			hoverBgColorOutput: {
				type: 'string',
				default: ''
			},
			hoverBgColor: {
				type: 'string',
			},
			hoverBgUseGradient: {
				type: 'boolean',
				default: false
			},
			hoverBgGradient : {
				type: 'string',
				// default: 'linear-gradient(135deg,rgb(6,147,227) 4%,rgb(155,81,224) 100%)'
			},
			hoverTextColor : {
				type: 'string',
				default: ''
			},
			hoverBorderColor: {
				type: 'string',
			},
		},
		__bgImage : {
			bgCover: {
				type: 'boolean',
				default: true // if true add 'bg-cover to cssClasses defaults
			},
			mediaURL: {
				type: 'string',
				default: ''
			},
			mediaID: {
				type: 'number',
			},
			mediaAlt: {
				type: 'string',
				default: ''
			},
			//-----------------------------
			includeAriaLabel: {
				type: 'boolean', // for ToggleControl we need boolean type
				default: false,
			},
			lazyBg: {
				type: 'boolean',
				default: false //
			},
		},


		__bgOverlayColors : {
			overlayColorOutput: {
				type: 'string',
				default: ''
			},
			overlayColor: {
				type: 'string',
			},
			overlayUseGradient: {
				type: 'boolean',
				default: false
			},
			overlayGradient: {
				type: 'string',
				// default: 'linear-gradient(135deg,rgb(6,147,227) 4%,rgb(155,81,224) 100%)'
			},
		},


		__magicColumn : {
			containerWidth: {
				type: 'integer',
				default: 1400
			},
			leftColWidth: {
				type: 'integer',
				default: 50
			},
			isLeftColumn: {
				type: 'boolean',
				default: true
			},
			colHeightCssClasses: {
				type: 'string',
				default: 'vh-50-min'
			},
			colContent: {
				type: 'string',
				default: ''
			},
			colCssExtra: {
				type: 'string',
				default: ''
			},
			containerClasses: {
				type: 'string',
				default: ''
			}
		},


		blockHeight: {
			type: 'object',

			// Set Default as empty string (due to bug in gutenberg)
			// - @ref: https://github.com/WordPress/gutenberg/issues/37967
			default: '',
			// - Use this when bug is fixed:
			// default: JSON.parse(JSON.stringify(defaultScreenSizeObj)), // copies default obj
		},
		vAlign: {
			type: 'object',

			// Set Default as empty string (due to bug in gutenberg)
			// - @ref: https://github.com/WordPress/gutenberg/issues/37967
			default: '',
			// -Use this when bug is fixed:
			// default: JSON.parse(JSON.stringify(defaultScreenSizeObj)) // copies default obj
		},
		hAlign: {
			type: 'object',

			// Set Default as empty string (due to bug in gutenberg)
			// - @ref: https://github.com/WordPress/gutenberg/issues/37967
			default: '',
			// -Use this when bug is fixed:
			// default: JSON.parse(JSON.stringify(defaultScreenSizeObj)) // copies default obj
		},

		slideObjModel: {
			key: 0,
			itemText: 'item 1',
			bgCol: '#5db96e',
			bgURL: false,
			bgID: false,
			postObj: false,
			btn: false,
			btnObj: {
				text: 'Button Text',
				url: '',
				target: null,
				newTab: false,
				dataToggle: null,
				dataShortcode: null,
				dataModalSize: null,
				dataModalHeading: null
			},
			btnPopover: false, // todo depreciated
			wpContentPopover: false,
			// testProp: true
		},

		defaultButtonObj : {
			text: 'Button Text',
			url: '',
			btnFunction: 'link',
			target: null, // ≈ TODO should be dataTarget because thats how CwbButton uses it
			newTab: true,
			dataToggle: null,
			dataShortcode: null,
			dataModalSize: null,
			dataModalHeading: null,
			dataSrc: null,
			postObj: null,
			btnType: 'primary',
			btnSize: 'default',
			hoverScale: false
		},





		// used to store parent block attributes for use by child blocks
		globalBlockAtts: {}
	}
};



// globalBlockAtts-related functions
// - used to store parent block attributes for use by child blocks
//-------------------------------------

// Function to update the global array
const updateGlobalBlockAtts = (blockUid, blockData) => {

	let globalBlockAtts = window.cwbComponents.cwbAttributes.globalBlockAtts;

	window.cwbComponents.cwbAttributes.globalBlockAtts = {
		...window.cwbComponents.cwbAttributes.globalBlockAtts,
		[blockUid]: blockData,
	};
};

// Function to retrieve data using block UID as a key
const getBlockDataFromGlobalBlockAtts = (blockUid) => {
	return updateGlobalBlockAtts[blockUid] || {};
};


