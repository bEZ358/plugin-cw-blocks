( function( blocks, editor, i18n, element, components, _, blockEditor, cwb ) {
	const __ = i18n.__;
	const el = element.createElement;
	const InnerBlocks = blockEditor.InnerBlocks;
	const useBlockProps = blockEditor.useBlockProps;
	const InspectorControls = blockEditor.InspectorControls;
	const MediaPlaceholder = blockEditor.MediaPlaceholder;
	const { Fragment } = element;

	const {
		RangeControl,
	// 	ToggleControl,
	// 	Panel,
	// 	PanelBody,
	// 	PanelRow
	} = components;

	const {
		cwbGlobals,
		cwbComponents
	} = cwb;

	const {
		screenSizeTabs,
		defaultScreenSizeObj
	} = cwbGlobals;

	const {
		cwbAttributes
	} = cwbComponents;



	blocks.registerBlockType( 'cwb/sandbox', {


		// Block ASSETS
		//-----------------------------

		title: __( 'CWB: Sandbox', 'CWB' ),
		icon: 'media-code', // see: /wp-includes/css/dashicons.css
		category: 'common',
		attributes: {
			sliderId: {
				type: 'string',
				default: false
			},
			cssClasses: {
				type: 'array',
				default: ['p-0', 'my-0', '_trace-green'],//['w-100', 'd-flex', 'm-0', 'align-items-center', 'bg-cover']
			},

			images: {
				type: 'array',
				default: ''
			},
			containerWidth : {
				type: 'number',
				default: 1400
			},
			leftColWidth : {
				type: 'number',
				default: 33
			},
			contentLeft: {
				type: 'string',
				default: ''
			},
			contentRight: {
				type: 'string',
				default: ''
			},

			// Common Attribute Sets
			//-----------------------------
			...cwbAttributes.__blockColors,
			...cwbAttributes.__bgImage,
			// ...cwbAttributes.__blockBorder,


			// // Layout Block Settings
			// //-----------------------------
			// fullWidth: {
			// 	type: 'boolean' // for ToggleControl we need boolean type
			// }
		},
		example: {
			attributes: {
				title: __( 'Chocolate Chip Cookies', 'gutenberg-examples' ),
				mediaID: 1,
				mediaURL:
					'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/2ChocolateChipCookies.jpg/320px-2ChocolateChipCookies.jpg',
			},
		},


		// Block FUNCTIONS
		//-----------------------------

		edit: function( props ) {
			let attributes = props.attributes;
			let blockStyle = {
				background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : attributes.bgColorOutput,
				width: '100%',
				margin: '0px auto',
				padding: '0px',
				color: attributes.textColor ? attributes.textColor : null,
			};
			let layout = 'left';//item.key % 2 !== 0 ? 'left' : 'right';



			// RETURN: SandBawx Block Editor
			//================================
			return el( 'div', useBlockProps( {
					className: attributes.cssClasses,
					style: blockStyle } ),

					// todo: depreciate (delete after dec 20224 if nothing breaks)
					// // TODO try a locked block template and see if MagicColumns can control col-width
					// el(MagicColumns, {
					// 	leftColWidth : attributes.leftColWidth, // 33,
					// 	containerWidth : attributes.containerWidth, // 1400,
					// 	leftColContent : function(){
					// 		return el(RichTextBlockInput, {
					// 			currentValue : attributes.contentLeft,
					// 			onChange : function(value){
					// 				props.setAttributes({
					// 					contentLeft : value
					// 				});
					// 				attributes = props.attributes;
					// 			},
					// 		})
					// 	}(),
					// 	rightColContent : function(){
					// 		return el(RichTextBlockInput, {
					// 			currentValue : attributes.contentRight,
					// 			onChange : function(value){
					// 				props.setAttributes({
					// 					contentRight : value
					// 				});
					// 				attributes = props.attributes;
					// 			},
					// 		})
					// 	}()
					// }),

					// InspectorControls
					//================================
					el( InspectorControls, { key: 'controls' },
						el('div', {className: '', style: {padding: '0 20px'}},
							el(RangeControl, {
								value : attributes.leftColWidth,
								label : 'Left Column - Content Width',
								// help : 'help',
								// beforeIcon : 'arrowLeft',
								// afterIcon : 'arrowRight',
								min : 25,
								max : 75,
								onChange :function(value){
									props.setAttributes({
										leftColWidth : value
									});
									attributes = props.attributes
								}
							}),
							el(RangeControl, {
								value : attributes.containerWidth,
								label : 'Container Width',
								min : 900,
								max : 1800,
								onChange :function(value){
									props.setAttributes({
										containerWidth : value
									});
									attributes = props.attributes
								}
							})
						)
					)
			);
		},

		save: function( props ) {

			let attributes = props.attributes;
			let blockStyle = {
				background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : attributes.bgColorOutput,
				margin: '0px',
				padding: '0px',
				width: '100%',
				color: attributes.textColor ? attributes.textColor : null,
			};


			// RETURN: SandBawx-Block (FRONT-end)
			//================================
			return el( 'div', useBlockProps.save( {
					className: attributes.cssClasses.join(' '),
					style: blockStyle
				} ), 'Sandbawx'


			);
		},
	} );
}(
	window.wp.blocks,
	window.wp.editor,
	window.wp.i18n,
	window.wp.element,
	window.wp.components,
	window._,
	window.wp.blockEditor,
	window.cwb
) );
