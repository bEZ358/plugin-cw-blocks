( function( blocks, editor, i18n, element, components, _, blockEditor, cwb ) {
	const __ = i18n.__;
	const el = element.createElement;
	const useState = element.useState;
	const useEffect = element.useEffect;
	const useBlockProps = blockEditor.useBlockProps;
	const InspectorControls = blockEditor.InspectorControls;
	const InnerBlocks = blockEditor.InnerBlocks;
	const LinkControl = blockEditor.__experimentalLinkControl;
	const useEntityProp =  wp.coreData.useEntityProp;
	const useSelect = wp.data.useSelect;


	const {
		Dashicon,
		Toolbar,
		ToolbarButton,
		// ToggleControl,
		// Panel,
		// PanelBody,
		// PanelRow
	} = components;

	const {
		cwbGlobals,
		cwbComponents
	} = cwb;

	const {
		screenSizeTabs,
		defaultScreenSizeObj,
	} = cwbGlobals;

	const {
		// RepeaterBtn,
		// RepeaterSliderEditBlock,
		// RepeaterBtnEditor,
		// RepeaterControls,
		// RepeaterToolbar,
		// Repeater,
		cwbAttributes
	} = cwbComponents;

	const QUERY_DEFAULTS = {
		// context: false,
		// page: false,
		per_page: -1,
		// search: false,
		// after: false,
		// author: false,
		// author_exclude: false,
		// before: false,
		exclude: '0',
		// include: '0', // ~ TODO: this one breaks things for some reason BUT would be useful if it worked
		// offset: false,
		order: 'desc',
		orderby: 'published',
		// slug: false,
		// status: false,
		// categories: false,
		// categories_exclude: false,
		// tags: false,
		// tags_exclude: false,
		// sticky: false,
	};


	// @ref links below
	// https://mediaron.com/how-to-create-a-repeater-field6-in-gutenberg/
	// https://reactnative.dev/docs/state

	blocks.registerBlockType( 'cwb/posts-offset-grid', {


		// Block ASSETS
		//-----------------------------

		title: __( 'CWB: Posts Offset-Grid', 'CWB' ),
		icon: 'slides', // see: /wp-includes/css/dashicons.css
		category: 'common',
		attributes: {
			cssClasses: {
				type: 'array',
				default: ['my-0'],//['w-100', 'd-flex', 'm-0', 'align-items-center', 'bg-cover']
			},

			// // Common Attribute Sets
			// //-----------------------------
			// ...cwbAttributes.__blockColors,
			// ...cwbAttributes.__bgImage,
			// // ...cwbAttributes.__blockBorder,

			// Wrap Settings
			//-----------------------------
			blockHeight: cwbAttributes.blockHeight,
			// vAlign: cwbAttributes.vAlign,


			query: {
				type: 'object',
				default: ''
			},

			siteUrl: {
				type: 'string',
				default: 'post'
			},

			postType: {
				type: 'string',
				default: 'post'
			},

			// tileWidthCss: {
			// 	type: 'string',
			// 	default: 'width-3'
			// },

			records: {
				type: 'array',
				default: ''
			},
			//
			// grid: {
			// 	type: 'object',
			//
			// 	// Set Default as empty string (due to bug in gutenberg)
			// 	// - @ref: https://github.com/WordPress/gutenberg/issues/37967
			// 	default: '',
			// 	// - Use this when bug is fixed:
			// 	// default: JSON.parse(JSON.stringify(defaultScreenSizeObj)), // copies default obj
			// },
			//
			// tileGap: {
			// 	type: 'object',
			//
			// 	// Set Default as empty string (due to bug in gutenberg)
			// 	// - @ref: https://github.com/WordPress/gutenberg/issues/37967
			// 	default: '',
			// 	// - Use this when bug is fixed:
			// 	// default: JSON.parse(JSON.stringify(defaultScreenSizeObj)), // copies default obj
			// },
			//
			// tileClassName: {
			// 	type: 'string',
			// 	default: 'post'
			// },

			contentClassName: {
				type: 'string',
				default: 'post'
			},

			btnObj: {
				type: 'object',
				default: false
			}
		},
		example: {
			attributes: {
				// title: __( 'Chocolate Chip Cookies', 'gutenberg-examples' ),
				// mediaID: 1,
				// mediaURL:
				// 	'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/2ChocolateChipCookies.jpg/320px-2ChocolateChipCookies.jpg',
			},
		},

		// Block FUNCTIONS
		//-----------------------------

		edit: function( props ) {

			let attributes = props.attributes;
			let blockStyle = {
				width: '100%',
				margin: '0px auto',
				padding: '0px',
			};
			let counter = 0;
			let globals = window.cwbOffsetPostGrid;

			if(!attributes.btnObj){
				props.setAttributes({
					btnObj : {...cwbAttributes.defaultButtonObj}
				});
				attributes = props.attributes;
			}


			// - get siteUrl
			const siteUrl = useSelect((select) => {
				return wp.data.select( 'core' ).getSite();
			});
			if(siteUrl && attributes.siteUrl !== siteUrl){
				props.setAttributes({
					siteUrl: siteUrl.url
				});
				attributes = props.attributes;
			}


			// WP Query
			//--------------------------------------
			function updateQuery(value, queryArg){

				let newQuery = {...attributes.query};

				if(typeof newQuery[queryArg] !== 'undefined')
					newQuery[queryArg] = value;

				newQuery.order = 'desc';
				newQuery.orderby = 'date';


				props.setAttributes({
					query: newQuery
				});

				attributes = props.attributes;
			}
			if(attributes.query === '') props.setAttributes({
				query: QUERY_DEFAULTS,
			});





			// Dynamic WP Content
			//--------------------------------------
			const records = useSelect((select) => {
				return wp.data.select('core').getEntityRecords('postType', attributes.postType, attributes.query);
			});

			useEffect(() => {
				if(records){
					props.setAttributes({
						records: records
					});
					attributes = props.attributes;

					records.push('btn');
				}
			}, [records]);

			counter++;

			function updateBtn(value, attrStr){
				let newBtnObj = {...attributes.btnObj};
				newBtnObj[attrStr] = value;
				props.setAttributes( {
					btnObj: newBtnObj
				} );
				attributes = props.attribute
			};


			// RETURN: Posts Block Editor
			//================================
			return el( 'div', useBlockProps( {
					className: attributes.cssClasses,
					style: blockStyle
				} ),


				el('div',{ className: 'container px-0'},
					el('div',{ className: 'row '},
						el('div', { className: 'col'},


							el('div', {className: 'cw-grid-intro',
									// style: {marginBottom: '-7.5%'}
								},

							el(InnerBlocks, {
								allowedBlocks : [
									'core/heading',
									'core/paragraph',
									'cwb/button',
								],
								template: [
									['core/heading'],
									// ['core/paragraph'],
									// ['cwb/button'],
								]
							}),
							),

							records ?
								el('div', { className: 'cw-grid text-right list'},

									records.map((post) => {

										counter++;
										let align = counter % 2 === 0 ? 'left' : 'right';
										let classArr = [...globals.classArr];
										let pads = globals.offsetPadding;

										classArr.push(
											counter % 2 !== 0 ?  pads.base[1] : pads.base[0],

											counter % 2 !== 0 ?  pads.sm[1] : pads.sm[0],
											counter % 2 !== 0 ?  pads.lg[1] : pads.lg[0],
											counter % 2 !== 0 ?  pads.xl[1] : pads.xl[0],
										);

										let imgObj = typeof post.featured_media_obj !== 'undefined' ? post.featured_media_obj : false;
										let img_src = imgObj ? getImageSource(imgObj) : false;
										let title = typeof post.title !== 'undefined' ? post.title.raw : false;
										// let read_more_text = typeof post.acf.read_more_text !== 'undefined' ? post.acf.read_more_text : false;
										// let external_link = typeof post.acf.external_link !== 'undefined' ? post.acf.external_link : false;
										// let excerpt = typeof post.excerpt !== 'undefined' ?
										// 	post.excerpt.raw !== '' ? post.excerpt.raw : removeTags(post.excerpt.rendered) :
										// 	false;
										// let linkProps = {
										// 	// href: external_link ? external_link : post.link,
										// 	// target: external_link ? '_blank' : null,
										// 	href: post.link,
										// };

										let btnClassArr = [...globals.btnClassArr];
										if(post === 'btn')
											btnClassArr.push(
												// records.length % 2 === 0 ?  'pr-4' : 'pl-4',
												attributes.records.length % 2 === 0 ?  pads.sm[1] : pads.sm[0],
												attributes.records.length % 2 === 0 ?  pads.lg[1] : pads.lg[0],
												attributes.records.length % 2 === 0 ?  pads.xl[1] : pads.xl[0],
											);

										//'pl-4 pl-sm-2 pl-lg-3 pl-xl-4'



										return post !== 'btn' ? el( 'div',{ className: classArr.join(' ')},
											el('div', {className: 'hover-zoom-wrap anim-on-scroll anim-b-in position-relative',
												},
												el ('a', { className: 'd-inline-block w-100 bg-cover bg-grey-65 text white px-4 pb-4 pt-phi float-right hover-zoom',
													// ...linkProps,
													['data-image']: img_src,
													style : {
														backgroundImage : 'url("'+img_src+'")'
													}
												}),
												el('div', { className: 'display-on-parent-hover', style:{ pointerEvents: 'none'}}),

												el ('a', { className: 'position-absolute bg-white ml-0 mb-4 min-w-66perc p-2 h4',
													// ...linkProps,
													style : {
														[align]: 0,
														bottom: 0
													}
												}, title),
											)
										)  :
										el('div', { className: btnClassArr.join(' ')},
											el('div', { style: { paddingTop: '62.5%', position: 'relative' }},
												el('div', {
														style: {
															position: 'absolute',
															top: '30%',
															left: '50%',
															transform: 'translate(-50%, -50%)'
														}
													},
													// el('div', { className: 'btn btn-primary d-inline-block btn-hover-scale',
													// 	type: 'button',
													// 	role: 'button',
													// 	href: null, // <-- TODO input
													// }, 'View All Projects' // <-- TODO input
													// ),
													el(CwbEditableButton, {
														btnObj: attributes.btnObj,
														allowedBtnTypes  : [
															// 'link',
															// 'wp_link',
															'modal_trigger',
															// 'iframe',
															// 'shortcode_modal_trigger'
														],
														// btnObj : {
														// 	text			: attributes.text,
														// 	// url 			: attributes.href,
														// 	btnFunction 	: attributes.btnFunction,
														// 	target			: attributes.target, // todo should be dataTarget because thats how CwbButton uses it
														// 	// newTab 			: attributes.openNewTab,
														// 	// // dataToggle 	: attributes.dataToggle,
														// 	// dataShortcode 	: attributes.dataShortcode,
														// 	dataModalHeading: attributes.popupHeading,
														// 	dataModalSize 	: attributes.popupSize,
														// 	// dataSrc 		: attributes.dataSource,
														// 	// postObj 		: attributes.postObj,
														// 	btnType 		: attributes.btnType,
														// 	btnSize 		: attributes.btnSize,
														// 	hoverScale 		: attributes.hoverScale,
														//
														// 	// permalink 		: attributes.permalink, // not part of default btnObj
														// },
														updateText : function(value){
															updateBtn(value, 'text');
														},
														updateUrl : function(value){
															updateBtn(value, 'href');
														},
														updateNewTab : function(value){
															updateBtn(value, 'newTab');
														},
														setButtonFunction : function(value){
															updateBtn(value, 'btnFunction');
														},
														updateDataSrc : function(value){
															updateBtn(value, 'dataSource');
														},
														updateDataTarget : function(value){
															updateBtn(value, 'target');
														},
														updateDataToggle : function(value){
															// props.setAttributes( {
															// 	dataToggle: value
															// } );
															// attributes = props.attributes;
														},
														updateShortcode : function(value){
															updateBtn(value, 'dataShortcode');
														},
														updateModalHeading : function(value){
															updateBtn(value, 'popupHeading');
														},
														updateModalSize : function(value){
															updateBtn(value, 'popupSize');
														},
														updatePostObj : function(value){
															updateBtn(value, 'postObj');
															// console.log('LinkControl', attributes.postObj);
														},




														// todo  btn styles
														updateBtnType : function(value){
															updateBtn(value, 'btnType');
														},
														updateBtnSize : function(value){
															updateBtn(value, 'btnSize');
														},
														updateHoverScale : function(value){
															updateBtn(value, 'hoverScale');
														},
													})
												)
											)
										);
									}),


								) : el('p',{ className: 'mt-5 pt-5'}, 'waiting for query results; post-type: '+ attributes.postType )
						),
					),
				),




				// InspectorControls
				//================================
				el( InspectorControls, {
						key: 'controls' },
					el( 'div', {},
						el( PanelBody, {
								style: {
									padding: '0 20px'
								},
								title: 'Query',
								initialOpen: false//!0,
							},


							// Posts Per Page
							//-------------------------
							el(NumberControl, {
								label: 'Posts Per Page',
								className: 'mt-0 mb-2 f-sz-14',
								value: attributes.query.per_page,
								// isShiftStepEnabled: true,
								// shiftStep : 1,
								max: 12,
								min: -1,
								onChange: function (value) {
									updateQuery(value, 'per_page');
								},
							}),



							// order: 'asc',
							// - select
							// orderby: 'title',
							// - select


							// Post Type
							//-----------------------------
							el( SelectControl, {
								key: 'multiple',
								label: __('Post Type'),
								value: attributes.postType,
								className: ['mt-3'],
								onChange: function(value){
									props.setAttributes({
										postType: value,
									});
									attributes = props.attributes;
								},
								// TODO: query available post types instead (move to child theme; disable in cw-blocks for now)
								// 	- try to query in index.php and save (localize?) as js variable
								options: globals.postTypeOptions
							}),



							// // Include Posts
							// // TODO this breaks this BUT would NICE if it worked
							// //-------------------------
							// el(InspectorTextInput, {...props,
							// 	label: __('Include posts'),
							// 	currentValue: attributes.query.include ? attributes.query.include : '',
							// 	onChange: function(value){
							// 		updateQuery(value, 'include');
							// 	},
							// }),


							// Exclude Posts
							//-------------------------
							el(InspectorTextInput, {...props,
								label: __('Exclude posts'),
								currentValue: attributes.query.exclude ? attributes.query.exclude : '',
								onChange: function(value){
									updateQuery(value, 'exclude');
								},
							}),
						)
					),

					el( 'div', {},

						// Grid Layout Configs
						//-------------------------
						el( PanelBody, {
								style: {
									padding: '0 20px'
								},
								title: 'Grid Configs',
								initialOpen: false//!0,
							},

							el(ResponsiveTabs, props,
								(gridTab) => {
									setGridTab(gridTab);
									props = {
										tab: {...gridTab},
										...props,
										gridCssClasses: gridCssClasses,
										onUpdateLayout: function(attrStr, responsiveCssObj){
											// console.log('gridTab', attrStr, responsiveCssObj);
										}
									};
									return el(GridSettingsTab, props);
								}
							),
							// tileGap
						),

						// Css Classes
						//-------------------------
						el( PanelBody, {
								style: {
									padding: '0 20px'
								},
								title: 'Css Classes',
								initialOpen: false//!0,
							},

							el(InspectorTextInput, {...props,
								label: __('Tile Css Classes'),
								currentValue: attributes.tileClassName,
								onChange: function(value){
									props.setAttributes({
										tileClassName: value
									});
									attributes = props.attributes;
								},
							}),

							el(InspectorTextInput, {...props,
								label: __('Content Wrap Css Classes'),
								currentValue: attributes.contentClassName,
								onChange: function(value){
									props.setAttributes({
										contentClassName: value
									});
									attributes = props.attributes;
								},
							}),
						)
					),
				)
			);
		},

		save: function( props ) {
			let attributes = props.attributes;
			let blockStyle = {
				// background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : attributes.bgColorOutput,
				margin: '0px',
				padding: '0px',
				width: '100%',
				// color: attributes.textColor ? attributes.textColor : null,
			};
			let counter = 0;
			let btnObj = {...attributes.btnObj};
			let globals = window.cwbOffsetPostGrid;

			// - get siteUrl
			if(attributes.btnObj){
				if(typeof attributes.btnObj.href !== 'undefined' && attributes.btnObj.href.startsWith('/')){

					let fullUrl = attributes.siteUrl+attributes.btnObj.href;
					btnObj = {
						...attributes.btnObj,
						href : fullUrl
					}
				}
			}

			console.log(btnObj);

			// RETURN: Layout-Block (FRONT-end)
			//================================
			return el( 'div', useBlockProps.save( {
					className: attributes.cssClasses.join(' '),
					style: blockStyle,
					// ['data-last-update']: 230503
				}),

				el('div',{ className: 'container px-0'},
					el('div',{ className: 'row '},
						el('div', { className: 'col'},

							el('div', {className: 'cw-grid-intro',
									// style: {marginBottom: '-7.5%'}
									},

								el( InnerBlocks.Content ),
							),

							attributes.records.length ?
								el('div', { className: 'cw-grid text-right list'},


									attributes.records.map((post) => {


										counter++;
										let align = counter % 2 !== 0 ? 'left' : 'right';
										let classArr = [...globals.classArr];
										let pads = globals.offsetPadding;

										classArr.push(
											counter % 2 !== 0 ?  pads.base[1] : pads.base[0],
											counter % 2 === 0 ?  pads.sm[1] : pads.sm[0],
											counter % 2 === 0 ?  pads.lg[1] : pads.lg[0],
											counter % 2 === 0 ?  pads.xl[1] : pads.xl[0],
										);

										let imgObj = typeof post.featured_media_obj !== 'undefined' ? post.featured_media_obj : false;
										let img_src = imgObj ? getImageSource(imgObj) : false;
										let title = typeof post.title !== 'undefined' ? post.title.raw : false;
										// let read_more_text = typeof post.acf.read_more_text !== 'undefined' ? post.acf.read_more_text : false;
										// let external_link = typeof post.acf.external_link !== 'undefined' ? post.acf.external_link : false;
										// let excerpt = typeof post.excerpt !== 'undefined' ?
										// 	post.excerpt.raw !== '' ? post.excerpt.raw : removeTags(post.excerpt.rendered) :
										// 	false;
										let linkProps = {
											// href: external_link ? external_link : post.link,
											// target: external_link ? '_blank' : null,
											href: post.link,
										};

										let btnClassArr = [...globals.btnClassArr];
										if(post === 'btn')
											btnClassArr.push(
												// attributes.records.length % 2 === 0 ?  'pr-2' : 'pl-2',
												attributes.records.length % 2 === 0 ?  pads.sm[1] : pads.sm[0],
												attributes.records.length % 2 === 0 ?  pads.lg[1] : pads.lg[0],
												attributes.records.length % 2 === 0 ?  pads.xl[1] : pads.xl[0],
											);




										return post !== 'btn' ? el( 'div',{ className: classArr.join(' ')},
											el('div', {className: 'hover-zoom-wrap anim-on-scroll anim-b-in position-relative',
												},
												el ('a', { className: 'd-inline-block w-100 bg-cover bg-grey-65 text white px-4 pb-4 pt-phi float-right hover-zoom',
													...linkProps,
													['data-image']: img_src,
													style : {
														backgroundImage : 'url("'+img_src+'")'
													}
												}),
												el('div', { className: 'display-on-parent-hover', style:{ pointerEvents: 'none'}}),

												el ('a', { className: 'position-absolute bg-white ml-0 mb-4 min-w-66perc p-2 h4',
													...linkProps,
													style : {
														[align]: 0,
														bottom: 0
													}
												}, title),
											)
											)  :
											el('div', { className: btnClassArr.join(' ')},
												el('div', { style: { paddingTop: '62.5%', position: 'relative' }},
													el('div', {
															style: {
																position: 'absolute',
																top: '30%',
																left: '50%',
																transform: 'translate(-50%, -50%)'
															}
														},
														// el('div', { className: 'btn btn-primary d-inline-block btn-hover-scale',
														// 	type: 'button',
														// 	role: 'button',
														// 	href: null, // <-- TODO input
														// }, 'View All Projects' // <-- TODO input
														// ),
														el(CwbButton, {
															btnObj: btnObj,
														})
													)
												)
											);
										})
								) : null
						),
					),
				),
			);
		},
	} );
}(
	window.wp.blocks,
	window.wp.editor,
	window.wp.i18n,
	window.wp.element,
	window.wp.components,
	window._,
	window.wp.blockEditor,
	window.cwb
) );



window.cwbOffsetPostGrid = {
	postTypeOptions : [
		{ value: 'post', label: 'Post'},
		{ value: 'project', label: 'Project'},
		// { value: 'patient_story', label: 'Patient Story'},
		// { value: '', label: ''},
	],
	classArr : [
		'item',
		'list-item',
		'width-1',
		'width-sm-2',
		'd-inline-block',
		'py-2',
		'py-lg-3',
		'py-xl-4',
		'text-left',
		'anim-on-scroll',
		'anim-alpha-in'
	],
	btnClassArr : [
		'item',
		'list-item',
		'width-1',
		'width-sm-2',
		'd-inline-block',
		'text-center',
		'anim-on-scroll',
		'anim-alpha-in',
	],
	offsetPadding : {

		// {size}: [{if false}, {if true}]
		base :  ['pr-4', 'pl-4'],
		sm :    ['pl-sm-2', 'pr-sm-2'],
		// md : [ 'pl-lg-3','pr-lg-3'],
		lg :    ['pl-lg-3', 'pr-lg-3'],
		xl :    ['pl-lx-4', 'pr-lx-4']
	}
};