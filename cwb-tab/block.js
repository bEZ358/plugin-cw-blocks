( function( blocks, editor, i18n, element, components, _, blockEditor, cwb ) {
	const __ = i18n.__;
	const el = element.createElement;
	const InnerBlocks = blockEditor.InnerBlocks;
	const useBlockProps = blockEditor.useBlockProps;
	const InspectorControls = blockEditor.InspectorControls;
	const select = wp.data.select;

	const {
	} = components;

	const {
		cwbGlobals,
		cwbComponents
	} = cwb;

	const {
	} = cwbGlobals;

	const {
		cwbAttributes,
	} = cwbComponents;


	// @ref links below
	// https://mediaron.com/how-to-create-a-repeater-field6-in-gutenberg/
	// https://reactnative.dev/docs/state

	console.log(WPURLS.siteurl);

	blocks.registerBlockType( 'cwb/tab', {


		// Block ASSETS
		//-----------------------------

		title: __( 'Tab', 'CWB' ),
		icon: 'admin-page', // see: /wp-includes/css/dashicons.css
		category: 'common',
		attributes: {
			anchor: {
				type: 'string',
				default: false,
			},
			isFirstTab: {
				type: 'boolean',
				default: false,
			},
			cssClasses: {
				type: 'array',
				default: ['my-0'],//['w-100', 'd-flex', 'm-0', 'align-items-center', 'bg-cover']
			},



			tabName: {
				type : 'string',
				default: 'tab name'
			},



			gridCssClasses: {
				type : 'string',
				default: ''
			},

			gapPaddingCss: {
				type : 'string',
				default: ''
			},

			// Common Attribute Sets
			//-----------------------------
			...cwbAttributes.__blockColors,
			...cwbAttributes.__bgImage,
			// ...cwbAttributes.__blockBorder,
			...cwbAttributes.__bgOverlayColors,
			overlayBlur: {
				type: 'integer',
				default: 0,
			},

			template: {
				type : 'array',
				default: [
					['core/image', {
						url: WPURLS.siteurl+'/wp-content/uploads/2023/12/icon-placeholder-80.png'
					}],
					['core/heading'],
					['core/paragraph'],
					// ['cwb/button'],
				]
			}
		},
		example: {
			attributes: {
				// title: __( 'Chocolate Chip Cookies', 'gutenberg-examples' ),
				// mediaID: 1,
				// mediaURL:
				// 	'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/2ChocolateChipCookies.jpg/320px-2ChocolateChipCookies.jpg',
			},
		},



		// Block FUNCTIONS
		//-----------------------------

		edit: function( props ) {
			let attributes = props.attributes;
			let blockStyle = {
				width: '100%',
				// margin: '0px',
				// padding: '0px',
				// display: 'flex',
				position: 'relative'
			};
			// const repItemBlocks = useSelect( ( select ) => select( 'core/block-editor' ).getBlock( props.clientId ).innerBlocks );
			// console.log('tab', props);



			if(!attributes.anchor){
				props.setAttributes({
					anchor: props.clientId
				})
			}



			// // Get & Set Dynamic Parent Block Attributes
			// //-----------------------------
			// let parentAtts = {...cwbAttributes.globalBlockAtts[props.clientId]};
			// props.setAttributes({
			// 	gridCssClasses : parentAtts.gridCssClasses,
			// 	gapPaddingCss : parentAtts.gapPaddingCss
			// });
			// attributes = props.attributes;


			// console.log('tab', props.clientId);

			// Set Grid & TileGap Classes
			//-----------------------------
			// let gridCssClasses = __get(parentAtts.gridCssClasses, attributes.gridCssClasses);
			// let gapPaddingCss = __get(parentAtts.gapPaddingCss, attributes.gapPaddingCss);
			let itemCss = [
				// gridCssClasses,
				// gapPaddingCss,
				'cwb-editor-outline',
				'tab',
				'mb-3'
			].join(' ');




			// RETURN: Tab
			//================================
			return el( 'div', {
					className: itemCss,
					style: blockStyle
				} ,
				el('div',  useBlockProps( {
						className: 'container px-0 cwb-editor-outline '+attributes.cssClasses.join(' '),
						style: {
							background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : attributes.bgColorOutput,
							color: attributes.textColor ? attributes.textColor : null,
						}
					}),


					// attributes.bgEffect ?
					el(CwbBgEffect, {
						bgOverlay : attributes.overlayColorOutput,
						backdropFilter: attributes.overlayBlur ?
							'blur('+attributes.overlayBlur+'px)' : null
					}),
					// : null,


					el(InnerBlocks, {
						allowedBlocks : [
							'core/image',
							'core/heading',
							'core/paragraph',
							'core/shortcode',
							'cwb/button',
							// plugin required: https://wordpress.org/plugins/icon-block/
							// - author mentions plugin will be merged into core
							'outermost/icon-block'
						],
						template: attributes.template
					})
				),


				// InspectorControls
				//================================
				el( InspectorControls, {
					key: 'controls' },

					el( 'div', {
							style: {
								padding: '0 20px 20px'
							}
						},
						attributes.isFirstTab ? el('p', {}, 'is first')
							: null,
						el( TextControl,
							{
								label: 'Tab Name',
								value: typeof attributes.tabName === 'string' ? attributes.tabName : null,
								onChange: function(value){
									props.setAttributes({
										tabName: value//limitForHtmlId(value)
									})
								},
							}
						),
					),

					// Panel : Block Colors
					//-----------------------------
					el( BlockColors, props),

					// // Panel : Background Image
					// //-----------------------------
					// el( BgImagePanel, {
					// 	props : props,
					// 	blockStyle : blockStyle
					// }),

					// Panel : BgEffects
					//-----------------------------
					el( BgEffects, props)

				)
			);
		},

		save: function( props ) {
			let attributes = props.attributes;
			let blockStyle = {
				background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` :
					attributes.bgColorOutput !== ''  ? attributes.bgColorOutput : null,
				margin: '0px',
				padding: '0px',
				width: '100%',
				// display: 'flex',
				color: attributes.textColor ? attributes.textColor : null,
				position: 'relative'
			};


			// Set Grid & TileGap CSS
			//-----------------------------
			let itemCss = [
				// attributes.gridCssClasses,
				// attributes.gapPaddingCss,
				'tab-pane fade'
			];
			if(attributes.isFirstTab) itemCss.push('show', 'active');


			// Repeater Item CSS
			//-----------------------------
			let tileCss = [
				...attributes.cssClasses,
				// 'container',
				// 'position-relative'
			];

			// RETURN: Layout-Block (FRONT-end)
			//================================
			return el( 'div', {
					id: typeof attributes.anchor === 'string' ? 'tab-'+attributes.anchor : null,
					className: itemCss.join(' '),
					style: blockStyle
				} ,
				el('div', useBlockProps.save({ className: tileCss.join(' ') }),

					// attributes.bgEffect ?
						el(CwbBgEffect, {
							bgOverlay : attributes.overlayColorOutput,
							backdropFilter: attributes.overlayBlur ?
								'blur('+attributes.overlayBlur+'px)' : null
						}),
						// : null,

					el('div', {className: 'position-relative'},
						// el(InnerBlocks.Content)
						InnerBlocks.Content()
					)
				)
			);
		},
	} );
}(
	window.wp.blocks,
	window.wp.editor,
	window.wp.i18n,
	window.wp.element,
	window.wp.components,
	window._,
	window.wp.blockEditor,
	window.cwb
) );
