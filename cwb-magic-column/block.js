( function( blocks, editor, i18n, element, components, _, blockEditor, cwb ) {
	const __ = i18n.__;
	const el = element.createElement;
	const InnerBlocks = blockEditor.InnerBlocks;
	const useBlockProps = blockEditor.useBlockProps;
	const InspectorControls = blockEditor.InspectorControls;

	const {
	} = components;

	const {
		cwbGlobals,
		cwbComponents
	} = cwb;

	const {
		// screenSizeTabs,
		// defaultScreenSizeObj
	} = cwbGlobals;

	const {
		cwbAttributes
	} = cwbComponents;


	// @ref links below
	// https://mediaron.com/how-to-create-a-repeater-field6-in-gutenberg/
	// https://reactnative.dev/docs/state

	blocks.registerBlockType( 'cwb/magic-column', {


		// Block ASSETS
		//-----------------------------

		title: __( 'Magic Column', 'CWB' ),
		icon: 'admin-page', // see: /wp-includes/css/dashicons.css
		category: 'common',
		attributes: {
			anchor: {
				type: 'string',
				default: false,
			},
			cssClasses: {
				type: 'array',
				default: ['my-0', 'col', 'magic-column'],//['w-100', 'd-flex', 'm-0', 'align-items-center', 'bg-cover']
			},


			// Common Attribute Sets
			//-----------------------------
			...cwbAttributes.__blockColors,
			...cwbAttributes.__bgImage,
			// ...cwbAttributes.__blockBorder,
			...cwbAttributes.__bgOverlayColors,
			overlayBlur: {
				type: 'integer',
				default: 0,
			},

			blockHeight: cwbAttributes.blockHeight,
			heightCss: {
				type : 'array',
				default: []
			},

			vAlign: cwbAttributes.vAlign,
			vAlignCss: {
				type : 'array',
				default: []
			},

			hAlign: cwbAttributes.hAlign,
			hAlignCss: {
				type : 'array',
				default: []
			},



			...cwbAttributes.__magicColumn,

			blockStyle: {
				type: 'object',
				default: ''
			},
		},
		example: {
			attributes: {
				// title: __( 'Chocolate Chip Cookies', 'gutenberg-examples' ),
				// mediaID: 1,
				// mediaURL:
				// 	'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/2ChocolateChipCookies.jpg/320px-2ChocolateChipCookies.jpg',
			},
		},



		// Block FUNCTIONS
		//-----------------------------

		edit: function( props ) {

			let attributes = 	props.attributes;
			let containerWidth 		= __get(attributes.containerWidth, 1400); // 1400
			let leftColWidth		= __get(attributes.leftColWidth, 50); // 25 < leftColWidth > 75
			let isLeftColumn		= __get(attributes.isLeftColumn, true);//attributes.isLeftColumn;
			let bgCover				= __get(attributes.bgCover, true);//attributes.isLeftColumn;

			// Calculate necessary offset Values
			//-----------------------------
			let colWidthDenominator = 100/leftColWidth ;
			let widthOffset 		= (containerWidth/colWidthDenominator);
			let wideScreenOffset = ((containerWidth/2)-widthOffset);

			// Clone Layout CSS
			//-----------------------------
			let heightCss = [...attributes.heightCss];
			let vAlignCss = [...attributes.vAlignCss];
			let hAlignCss = [...attributes.hAlignCss];

			// Block Style
			//-----------------------------
			let blockStyle = {
				width: '100%',
				padding: '0px',
				color: attributes.textColor ? attributes.textColor : null,
				background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : attributes.bgColorOutput,
				position: 'relative'
			};

			// Set Column CSS
			//-----------------------------
			let colCssExtra	= __exists(attributes.colCssExtra) ?
				attributes.colCssExtra.split(' ') : [];
			colCssExtra.push('cw-lazy-bg');
			if(bgCover) colCssExtra.push('bg-cover');
			if(isLeftColumn) colCssExtra.push('pr-md-0', 'd-flex','justify-content-end');
			if(!isLeftColumn) colCssExtra.push('pl-md-0');
			let magicColCss = [
				...colCssExtra,
				...attributes.cssClasses
			];

			// Set Content-Container CSS
			//-----------------------------
			let containerClasses = attributes.containerClasses.split(' ');
			let magicContentCss = [
				'w-100', 'magic-content', 'd-flex', //'align-items-center', 'justify-content-start',
				...heightCss,
				...vAlignCss,
				...hAlignCss,
				...containerClasses,
			];


			// Layout Mod Functions
			//-----------------------------
			const setVAlign = function(value) {
				genResponsiveCss(value, 'vAlign','align-items-', vAlignCss, attributes, props );
				props.setAttributes({ vAlignCss : vAlignCss });
				attributes = props.attributes;
			};
			const setHAlign = function(value) {
				genResponsiveCss(value, 'hAlign','text-', hAlignCss, attributes, props );
				props.setAttributes({ hAlignCss : hAlignCss });
				attributes = props.attributes;
			};
			const setBlockHeight = function(value) {
				genResponsiveCss(value, 'blockHeight','vh-', heightCss, attributes, props );
				props.setAttributes({
					heightCss : heightCss
				});
				attributes = props.attributes;
			};
			const onUpdateLayout = function(data, attrStr){
				switch(attrStr){
					case 'blockHeight': setBlockHeight(data); break;
					case 'vAlign': setVAlign(data); break;
					case 'hAlign': setHAlign(data); break;
					default: break;
				}
			};
			const setContainerClasses = function( value ) {
				props.setAttributes( { containerClasses: value } );
				attributes = props.attributes;
			};


			// states: for ResponsiveTabs
			//-----------------------------
			const [layoutTab, setLayoutTab] = useState(false);

			// console.log('w-100 magic-content '+colHeightCssClasses+' d-flex align-items-center justify-content-start ', magicContentCss);


			// RETURN: Block Editor
			//================================
			return el( 'div', useBlockProps({ className: magicColCss.join(' '),
					style: {
						...blockStyle,

						border: '1px dotted rgba(0,0,0,0.2)',
						maxWidth: isLeftColumn ?
							'calc(50% - '+ wideScreenOffset +'px)' : 'calc(50% + '+ wideScreenOffset +'px)',

						// Simple Background
						// background: rightColBg && !rightColLazyBg ? rightColBg : null
						// ^TODO: background: colBg && !colLazyBg ? colBgObj ? colBgObj.value : colBg : null,
					},
					// Lazy (Load) Background
					// ['data-bg']: colBg && colLazyBg ? colBgObj.value : null
				}),

				// BG Effects Overlay
				//-----------------------------
				el(CwbBgEffect, {
					bgOverlay : attributes.overlayColorOutput,
					backdropFilter: attributes.overlayBlur ?
						'blur('+attributes.overlayBlur+'px)' : null
				}),

				// Magic Content
				//-----------------------------
				el( 'div', { className: magicContentCss.join(' '),
						style: {
							maxWidth: isLeftColumn ?
								widthOffset + 'px' : (containerWidth-widthOffset) + 'px'
						}
					},

					// Content Position Wrap
					//-----------------------------
					el( 'div', { className: 'w-100'},
						// rightColContent

						el(InnerBlocks, {
							// allowedBlocks : [
							// 	'cwb/button',
							// ],

							// template : [
							// 	[ 'cwb/'+attributes.repeaterItemType, { heading: 'Slide 1' } ],
							// 	[ 'cwb/'+attributes.repeaterItemType, { heading: 'Slide 2' } ],
							// ],

							// renderAppender : allowMoreItems ? null : false
						}),
					)
				),

				// rightColHover ? rightColHover : null // todo not sure what this is... might need it for LON360


				// InspectorControls
				//================================
				el( InspectorControls, {
						key: 'controls' },
					el( 'div', {
							style: {
								padding: '0 20px'
							}
						},
						el( TextControl,
							{
								label: 'Block Anchor',
								value: typeof attributes.anchor === 'string' ? attributes.anchor : null,
								onChange: function(value){
									props.setAttributes({
										anchor: limitForHtmlId(value)
									})
								},
							}
						),
					),


					// Panel : Layout Settings
					//-----------------------------
					el( PanelBody, {
							title: 'Layout Settings',
							initialOpen: false//!0
						},

						// Block Layout Settings : Magic-Column
						//-----------------------------

						el(TextControl, {
							label: 'Container CSS Class(es)',
							help: 'Separate multiple classes with spaces.',
							value: attributes.containerClasses,
							type: 'text',
							onChange: setContainerClasses,
						}),

						el(ToggleControl, {
							label: 'Is Left Column',
							onChange: function (value) {
								props.setAttributes({
									isLeftColumn: value,
								});
								attributes = props.attributes;
							},
							checked: isLeftColumn,
							// help: focusItem.btn ? 'Click the slide button to edit it' : null,
						}),

						el(RangeControl, {
							label : 'Left Column - Content Width',
							value : attributes.leftColWidth,
							// help : 'help',
							// beforeIcon : 'arrowLeft',
							// afterIcon : 'arrowRight',
							min : 25,
							max : 75,
							onChange :function(value){
								props.setAttributes({
									leftColWidth : value
								});
								attributes = props.attributes;
							}
						}),

						el(RangeControl, {
							label : 'Container Width',
							value : attributes.containerWidth,
							min : 900,
							max : 1800,
							onChange :function(value){
								props.setAttributes({
									containerWidth : value
								});
								attributes = props.attributes;
							}
						}),


						// Block Layout Settings : standard
						//-----------------------------

						el(ResponsiveTabs, props,
							(tab) => {
								setLayoutTab(tab);
								// ^ fixes something (not sure what yet) to allow below to work
								props = {
									// tab:{...layoutTab},
									// ^ this would make more sense (maybe?) but somehow below gets fixed after
									//   setLayoutTab(tab) is called... don't understand why
									tab: {...tab},
									...props,
									onUpdateLayout: function(data, attrStr){
										onUpdateLayout(data, attrStr);
									}
								};
								// console.log('props.tab.name', props.tab.name);
								return el(BlockLayoutSettingsTab, props);
							}
						)
					),


					// Panel : Block Colors
					//-----------------------------
					el( BlockColors, props),

					// Panel : Background Image
					//-----------------------------
					el( BgImagePanel, {
						props : props,
						blockStyle : blockStyle
					}),

					// Panel : BgEffects
					//-----------------------------
					el( BgEffects, props),
				)
			)
		},

		save: function( props ) {
			let attributes = props.attributes;
			let blockStyle = {
				width: '100%',
				padding: '0px',
				color: attributes.textColor ? attributes.textColor : null,
				// background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : attributes.bgColorOutput,
				background: attributes.bgColorOutput, // <-- lazy-load version
				position: 'relative'
			};


			// Calculate necessary offset Values
			//-----------------------------
			let colWidthDenominator = 100/attributes.leftColWidth ;//3;
			let widthOffset 		= (attributes.containerWidth/colWidthDenominator);
			let wideScreenOffset = ((attributes.containerWidth/2)-widthOffset);

			// Set Column CSS
			//-----------------------------
			let colCssExtra	= __exists(attributes.colCssExtra) ?
				attributes.colCssExtra.split(' ') : [];
			if(attributes.mediaURL ) colCssExtra.push('cw-lazy-bg');
			if(attributes.bgCover) colCssExtra.push('bg-cover');
			if(attributes.isLeftColumn) colCssExtra.push('pr-md-0', 'd-flex','justify-content-end');
			if(!attributes.isLeftColumn) colCssExtra.push('pl-md-0');
			let magicColCss = [
				...colCssExtra,
				...attributes.cssClasses
			];

			// Set Content-Container CSS
			//-----------------------------
			let heightCss = __get(attributes.heightCss,[]);
			let vAlignCss = __get(attributes.vAlignCss,[]);
			let hAlignCss = __get(attributes.hAlignCss,[]);
			let containerClasses = attributes.containerClasses.split(' ');
			let magicContentCss = [
				'w-100', 'magic-content', 'd-flex', //'align-items-center', 'justify-content-start',
				...heightCss,
				...vAlignCss,
				...hAlignCss,
				...containerClasses,
			];





			// RETURN: Layout-Block (FRONT-end)
			//================================
			return el( 'div', useBlockProps.save( {className: magicColCss.join(' '),
					id: typeof attributes.anchor === 'string' ? attributes.anchor : null,
					style: {
						...blockStyle,
						maxWidth: attributes.isLeftColumn ?
							'calc(50% - '+ wideScreenOffset +'px)' : 'calc(50% + '+ wideScreenOffset +'px)',
					},
					// Lazy (Load) Background
					['data-bg']:  attributes.mediaURL !== '' ? attributes.mediaURL : null
				}),

				// BG Effects Overlay
				//-----------------------------
				el(CwbBgEffect, {
					bgOverlay : attributes.overlayColorOutput,
					backdropFilter: attributes.overlayBlur ?
						'blur('+attributes.overlayBlur+'px)' : null
				}),

				// Magic Content
				//-----------------------------------------
				el( 'div', { className: magicContentCss.join(' '),
						style: {
							position: 'relative',
							maxWidth: attributes.isLeftColumn ?
								widthOffset + 'px' : (attributes.containerWidth-widthOffset) + 'px'
						}
					},

					// Content Position Wrap
					//-----------------------------------------
					el( 'div', { className: 'w-100'},

						el( InnerBlocks.Content )
					)
				),
			);
		},
	} );
}(
	window.wp.blocks,
	window.wp.editor,
	window.wp.i18n,
	window.wp.element,
	window.wp.components,
	window._,
	window.wp.blockEditor,
	window.cwb
) );
