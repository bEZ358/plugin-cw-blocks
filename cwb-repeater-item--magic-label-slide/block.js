( function( blocks, editor, i18n, element, components, _, blockEditor, cwb ) {
	const __ = i18n.__;
	const el = element.createElement;
	const InnerBlocks = blockEditor.InnerBlocks;
	const useBlockProps = blockEditor.useBlockProps;
	const InspectorControls = blockEditor.InspectorControls;
	const { Fragment } = element;

	const {
		// __experimentalRadio,
		// __experimentalRadioGroup
	} = components;
	const Radio = components.__experimentalRadio;
	const RadioGroup = components.__experimentalRadioGroup;

	const {
		cwbGlobals,
		cwbComponents
	} = cwb;

	const {
	} = cwbGlobals;

	const {
		cwbAttributes
	} = cwbComponents;


	// @ref links below
	// https://mediaron.com/how-to-create-a-repeater-field6-in-gutenberg/
	// https://reactnative.dev/docs/state

	blocks.registerBlockType( 'cwb/repeater-item--magic-label-slide', {


		// Block ASSETS
		//-----------------------------

		title: __( 'Repeater Item (Magic Label Slide)', 'CWB' ),
		icon: 'admin-page', // see: /wp-includes/css/dashicons.css
		category: 'common',
		attributes: {
			cssClasses: {
				type: 'array',
				default: ['slide'],//['w-100', 'd-flex', 'm-0', 'align-items-center', 'bg-cover']
			},

			// Common Attribute Sets
			//-----------------------------
			...cwbAttributes.__blockColors,
			...cwbAttributes.__bgImage,
			...cwbAttributes.__bgOverlayColors,
			overlayBlur: {
				type: 'integer',
				default: 0,
			},



			// Slide Layout CSS Attributes 
			//-----------------------------
			slideInnerClasses: {
				type: 'string',
				default: ''
			},
			vAlign: cwbAttributes.vAlign,
			vAlignCss: {
				type : 'array',
				default: []
			},



			// Magic Column CSS Attributes 
			//-----------------------------
			...cwbAttributes.__magicColumn,
			contentHeight: {
				type: 'object',

				// Set Default as empty string (due to bug in gutenberg)
				// - @ref: https://github.com/WordPress/gutenberg/issues/37967
				default: '',
				// - Use this when bug is fixed:
				// default: JSON.parse(JSON.stringify(defaultScreenSizeObj)), // copies default obj
			},
			contentHeightCss: { // todo remove
				type : 'array',
				default: []
			},



			// Slide Content Attributes 
			//-----------------------------
			heading: {
				type : 'string',
				default: 'Default Heading',
				// selector: 'h2'
			},
			body: {
				type : 'string',
				default: 'Default Body, lorem ipsum...'
			},
			postObj: {
				type: 'object',

				// Set Default as empty string (due to bug in gutenberg)
				// - @ref: https://github.com/WordPress/gutenberg/issues/37967
				default: '',
				// - Use this when bug is fixed:
				// default: JSON.parse(JSON.stringify(defaultScreenSizeObj)), // copies default obj
			},
			btnObj: {
				type: 'object',
				default: false
			},
			layout: {
				type: 'string',
				default: 'left'
			},

			labelClasses: {
				type: 'string',
				default: ''
			},

		},
		example: {
			attributes: {
				// title: __( 'Chocolate Chip Cookies', 'gutenberg-examples' ),
				// mediaID: 1,
				// mediaURL:
				// 	'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/2ChocolateChipCookies.jpg/320px-2ChocolateChipCookies.jpg',
			},
		},



		// Block FUNCTIONS
		//-----------------------------

		edit: function( props ) {
			let postType = 'project'; // <-- todo: via editor
			let attributes = props.attributes;
			let vAlignCss = [...attributes.vAlignCss];
			// let hAlignCss = [...attributes.hAlignCss];
			let contentHeightCss = [...attributes.contentHeightCss];
			let blockStyle = {
				width: '100%',
				// margin: '0px auto',
				padding: '0px',
				border: '1px dotted rgba(0,0,0,0.2)',
				color: attributes.textColor ? attributes.textColor : null,
				background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : attributes.bgColorOutput,
				position: 'relative'
			};




			/* Layout Mod Functions */
			/* ----------------------------- */
			const setVAlign = function(value) {
				genResponsiveCss(value, 'vAlign','align-items-', vAlignCss, attributes, props );
				props.setAttributes({ vAlignCss : vAlignCss });
				attributes = props.attributes;
			};
			const setContentHeight = function(value) {
				let updateVal = JSON.parse(value);
				let updatedAttr = attributes.contentHeight !== '' ? attributes.contentHeight : {};
				updatedAttr[updateVal[0]] = updateVal[1] ? updateVal[1] : false;
				genResponsiveCss(value, 'contentHeight','vh-', contentHeightCss, attributes, props );
				props.setAttributes({
					contentHeight : updatedAttr,
					contentHeightCss : contentHeightCss
				});
				attributes = props.attributes;
			};
			const onUpdateLayout = function(data, attrStr){

				// console.log('onUpdateLayout', attrStr, data);

				switch(attrStr){
					case 'vAlign':
						setVAlign(data);
						break;
					case 'hAlign':
						setHAlign(data);
						break;
					case 'contentHeight':
						setContentHeight(data);
						break;
					default:
						break;
				}
			};

			// Pre-Save Object type Attributes (due to bug in gutenberg)
			// - @ref: https://github.com/WordPress/gutenberg/issues/37967
			//-----------------------------
			if(attributes.contentHeight === '') {
				props.setAttributes( { contentHeight: {} } );
				props.setAttributes( { contentHeight: JSON.parse(JSON.stringify(defaultScreenSizeObj)) } )
			}
			if(attributes.vAlign === '') {
				props.setAttributes( { vAlign:{} } );
				props.setAttributes( { vAlign: JSON.parse(JSON.stringify(defaultScreenSizeObj)) } )
			}




			/* CSS Mod Functions */
			/* ----------------------------- */
			const setContainerClasses = function( value ) {
				props.setAttributes( { containerClasses: value } );
				attributes = props.attributes;
			};
			const setSlideInnerClasses = function( value ) {
				props.setAttributes( { slideInnerClasses: value } );
				attributes = props.attributes;
			};
			const setLabelClasses = function( value ) {
				props.setAttributes( { labelClasses: value } );
				attributes = props.attributes;
			};




			/* for ResponsiveTabs */
			/* ----------------------------- */
			const [layoutTab, setLayoutTab] = useState(false);
			const gencontentHeightOptions = function(tab){

				let prefix = tab.name === 'base' ? '' : tab.name+'-';

				return [
					{ value: JSON.stringify([tab.name, 0]), label: tab.name === 'base' ? 'default' : 'inherit from smaller' },
					{ value: JSON.stringify([tab.name, 'vh-'+prefix+'25-min']), label: tab.name+'-25' },
					{ value: JSON.stringify([tab.name, 'vh-'+prefix+'33-min']), label: tab.name+'-33' },
					{ value: JSON.stringify([tab.name, 'vh-'+prefix+'50-min']), label: tab.name+'-50' },
					{ value: JSON.stringify([tab.name, 'vh-'+prefix+'66-min']), label: tab.name+'-66' },
					{ value: JSON.stringify([tab.name, 'vh-'+prefix+'75-min']), label: tab.name+'-75' },
					{ value: JSON.stringify([tab.name, 'vh-'+prefix+'100-min']), label: tab.name+'-100' },
				]
			};




			/* Logic for: Content via WP Post */
			/* ----------------------------- */

			// - get Post title & thumbnail id
			const selectedPost = useSelect((select) => {
				return wp.data.select('core').getEntityRecord('postType','project', attributes.postObj.id);
			});
			let postObj = {...attributes.postObj};
			if(selectedPost){
				postObj = {
					...attributes.postObj,
					title: selectedPost.title.raw,
					featured_media: selectedPost.featured_media,
					link: selectedPost.link
				};
			}

			// - get post thumbnail
			const slideImage = useSelect((select) => {
				return wp.data.select('core').getEntityRecord('postType','attachment', postObj.featured_media);
			});
			if(slideImage){
				postObj = {
					...postObj,
					image: slideImage
				};
			}

			// // - get post excerpt
			// const excerpt = useSelect((select) => {
			// 	return wp.data.select('core').getEntityRecord('postType','excerpt', postObj.id);
			// });
			// if(excerpt){
			// 	console.log('excerpt');
			// }
			// console.log('postObj', postObj, attributes.postObj);

			// - Button for populating slide with WP Post content
			if(!attributes.btnObj){
				props.setAttributes({
					btnObj : {...cwbAttributes.defaultButtonObj}
				});
				attributes = props.attributes;
			}
			function updateBtn(value, attrStr){
				let newBtnObj = {...attributes.btnObj};
				newBtnObj[attrStr] = value;
				props.setAttributes( {
					btnObj: newBtnObj
				} );
				attributes = props.attribute
			};




			/* RETURN: Simple-Repeater-Block (BACK-end) */
			/* ================================ */
			return el( 'div', useBlockProps( {
					className: attributes.cssClasses,
					style: blockStyle
				} ),



				/* CwbBgEffect */
				/* ----------------------------- */
				el(CwbBgEffect, {
					bgOverlay : attributes.overlayColorOutput,
					backdropFilter: attributes.overlayBlur ?
						'blur('+attributes.overlayBlur+'px)' : null
				}),



				/* Slide Content */
				/* ----------------------------- */
				el('div', {
						className: [
							// attributes.containerClasses,
							...attributes.vAlignCss,
							'd-flex',
							'slide-flex',
							attributes.slideInnerClasses
						].join(' ')
					},
					el('div', {
							className: [
								// ...attributes.hAlignCss,
								'slide-content w-100 position-relative',
								''
							].join(' ')
						},

						el(MagicLabel, {
							leftColWidth : attributes.leftColWidth, // 33,
							containerWidth : attributes.containerWidth, // 1400,
							// leftColContent : function(){
							// 	return el(RichTextBlockInput, {
							// 		currentValue : attributes.contentLeft,
							// 		onChange : function(value){
							// 			props.setAttributes({
							// 				contentLeft : value
							// 			});
							// 			attributes = props.attributes;
							// 		},
							// 	})
							// }(),

							layout: attributes.layout,
							labelCss: [attributes.labelClasses],
							labelContentCss: [attributes.containerClasses],
							labelContent: function(){
								return el('div', {} ,

									el('p', {
											className: 'item-head h2',
											style: {
												position: 'relative'
											}
										},
										el(RichTextBlockInput, {
											currentValue : attributes.heading,
											onChange : function(value){

												props.setAttributes({
													heading : value
												});
												attributes = props.attributes;
											},
											multiLine: false
										}),
									),

									// el('p', {
									// 		className: 'item-body',
									// 		style: {
									// 			position: 'relative'
									// 		}
									// 	},
									// 	el(RichTextBlockInput, {
									// 		currentValue : attributes.body,
									// 		onChange : function(value){
									//
									// 			props.setAttributes({
									// 				body : value
									// 			});
									// 			attributes = props.attributes;
									// 		},
									// 		multiLine: false
									// 	}),
									// ),

									el(CwbEditableButton, {
										btnObj: attributes.btnObj,
										allowedBtnTypes  : [
											// 'link',
											'wp_link',
											// 'modal_trigger',
											// 'iframe',
											// 'shortcode_modal_trigger'
										],
										// btnObj : {
										// 	text			: attributes.text,
										// 	// url 			: attributes.href,
										// 	btnFunction 	: attributes.btnFunction,
										// 	target			: attributes.target, // todo should be dataTarget because thats how CwbButton uses it
										// 	// newTab 			: attributes.openNewTab,
										// 	// // dataToggle 	: attributes.dataToggle,
										// 	// dataShortcode 	: attributes.dataShortcode,
										// 	dataModalHeading: attributes.popupHeading,
										// 	dataModalSize 	: attributes.popupSize,
										// 	// dataSrc 		: attributes.dataSource,
										// 	// postObj 		: attributes.postObj,
										// 	btnType 		: attributes.btnType,
										// 	btnSize 		: attributes.btnSize,
										// 	hoverScale 		: attributes.hoverScale,
										//
										// 	// permalink 		: attributes.permalink, // not part of default btnObj
										// },
										updateText : function(value){
											updateBtn(value, 'text');
										},
										updateUrl : function(value){
											updateBtn(value, 'href');
										},
										updateNewTab : function(value){
											updateBtn(value, 'newTab');
										},
										setButtonFunction : function(value){
											updateBtn(value, 'btnFunction');
										},
										updateDataSrc : function(value){
											updateBtn(value, 'dataSource');
										},
										updateDataTarget : function(value){
											updateBtn(value, 'target');
										},
										updateDataToggle : function(value){
											// props.setAttributes( {
											// 	dataToggle: value
											// } );
											// attributes = props.attributes;
										},
										updateShortcode : function(value){
											updateBtn(value, 'dataShortcode');
										},
										updateModalHeading : function(value){
											updateBtn(value, 'popupHeading');
										},
										updateModalSize : function(value){
											updateBtn(value, 'popupSize');
										},
										updatePostObj : function(value){
											updateBtn(value, 'postObj');
											// console.log('LinkControl', attributes.postObj);
										},




										// todo  btn styles
										updateBtnType : function(value){
											updateBtn(value, 'btnType');
										},
										updateBtnSize : function(value){
											updateBtn(value, 'btnSize');
										},
										updateHoverScale : function(value){
											updateBtn(value, 'hoverScale');
										},
									})

									// el(InnerBlocks, {
									// 	allowedBlocks : [
									// 		'cwb/button',
									// 	],
									// 	// template : [
									// 	// 	[ 'cwb/'+attributes.repeaterItemType, { heading: 'Slide 1' } ],
									// 	// 	[ 'cwb/'+attributes.repeaterItemType, { heading: 'Slide 2' } ],
									// 	// ],
									// 	// renderAppender : allowMoreItems ? null : false
									// }),
								);
							}()
						}),
					),
				),




				/* InspectorControls */
				/* ================================ */
				el( InspectorControls, {
						key: 'controls' },



					/* Panel : Slide Content */
					/* ----------------------------- */
					el( PanelBody, {
							title: 'Slide Content',
							initialOpen: false//!0
						},

						/* Slide Content via WP Post */
						/* ----------------------------- */
						el('div', {className: 'cw-link-control'},

							/* Post Selector */
							/* ----------------------------- */
							el(LinkControl, {
								searchInputPlaceholder: "Search here...",
								value: attributes.postObj !== '' ? attributes.postObj : '',//focusItem.postObj,
								onChange: function (linkObj) {
									props.setAttributes({
										postObj : linkObj
									});
									attributes = props.attributes;
								},
								settings : [], // disable "open in new tab" toggle
								suggestionsQuery : postType ? {type: 'post', subtype: postType} : null,
								// withCreateSuggestion : true // https://wordpress.stackexchange.com/questions/399076/gutenberg-linkcontrol-suggestionsquery-not-working
							}),

							/* Btn : Populates Slide with Post Data */
							/* ----------------------------- */
							typeof postObj === 'object' ?
								el(components.Button, {
									className: 'button button-large cursor-pointer mb-4',
									onClick: function(){

										let newBtnObj = {...attributes.btnObj};
										newBtnObj.text = 'View '+toTitleCase(postType);
										newBtnObj.href = typeof postObj.url !== 'undefined' ? postObj.url : postObj.link;

										props.setAttributes({
											btnObj: newBtnObj,
											heading: postObj.title,
											// body: postObj.excerpt,
											mediaURL: postObj.image.media_details.sizes['web-full'].source_url,
											mediaID: postObj.image.id,
											mediaAlt: postObj.image.alt_text
										});
										attributes = props.attributes;
									}
								}, postType ? 'Use '+toTitleCase(postType)+' Content' : 'Use WP Content')
								: null,
						)
					),


					/* Panel : Layout Settings */
					/* ----------------------------- */
					el( PanelBody, {
							title: 'Layout Settings',
							initialOpen: false//!0
						},

						/* Left Col Width */
						/* ----------------------------- */
						el(RangeControl, {
							value : attributes.leftColWidth,
							label : 'Left Column - Content Width',
							// help : 'help',
							// beforeIcon : 'arrowLeft',
							// afterIcon : 'arrowRight',
							min : 25,
							max : 75,
							onChange :function(value){
								props.setAttributes({
									leftColWidth : value
								});
								attributes = props.attributes
							}
						}),

						/* Container Width */
						/* ----------------------------- */
						el(RangeControl, {
							value : attributes.containerWidth,
							label : 'Container Width',
							min : 900,
							max : 1800,
							onChange :function(value){
								props.setAttributes({
									containerWidth : value
								});
								attributes = props.attributes
							}
						}),

						/* Slide Inner CSS TODO: use block classes instead? */
						/* ----------------------------- */
						el( TextControl, {
							label: __( 'Slide Inner CSS Class(es)'),
							help: 'Separate multiple classes with spaces.',
							value: attributes.slideInnerClasses,
							type: 'text',
							onChange: setSlideInnerClasses,
						}),

						/* Label CSS */
						/* ----------------------------- */
						el( TextControl, {
							label: __( 'Label CSS Class(es)'),
							help: 'Separate multiple classes with spaces.',
							value: attributes.labelClasses,
							type: 'text',
							onChange: setLabelClasses,
						}),

						/* Content Container CSS */
						/* ----------------------------- */
						el( TextControl, {
							label: __( 'Content Container CSS Class(es)'),
							help: 'Separate multiple classes with spaces.',
							value: attributes.containerClasses,
							type: 'text',
							onChange: setContainerClasses,
						}),



						el(RadioGroup, {
							label: 'Horizontal Alignment',
							defaultChecked: attributes.layout,
							onChange: function(value){
								props.setAttributes({
									layout: value
								});
							}
						},[
							el(Radio,{ value: 'left'}, 'left'),
							el(Radio,{ value: 'right'}, 'right')
							]
						),
						el(ResponsiveTabs, props,
							(tab) => {
								setLayoutTab(tab);
								// ^ fixes something (not sure what yet) to allow below to work
								props = {
									// tab:{...layoutTab},
									// ^ this would make more sense (maybe?) but somehow below gets fixed after
									//   setLayoutTab(tab) is called... don't understand why
									tab: {...tab},
									...props,
								};
								return el(BlockLayoutSettingsTab, {
									...props,
									onUpdateLayout: function(data, attrStr){
										onUpdateLayout(data, attrStr)
									}
								},
									el('div',{},
										el( SelectControl, {
											key: 'multiple',
											className: ['mt-3'],

											label: __('Content Height'),
											value: JSON.stringify([tab.name, attributes.contentHeight[tab.name]]),
											onChange: function(value){
												onUpdateLayout(value, 'contentHeight');
											},
											options: gencontentHeightOptions(tab)
										}),
									)
								);
							}
						)
					),


					// Panel : Block Colors
					//-----------------------------
					el( BlockColors, props),

					// Panel : Background Image
					//-----------------------------
					el( BgImagePanel, {
						props : props,
						blockStyle : blockStyle
					}),

					// Panel : BgEffects
					//-----------------------------
					el( BgEffects, props),
				)
			);
		},



		save: function( props ) {
			let attributes = props.attributes;
			let includeAltText = attributes.includeAriaLabel && attributes.mediaAlt.length;
			let blockStyle = {
				// background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : attributes.bgColorOutput,
				margin: '0px',
				padding: '0px',
				width: '100%',
				color: attributes.textColor ? attributes.textColor : null,
				background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : attributes.bgColorOutput,
				position:'relative'
				// color: attributes.textColor ? attributes.textColor : null,
			};


			// RETURN: Layout-Block (FRONT-end)
			//================================
			return el( 'div', useBlockProps.save( {
					className: attributes.cssClasses.join(' '),
					style: blockStyle,
					role: includeAltText ? 'img' : null,
					['aria-label']: includeAltText ? attributes.mediaAlt : null,
				} ),


				// attributes.bgEffect ?
				el(CwbBgEffect, {
					bgOverlay : attributes.overlayColorOutput,
					backdropFilter: attributes.overlayBlur ?
						'blur('+attributes.overlayBlur+'px)' : null
				}),
				// : null,

				el('div', {
						className: [
							// attributes.containerClasses,
							...attributes.vAlignCss,
							'd-flex',
							'slide-flex',
							attributes.slideInnerClasses
						].join(' ')
					},

					el('div', {
							className: [
								// ...attributes.hAlignCss,
								'slide-content w-100 sticky-buffer-child position-relative',
							].join(' ')
						},
						el(MagicLabel, {
							leftColWidth : attributes.leftColWidth, // 33,
							containerWidth : attributes.containerWidth, // 1400,

							layout: attributes.layout,
							labelCss: [attributes.labelClasses],
							labelContentCss: [attributes.containerClasses],
							labelContent: function(){
								return el(Fragment, {} ,

									el('p', {
											className: 'item-head h2 mb-3',
											style: {
												position: 'relative'
											}
										}, attributes.heading
									),

									// el('p', {
									// 		className: 'item-body',
									// 		style: {
									// 			position: 'relative'
									// 		}
									// 	},attributes.body,

									el(CwbButton, {
										btnObj : attributes.btnObj,
										// extraBtnCss : setBtnCssClasses(btnObj, blockProps.className),
										// btnType : ''
									})

									// el(InnerBlocks),
								);
							}(),
						}),

						// el(MagicColumns, {
						// 	// content left v. content right<-- todo: via editor
						// 	colHeightCssClasses: attributes.contentHeightCss.join(' '),//attributes.colHeightCssClasses,
						// 	// reverseOnMobile: '',
						// 	leftColWidth : attributes.leftColWidth, // 33,
						// 	containerWidth : attributes.containerWidth, // 1400,
						// 	// leftColContent : function(){
						// 	// 	return el(RichTextBlockInput, {
						// 	// 		currentValue : attributes.contentLeft,
						// 	// 		onChange : function(value){
						// 	// 			props.setAttributes({
						// 	// 				contentLeft : value
						// 	// 			});
						// 	// 			attributes = props.attributes;
						// 	// 		},
						// 	// 	})
						// 	// }(),
						// 	rightContentCss: attributes.containerClasses,
						// 	// rightColCss: 'ml-6', // <-- todo: via editor
						// 	rightColBg: {
						// 		type: 'color',
						// 		value: '#ffffff'
						// 	}, // <-- todo: via editor
						// 	rightColContent : function(){
						// 		return el('div', {} ,
						//
						// 			el('p', {
						// 					className: 'item-head h2',
						// 					style: {
						// 						position: 'relative'
						// 					}
						// 				}, attributes.heading
						// 			),
						//
						// 			// el('p', {
						// 			// 		className: 'item-body',
						// 			// 		style: {
						// 			// 			position: 'relative'
						// 			// 		}
						// 			// 	},attributes.body,
						//
						// 			el(CwbButton, {
						// 				btnObj : attributes.btnObj,
						// 				// extraBtnCss : setBtnCssClasses(btnObj, blockProps.className),
						// 				// btnType : ''
						// 			})
						//
						// 			// el(InnerBlocks),
						// 		);
						// 	}()
						// }),
					)
				)
			);
		},
	} );
}(
	window.wp.blocks,
	window.wp.editor,
	window.wp.i18n,
	window.wp.element,
	window.wp.components,
	window._,
	window.wp.blockEditor,
	window.cwb
) );
