( function( blocks, editor, i18n, element, components, _, blockEditor, cwb ) {
	const __ = i18n.__;
	const el = element.createElement;
	const InnerBlocks = blockEditor.InnerBlocks;
	const useBlockProps = blockEditor.useBlockProps;
	const InspectorControls = blockEditor.InspectorControls;
	const MediaPlaceholder = blockEditor.MediaPlaceholder;
	const { Fragment } = element;

	// const {
	// 	ToggleControl,
	// 	Panel,
	// 	PanelBody,
	// 	PanelRow
	// } = components;

	const {
		cwbGlobals,
		cwbComponents
	} = cwb;

	const {
		cwbAttributes
	} = cwbComponents;


	blocks.registerBlockType( 'cwb/grid-gallery', {


		// Block ASSETS
		//-----------------------------

		title: __( 'CWB: Grid Gallery', 'CWB' ),
		icon: 'images-alt2',
		category: 'common',
		attributes: {
			sliderId: {
				type: 'number',
				default: false
			},
			cssClasses: {
				type: 'array',
				default: ['p-0', 'my-0']
			},
			grid: {
				type: 'object',

				// Set Default as empty string (due to bug in gutenberg)
				// - @ref: https://github.com/WordPress/gutenberg/issues/37967
				default: '',
				// - Use this when bug is fixed:
				// default: JSON.parse(JSON.stringify(defaultScreenSizeObj)), // copies default obj
			},

			tileGap: {
				type: 'object',

				// Set Default as empty string (due to bug in gutenberg)
				// - @ref: https://github.com/WordPress/gutenberg/issues/37967
				default: '',
				// - Use this when bug is fixed:
				// default: JSON.parse(JSON.stringify(defaultScreenSizeObj)), // copies default obj
			},

			images: {
				type: 'array',
				default: ''
			},

			modalHeading: {
				type: 'string',
				default: 'Image Slider'
			},
		},
		example: {
			attributes: {
				title: __( 'Chocolate Chip Cookies', 'gutenberg-examples' ),
				// mediaID: 1,
				// mediaURL:
				// 	'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/2ChocolateChipCookies.jpg/320px-2ChocolateChipCookies.jpg',
			},
		},


		// Block FUNCTIONS
		//-----------------------------

		edit: function( props ) {
			let attributes = props.attributes;
			let blockStyle = {
				width: '100%',
				margin: '0px auto',
				padding: '0px',
				color: attributes.textColor ? attributes.textColor : null,
			};

			// Set Slider ID upon Block instantiation
			if (!attributes.sliderId){
				props.setAttributes({
					sliderId: Math.floor(Math.random() * 999999)
				});
				attributes = props.attributes
			}

			let gridCssClasses = genResponsiveClasses(attributes.grid);

			if(attributes.tileGap === '') props.setAttributes({
				tileGap: {...defaultScreenSizeObj, base: '2', md: '3'},
			});
			let tileGap = genTileGapCss(attributes.tileGap);
			let gapMarginCss = tileGap.gapMarginCss;
			let gapPaddingCss = tileGap.gapPaddingCss;

			// console.log('gridCssClasses', gridCssClasses);


			// Block States
			//--------------------------------------

			const [gridTab, setGridTab] = useState(false);


			// RETURN: Gallery Block Editor
			//================================
			return el( 'div', useBlockProps( {
				className: attributes.cssClasses,
				style: blockStyle } ),


				// Gallery Editor Block
				//================================
				el(CwGalleryEditor, {
					images: attributes.images,
					gridCssClasses 	: gridCssClasses,
					gapMarginCss 	: gapMarginCss,
					gapPaddingCss 	: gapPaddingCss,
					onSelect: function (images) {
						// console.log(images);
						props.setAttributes({ images : images });
						attributes = props.attributes;
					},
				}),


				// InspectorControls
				//================================
				el( InspectorControls, { key: 'controls' },

					el( PanelBody, {
							title: 'Header Layout',
							initialOpen: true//!0,
						},


						el(ResponsiveTabs, props,
							(gridTab) => {
								setGridTab(gridTab);
								props = {
									tab: {...gridTab},
									...props,
									// allowedGridConfigs: ['tile-width'],
									gridCssClasses: gridCssClasses,
									onUpdateLayout: function(attrStr, responsiveCssObj){
										// console.log('layoutTab', attrStr, responsiveCssObj);
									}
								};
								return el(GridSettingsTab, props);
							}
						),

						// Item Controls:
						el( TextControl,
							{
								label: 'Modal Heading',
								value: attributes.modalHeading,
								onChange: function(value){
									props.setAttributes({ modalHeading : value });
									attributes = props.attributes;
								}
							}
						),
					)

				)
			);
		},

		save: function( props ) {

			let attributes = props.attributes;
			let blockStyle = {
				margin: '0px',
				padding: '0px',
				width: '100%',
				color: attributes.textColor ? attributes.textColor : null,
			};
			let modalId = 'image-slider-'+ attributes.sliderId;
			let imageIndex = 0;

			let gridCssClasses = genResponsiveClasses(attributes.grid);
			let tileGap = genTileGapCss(attributes.tileGap);
			let gapMarginCss = tileGap.gapMarginCss;
			let gapPaddingCss = tileGap.gapPaddingCss;


			// RETURN: Gallery-Block (FRONT-end)
			//================================
			return el( 'div', useBlockProps.save( {
					className: attributes.cssClasses.join(' '),
					style: blockStyle,
					['last-block-update']: '20230607'
				} ),


				// (The Gallery)
				//-----------------------------
				el(CwGridGallery, {
					images : attributes.images,
					gridCssClasses 	: gridCssClasses,
					gapMarginCss 	: gapMarginCss,
					gapPaddingCss 	: gapPaddingCss,

					// Gallery Image
					//-----------------------------
					imageEl : function (image) {
						imageIndex++;

						return el(CwGalleryImage, {
							lazyLoad: true,
							captionOnHover: true,
							image : image,
							imgSize : 'mobile-crop',
							modalId : modalId,
							imageIndex : imageIndex,
							extraWrapClass : 'anim-on-scroll anim-b-in', // requires ez-core (wp_parent_theme) v3.0.13
							// extraImgClass : 'anim-on-scroll anim-alpha-in', // requires ez-core (wp_parent_theme) v3.0.13
						})
					}
				}),


				// JQuery Image Slider Modal
				//-----------------------------
				el(BootstrapModal, {
					modalId: modalId,
					modalHeading: attributes.modalHeading
					},
					el(CwImageSlider, {
						images: attributes.images,
						sliderId: typeof attributes.sliderId !== 'undefined' ?
							attributes.sliderId : null,
						lazyLoad: true
					})
				)
			);
		},
	} );
}(
	window.wp.blocks,
	window.wp.editor,
	window.wp.i18n,
	window.wp.element,
	window.wp.components,
	window._,
	window.wp.blockEditor,
	window.cwb
) );
