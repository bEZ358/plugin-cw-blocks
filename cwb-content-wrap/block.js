// console.log(window.cwb);

( function( blocks, editor, i18n, element, components, _, blockEditor, cwb ) {
	// @ref https://wordpress.github.io/gutenberg/?path=/story/docs-introduction--page

	const __ = i18n.__;
	const el = element.createElement;
	const InnerBlocks = blockEditor.InnerBlocks;
	const useBlockProps = blockEditor.useBlockProps;
	const InspectorControls = blockEditor.InspectorControls;

	// // const PanelBody = components.PanelBody;
	// const {
	// 	SelectControl,
	// 	TextControl,
	// 	TabPanel,
	// 	PanelBody
	// } = components;


	const {
		cwbGlobals,
		cwbComponents
	} = cwb;

	const {
		screenSizeTabs,
		defaultScreenSizeObj
	} = cwbGlobals;

	const {
		BgImagePanel,
		cwbAttributes,
		BlockColors
	} = cwbComponents;



	blocks.registerBlockType( 'cwb/content-wrap', {


		// Block ASSETS
		//-----------------------------

		title: __( 'CWB: Content Wrap', 'CWB' ),
		description: 'Allows vertical alignment of content, background colors with opacity values, and background images.',
		icon: 'layout',
		category: 'common',
		attributes: {
			anchor: {
				type: 'string',
				default: false,
			},
			cssClasses: {
				type: 'array',
				// default: ['w-100', 'd-flex', 'm-0', 'align-items-center', 'bg-cover']
				default: ['w-100', 'd-flex', 'm-0', 'bg-cover']
			},

			// Common Attribute Sets
			//-----------------------------
			...cwbAttributes.__blockColors,
			...cwbAttributes.__bgImage,
			// ...cwbAttributes.__blockBorder,


			// Wrap Settings
			//-----------------------------
			blockHeight: cwbAttributes.blockHeight,
			vAlign: cwbAttributes.vAlign,
			contain: {
				type: 'string',
				default: ''
			},
			containerClasses: {
				type: 'string',
				default: ''
			},
		},
		example: {
			attributes: {
				title: __( 'Chocolate Chip Cookies', 'gutenberg-examples' ),
				mediaID: 1,
				mediaURL:
					'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/2ChocolateChipCookies.jpg/320px-2ChocolateChipCookies.jpg',
			},
		},


		// Block FUNCTIONS
		//-----------------------------

		edit: function( props ) {
			let attributes 		= props.attributes;
			let newCssClasses 	= [...attributes.cssClasses];
			let blockStyle 		= {
				// background: `${ attributes.bgColorOutput }`,
				// backgroundImage: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : null,
				color: attributes.textColor ? attributes.textColor : null,
				background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : attributes.bgColorOutput,
				// border: '1px dashed rgba(255,255,255,0.6)',
			};



			// Pre-Save Object type Attributes (due to bug in gutenberg)
			// - @ref: https://github.com/WordPress/gutenberg/issues/37967
			//-----------------------------
			if(attributes.blockHeight === '') {
				props.setAttributes( { blockHeight:{} } );
				props.setAttributes( { blockHeight: JSON.parse(JSON.stringify(defaultScreenSizeObj)) } )
			}
			if(attributes.vAlign === '') {
				props.setAttributes( { vAlign:{} } );
				props.setAttributes( { vAlign: JSON.parse(JSON.stringify(defaultScreenSizeObj)) } )
			}



			// Block Contain
			//-----------------------------
			const containOptions = [
				// { value: null, label: 'Select', disabled: true },
				{ value: '', label: 'Stretch (default)' },
				{ value: 'justify-content-start', label: 'Contain Left' },
				{ value: 'justify-content-end', label: 'Contain Right' },
			];
			const setContain = function( value ) {

				// let replace = value !== '' ? value : false;
				let substr = 'justify-content-';

				newCssClasses = searchAndReplaceSubstrArr(substr, newCssClasses, value);

				props.setAttributes( { contain: value } );
				props.setAttributes( { cssClasses: newCssClasses } );
				attributes = props.attributes;
			};


			// Block Height
			//-----------------------------
			const genBlockHeightOptions = function(tab){

				// let prefix = tab.name+'-';
				let prefix = tab.name === 'base' ? '' : tab.name+'-';

				return [
					{ value: JSON.stringify([tab.name, 0]), label: tab.name === 'base' ? 'default' : 'inherit from smaller' },
					{ value: JSON.stringify([tab.name, 'vh-'+prefix+'25-min']), label: tab.name+'-25' },
					{ value: JSON.stringify([tab.name, 'vh-'+prefix+'33-min']), label: tab.name+'-33' },
					{ value: JSON.stringify([tab.name, 'vh-'+prefix+'50-min']), label: tab.name+'-50' },
					{ value: JSON.stringify([tab.name, 'vh-'+prefix+'66-min']), label: tab.name+'-66' },
					{ value: JSON.stringify([tab.name, 'vh-'+prefix+'75-min']), label: tab.name+'-75' },
					{ value: JSON.stringify([tab.name, 'vh-'+prefix+'100-min']), label: tab.name+'-100' },
				]
			};
			const setBlockHeight = function(value) {
				setResponsiveClasses(value, 'blockHeight','vh-', newCssClasses, attributes, props );
			};


			// Vertical Alignment
			//-----------------------------
			const genVAlignOptions = function(tab){

				// let prefix = tab.name+'-';
				let prefix = tab.name === 'base' ? '' : tab.name+'-';

				return [
					{ value: JSON.stringify([tab.name, 'align-items-center']), label: tab.name === 'base' ? 'default' : 'inherit from smaller' },
					{ value: JSON.stringify([tab.name, 'align-items-'+prefix+'start']), label: 'Align Top' },
					{ value: JSON.stringify([tab.name, 'align-items-'+prefix+'center']), label: 'Align Middle' },
					{ value: JSON.stringify([tab.name, 'align-items-'+prefix+'end']), label: 'Align Bottom' },
				]
			};
			const setVAlign = function(value) {
				setResponsiveClasses(value, 'vAlign','align-items-', newCssClasses, attributes, props );
			};

			const setContainerClasses = function( value ) {
				props.setAttributes( { containerClasses: value } );
			};

			// RETURN: Layout-Block (BACK-end)
			//================================
			// return el( 'div', { className: 'cwb-editor-outline w-100' },
			// todo: ^ REMOVE if nothing breaks after APR 2024


				// The Block
				//-----------------------------
			return	el( 'div', useBlockProps( {
						className: attributes.cssClasses,
						style: blockStyle }
					),


					// Container Wrap
					//-----------------------------
					el( 'div', {
							className: attributes.contain !== '' ? 'container-2 w-100 '+attributes.containerClasses : 'w-100 '+attributes.containerClasses,						// TODO ^ innerClasses
						},
						el( 'div', { className: 'cwb-editor-outline w-100' },

							// Inner Blocks
							//-----------------------------
							el( InnerBlocks, {
								template: [
									['core/heading']
								]
							} )
						),
					),



					// InspectorControls
					//================================
					el( InspectorControls, {
							key: 'controls' },
						el( 'div', {
								style: {
									padding: '0 20px 20px'
								}
							},
							el( TextControl,
								{
									label: 'Block Anchor',
									value: typeof attributes.anchor === 'string' ? attributes.anchor : null,
									onChange: function(value){
										props.setAttributes({
											anchor: limitForHtmlId(value)
										})
									},
								}
							),
						),


						// Panel : Wrap Settings
						//-----------------------------
						el( PanelBody, {
								title: 'Wrap Settings',
								initialOpen: false//!0
							},
							el( SelectControl, {
								key: 'multiple',
								label: __( 'Contain Content'),
								help: 'Used for Desktop layouts and larger screens',
								value: attributes.contain,
								onChange: setContain,
								options: containOptions
							}),
							el('div',{ style: {
								border: '1px solid #ddd'
							}},
								el( TabPanel, {
										// label: ,
										key: 'multiple',
										activeClass: 'is-active',
										orientation: 'horizontal',
										tabs: screenSizeTabs,
									},
									( tab ) => {
										return el( 'div', { style: {
											padding: '20px 10px',
											background: '#f8f8f8'
											}},
											el('p',{},`Responsive Wrap Settings: `,
												el('strong',{},
													`${ getTabScreenSize(tab.name) }`
												)
											),
											el( SelectControl, {
												key: 'multiple',
												label: __('min-height'),
												value: JSON.stringify([tab.name, attributes.blockHeight[tab.name]]),
												className: ['mt-3'],
												onChange: setBlockHeight,
												options: genBlockHeightOptions(tab)
											}),
											el( SelectControl, {
												key: 'multiple',
												label: __('vertical alignment'),
												value: JSON.stringify([tab.name, attributes.vAlign[tab.name]]),
												className: ['mt-3'],
												onChange: setVAlign,
												options: genVAlignOptions(tab)
											})//,`Selected: ${attributes.blockHeight[tab.name]}`
										);
									}
								),
							),
						// el('div',{ style: {
						// 			margin: '20px'
						// 		}},
							el( TextControl, {
								style: {
									// marginTop: '10px'
								},
								label: __( 'Container CSS Class(es)'),
								help: 'Separate multiple classes with spaces.',
								value: attributes.containerClasses,
								type: 'text',
								onChange: setContainerClasses,
							})
						// ),
						),

						// Panel : Block Colors
						//-----------------------------
						el( BlockColors, props),

						// Panel : Background Image
						//-----------------------------
						el( BgImagePanel, {
							props : props,
							blockStyle : blockStyle
						}),
					),
				)
			// )
			// todo: ^ REMOVE if nothing breaks by APR 2024
		},

		save: function( props ) {
			let attributes = props.attributes;
			let includeAltText = attributes.includeAriaLabel && attributes.mediaAlt.length;
			let blockStyle = {
				// background: attributes.bgColorOutput,
				// backgroundImage: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : null,
				color: attributes.textColor ? attributes.textColor : null,
				background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : attributes.bgColorOutput,

			};

			// RETURN: Layout-Block (FRONT-end)
			//================================
			return el( 'div', useBlockProps.save( {
					id: typeof attributes.anchor === 'string' ? attributes.anchor : null,
					className: attributes.cssClasses.join(' '),
					style: blockStyle,
					role: includeAltText ? 'img' : null,
					['aria-label']: includeAltText ? attributes.mediaAlt : null,
				} ),

				// Container Wrap
				//-----------------------------
				el( 'div', {
						className: attributes.contain !== '' ? 'container-2 w-100 '+attributes.containerClasses : 'w-100 '+attributes.containerClasses,						// TODO ^ innerClasses
						// border: '1px dashed rgba(255,255,255,0.6)',
					},

					// Inner Blocks
					//-----------------------------
					el( InnerBlocks.Content )
				)
			);
		},
	} );
}(
	window.wp.blocks,
	window.wp.editor,
	window.wp.i18n,
	window.wp.element,
	window.wp.components,
	window._,
	window.wp.blockEditor,
	window.cwb
) );
