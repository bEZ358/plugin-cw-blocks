( function( blocks, editor, i18n, element, components, _, blockEditor, cwb ) {
	const __ = i18n.__;
	const el = element.createElement;
	const InnerBlocks = blockEditor.InnerBlocks;
	const useBlockProps = blockEditor.useBlockProps;
	const InspectorControls = blockEditor.InspectorControls;
	const MediaPlaceholder = blockEditor.MediaPlaceholder;
	const { Fragment } = element;

	// const {
	// 	ToggleControl,
	// 	Panel,
	// 	PanelBody,
	// 	PanelRow
	// } = components;

	const {
		cwbGlobals,
		cwbComponents
	} = cwb;

	const {
		cwbAttributes
	} = cwbComponents;


	blocks.registerBlockType( 'cwb/asymmetric-gallery', {


		// Block ASSETS
		//-----------------------------

		title: __( 'CWB: Asymmetric Gallery', 'CWB' ),
		icon: 'images-alt2',
		category: 'common',
		attributes: {
			sliderId: {
				type: 'number',
				default: false
			},
			cssClasses: {
				type: 'array',
				default: ['p-0', 'my-0']
			},

			images: {
				type: 'array',
				default: ''
			},
		},
		example: {
			attributes: {
				title: __( 'Chocolate Chip Cookies', 'gutenberg-examples' ),
				// mediaID: 1,
				// mediaURL:
				// 	'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/2ChocolateChipCookies.jpg/320px-2ChocolateChipCookies.jpg',
			},
		},


		// Block FUNCTIONS
		//-----------------------------

		edit: function( props ) {
			let attributes = props.attributes;
			let blockStyle = {
				width: '100%',
				margin: '0px auto',
				padding: '0px',
				color: attributes.textColor ? attributes.textColor : null,
			};

			// Set Slider ID upon Block instantiation
			if (!attributes.sliderId){
				props.setAttributes({
					sliderId: Math.floor(Math.random() * 999999)
				});
				attributes = props.attributes
			}


			// RETURN: Gallery Block Editor
			//================================
			return el( 'div', useBlockProps( {
					className: attributes.cssClasses,
					style: blockStyle } ),


					// Gallery Editor Block
					//================================
					el(CwAsymmetricGalleryEditor, {
						images: attributes.images,
						onSelect: function (images) {
							props.setAttributes({ images : images });
							attributes = props.attributes;
						},
					}),


					// InspectorControls
					//================================
					el( InspectorControls, { key: 'controls' },
				)
			);
		},

		save: function( props ) {

			let attributes = props.attributes;
			let blockStyle = {
				margin: '0px',
				padding: '0px',
				width: '100%',
				color: attributes.textColor ? attributes.textColor : null,
			};
			let modalId = 'image-slider-'+ attributes.sliderId;
			let imageIndex = 0;



			// RETURN: Gallery-Block (FRONT-end)
			//================================
			return el( 'div', useBlockProps.save( {
					className: attributes.cssClasses.join(' '),
					style: blockStyle,
					['last-block-update']: '20230214'
				} ),


				// Image Cols (The Gallery)
				//-----------------------------
				el(CwAsymmetricGallery, {
					images : attributes.images,

					// Gallery Image
					//-----------------------------
					imageEl : function (image) {
						imageIndex++;
						return el(CwGalleryImage, {
							lazyLoad: true,
							image : image,
							modalId : modalId,
							imageIndex : imageIndex,
							extraWrapClass : 'anim-on-scroll anim-b-in', // requires ez-core (wp_parent_theme) v3.0.13
							// extraImgClass : 'd-inline-block', // requires ez-core (wp_parent_theme) v3.0.13
						})
					}
				}),


				// JQuery Image Slider Modal
				//-----------------------------
				el(BootstrapModal, { modalId: modalId },
					el(CwImageSlider, {
						images: attributes.images,
						sliderId: typeof attributes.sliderId !== 'undefined' ?
							attributes.sliderId : null,
						lazyLoad: true
					})
				)
			);
		},
	} );
}(
	window.wp.blocks,
	window.wp.editor,
	window.wp.i18n,
	window.wp.element,
	window.wp.components,
	window._,
	window.wp.blockEditor,
	window.cwb
) );
