
( function( blocks, editor, i18n, element, components, _, blockEditor, cwb ) {
	const __ = i18n.__;
	const el = element.createElement;
	const InnerBlocks = blockEditor.InnerBlocks;
	const useBlockProps = blockEditor.useBlockProps;
	const InspectorControls = blockEditor.InspectorControls;
	const useSelect = wp.data.useSelect;

	const {
	} = components;

	const {
		cwbGlobals,
		cwbComponents
	} = cwb;

	const {
	} = cwbGlobals;

	const {
		cwbAttributes
	} = cwbComponents;

	// const useInnerBlocksProps = blockEditor.useInnerBlocksProps;


	blocks.registerBlockType( 'cwb/repeater', {


		// Block ASSETS
		//-----------------------------

		title: __( 'Repeater', 'CWB' ),
		icon: 'admin-page', // see: /wp-includes/css/dashicons.css
		category: 'common',
		attributes: {
			anchor: {
				type: 'string',
				default: false,
			},
			cssClasses: {
				type: 'array',
				default: ['d-flex', 'flex-wrap'],//['w-100', 'd-flex', 'm-0', 'align-items-center', 'bg-cover']
			},


			// // Repeater Block Settings
			// //-----------------------------
			repeaterItemType: {
				type : 'string',
				default: 'repeater-item--itb'
			},

			grid: {
				type: 'object',

				// Set Default as empty string (due to bug in gutenberg)
				// - @ref: https://github.com/WordPress/gutenberg/issues/37967
				default: '',
				// - Use this when bug is fixed:
				// default: JSON.parse(JSON.stringify(defaultScreenSizeObj)), // copies default obj
			},

			tileGap: {
				type: 'object',

				// Set Default as empty string (due to bug in gutenberg)
				// - @ref: https://github.com/WordPress/gutenberg/issues/37967
				default: '',
				// - Use this when bug is fixed:
				// default: JSON.parse(JSON.stringify(defaultScreenSizeObj)), // copies default obj
			},


			gridAlign: {
				type: 'object',

				// Set Default as empty string (due to bug in gutenberg)
				// - @ref: https://github.com/WordPress/gutenberg/issues/37967
				default: '',
				// -Use this when bug is fixed:
				// default: JSON.parse(JSON.stringify(defaultScreenSizeObj)) // copies default obj
			},
			gridAlignCss: {
				type : 'array',
				default: []
			},

			allowedGridConfigs : {
				type : 'array',
				default: ['tile-width', 'tile-gap', 'grid-align']
			},
		},
		example: {
			attributes: {
				// title: __( 'Chocolate Chip Cookies', 'gutenberg-examples' ),
				// mediaID: 1,
				// mediaURL:
				// 	'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/2ChocolateChipCookies.jpg/320px-2ChocolateChipCookies.jpg',
			},
		},


		// Block FUNCTIONS
		//-----------------------------

		edit: function( props ) {

			const repItemBlocks = useSelect( ( select ) => select( 'core/block-editor' ).getBlock( props.clientId ).innerBlocks );
			const repItemCount = repItemBlocks.length;

			let attributes = props.attributes;
			let repItemLimit = 5;
			let allowMoreItems = repItemCount < repItemLimit;
			let blockStyle = {
				width: 'auto',
				margin: '0px auto',
				padding: '0px',
			};




			// Grid Layout
			//================================
			const [gridTab, setGridTab] = useState(false);


			// Set Defaults
			// ~ todo try to set these in attributes declaration
			// 		- (maybe the bug has been fixed)
			//---------------------------------------------------
			if(!__exists(attributes.grid) || attributes.grid === '') props.setAttributes({
				grid: {...defaultScreenSizeObj, base: 'width-2', md: 'width-md-3'},
			});
			if(!__exists(attributes.tileGap) || attributes.tileGap === '') props.setAttributes({
				tileGap: {...defaultScreenSizeObj, base: '2', md: '3'},
			});
			if(!__exists(attributes.gridAlign) || attributes.gridAlign === '') props.setAttributes({
				gridAlign: {...defaultScreenSizeObj, base: 'justify-content-center'},
			});


			// Generate Grid Layout CSS
			//--------------------------------------
			let gridCssClasses = genResponsiveClasses(attributes.grid);
			let tileGap = genTileGapCss(attributes.tileGap);
			let gapMarginCss = tileGap.gapMarginCss;
			let gapPaddingCss = tileGap.gapPaddingCss;
			let gridAlignCss = genResponsiveClasses(attributes.gridAlign);





			// Dynamic Child-Block Editing Logic
			// todo... getting there, good enough for now... preserve tab & panel state with attribute(or local js var) in the future
			//================================
			const thisBlockElement = document.querySelector('[data-block="'+props.clientId+'"]');
			const childBlockElement = __exists(repItemBlocks[0]) ?
				document.querySelector('[data-block="'+repItemBlocks[0].clientId+'"]') : false;
			const onUpdateLayout = function(data, attrStr){
				if(childBlockElement) {
					childBlockElement.focus();
					setTimeout(function(){
						thisBlockElement.focus();
					},1);
				}
			};
			// TODO (*): try this instead (from cwbTabs block):
			// // required for save function (use to access inner-block atts)
			// props.setAttributes({
			// 	tabItemBlocks : tabItemBlocks
			// });
			// attributes = props.attributes;
			// const tabItemBlocks = useSelect( ( select ) => select( 'core/block-editor' ).getBlock( props.clientId ).innerBlocks );
			// // Function to update child block attributes
			// function updateChildAttributes(newAttributes, clientId) {
			// 	wp.data.dispatch('core/block-editor').updateBlockAttributes(clientId, newAttributes)
			// }
			//
			// for(let i = 0; i < tabItemBlocks.length; i++){
			// 	if(i === 0 ){
			// 		updateChildAttributes({
			// 			isFirstTab: true
			// 		}, tabItemBlocks[i].clientId);
			// 	}
			// }
			//








			// Update Global CSS for use by child (repeater-item) blocks
			//--------------------------------------
			for(let i = 0; i < repItemBlocks.length; i++) {
				updateGlobalBlockAtts( repItemBlocks[i].clientId,  {
					gridCssClasses: gridCssClasses,
					gapPaddingCss: gapPaddingCss
				})
			}





			// RETURN: Simple-Repeater-Block (BACK-end)
			//================================
			return el( 'div', useBlockProps( {
					className: [
						...attributes.cssClasses,
						gridAlignCss,
						gapMarginCss,
						'cwb-editor-outline',
					].join(' '),
					style: blockStyle
				} ),



				el(InnerBlocks, {
					allowedBlocks: [
						'cwb/' + attributes.repeaterItemType,
					],
					template: [
						['cwb/' + attributes.repeaterItemType, {
							template: [
								['outermost/icon-block',{
									"icon": "",
									"iconName": "wordpress-siteLogo",
									"itemsJustification": "center",
									// "iconColor": "secondary_l1",
									// "iconColorValue": "#28aaef",
									"width": "100px",
									"className": "my-0 py-0"
								}],
								// ['core/image', {
								// 	url: 'http://localhost/Clickwurx/Blank_333/wp-content/uploads/2023/12/icon-placeholder-80.png'
								// }],
								['core/heading', {
									content: 'Veni',
									level: 3
								}],
								['core/paragraph'],
							]
						}],
						['cwb/' + attributes.repeaterItemType, {
							template: [
								['outermost/icon-block',{
									"icon": "",
									"iconName": "wordpress-siteLogo",
									"itemsJustification": "center",
									// "iconColor": "secondary_l1",
									// "iconColorValue": "#28aaef",
									"width": "100px",
									"className": "my-0 py-0"
								}],
								// ['core/image', {
								// 	url: 'http://localhost/Clickwurx/Blank_333/wp-content/uploads/2023/12/icon-placeholder-80.png'
								// }],
								['core/heading', {
									content: 'Vidi',
									level: 3
								}],
								['core/paragraph'],
							]
						}],
						['cwb/' + attributes.repeaterItemType, {
							template: [
								['outermost/icon-block',{
									"icon": "",
									"iconName": "wordpress-siteLogo",
									"itemsJustification": "center",
									// "iconColor": "secondary_l1",
									// "iconColorValue": "#28aaef",
									"width": "100px",
									"className": "my-0 py-0"
								}],
								// ['core/image', {
								// 	url: 'http://localhost/Clickwurx/Blank_333/wp-content/uploads/2023/12/icon-placeholder-80.png'
								// }],
								['core/heading', {
									content: 'Vici',
									level: 3
								}],
								['core/paragraph'],
							]
						}],
					],
					// // TODO (*): try this instead (from cwbTabs block):
					// onChange: function(newBlocks) {
					// 	// Update child block attributes based on parent block attributes
					// 	newBlocks.forEach(function(block) {
					// 		console.log('InnerBlock',block);
					// 		if (block.attributes) {
					//
					// 			// console.log('InnerBlock.atts',block.attributes);
					//
					// 			// var updatedAttributes = {
					// 			// 	childAttribute: props.attributes.parentAttribute,
					// 			// };
					// 			// // Update child block attributes
					// 			// updateChildAttributes(updatedAttributes, block.clientId);
					// 		}
					// 	});
					// },
				}),

				// InspectorControls
				//================================
				el( InspectorControls, {
					key: 'controls' },

					el( 'div', {
							style: {
								margin: '0 0 20px',
								padding: '0 20px'
							},
						},
						el( TextControl,
							{
								label: 'Block Anchor',
								value: typeof attributes.anchor === 'string' ? attributes.anchor : null,
								onChange: function(value){
									props.setAttributes({
										anchor: limitForHtmlId(value)
									})
								},
							}
						),

						el( SelectControl, {
							key: 'multiple',
							label: __('Select Repeater Item Type'),
							value: attributes.repeaterItemType,
							className: ['mt-3'],
							onChange: function(value){
								props.setAttributes({
									repeaterItemType : value
								});
								attributes = props.attributes;
							},
							options: [
								{ value: 'repeater-item--basic', label: 'Basic Repeater Item' },
								// { value: 'repeater-item--slide', label: 'Slide (Basic)' },
								{ value: 'repeater-item--modal-video', label: 'Modal Video' },
								{ value: 'repeater-item--modal-iframe', label: 'Modal iFrame' },
								{ value: 'repeater-item--itb', label: 'Image Text Button (ITB)' }
							]
						}),
					),


					// Grid Layout Configs
					//-------------------------
					el( PanelBody, {
							style: {
								padding: '0 20px'
							},
							title: 'Grid Configs',
							initialOpen: true//!0,
						},

						el(ResponsiveTabs, props,
							(gridTab) => {
								setGridTab(gridTab);
								props = {
									tab: {...gridTab},
									...props,
									gridCssClasses: gridCssClasses,
									onUpdateLayout: function(attrStr, responsiveCssObj){
										onUpdateLayout(attrStr, responsiveCssObj)
									}
								};
								return el(GridSettingsTab, props);
							}
						),
					)
				)
			);
		},



		save: function( props ) {
			let attributes = props.attributes;
			let blockStyle = {
				margin: '0px',
				padding: '0px',
				width: 'auto',
				// background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : attributes.bgColorOutput,
				// color: attributes.textColor ? attributes.textColor : null,
			};

			// Generate Grid Layout CSS (for repeater)
			//--------------------------------------
			let tileGap = genTileGapCss(attributes.tileGap);
			let gapMarginCss = tileGap.gapMarginCss;
			let gridAlignCss = genResponsiveClasses(attributes.gridAlign);


			// RETURN: Layout-Block (FRONT-end)
			//================================
			return el( 'div', useBlockProps.save( {
					id: typeof attributes.anchor === 'string' ? attributes.anchor : null,
					className: attributes.cssClasses.join(' '),
					style: blockStyle
				} ),

				el( 'div', { className: 'col px-0'},
					el( 'div', { className: 'container-fluid px-0'},
						el( 'div', { className: 'row '+gapMarginCss+' '+gridAlignCss},
							el( InnerBlocks.Content ),
						)
					)
				),
			);
		},
	} );
}(
	window.wp.blocks,
	window.wp.editor,
	window.wp.i18n,
	window.wp.element,
	window.wp.components,
	window._,
	window.wp.blockEditor,
	window.cwb
) );
