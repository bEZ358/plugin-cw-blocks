( function( blocks, editor, i18n, element, components, _, blockEditor, cwb ) {
	const __ = i18n.__;
	const el = element.createElement;
	const InnerBlocks = blockEditor.InnerBlocks;
	const useBlockProps = blockEditor.useBlockProps;
	const InspectorControls = blockEditor.InspectorControls;

	const {
		TextareaControl,
	} = components;

	const {
		cwbGlobals,
		cwbComponents
	} = cwb;

	const {
		// screenSizeTabs,
		// defaultScreenSizeObj
	} = cwbGlobals;

	const {
		RepeaterControls,
	} = cwbComponents;
	
	
	const GridItemObjModel = {
		key: 0,
		imgURL: false,
		imgID: false,
		imgObj: false,

		itemText: 'item 1', // (!) cannot change param name "itemText" becaase it break repeater
		quoteSourceTitle: false,
		quoteBody: 'Lorem Ipsum',
		quoteLink: false,

		// // postObj: false,
		// // wpContentPopover: false,
		btn: false,
		btnObj: {
			text: 'Button Text111',
			url: '',
			target: null,
			newTab: false,
			dataToggle: null,
			dataShortcode: null,
			dataModalSize: null,
			dataModalHeading: null,
			btnType: 'primary'
		},
		// btnPopover: false, // todo depreciated
		// testProp: true
	};


	// @ref links below
	// https://mediaron.com/how-to-create-a-repeater-field6-in-gutenberg/
	// https://reactnative.dev/docs/state

	blocks.registerBlockType( 'cwb/testimonial-grid', {


		// Block ASSETS
		//-----------------------------

		title: __( 'Testimonial Grid', 'CWB' ),
		icon: 'blockquote', // see: /wp-includes/css/dashicons.css
		category: 'common',
		attributes: {
			cssClasses: {
				type: 'array',
				default: ['py-3', 'my-0'],//['w-100', 'd-flex', 'm-0', 'align-items-center', 'bg-cover']
			},

			// // Common Attribute Sets
			// //-----------------------------
			// ...cwbAttributes.__blockColors,
			// ...cwbAttributes.__bgImage,
			// // ...cwbAttributes.__blockBorder,


			// // Repeater Block Settings
			// //-----------------------------
			// fullWidth: {
			// 	type: 'boolean' // for ToggleControl we need boolean type
			// }
			items: {
				type : 'array',
				default: [
					{ ...GridItemObjModel, key: 0, itemText: 'item 1'},
					{ ...GridItemObjModel, key: 1, itemText: 'item 2'},
					{ ...GridItemObjModel, key: 2, itemText: 'item threee'},
				]
			},
			focusItem: {
				type: 'integer',
				default: null
			},

			grid: {
				type: 'object',

				// Set Default as empty string (due to bug in gutenberg)
				// - @ref: https://github.com/WordPress/gutenberg/issues/37967
				default: '',
				// - Use this when bug is fixed:
				// default: JSON.parse(JSON.stringify(defaultScreenSizeObj)), // copies default obj
			},

			tileGap: {
				type: 'object',

				// Set Default as empty string (due to bug in gutenberg)
				// - @ref: https://github.com/WordPress/gutenberg/issues/37967
				default: '',
				// - Use this when bug is fixed:
				// default: JSON.parse(JSON.stringify(defaultScreenSizeObj)), // copies default obj
			},

			itemLayout: {
				type: 'text',
				default: 'quote-top'
			}
		},
		example: {
			attributes: {
				// title: __( 'Chocolate Chip Cookies', 'gutenberg-examples' ),
				// mediaID: 1,
				// mediaURL:
				// 	'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/2ChocolateChipCookies.jpg/320px-2ChocolateChipCookies.jpg',
			},
		},



		// Block FUNCTIONS
		//-----------------------------

		edit: function( props ) {
			let attributes = props.attributes;
			let blockStyle = {
				width: '100%',
				margin: '0px auto',
				padding: '0px',
			};

			// Options
			let ops = [];
			attributes.items.map((item) =>
			{
				ops.push({
					value : item.key,
					label : 'item '+item.key+': '+item.itemText
				});
			});


			// Grid Layout
			//--------------------------------------
			const [gridTab, setGridTab] = useState(false);

			if(attributes.grid === '') props.setAttributes({
				grid: {...defaultScreenSizeObj, base: 'width-2', md: 'width-md-3'},
			});
			let gridCssClasses = genResponsiveClasses(attributes.grid);



			// Block DRY Functions
			//--------------------------------------

			const updateRepeaterItems = function(newArr){
				props.setAttributes({
					items : newArr
				});
				attributes = props.attributes;
			};

			const setItemAttr = function(value, focusItem, property){

				let newArr = [...attributes.items];

				// update item
				newArr[focusItem.key][property] = value;

				//
				// update block
				updateRepeaterItems(newArr);
			};



			// used for WYSIWYG edits rather than edits via inspectorPanel
			// - called when updating text todo see if there's a way to pull this from RepeaterComponent
			const updateFocus = function(item){
				props.setAttributes({
					focusItem : item//.key.toString()
				});
				attributes = props.attributes;
			};



			// Content Modifiers
			//--------------------------------------

			const updateText = function(value, item){
				setItemAttr(value, item, 'itemText');
			};


			let focusItem = attributes.items[ attributes.focusItem !== null ? attributes.focusItem : 0 ];
			// console.log('focusItem:', attributes.focusItem, focusItem.itemText, focusItem.btnObj);







			// RETURN: Simple-Repeater-Block (BACK-end)
			//================================
			return el( 'div', useBlockProps( {
					className: attributes.cssClasses,
					style: blockStyle
				} ),

				// // Container Wrap
				// //-----------------------------
				// el( 'div', {
				// 		className: ''
				// 	},


					el(SimpleRepeaterBlock, {
							// ...props,
							blockProps: props,
							itemObjModel: GridItemObjModel,
							className: 'cwb-testimonial-grid container px-0 d-flex flex-wrap justify-content-center' },
						(item, toolbar) => {

							const quoteImgText = (()=>{

								return el('div', {},

									el('h3', {
											className: 'quote-source',
											style: {
												position: 'relative'
											}
										},
										el(RichTextBlockInput, {
											currentValue : item.itemText,
											onChange : function(value){
												updateText(value, item);
											},
											onClick: function(){
												updateFocus(item.key.toString());
											}
										}),
									),



									item.quoteSourceTitle ?
										el('h4', {
												className: 'quote-source-title',
												style: {
													position: 'relative'
												}
											},
											el(RichTextBlockInput, {
													currentValue :item.quoteSourceTitle,
													onChange: function(value){
														setItemAttr(value, item, 'quoteSourceTitle')
													},
													onClick: function(){
														updateFocus(item.key.toString());
													}
												}
											)
										) : null,



									item.btn ? el( RepeaterBtn, {
										blockProps: props,
										// ...props,
										item: item,
									}) : null,
								)
							})();

							const quoteBody = (()=>{

								return item.quoteBody ?
									el('p', {
											className: 'quote-body',
											style: {
												position: 'relative'
											}
										},
										el(RichTextBlockInput, {
												currentValue: item.quoteBody,
												onChange: function(value){
													setItemAttr(value, item, 'quoteBody')
												},
												onClick: function(){
													updateFocus(item.key.toString());
												}
											}
										)
									): null
							})();

							const quoteImg = (()=>{

								return item.imgObj ?

									el('div', {
											className: 'quote-image'
										},
										el(BlockMediaEditor,{
											label : 'Select a Top Image',
											imageSize: 'large',
											image : item.imgObj,
											onClick: function () {
												updateFocus(item.key.toString())
											},
											onSelect: function(value){
												setItemAttr(value, item, 'imgObj')
											},
											onRemove: function(){
												setItemAttr(false, item, 'imgObj')
											},
										})
									)
									: null
							})();

							return el('div', {
									className: attributes.itemLayout+' cwb-testimonial-grid-item text-left d-inline-block position-relative p-2 '+gridCssClasses,
								},

								// attributes.itemLayout === 'img-top' ?


								el('div', { className:
											attributes.itemLayout === 'quote-left' ||
											attributes.itemLayout === 'quote-right' ?
										'row item-inner' : ' item-inner'},


									// Image Left (quote-right)
									//-----------------------------
									attributes.itemLayout === 'quote-right' ? el('div', { className:
											'col-3 quote-img-wrap'// : ' quote-img-wrap'
										},

										el('div', {className: attributes.itemLayout === 'quote-top' ? 'row' :''},
											el('div', {className: attributes.itemLayout === 'quote-top' ? 'col-3' :''},
												quoteImg,
											),
											el('div', {className: attributes.itemLayout === 'quote-top' ? 'col-9' :''},
												quoteImgText,
											)
										)
									) : null ,




									// Quote Body
									//-----------------------------
									el('div', { className:
												attributes.itemLayout === 'quote-left' ||
												attributes.itemLayout === 'quote-right' ?
											'col-9 quote-body-wrap' : ' quote-body-wrap'},
										quoteBody,
									),




									// Image Right/Bottom (quote-right/quote-top)
									//-----------------------------
									attributes.itemLayout === 'quote-left' ||
									attributes.itemLayout === 'quote-top' ? el('div', { className:
											attributes.itemLayout === 'quote-left' ? 'col-3 quote-img-wrap' : ' quote-img-wrap'
										},

										el('div', {className: attributes.itemLayout === 'quote-top' ? 'row' :''},
											el('div', {className: attributes.itemLayout === 'quote-top' ? 'col-3' :''},
												quoteImg,
											),
											el('div', {className: attributes.itemLayout === 'quote-top' ? 'col-9' :''},
												quoteImgText,
											)
										)
									) : null,


									toolbar,
								)
							)
						}
					),
				// ),




				// InspectorControls
				//================================
				el( InspectorControls, {
					key: 'controls' },

					el( 'div', {},

						// Grid Layout Configs
						//-------------------------
						el( PanelBody, {
								style: {
									padding: '0 20px'
								},
								title: 'Layout',
								initialOpen: false//!0,
							},

							el(ResponsiveTabs, props,
								(gridTab) => {
									setGridTab(gridTab);
									props = {
										tab: {...gridTab},
										...props,
										gridCssClasses: gridCssClasses,
										onUpdateLayout: function(attrStr, responsiveCssObj){
											// console.log('gridTab', attrStr, responsiveCssObj);
										}
									};
									return el(GridSettingsTab, props);
								}
							),

							el( SelectControl, {
								key: 'multiple',
								label: __('Item Layout'),
								className: ['mt-3'],
								value: false,
								options: [
									{ value: 'quote-top', label: 'Quote Top' },
									{ value: 'quote-left', label: 'Quote Left' },
									{ value: 'quote-right', label: 'Quote Right' },
								],
								onChange: function(value){
									props.setAttributes({
										itemLayout: value
									});
									attributes = props.attributes;
								}
							}),
						)
					),

					el(RepeaterControls, {
							// ...props,
							blockProps: props,
							itemObjModel : GridItemObjModel,
						},

						el( 'div', {
							style: {
								padding: '0 20px'
							}},
							el( TextareaControl,
								{
									label: 'item body',
									value: focusItem.quoteBody,// todo quoteBody
									onChange: function(value){
										setItemAttr(value, focusItem, 'quoteBody')
									}
								}
							),

							el(InspectorMediaInput,{
								currentValue: focusItem.imgObj,
								onSelect: function(value){
									setItemAttr(value, focusItem, 'imgObj')
								},
								onRemove: function(){
									setItemAttr(false, focusItem, 'imgObj')
								},
							}),

							el( TextControl,
								{
									label: 'Quote Source',
									value: focusItem.itemText,// todo quoteSource
									onChange: function(value){
										updateText(value, focusItem);
									}
								}
							),
							el( TextControl,
								{
									label: 'Quote Source Title',
									value: focusItem.quoteSourceTitle,// todo quoteSourceTitle
									onChange: function(value){
										setItemAttr(value, focusItem, 'quoteSourceTitle')
									}
								}
							),



							// Slide Button Toggle
							//--------------------------------------
							el('div', { className: 'my-3'},//style: {margin: ' 0 0 30px'}},
								el(ToggleControl,
									{
										label: 'include a button',
										onChange: function (value) {
											setItemAttr(value, focusItem, 'btn');
										},
										checked: focusItem.btn,
										help: focusItem.btn ? 'Click the slide button to edit it' : null,
									}
								),
							),
						)
					),



					// // Dev Utils
					// // TODO make conditional & compare item model vs current item
					// // --------------------------------------
					// el( 'div', {
					// 		style: {
					// 			padding: '0 20px 20px',
					// 			// margin: '30px 0 0'
					// 		},
					// 	},
					// 	el( components.Button, {
					// 		className: 'button button-large cursor-pointer',
					// 		onClick: function(){
					//
					//
					// 			let newArr = [...attributes.items];
					// 			let newSlide = updateItemModel(focusItem, GridItemObjModel );
					//
					// 			// console.log( newSlide);
					//
					//
					// 			// update item
					// 			newArr[focusItem.key] = newSlide;
					// 			//
					// 			// update block
					// 			props.setAttributes({
					// 				items : newArr
					// 			});
					// 			attributes = props.attributes;
					//
					// 		}
					// 	}, 'Update Grid Item Model' ),
					// ),
				)
			);
		},

		save: function( props ) {
			let attributes = props.attributes;
			let blockStyle = {
				// background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : attributes.bgColorOutput,
				margin: '0px',
				padding: '0px',
				width: '100%',
				// color: attributes.textColor ? attributes.textColor : null,
			};
			let gridCssClasses = genResponsiveClasses(attributes.grid);

			let imageSize = 'gallery';//item.imgObj && typeof props.imageSize !== 'undefined' ? props.imageSize : false;



			// RETURN: Layout-Block (FRONT-end)
			//================================
			return el( 'div', useBlockProps.save( {
					className: attributes.cssClasses.join(' '),
					style: blockStyle
				} ),

				// Container Wrap
				//-----------------------------
				el( 'div', {
						['data-update']: '230505-00',
						className: 'cwb-testimonial-grid container px-0 d-flex flex-wrap justify-content-center'
					},


					attributes.items.map((item) => {


						// todo getImageSource()?
						let imageUrl = item.imgObj && typeof item.imgObj.sizes.medium !== 'undefined' ?
							item.imgObj.sizes.medium.url : item.imgObj ? item.imgObj.url : '';
						let imageWidth = item.imgObj && typeof item.imgObj.sizes.medium !== 'undefined' ?
							item.imgObj.sizes.medium.width : item.imgObj ? item.imgObj.width : '';
						let imageHeight = item.imgObj && typeof item.imgObj.sizes.medium !== 'undefined' ?
							item.imgObj.sizes.medium.height : item.imgObj ? item.imgObj.height : '';


						imageUrl = imageSize && typeof item.imgObj.sizes !== 'undefined' && typeof item.imgObj.sizes[imageSize] !== 'undefined' ?
							item.imgObj.sizes[imageSize].url : imageUrl;
						imageWidth = imageSize && typeof item.imgObj.sizes !== 'undefined' && typeof item.imgObj.sizes[imageSize] !== 'undefined' ?
							item.imgObj.sizes[imageSize].width : imageWidth;
						imageHeight = imageSize && typeof item.imgObj.sizes !== 'undefined' && typeof item.imgObj.sizes[imageSize] !== 'undefined' ?
							item.imgObj.sizes[imageSize].height : imageHeight;



						const quoteImgText = (()=>{

							return el('div', {},

								el('h3', {
										className: 'quote-source',
										style: {
											position: 'relative'
										}
									}, item.itemText
								),

								item.itemSubHead ?
									el('h4', {
											className: 'quote-source-title',
											style: {
												position: 'relative'
											}
										}, item.quoteSourceTitle
									) : null,

								item.btn ?
									el( CwbButton, {
										btnObj : item.btnObj,
										btnType : item.btnObj.btnType,
										// extraBtnCss : ['btn-hover-scale']
									}): null
							)
						})();

						const quoteBody = (()=>{

							return item.quoteBody ?
								el('p', {
										className: 'quote-body',
										style: {
											position: 'relative'
										}
									}, item.quoteBody
								) : null
						})();

						const quoteImg = (()=>{

							let alt = item.imgObj ? getImageAlt(item.imgObj) : false;



							return item.imgObj ?

								el('div', {
										className: 'quote-image'
									},
									el('img',{
										src: imageUrl,
										alt: alt ? alt : null,
										width: imageWidth ? imageWidth : null,
										height: imageHeight ? imageHeight : null
									})
								)
								: null
						})();

						return el('article', {
								className: attributes.itemLayout+' cwb-testimonial-grid-item d-inline-block text-left p-2 '+gridCssClasses,
							},


							el('div', { className:
										attributes.itemLayout === 'quote-left' ||
										attributes.itemLayout === 'quote-right' ?
											'row item-inner' : ' item-inner'},


								// Image Left (quote-right)
								//-----------------------------
								attributes.itemLayout === 'quote-right' ? el('div', { className:
											'col-3 quote-img-wrap'// : ' quote-img-wrap'
									},

									el('div', {className: attributes.itemLayout === 'quote-top' ? 'row' :''},
										el('div', {className: attributes.itemLayout === 'quote-top' ? 'col-3' :''},
											quoteImg,
										),
										el('div', {className: attributes.itemLayout === 'quote-top' ? 'col-9' :''},
											quoteImgText,
										)
									)
								) :null ,




								// Quote Body
								//-----------------------------
								el('div', { className:
											attributes.itemLayout === 'quote-left' ||
											attributes.itemLayout === 'quote-right' ?
												'col-9 quote-body-wrap' : ' quote-body-wrap'},
									quoteBody
								),




								// Image Right/Bottom (quote-right/quote-top)
								//-----------------------------
								attributes.itemLayout === 'quote-left' ||
								attributes.itemLayout === 'quote-top' ? el('div', { className:
											attributes.itemLayout === 'quote-left' ? 'col-3 quote-img-wrap' : ' quote-img-wrap'
									},

									el('div', {className: attributes.itemLayout === 'quote-top' ? 'row' :''},
										el('div', {className: attributes.itemLayout === 'quote-top' ? 'col-3' :''},
											quoteImg,
										),
										el('div', {className: attributes.itemLayout === 'quote-top' ? 'col-9' :''},
											quoteImgText,
										)
									)
								) : null,
							)
						)
					})
				)
			);
		},
	} );
}(
	window.wp.blocks,
	window.wp.editor,
	window.wp.i18n,
	window.wp.element,
	window.wp.components,
	window._,
	window.wp.blockEditor,
	window.cwb
) );
