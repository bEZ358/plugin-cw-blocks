( function( blocks, editor, i18n, element, components, _, blockEditor, cwb ) {
	const __ = i18n.__;
	const el = element.createElement;
	const InnerBlocks = blockEditor.InnerBlocks;
	const useBlockProps = blockEditor.useBlockProps;
	const InspectorControls = blockEditor.InspectorControls;

	// const {
	// 	ToggleControl,
	// 	Panel,
	// 	PanelBody,
	// 	PanelRow
	// } = components;

	const {
		cwbGlobals,
		cwbComponents
	} = cwb;

	const {
		screenSizeTabs,
		defaultScreenSizeObj
	} = cwbGlobals;

	const {
		BgImagePanel,
		BlockColors,
		cwbAttributes
	} = cwbComponents;


	blocks.registerBlockType( 'cwb/layout-block', {


		// Block ASSETS
		//-----------------------------

		title: __( 'CWB: Layout Block', 'CWB' ),
		icon: 'layout',
		category: 'common',
		attributes: {
			anchor: {
				type: 'string',
				default: false,
			},
			cssClasses: {
				type: 'array',
				default: ['p-0', 'my-0'],//['w-100', 'd-flex', 'm-0', 'align-items-center', 'bg-cover']
			},

			// Common Attribute Sets
			//-----------------------------
			...cwbAttributes.__blockColors,
			...cwbAttributes.__bgImage,
			// ...cwbAttributes.__blockBorder,
			...cwbAttributes.__bgOverlayColors,
			overlayBlur: {
				type: 'integer',
				default: 0,
			},


			// Layout Block Settings
			//-----------------------------
			fullWidth: {
				type: 'boolean' // for ToggleControl we need boolean type
			},
		},
		example: {
			attributes: {
				title: __( 'Chocolate Chip Cookies', 'gutenberg-examples' ),
				mediaID: 1,
				mediaURL:
					'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/2ChocolateChipCookies.jpg/320px-2ChocolateChipCookies.jpg',
			},
		},


		// Block FUNCTIONS
		//-----------------------------

		edit: function( props ) {
			let attributes = props.attributes;
			let blockStyle = {
				background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : attributes.bgColorOutput,
				width: '100%',
				margin: '0px auto',
				padding: '0px',
				color: attributes.textColor ? attributes.textColor : null,
				position: 'relative'
			};

			const toggleFullWidth = function( value ) {
				props.setAttributes( { fullWidth: value } );
			};


			// RETURN: Layout-Block (BACK-end)
			//================================
			return el( 'div', useBlockProps( {
					className: attributes.cssClasses,
					style: blockStyle } ),


				// attributes.bgEffect ?
				el(CwbBgEffect, {
					bgOverlay : attributes.overlayColorOutput,
					backdropFilter: attributes.overlayBlur ?
						'blur('+attributes.overlayBlur+'px)' : null
				}),
				// : null,

				// Container Wrap
				//-----------------------------
				el( 'div', {
						className: attributes.fullWidth ? '' : 'container px-0',
						style: {
							position: 'relative'
						}
					},

					// Inner Blocks
					//-----------------------------
					el( InnerBlocks, {
						template: [
							['cwb/content-wrap']
						]
					} )
				),


				// InspectorControls
				//================================
				el( InspectorControls, {
						key: 'controls' },


					// Toggle : Full-Width
					//-----------------------------
					el( 'div', {
							style: {
								padding: '0 20px'
							}
						},
						el( TextControl,
							{
								label: 'Block Anchor',
								value: typeof attributes.anchor === 'string' ? attributes.anchor : null,
								onChange: function(value){
									props.setAttributes({
										anchor: limitForHtmlId(value)
									})
								},
							}
						),

						el( ToggleControl,
							{
								label: 'Full-Width Content',
								onChange: toggleFullWidth,
								checked: props.attributes.fullWidth,
							}
						),

						el( ToggleControl,
							{
								label: 'Lazy-load Background',
								onChange: function(value){
									props.setAttributes( { lazyBg: value } );
								},
								checked: props.attributes.lazyBg,
							}
						),
						el( TextControl,
							{
								label: 'Block Anchor',
								value: typeof attributes.anchor === 'string' ? attributes.anchor : null,
								// todo limit to id friendly characters, add hashatg after input
								onChange: function(){

								},
								// checked: props.attributes.fullWidth,
							}
						)
					),

					// Panel : Block Colors
					//-----------------------------
					el( BlockColors, props),

					// Panel : Background Image
					//-----------------------------
					el( BgImagePanel, {
						props : props,
						blockStyle : blockStyle
					}),

					// Panel : BgEffects
					//-----------------------------
					el( BgEffects, props)

				)
			);
		},

		save: function( props ) {
			let attributes = props.attributes;
			let includeAltText = attributes.includeAriaLabel && attributes.mediaAlt.length;



			let blockStyle = {
				background: !attributes.lazyBg && attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` :
					attributes.bgColorOutput !== ''  ? attributes.bgColorOutput : null,
				margin: '0px',
				padding: '0px',
				width: '100%',
				color: attributes.textColor ? attributes.textColor : null,
				position: 'relative'
			};
			let classList = [...attributes.cssClasses];

			if(attributes.lazyBg && !classList.includes('cw-lazy-bg')){
				classList.push('cw-lazy-bg');
			}


			// console.log(attributes.includeAriaLabel && attributes.mediaAlt.length, attributes.includeAriaLabel, attributes.mediaAlt.length);

			// RETURN: Layout-Block (FRONT-end)
			//================================
			return el( 'div', useBlockProps.save( {
					id: typeof attributes.anchor === 'string' ? attributes.anchor : null,
					className: classList.join(' '),
					style: blockStyle,
					role: includeAltText ? 'img' : null,
					['aria-label']: includeAltText ? attributes.mediaAlt : null,
					['data-bg'] : attributes.lazyBg && attributes.mediaURL !== '' ? attributes.mediaURL : null
				} ),

				// attributes.bgEffect ?
				el(CwbBgEffect, {
					bgOverlay : attributes.overlayColorOutput,
					backdropFilter: attributes.overlayBlur ?
						'blur('+attributes.overlayBlur+'px)' : null
				}),
				// : null,

				// Container Wrap
				//-----------------------------
				el( 'div', {
						className: attributes.fullWidth ? '' : 'container px-0',
						style: {
							position: 'relative'
						}
					},

					// Inner Blocks
					//-----------------------------
					el( InnerBlocks.Content )
				)
			);
		},
	} );
}(
	window.wp.blocks,
	window.wp.editor,
	window.wp.i18n,
	window.wp.element,
	window.wp.components,
	window._,
	window.wp.blockEditor,
	window.cwb
) );
