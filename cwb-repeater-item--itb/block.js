( function( blocks, editor, i18n, element, components, _, blockEditor, cwb ) {
	const __ = i18n.__;
	const el = element.createElement;
	const InnerBlocks = blockEditor.InnerBlocks;
	const useBlockProps = blockEditor.useBlockProps;
	const InspectorControls = blockEditor.InspectorControls;
	const select = wp.data.select;

	const {
	} = components;

	const {
		cwbGlobals,
		cwbComponents
	} = cwb;

	const {
	} = cwbGlobals;

	const {
		cwbAttributes,
	} = cwbComponents;


	// @ref links below
	// https://mediaron.com/how-to-create-a-repeater-field6-in-gutenberg/
	// https://reactnative.dev/docs/state

	// console.log(WPURLS.siteurl);

	blocks.registerBlockType( 'cwb/repeater-item--itb', {


		// Block ASSETS
		//-----------------------------

		title: __( 'ITB Repeater Item', 'CWB' ),
		icon: 'admin-page', // see: /wp-includes/css/dashicons.css
		category: 'common',
		attributes: {
			cssClasses: {
				type: 'array',
				default: ['my-0'],//['w-100', 'd-flex', 'm-0', 'align-items-center', 'bg-cover']
			},

			gridCssClasses: {
				type : 'string',
				default: ''
			},

			gapPaddingCss: {
				type : 'string',
				default: ''
			},

			vAlignCss: {
				type : 'array',
				default: []
			},



			useGridOverride: {
				type: 'boolean', // for ToggleControl we need boolean type
				default: false
			},

			grid: {
				type: 'object',

				// Set Default as empty string (due to bug in gutenberg)
				// - @ref: https://github.com/WordPress/gutenberg/issues/37967
				default: '',
				// - Use this when bug is fixed:
				// default: JSON.parse(JSON.stringify(defaultScreenSizeObj)), // copies default obj
			},


			tileGap: {
				type: 'object',

				// Set Default as empty string (due to bug in gutenberg)
				// - @ref: https://github.com/WordPress/gutenberg/issues/37967
				default: '',
				// - Use this when bug is fixed:
				// default: JSON.parse(JSON.stringify(defaultScreenSizeObj)), // copies default obj
			},

			// vAlign: cwbAttributes.vAlign,






			// Common Attribute Sets
			//-----------------------------
			...cwbAttributes.__blockColors,
			...cwbAttributes.__bgImage,
			// ...cwbAttributes.__blockBorder,
			...cwbAttributes.__bgOverlayColors,
			overlayBlur: {
				type: 'integer',
				default: 0,
			},

			template: {
				type : 'array',
				default: [
					['core/image', {
						url: WPURLS.siteurl+'/wp-content/uploads/2023/12/icon-placeholder-80.png'
					}],
					['core/heading'],
					['core/paragraph'],
					// ['cwb/button'],
				]
			}
		},
		example: {
			attributes: {
				// title: __( 'Chocolate Chip Cookies', 'gutenberg-examples' ),
				// mediaID: 1,
				// mediaURL:
				// 	'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/2ChocolateChipCookies.jpg/320px-2ChocolateChipCookies.jpg',
			},
		},



		// Block FUNCTIONS
		//-----------------------------

		edit: function( props ) {
			let attributes = props.attributes;
			let vAlignCss = [...attributes.vAlignCss];
			let blockStyle = {
				width: '100%',
				margin: '0px',
				padding: '0px',
				display: 'flex',
				position: 'relative'
			};
			// const repItemBlocks = useSelect( ( select ) => select( 'core/block-editor' ).getBlock( props.clientId ).innerBlocks );
			// console.log(repItemBlocks);


			// Get & Set Dynamic Parent Block Attributes
			//-----------------------------
			let parentAtts = {...cwbAttributes.globalBlockAtts[props.clientId]};
			console.log('parentAtts', parentAtts);
			props.setAttributes({
				gridCssClasses : parentAtts.gridCssClasses,
				gapPaddingCss : parentAtts.gapPaddingCss
			});
			attributes = props.attributes;




			// Grid Layout
			//================================
			const [gridTab, setGridTab] = useState(false);


			// Set Defaults
			// ~ todo try to set these in attributes declaration
			// 		- (maybe the bug has been fixed)
			//---------------------------------------------------
			if(!__exists(attributes.grid) || attributes.grid === '') props.setAttributes({
				grid: {...defaultScreenSizeObj, base: 'placeholder'},
			});

			if(!__exists(attributes.tileGap) || attributes.tileGap === '') props.setAttributes({
				tileGap: {...defaultScreenSizeObj, base: '2', md: '3'},
			});

			// if(!__exists(attributes.gridAlign) || attributes.gridAlign === '') props.setAttributes({
			// 	gridAlign: {...defaultScreenSizeObj, base: 'justify-content-center'},
			// });



			// Generate Grid Layout CSS
			//--------------------------------------
			// let gridCssClasses = genResponsiveClasses(attributes.grid);
			// let tileGap = genTileGapCss(attributes.tileGap);
			// let gapMarginCss = tileGap.gapMarginCss;
			// let gapPaddingCss = tileGap.gapPaddingCss;
			// let gridAlignCss = genResponsiveClasses(attributes.gridAlign);





			// Set Grid & TileGap Classes
			//-----------------------------
			let gridCssClasses = __get(parentAtts.gridCssClasses, attributes.gridCssClasses);
			let gapPaddingCss = __get(parentAtts.gapPaddingCss, attributes.gapPaddingCss);

			gridCssClasses = typeof attributes.grid !== 'undefined' && attributes.useGridOverride ?
				genResponsiveClasses(attributes.grid) : gridCssClasses;


			gapPaddingCss = typeof attributes.tileGap !== 'undefined' && attributes.useGridOverride ?
				genResponsiveClasses(attributes.tileGap) : gapPaddingCss;








			//
			// // Layout Mod Functions
			// //-----------------------------
			// const setVAlign = function(value) {
			// 	genResponsiveCss(value, 'vAlign','align-items-', vAlignCss, attributes, props );
			//
			//
			// 	props.setAttributes({ vAlignCss : vAlignCss });
			// 	attributes = props.attributes;
			//
			// 	// console.log('vAlignCss', vAlignCss);
			// };





















			let itemCss = [
				gridCssClasses,
				gapPaddingCss,
				'cwb-editor-outline',
			].join(' ');




			// RETURN: ITB Repeater Item (BACK-end)
			//================================
			return el( 'div', {
					className: itemCss,
					style: blockStyle
				} ,
				el('div',  useBlockProps( {
						className: 'container px-0 cwb-editor-outline '+attributes.cssClasses.join(' '),
						style: {
							background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : attributes.bgColorOutput,
							color: attributes.textColor ? attributes.textColor : null,
						}
					}),


					// attributes.bgEffect ?
					el(CwbBgEffect, {
						bgOverlay : attributes.overlayColorOutput,
						backdropFilter: attributes.overlayBlur ?
							'blur('+attributes.overlayBlur+'px)' : null
					}),
					// : null,


					el(InnerBlocks, {
						allowedBlocks : [
							'core/image',
							'core/heading',
							'core/paragraph',
							'cwb/button',
							// plugin required: https://wordpress.org/plugins/icon-block/
							// - author mentions plugin will be merged into core
							'outermost/icon-block'
						],
						template: attributes.template
					})
				),


				// InspectorControls
				//================================
				el( InspectorControls, {
					key: 'controls' },

					// Panel : Block Colors
					//-----------------------------
					el( BlockColors, props),

					// // Panel : Background Image
					// //-----------------------------
					// el( BgImagePanel, {
					// 	props : props,
					// 	blockStyle : blockStyle
					// }),

					// Panel : BgEffects
					//-----------------------------
					el( BgEffects, props),


					// Grid Layout Configs
					//-------------------------
					el( PanelBody, {
							style: {
								padding: '0 20px'
							},
							title: 'Grid Configs',
							initialOpen: true//!0,
						},

						el( ToggleControl,
							{
								label: 'Use Custom Grid Values',
								instructions: 'Override Repeater grid settings for this specific repeater item',
								onChange: function(value){
									props.setAttributes( { useGridOverride: value } );
									attributes = props.attributes;
								},
								checked: props.attributes.useGridOverride,
							}
						),

						attributes.useGridOverride ?
							el(ResponsiveTabs, props,
								(gridTab) => {
									setGridTab(gridTab);
									props = {
										tab: {...gridTab},
										...props,
										gridCssClasses: attributes.grid,
										// allowedGridConfigs: ['tile-width'],
										// onUpdateLayout: function(attrStr, responsiveCssObj){
										// 	onUpdateLayout(attrStr, responsiveCssObj)
										// }
									};
									return el(GridSettingsTab, props);
								}
							) : null,
					)

				)
			);
		},

		save: function( props ) {
			let attributes = props.attributes;
			let blockStyle = {
				background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` :
					attributes.bgColorOutput !== ''  ? attributes.bgColorOutput : null,
				margin: '0px',
				padding: '0px',
				width: '100%',
				display: 'flex',
				color: attributes.textColor ? attributes.textColor : null,
				position: 'relative'
			};

			// Set Grid & TileGap Classes
			//-----------------------------
			let gridCssClasses = attributes.gridCssClasses;
			let gapPaddingCss = attributes.gapPaddingCss;

			gridCssClasses = typeof attributes.grid !== 'undefined' && attributes.useGridOverride ?
				genResponsiveClasses(attributes.grid) : gridCssClasses;


			gapPaddingCss = typeof attributes.tileGap !== 'undefined' && attributes.useGridOverride ?
				genResponsiveClasses(attributes.tileGap) : gapPaddingCss;


			// Set Grid & TileGap CSS
			//-----------------------------
			let itemCss = [
				gridCssClasses,
				gapPaddingCss,
			];




			// Repeater Item CSS
			//-----------------------------
			let tileCss = [
				...attributes.cssClasses,
				'container',
				'position-relative'
			];

			// RETURN: Layout-Block (FRONT-end)
			//================================
			return el( 'div', {
					className: itemCss.join(' '),
					style: blockStyle
				} ,
				el('div', useBlockProps.save({ className: tileCss.join(' ') }),

					// attributes.bgEffect ?
						el(CwbBgEffect, {
							bgOverlay : attributes.overlayColorOutput,
							backdropFilter: attributes.overlayBlur ?
								'blur('+attributes.overlayBlur+'px)' : null
						}),
						// : null,

					el('div', {className: 'position-relative'},
						// el(InnerBlocks.Content)
						InnerBlocks.Content()
					)
				)
			);
		},
	} );
}(
	window.wp.blocks,
	window.wp.editor,
	window.wp.i18n,
	window.wp.element,
	window.wp.components,
	window._,
	window.wp.blockEditor,
	window.cwb
) );
