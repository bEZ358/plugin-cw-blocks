( function( blocks, editor, i18n, element, components, _, blockEditor, cwb ) {
	const __ = i18n.__;
	const el = element.createElement;
	const InnerBlocks = blockEditor.InnerBlocks;
	const useBlockProps = blockEditor.useBlockProps;
	const InspectorControls = blockEditor.InspectorControls;
	const useSelect = wp.data.useSelect;

	const useInnerBlocksProps = blockEditor.useInnerBlocksProps;

	// import { useBlockProps, __experimentalUseInnerBlocksProps as useInnerBlocksProps } from "@wordpress/block-editor";

	const {
	} = components;

	const {
		cwbGlobals,
		cwbComponents
	} = cwb;

	const {
		screenSizeTabs,
		defaultScreenSizeObj
	} = cwbGlobals;

	const {
		BgImagePanel,
		cwbAttributes,
		BlockColors
	} = cwbComponents;



	blocks.registerBlockType( 'cwb/slider', {


		// Block ASSETS
		//-----------------------------

		title: __( 'Slider', 'CWB' ),
		icon: 'admin-page', // see: /wp-includes/css/dashicons.css
		category: 'common',
		attributes: {
			anchor: {
				type: 'string',
				default: false,
			},
			cssClasses: {
				type: 'array',
				default: ['cwb-slider', 'my-0'],//['w-100', 'd-flex', 'm-0', 'align-items-center', 'bg-cover']
			},


			// Repeater Block Settings
			//-----------------------------
			repeaterItemType: {
				type : 'string',
				default: 'repeater-item--slide'
			},


			// Repeater Block Settings
			//-----------------------------
			repItemBlocks: {
				type : 'array',
				default: []
			},

			blockHeight: cwbAttributes.blockHeight,
			heightCss: {
				type : 'array',
				default: []
			},

			useStickyBuffer: {
				type : 'boolean',
				default: false
			},

			hasVideo: {
				type : 'boolean',
				default: false
			},

			sliderId: {
				type : 'string',
				default: ''
			},
		},
		example: {
			attributes: {
				// title: __( 'Chocolate Chip Cookies', 'gutenberg-examples' ),
				// mediaID: 1,
				// mediaURL:
				// 	'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/2ChocolateChipCookies.jpg/320px-2ChocolateChipCookies.jpg',
			},
		},


		// Block FUNCTIONS
		//-----------------------------

		edit: function( props ) {

			let slideTypeOptions = typeof window.sliderSlides !== 'undefined' && Array.isArray(window.sliderSlides) ?
				window.sliderSlides : [
				{ value: 'repeater-item--basic', label: 'Basic Repeater Item' },
				{ value: 'repeater-item--slide', label: 'Slide (Basic)' },
				// { value: 'repeater-item--lh-project-slide', label: 'LH Project Slide' },
			];



			let attributes = props.attributes;
			let repItemLimit = 5;
			let heightCss = [...attributes.heightCss];
			let optionCount = 0;
			let blockStyle = {
				width: '100%',
				margin: '0px auto',
				padding: '0px',
				style: { position: 'relative'},
			};


			// TODO
			if(attributes.sliderId === ''){
				let sliderId = '-';
				const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
				const charactersLength = 5;//characters.length;
				let counter = 0;
				while (counter < charactersLength) {
					sliderId += characters.charAt(Math.floor(Math.random() * charactersLength));
					counter += 1;
				}

				console.log('sliderId', sliderId);

				props.setAttributes({
					sliderId : sliderId
				})
			}


			// states: for ResponsiveTabs
			//-----------------------------
			const [layoutTab, setLayoutTab] = useState(false);


			// Navigate/Set current slide via state
			//-----------------------------
			const [currentItem, setCurrentItem] = useState(1);
			const prevSlide = function(){
				let prev = currentItem > 1 ? currentItem - 1 : 1;
				setCurrentItem(prev)
			};
			const nextSlide = function(){
				let next = currentItem < repItemCount ? currentItem + 1 : 1;
				setCurrentItem(next)
			};


			// Layout Mod Functions
			//-----------------------------
			const setBlockHeight = function(value) {

				genResponsiveCss(value, 'blockHeight','vh-', heightCss, attributes, props );

				props.setAttributes({
					heightCss : heightCss
				});
				attributes = props.attributes;
			};
			const onUpdateLayout = function(data, attrStr){

				switch(attrStr){
					case 'blockHeight':
						setBlockHeight(data);
						break;
					default:
						break;
				}
			};
			// TODO (*): try this for onUpdateLayout (from cwbTabs block):
			// 	- once implemented, may end up not needing .slide-flex class and cwb-sleder-editor.css
			// // required for save function (use to access inner-block atts)
			// props.setAttributes({
			// 	tabItemBlocks : tabItemBlocks
			// });
			// attributes = props.attributes;
			// const tabItemBlocks = useSelect( ( select ) => select( 'core/block-editor' ).getBlock( props.clientId ).innerBlocks );
			// // Function to update child block attributes
			// function updateChildAttributes(newAttributes, clientId) {
			// 	wp.data.dispatch('core/block-editor').updateBlockAttributes(clientId, newAttributes)
			// }
			//
			// for(let i = 0; i < tabItemBlocks.length; i++){
			// 	if(i === 0 ){
			// 		updateChildAttributes({
			// 			isFirstTab: true
			// 		}, tabItemBlocks[i].clientId);
			// 	}
			// }
			//






			// repItemBlocks
			// (required to generate slider options nav)
			//-----------------------------
			const repItemBlocks = useSelect( ( select ) => select( 'core/block-editor' ).getBlock( props.clientId ).innerBlocks );
			const repItemCount = repItemBlocks.length;
			let allowMoreItems = repItemCount < repItemLimit;
			props.setAttributes({
				repItemBlocks : repItemBlocks
			});
			attributes = props.attributes;



			// RETURN: CWB Slider Editor
			//================================
			return el( 'div', useBlockProps( {
					className: attributes.cssClasses,
					style: blockStyle,
				} ),


				// Slider Editor Wrap
				//------------------------

				el( 'div', { className: [...attributes.heightCss, 'slider d-flex align-items-center'].join(' '),
						['data-show']: currentItem
					},



					// Slider Editor Nav
					//------------------------

					el( 'div', {
						className: 'slide-prev slider-arrow',
						style: {
							position: 'absolute'
						},
						onClick: prevSlide
					}),

					el( 'div', {
						className: 'slide-next slider-arrow',
						style: {
							position: 'absolute'
						},
						onClick: nextSlide
					}),

					el( 'div', {className: 'slider-options',},
						el( 'div', {className: 'slider-options-inner',},
							repItemBlocks.map((item) => {

								optionCount++;
								let itemKey = optionCount;

								return el( 'div', {
										className: itemKey === parseInt(currentItem) ? 'slider-option active' : 'slider-option',
										['data-key']: itemKey,
										onClick: function(){
											setCurrentItem(itemKey)
										}
									},
								)
							})
						)
					),


					// Slides
					//------------------------

					el( 'div', {className: 'slides w-100',},
						el(InnerBlocks, {
							allowedBlocks : [
								'cwb/'+attributes.repeaterItemType,
							],
							template : [
								[ 'cwb/'+attributes.repeaterItemType, { heading: 'Slide 1' } ],
								[ 'cwb/'+attributes.repeaterItemType, { heading: 'Slide 2' } ],
							],
							renderAppender : allowMoreItems ? null : false
						}),
						// // TODO (*): try this for onUpdateLayout (from cwbTabs block):
						// onChange: function(newBlocks) {
						// 	// Update child block attributes based on parent block attributes
						// 	newBlocks.forEach(function(block) {
						// 		console.log('InnerBlock',block);
						// 		if (block.attributes) {
						//
						// 			// console.log('InnerBlock.atts',block.attributes);
						//
						// 			// var updatedAttributes = {
						// 			// 	childAttribute: props.attributes.parentAttribute,
						// 			// };
						// 			// // Update child block attributes
						// 			// updateChildAttributes(updatedAttributes, block.clientId);
						// 		}
						// 	});
						// },
					)
				),



				// InspectorControls
				//================================
				el( InspectorControls, {
						key: 'controls' },

					el( 'div', {
							style: {
								margin: '0 20px 20px'
							},
						},
						el( TextControl,
							{
								label: 'Block Anchor',
								value: typeof attributes.anchor === 'string' ? attributes.anchor : null,
								onChange: function(value){
									props.setAttributes({
										anchor: limitForHtmlId(value)
									})
								},
							}
						),

						el( SelectControl, {
							key: 'multiple',
							label: __('Select Repeater Item Type'),
							value: attributes.repeaterItemType,
							className: ['mt-3'],
							onChange: function(value){
								props.setAttributes({
									repeaterItemType : value
								});
								attributes = props.attributes;
							},
							options: slideTypeOptions
						})
					),




					// Panel : Wrap Settings
					//-----------------------------
					el( PanelBody, {
							title: 'Slider Height',
							initialOpen: false//!0
						},
						el('div',{ style: {
									border: '1px solid #ddd'
								}},
							el(ResponsiveTabs, props,
								(tab) => {
									setLayoutTab(tab);
									// ^ fixes something (not sure what yet) to allow below to work
									props = {
										// tab:{...layoutTab},
										// ^ this would make more sense (maybe?) but somehow below gets fixed after
										//   setLayoutTab(tab) is called... don't understand why
										tab: {...tab},
										...props,
										// enabled: [
										// 	'blockHeight',
										// 	//'hAlign'
										// ],
										onUpdateLayout: function(data, attrStr){
											onUpdateLayout(data, attrStr)
										}
									};
									// console.log('props.tab.name', props.tab.name);
									return el(BlockLayoutSettingsTab, props);
								}
							),
						),

						el( ToggleControl,{
							style: {
								margin: '20px 0 0'
							},
							label: 'Add sticky-buffer to slides',
							onChange: function (value) {
								props.setAttributes({
									useStickyBuffer : value
								});
								attributes = props.attributes;
							},
							checked: attributes.useStickyBuffer,
							help: 'Use only for hero sliders. Inserts a spacer that dynmically changes it\' height based on the height to the sticky height',
						}),

						el( ToggleControl,{
							style: {
								margin: '20px 0 0'
							},
							label: 'Slides have video',
							onChange: function (value) {
								props.setAttributes({
									hasVideo : value
								});
								attributes = props.attributes;
							},
							checked: attributes.hasVideo,
							// help: 'Use only for hero sliders. Inserts a spacer that dynmically changes it\' height based on the height to the sticky height',
						})
					),


				)
			);
		},

		save: function( props ) {
			let attributes = props.attributes;
			let blockStyle = {
				margin: '0px',
				padding: '0px',
				position: 'relative',
				width: '100%',
				// background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : attributes.bgColorOutput,
				// color: attributes.textColor ? attributes.textColor : null,
			};

			// RETURN: CWB Slider Front-End
			//================================
			return el( 'div', useBlockProps.save( {
					id: typeof attributes.anchor === 'string' ? attributes.anchor : null,
					className: attributes.cssClasses.join(' '),
					style: blockStyle
				} ),

				el(CwbBlockSlider, props,
					el( InnerBlocks.Content )
				),
			);
		},
	} );
}(
	window.wp.blocks,
	window.wp.editor,
	window.wp.i18n,
	window.wp.element,
	window.wp.components,
	window._,
	window.wp.blockEditor,
	window.cwb
) );



window.sliderSlides = [
	{ value: 'repeater-item--basic', label: 'Basic Repeater Item' },
	{ value: 'repeater-item--slide', label: 'Slide (Basic)' },
	{ value: 'repeater-item--magic-label-slide', label: 'Magic Label Slide' },
	// { value: 'repeater-item--lh-project-slide', label: 'LH Project Slide' },
];