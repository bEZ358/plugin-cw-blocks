<?php

/**
 * Plugin Name: Gutenberg Examples Recipe Card
 * Plugin URI: https://github.com/WordPress/gutenberg-examples
 * Description: This is a plugin demonstrating how to register new blocks for the Gutenberg editor.
 * Version: 1.1.0
 * Author: the Gutenberg Team
 *
 * @package gutenberg-examples
 */

defined( 'ABSPATH' ) || exit;

/**
 * Registers all block assets so that they can be enqueued through Gutenberg in
 * the corresponding context.
 *
 * Passes translations to JavaScript.
 */
function register_block__cwb_slider() {

	if ( ! function_exists( 'register_block_type' ) ) {
		// Gutenberg is not active.
		return;
	}
	// Register the block by passing the location of block.json to register_block_type.
	register_block_type( __DIR__ );

}
add_action( 'init', 'register_block__cwb_slider' );



// todo
//==================================================

if ( ! function_exists( 'enqueue_cwb_slider_editor' ) ) {

    /**
     * Enqueue THEME FRONT-END CONFIGS
     *
     * - enqueue_parent_theme_front_end_scripts() MUST be active in parent theme
     * - cannot check if parent theme function exists because child theme functions get called first
     */
    function enqueue_cwb_slider_editor(){

        $version = null;//$cw->theme->script_version;

        wp_register_style( 'cwb-slider-editor-css', plugin_dir_url(__FILE__).'cwb-slider-editor.css', array(), $version, false );
        wp_enqueue_style('cwb-slider-editor-css');
    }
    add_action( 'admin_enqueue_scripts', 'enqueue_cwb_slider_editor', 1 );
}