( function( blocks, editor, i18n, element, components, _, blockEditor, cwb ) {
	const __ = i18n.__;
	const el = element.createElement;
	const InnerBlocks = blockEditor.InnerBlocks;
	const useBlockProps = blockEditor.useBlockProps;
	const InspectorControls = blockEditor.InspectorControls;

	const {
		Dashicon,
		Toolbar,
		ToolbarButton,
		// ToggleControl,
		// Panel,
		// PanelBody,
		// PanelRow
	} = components;

	const {
		cwbGlobals,
		cwbComponents
	} = cwb;

	const {
		// screenSizeTabs,
		// defaultScreenSizeObj
	} = cwbGlobals;

	const {
		RepeaterControls,
		RepeaterToolbar,
		Repeater,
		// BgImagePanel,
		// BlockColors,
		// cwbAttributes
	} = cwbComponents;


	// @ref links below
	// https://mediaron.com/how-to-create-a-repeater-field6-in-gutenberg/
	// https://reactnative.dev/docs/state

	blocks.registerBlockType( 'cwb/repeater-block', {


		// Block ASSETS
		//-----------------------------

		title: __( 'Simple Repeater Block', 'CWB' ),
		icon: 'admin-page', // see: /wp-includes/css/dashicons.css
		category: 'common',
		attributes: {
			cssClasses: {
				type: 'array',
				default: ['p-3', 'my-0'],//['w-100', 'd-flex', 'm-0', 'align-items-center', 'bg-cover']
			},

			// // Common Attribute Sets
			// //-----------------------------
			// ...cwbAttributes.__blockColors,
			// ...cwbAttributes.__bgImage,
			// // ...cwbAttributes.__blockBorder,


			// // Repeater Block Settings
			// //-----------------------------
			// fullWidth: {
			// 	type: 'boolean' // for ToggleControl we need boolean type
			// }
			items: {
				type : 'array',
				default: [
					{ key: 0, itemText: 'item 1'},
					{ key: 1, itemText: 'item 2'},
					{ key: 2, itemText: 'item three'},
				]
			},
			focusItem: {
				type: 'integer',
				default: null
			}
		},
		example: {
			attributes: {
				// title: __( 'Chocolate Chip Cookies', 'gutenberg-examples' ),
				// mediaID: 1,
				// mediaURL:
				// 	'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/2ChocolateChipCookies.jpg/320px-2ChocolateChipCookies.jpg',
			},
		},



		// Block FUNCTIONS
		//-----------------------------

		edit: function( props ) {
			let attributes = props.attributes;
			let blockStyle = {
				width: '100%',
				margin: '0px auto',
				padding: '0px',
			};

			// // Options
			// let ops = [];
			// // attributes.items.map((item) =>
			// // {
			// // 	ops.push({
			// // 		value : item.key,
			// // 		label : 'item '+item.key+': '+item.itemText
			// // 	});
			// // });

			// Block DRY Functions
			//--------------------------------------

			const updateRepeaterItems = function(newArr){
				props.setAttributes({
					items : newArr
				});
				attributes = props.attributes;
			};

			const setItemAttr = function(value, focusItem, property){

				let newArr = [...attributes.items];

				// update item
				newArr[focusItem.key][property] = value;

				//
				// update block
				updateRepeaterItems(newArr);
			};



			// used for WYSIWYG edits rather than edits via inspectorPanel
			// - called when updating text todo see if there's a way to pull this from RepeaterComponent
			const updateFocus = function(item){
				props.setAttributes({
					focusItem : item//.key.toString()
				});
				attributes = props.attributes;
			};



			// Content Modifiers
			//--------------------------------------

			const updateText = function(value, item){
				setItemAttr(value, item, 'itemText');
			};





			const updateRepeater = function(newArr, focusItem) {

				// reset item keys
				for(let i = 0; i < newArr.length; i++){
					newArr[i].key = i;
				}
				// update block
				props.setAttributes({
					items : newArr,
					focusItem : focusItem.toString()
				});
				attributes = props.attributes;
			};

			// const addItem = function () {
			//
			// 	let newArr = [...attributes.items];
			//
			// 	// add item
			// 	let newKey = newArr.length;
			// 	newArr.push({
			// 		key: newKey,
			// 		itemText: 'item ' + newKey
			// 	});
			// 	updateRepeater(newArr, newArr.length-1);
			// };
			//
			//
			//




			let focusItem = attributes.items[ attributes.focusItem !== null ? attributes.focusItem : 0 ];
			focusItem = fixFocusItem(focusItem, attributes);


			// RETURN: Simple-Repeater-Block (BACK-end)
			//================================
			return el( 'div', useBlockProps( {
					className: attributes.cssClasses,
					style: blockStyle
				} ),

				// Container Wrap
				//-----------------------------
				el( 'div', {
						className: attributes.fullWidth ? '' : 'container px-0'
					},

					el(SimpleRepeaterBlock, {
							// ...props,
							blockProps: props,
							// itemObjModel : itemObjModel,
						},
						(item, toolbar) => {
							return el('h1', {
									style: {
										position: 'relative'
									}
								},
								el( TextControl,{
										value: item.itemText,
										onChange: function(value){
											updateText(value, item);
										},
										onFocus: function(){
											updateFocus(item.key.toString());
										}
									}
								),
								toolbar
							)
						}
					)
				),




				// InspectorControls
				//================================
				el( InspectorControls, {
					key: 'controls' },

					el(RepeaterControls, {
							// ...props,
							blockProps: props,
							// itemObjModel : itemObjModel,
						},

						// Item Controls:
						el( TextControl,
							{
								label: 'item text',
								value: focusItem.itemText,
								onChange: function(){
									updateText(item);
								}
							}
						),
					),
				)
			);
		},

		save: function( props ) {
			let attributes = props.attributes;
			let blockStyle = {
				// background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : attributes.bgColorOutput,
				margin: '0px',
				padding: '0px',
				width: '100%',
				// color: attributes.textColor ? attributes.textColor : null,
			};

			// RETURN: Layout-Block (FRONT-end)
			//================================
			return el( 'div', useBlockProps.save( {
					className: attributes.cssClasses.join(' '),
					style: blockStyle
				} ),

				// Container Wrap
				//-----------------------------
				el( 'div', {
						['data-bez']: 'bez', // toggle this to force a block markup refresh
						className: attributes.fullWidth ? '' : 'container px-0'
					},


					attributes.items.map((item) => {
							return el('h1', {
									style: {
										position: 'relative'
									}
								},

								item.itemText
							)
						}
					)
				)
			);
		},
	} );
}(
	window.wp.blocks,
	window.wp.editor,
	window.wp.i18n,
	window.wp.element,
	window.wp.components,
	window._,
	window.wp.blockEditor,
	window.cwb
) );
