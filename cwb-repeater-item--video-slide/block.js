( function( blocks, editor, i18n, element, components, _, blockEditor, cwb ) {
	const __ = i18n.__;
	const el = element.createElement;
	const InnerBlocks = blockEditor.InnerBlocks;
	const useBlockProps = blockEditor.useBlockProps;
	const InspectorControls = blockEditor.InspectorControls;

	const {
	} = components;

	const {
		cwbGlobals,
		cwbComponents
	} = cwb;

	const {
	} = cwbGlobals;

	const {
		cwbAttributes
	} = cwbComponents;


	// @ref links below
	// https://mediaron.com/how-to-create-a-repeater-field6-in-gutenberg/
	// https://reactnative.dev/docs/state

	blocks.registerBlockType( 'cwb/repeater-item--video-slide', {


		// Block ASSETS
		//-----------------------------

		title: __( 'Repeater Item (Video Slide)', 'CWB' ),
		icon: 'admin-page', // see: /wp-includes/css/dashicons.css
		category: 'common',
		attributes: {
			cssClasses: {
				type: 'array',
				default: ['slide'],//['w-100', 'd-flex', 'm-0', 'align-items-center', 'bg-cover']
			},

			videoId: {
				type: 'id',
				default: ''
			},
			vidSrc: {
				type: 'object',
				default: ''
			},

			// Common Attribute Sets
			//-----------------------------
			// ...cwbAttributes.__blockColors,
			// ...cwbAttributes.__bgImage,
			// ...cwbAttributes.__blockBorder,
			...cwbAttributes.__bgOverlayColors,
			// overlayBlur: {
			// 	type: 'integer',
			// 	default: 0,
			// },


			vAlign: cwbAttributes.vAlign,
			vAlignCss: {
				type : 'array',
				default: []
			},

			hAlign: cwbAttributes.hAlign,
			hAlignCss: {
				type : 'array',
				default: []
			},

			//
			// // // Repeater Item Settings
			// // //-----------------------------
			// heading: {
			// 	type : 'string',
			// 	default: 'Default Heading',
			// 	// selector: 'h2'
			// },
			//
			// body: {
			// 	type : 'string',
			// 	default: 'Default Body, lorem ipsum...'
			// },


			btnObj: {
				type: 'object',
				default: false
			},


			muteVideo: {
				type: 'boolean', // for ToggleControl we need boolean type
				default: true
			},
		},
		example: {
			attributes: {
				// title: __( 'Chocolate Chip Cookies', 'gutenberg-examples' ),
				// mediaID: 1,
				// mediaURL:
				// 	'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/2ChocolateChipCookies.jpg/320px-2ChocolateChipCookies.jpg',
			},
		},



		// Block FUNCTIONS
		//-----------------------------

		edit: function( props ) {
			let attributes = props.attributes;
			let vAlignCss = [...attributes.vAlignCss];
			let hAlignCss = [...attributes.hAlignCss];
			let blockStyle = {
				width: '100%',
				margin: '0px auto',
				padding: '0px',
				border: '1px dotted rgba(0,0,0,0.2)',
				color: attributes.textColor ? attributes.textColor : null,
				background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : attributes.bgColorOutput,
				position: 'relative'
			};

			attributes.vidSrc = attributes.vidSrc === '' ? false : attributes.vidSrc;









			// Layout Mod Functions
			//-----------------------------
			const setVAlign = function(value) {
				genResponsiveCss(value, 'vAlign','align-items-', vAlignCss, attributes, props );


				props.setAttributes({ vAlignCss : vAlignCss });
				attributes = props.attributes;

				// console.log('vAlignCss', vAlignCss);
			};
			const setHAlign = function(value) {
				genResponsiveCss(value, 'hAlign','text-', hAlignCss, attributes, props );

				props.setAttributes({ hAlignCss : hAlignCss });
				attributes = props.attributes;

				// console.log('hAlignCss', hAlignCss);
			};
			const onUpdateLayout = function(data, attrStr){

				console.log('onUpdateLayout', attrStr, data);

				switch(attrStr){
					case 'vAlign':
						setVAlign(data);
						break;
					case 'hAlign':
						setHAlign(data);
						break;
					default:
						break;
				}
			};
			// const setContainerClasses = function( value ) {
			// 	props.setAttributes( { containerClasses: value } );
			// 	attributes = props.attributes;
			// };


			// for ResponsiveTabs
			//-----------------------------
			const [layoutTab, setLayoutTab] = useState(false);


			function updateBtn(value, attrStr){
				let newBtnObj = {...attributes.btnObj};
				newBtnObj[attrStr] = value;
				props.setAttributes( {
					btnObj: newBtnObj
				} );
				attributes = props.attribute
			};



			if(!attributes.btnObj){
				props.setAttributes({
					btnObj : {...cwbAttributes.defaultButtonObj}
				});
				attributes = props.attributes;
			}


			// RETURN: Simple-Repeater-Block (BACK-end)
			//================================
			return el( 'div', useBlockProps( {
					className: attributes.cssClasses,
					style: blockStyle
				} ),



				// attributes.bgEffect ?
					el(CwbBgEffect, {
						bgOverlay : attributes.overlayColorOutput,
						backdropFilter: attributes.overlayBlur ?
							'blur('+attributes.overlayBlur+'px)' : null
					}),
					// : null,


				el('div', {
						className: [
							// attributes.containerClasses,
							...attributes.vAlignCss,
							'd-flex',
							'slide-flex',
							'vh-50',
							'py-5',
							'px-4'
						].join(' ')
					},
					el('div', {
							className: [
								...attributes.hAlignCss,
								'slide-content w-100'
							].join(' ')
						},

							el( MediaUpload, {
								onSelect: function(media){
									// console.log('media', media);
									// todo DRY
									props.setAttributes({
										vidSrc : media
									});
									attributes = props.attributes
								},
								allowedTypes: 'video',
								value: attributes.vidSrc,

								// todo DRY: MediaEditHover
								render: function( obj ) {
									return el('div', { className: 'position-relative _trace-green w-100 _bg-black'},
										el(CwbEditableButton, {
											btnObj: attributes.btnObj,
											allowedBtnTypes  : [
												// 'link',
												'wp_link',
												// 'modal_trigger',
												// 'iframe',
												// 'shortcode_modal_trigger'
											],
											updateText : function(value){
												updateBtn(value, 'text');
											},
											updateUrl : function(value){
												updateBtn(value, 'href');
											},
											updateNewTab : function(value){
												updateBtn(value, 'newTab');
											},
											setButtonFunction : function(value){
												updateBtn(value, 'btnFunction');
											},
											updateDataSrc : function(value){
												updateBtn(value, 'dataSource');
											},
											updateDataTarget : function(value){
												updateBtn(value, 'target');
											},
											updateDataToggle : function(value){
												// props.setAttributes( {
												// 	dataToggle: value
												// } );
												// attributes = props.attributes;
											},
											updateShortcode : function(value){
												updateBtn(value, 'dataShortcode');
											},
											updateModalHeading : function(value){
												updateBtn(value, 'popupHeading');
											},
											updateModalSize : function(value){
												updateBtn(value, 'popupSize');
											},
											updatePostObj : function(value){
												updateBtn(value, 'postObj');
												// console.log('LinkControl', attributes.postObj);
											},




											// todo  btn styles
											updateBtnType : function(value){
												updateBtn(value, 'btnType');
											},
											updateBtnSize : function(value){
												updateBtn(value, 'btnSize');
											},
											updateHoverScale : function(value){
												updateBtn(value, 'hoverScale');
											},
										})

										// attributes.vidSrc ? el('div', {},
										// 	el(CwbVideo, {
										// 		vidSrc : attributes.vidSrc.url,
										// 	}),
										// 	el('div', {className:'display-on-parent-hover text-white',
										// 			style: {
										// 				position: 'absolute',
										// 				width: '100%',
										// 				height: '100%',
										// 				background: "rgba(0,0,0,0.5)",
										// 				top: 0,
										// 				alignItems: 'center',
										// 				justifyContent: 'center',
										// 				flexDirection: 'column'
										// 			},
										// 		},
										// 		el('p', { className: 'mb-0'}, 'file: ',
										// 			el('strong', {}, attributes.vidSrc.filename)
										// 		),
										// 		el('p', { className: ''}, "Video player is disabled in editor mode"),
										//
										// 		el('div', { className: 'text-white',
										// 				style: {
										// 					border: "1px solid rgba(255,255,255,0.5)",
										// 					background: "rgba(0,0,0,0.5)",
										// 				}},
										//
										// 			el(components.Button,{
										// 					onClick: function(){
										// 						// todo DRY
										// 						props.setAttributes({
										// 							vidSrc : false,
										// 						});
										// 						attributes = props.attributes
										// 					},
										// 				},
										// 				el( Icon, { icon: 'trash', style: {
										// 						color: 'white'
										// 					} }),
										// 			)
										// 		)
										// 	),
										// 	)
										// 	: el(MediaPlaceholder,
										// 	{
										// 		labels: {
										// 			title: 'Add / Upload a Video'
										// 		},
										// 		icon: el(Icon, {icon: 'format-video', style: {margin: '0 20px 0 0'}}),
										// 		// dropZoneUIOnly: true,
										// 		allowedTypes: 'video',
										// 		onSelect: function(media){
										// 			// console.log('MediaPlaceholder', media);
										// 			props.setAttributes({
										// 				vidSrc : media
										// 			});
										// 			attributes = props.attributes
										// 		},
										// 	}
										// 	),
									)
								}
							}),


						// )
					),
				),




				// InspectorControls
				//================================
				el( InspectorControls, {
					key: 'controls' },


					// Panel : Wrap Settings
					//-----------------------------
					el( PanelBody, {
							title: 'Video Settings',
							initialOpen: false//!0
						},

						// el( TextControl, {
						// 	label: __( 'Container CSS Class(es)'),
						// 	help: 'Separate multiple classes with spaces.',
						// 	value: attributes.containerClasses,
						// 	type: 'text',
						// 	onChange: setContainerClasses,
						// }),

						el( MediaUpload, {
							onSelect: function(media){
								console.log(media); // todo
								props.setAttributes({
									vidSrc : media
								});
								attributes = props.attributes
							},
							allowedTypes: 'video',
							value: attributes.vidSrc,
							render: function( obj ) {
								return el('div', { className: '_text-white',
										style: {
											// border: "1px solid rgba(255,255,255,0.5)",
											// background: "rgba(0,0,0,0.5)",
											marginBottom: '20px'
										}},

									el('div', {},

										el(TextControl, {
											label : 'Video File URL',
											help : '',
											value : attributes.vidSrc.url,
											onChange :function(value){
												let filename = attributes.vidSrc.filename;

												if(value !== '') {
													props.setAttributes({
														vidSrc: {
															url: value,
															filename: value !== filename ?
																value !== '' ? 'external link' : 'none'
																: filename
														}
													});
													attributes = props.attributes
												}
												else {
													props.setAttributes({
														vidSrc: false
													});
													attributes = props.attributes
												}
											}
										}),

										attributes.vidSrc ?
											el('p', { className: 'mb-0'}, 'file: ',
												el('strong', {}, attributes.vidSrc.filename)
											) : null

									),

									attributes.vidSrc ?
										el(components.Button,{
												className: 'components-button is-tertiary',
												onClick: function(){
													props.setAttributes({
														vidSrc : false,
													});
													attributes = props.attributes
												},
											},
											el( Icon, { icon: 'trash', style: {
													// color: 'white'
												} }),
										)
										: el(components.Button,{
											onClick: obj.open,
											className: 'components-button is-tertiary'
										},
										'Video Library ',
										el( Icon, { icon: 'format-video', style: {
												marginLeft: '10px'
												// color: 'white'
											} }),
										),
								)
							}
						}),


						// el( ToggleControl,
						// 	{
						// 		label: 'Mute Video',
						// 		onChange: function(value){
						// 			props.setAttributes( { muteVideo: value } );
						// 			attributes = props.attributes;
						// 		},
						// 		checked: props.attributes.muteVideo,
						// 	}
						// ),

						el(ResponsiveTabs, props,
							(tab) => {
								setLayoutTab(tab);
								// ^ fixes something (not sure what yet) to allow below to work
								props = {
									// tab:{...layoutTab},
									// ^ this would make more sense (maybe?) but somehow below gets fixed after
									//   setLayoutTab(tab) is called... don't understand why
									tab: {...tab},
									...props,
									// enabled: [
									// 	'vAlign',
									// 	//'hAlign'
									// ],
								};
								// console.log('props.tab.name', props.tab.name);
								return el(BlockLayoutSettingsTab, {
									...props,
									onUpdateLayout: function(data, attrStr){
										onUpdateLayout(data, attrStr)
									}
								});
							}
						)
					),


					// Panel : Block Colors
					//-----------------------------
					el( BlockColors, props),

					// Panel : Background Image
					//-----------------------------
					el( BgImagePanel, {
						props : props,
						blockStyle : blockStyle
					}),


					// Panel : BgEffects
					//-----------------------------
					el( BgEffects, props),

					// el('hr',{}),


					el('div',{className: "py-4"},
						el('h3',{className: "px-3 f-w-600"}, "Button Settings"),
						el(CwbButtonEditor,{
							allowedBtnTypes  : [
								// 'link',
								'wp_link',
								'modal_trigger',
								// 'iframe',
								// 'shortcode_modal_trigger'
							],
							btnObj: attributes.btnObj,
							updateText : function(value){
								updateBtn(value, 'text');
							},
							updateUrl : function(value){
								updateBtn(value, 'href');
							},
							updateNewTab : function(value){
								updateBtn(value, 'newTab');
							},
							setButtonFunction : function(value){
								updateBtn(value, 'btnFunction');
							},
							updateDataSrc : function(value){
								updateBtn(value, 'dataSource');
							},
							updateDataTarget : function(value){
								updateBtn(value, 'target');
							},
							updateDataToggle : function(value){
								// props.setAttributes( {
								// 	dataToggle: value
								// } );
								// attributes = props.attributes;
							},
							updateShortcode : function(value){
								updateBtn(value, 'dataShortcode');
							},
							updateModalHeading : function(value){
								updateBtn(value, 'popupHeading');
							},
							updateModalSize : function(value){
								updateBtn(value, 'popupSize');
							},
							updatePostObj : function(value){
								updateBtn(value, 'postObj');
							},




							// todo  btn styles
							updateBtnType : function(value){
								updateBtn(value, 'btnType');
							},
							updateBtnSize : function(value){
								updateBtn(value, 'btnSize');
							},
							updateHoverScale : function(value){
								updateBtn(value, 'hoverScale');
							},
						}),
					)
				)
			);
		},

		save: function( props ) {
			let attributes = props.attributes;
			// let includeAltText = attributes.includeAriaLabel && attributes.mediaAlt.length;
			let blockStyle = {
				// background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : attributes.bgColorOutput,
				margin: '0px',
				padding: '0px',
				width: '100%',
				// color: attributes.textColor ? attributes.textColor : null,
				// background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : attributes.bgColorOutput,
				position:'relative'
				// color: attributes.textColor ? attributes.textColor : null,
			};


			// RETURN: Layout-Block (FRONT-end)
			//================================
			return el( 'div', useBlockProps.save( {
					className: attributes.cssClasses.join(' ') + ' test',
					style: blockStyle,
					// role: includeAltText ? 'img' : null,
					// ['aria-label']: includeAltText ? attributes.mediaAlt : null,
				} ),


				// attributes.bgEffect ?
				el(CwbBgEffect, {
					bgOverlay : attributes.overlayColorOutput,
					// backdropFilter: attributes.overlayBlur ?
					// 	'blur('+attributes.overlayBlur+'px)' : null
				}),
				// : null,

				el('div', {
						className: [
							// attributes.containerClasses,
							// ...attributes.vAlignCss,
							'd-flex',
							'slide-flex'
						].join(' ')
					},

					el('div', {
							className: [
								// ...attributes.hAlignCss,
								'slide-content w-100 _sticky-buffer-child'
							].join(' ')
						},

						el('div', { className: [
									'position-absolute w-100 d-flex h-100 _vh-90-min px-5 py-0 mt-n-admin-bar',
									'sticky-buffer-here', // TODO make conditional
									'flex-column'
								].join(' ')},

							el('div', { className: [
										// attributes.containerClasses,
										...attributes.vAlignCss,
										'd-flex',
										'py-4 py-md-5',
										'video-click clickable w-100 flex-grow-1 _trace-green cursor-pointer',
									].join(' ')},

								el('div', {
										className: [
											...attributes.hAlignCss,
											'w-100'
										].join(' ')
									},

									// el('p', {
									// 	className: [
									// 		'audio-toggle text-white'
									// 	].join(' ')
									// }, 'audio toggle'),

									el(CwbButton, {
										btnObj : attributes.btnObj,
										extraBtnCss : ['clickable'],//setBtnCssClasses(btnObj, blockProps.className),
										// btnType : ''
									})
								)
							),
						),

						el(CwbVideo, {
							id: typeof attributes.anchor === 'string' ? attributes.anchor : null,
							vidSrc : attributes.vidSrc.url,
							loop: false,
							muted: true// TODO: attributes.muteVideo
						})
					)
				)
			);
		},
	} );
}(
	window.wp.blocks,
	window.wp.editor,
	window.wp.i18n,
	window.wp.element,
	window.wp.components,
	window._,
	window.wp.blockEditor,
	window.cwb
) );
