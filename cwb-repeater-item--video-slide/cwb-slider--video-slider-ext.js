// console.log('cwb-slider--video-slider-ext: wurx');


jQuery(function($) {



    $(document).ready(function($) {


        let slider_listener = setInterval(function(){


            // WAit for slider_collection to be generated
            if(typeof slider_collection === 'object'){

                clearInterval(slider_listener);
                let video_sliders = [];//$('.slider[data-video-slider="true"]');


                // FIND existing video sliders
                for (var slider in slider_collection) {
                    if (Object.prototype.hasOwnProperty.call(slider_collection, slider)) {

                        // do stuff
                        video_sliders.push(slider_collection[slider]);
                    }
                }




                // LOOP all existing video sliders
                for(let x =0; x < video_sliders.length; x++){

                    let v_slider = video_sliders[x];
                    let slider_videos = $(v_slider.elements.slider).find('video');
                    let videoClick = $(v_slider.elements.slider).find('.video-click');
                    let slider_options = $(v_slider.elements.slider).find('.slider-option');


                    // console.log('slider_options', slider_options);


                    // APPEND elements for timer animation
                    for(let o = 0; o < slider_options.length; o++){

                        let option_inner = $DIV('option-inner');
                        $(option_inner)
                            .addClass('bg-black')
                            .addClass('pt-2')
                            .css('width', 0);
                        $(slider_options[o]).append(option_inner);

                    }




                    // BIND video events
                    for(let i = 0; i < slider_videos.length; i++){

                        let video = $(slider_videos[i])[0];
                        // console.log(video, video.volume);
                        video.volume = 0.25;


                        video.onloadedmetadata = function() {
                            // console.log('onloadedmetadata', video);

                            $(slider_videos[i]).attr('data-duration', video.duration);
                            // console.log('Video duration: ', video.duration, ' seconds');
                        };

                        // Optionally, load the video metadata (if videos are not preloaded)
                        video.load();  // Force loading metadata

                        // console.log('v_slider', v_slider.elements.slider[0]);
                        $(slider_videos[i])
                            .attr('data-slider', $(v_slider.elements.slider[0]).attr('id'))
                            .attr('data-slider-index', x)
                            .attr('data-option-id', i+1);


                        // let prog_circle = $EL('svg', 'progress-circle');
                        // let circle_bg = $EL('circle', 'progress-bg');
                        // let circle_progress = $EL('circle', 'progress-c-bar');
                        //
                        // $(prog_circle)
                        //     .attr('viewBox', '0 0 100 100')
                        //     .attr('width', '50')
                        //     .attr('height', '50');
                        //
                        //
                        //
                        // $(circle_bg)
                        //     .attr('cx', '50')
                        //     .attr('cy', '50')
                        //     .attr('r', '45')
                        //     .attr('fill', 'none')
                        //     .attr('stroke', '#333')
                        //     .attr('stroke-width', '2')
                        //
                        // $(circle_progress)
                        //     .attr('cx', '50')
                        //     .attr('cy', '50')
                        //     .attr('r', '45')
                        //     .attr('fill', 'none')
                        //     .attr('stroke', '#fff')
                        //     .attr('stroke-width', '2')
                        //
                        //
                        // $(prog_circle)
                        //     .append(circle_bg)
                        //     .append(circle_progress);
                        // $(slider_options[o]).append(prog_circle);

                        // Detect when the video is paused
                        $(video).on('pause', function() {
                            // console.log('Video is paused', $(video).attr('data-slider') );
                        });

                        // Detect when the video is playing
                        $(video).on('play', function() {
                            // console.log('Video is playing', $(video).attr('data-slider') );

                            v_slider.update_ui();
                        });

                        // Detect when the video has ended
                        $(video).on('ended', function() {
                            // console.log('Video has ended', $(video).attr('data-slider') );

                            video_sliders[ $(video).attr('data-slider-index') ].next_slide();
                        });


                        // let audioToggle = $( $( $(video).parent()).find('.audio-toggle') );
                        // if(audioToggle.length)
                        //     audioToggle.click( function (e) {
                        //         e.stopPropagation();
                        //         let isMuted = video.volume === 0;
                        //         video.volume = isMuted ? 0.25 : 0;
                        //
                        //         //
                        //         // $(video).attr('muted', null);
                        //         // // $(video).attr('autoplay', null);
                        //         //
                        //         console.log(video.volume);
                        //     });


                        // var video = $('#myVideo')[0]; // Get the video element

                        // Update the progress bar as the video plays
                        video.addEventListener('timeupdate', function() {
                            let percentage = (video.currentTime / video.duration) * 100;
                            let progress_bar = $('.slider-option[data-id="'+ $(video).attr('data-option-id') +'"] .option-inner');


                            // console.log($('option[data-id="'+ $(video).attr('data-option-id') +'"]').length);

                            progress_bar.css('width', percentage + '%');
                        });



                        // video.addEventListener('timeupdate', function() {
                        //     let percentage = (video.currentTime / video.duration) * 100;
                        //     let progress_bar = $('.slider-option[data-id="'+ $(video).attr('data-option-id') +'"] .progress-c-bar');
                        //
                        //     // Calculate stroke-dashoffset based on the percentage
                        //     let circumference = 2 * Math.PI * 45; // Radius of 45 (same as in SVG)
                        //     let offset = circumference - (percentage / 100) * circumference;
                        //
                        //     // Set the offset to create the circular progress effect
                        //     progress_bar.css('stroke-dashoffset', offset);
                        // });


                    }

                    // let autoplayEnabled = true;
                    // // console.log(v_slider);
                    // $(v_slider.elements.slider).mousemove(function(){
                    //     if(autoplayEnabled){
                    //         slider_videos[0].play();
                    //         console.log(slider_videos[0]);
                    //         slider_videos = false;
                    //     }
                    // });

                    // BIND click overlay events
                    $(videoClick).click(function(){

                        let this_video = $($($(this).closest('.slide')).find('video'))[0];
                        if (this_video.paused === false) {
                            this_video.pause();
                        } else {
                            this_video.play();
                        }


                        // return false;
                    });



                    // BIND slider callback
                    v_slider.on("toggle_slides_called", function(slider_position) {

                        // console.log("Slider position in callback:", slider_position, $(slider_videos[slider_position-1]));
                        slider_videos.each(function() {
                            this.pause();
                            this.currentTime = 0; // Reset to start of the video
                        });

                        // start playin the new video
                        $(slider_videos[slider_position - 1])[0].play();
                    });
                };
                // console.log('video_sliders', video_sliders);




            // | trigger next on video end
            // | pause slider on video pause
            // | do not reset paused video on next trigger
            // | play video on slide toggle: requires a callback tunnel for slider._toggle_slide()
            // \ option nav timer animation?
                // | append progress bars to option nav?
                // | animate bar
                // = translate to circular options elements




            }

        }, 100);
    })
});