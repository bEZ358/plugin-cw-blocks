/*
 * name: CWB
 * handle: 'cwb'
 *
 * Must Run After 'cwb-globals' && 'cwb-components'
 */

window.cwb = {
    cwbGlobals : cwbGlobals,
    cwbComponents : cwbComponents
};