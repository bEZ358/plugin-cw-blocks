( function( blocks, editor, i18n, element, components, _, blockEditor, cwb ) {
	const __ = i18n.__;
	const el = element.createElement;
	const InnerBlocks = blockEditor.InnerBlocks;
	const useBlockProps = blockEditor.useBlockProps;
	const InspectorControls = blockEditor.InspectorControls;
	const MediaPlaceholder = blockEditor.MediaPlaceholder;
	const { Fragment } = element;

	// const {
	// 	ToggleControl,
	// 	Panel,
	// 	PanelBody,
	// 	PanelRow
	// } = components;

	const {
		cwbGlobals,
		cwbComponents
	} = cwb;

	const {
		cwbAttributes
	} = cwbComponents;


	blocks.registerBlockType( 'cwb/slider-gallery-button', {


		// Block ASSETS
		//-----------------------------

		title: __( 'CWB: Slider Gallery Button', 'CWB' ),
		icon: 'images-alt2',
		category: 'common',
		attributes: {
			sliderId: {
				type: 'number',
				default: false
			},
			cssClasses: {
				type: 'array',
				default: ['btn btn-primary cursor-pointer gallery-slider-modal-trigger'],//['p-0', 'my-0'],
			},
			grid: {
				type: 'object',

				// Set Default as empty string (due to bug in gutenberg)
				// - @ref: https://github.com/WordPress/gutenberg/issues/37967
				default: '',
				// - Use this when bug is fixed:
				// default: JSON.parse(JSON.stringify(defaultScreenSizeObj)), // copies default obj
			},

			tileGap: {
				type: 'object',

				// Set Default as empty string (due to bug in gutenberg)
				// - @ref: https://github.com/WordPress/gutenberg/issues/37967
				default: '',
				// - Use this when bug is fixed:
				// default: JSON.parse(JSON.stringify(defaultScreenSizeObj)), // copies default obj
			},

			images: {
				type: 'array',
				default: ''
			},


			// Button Meta
			//-----------------------------
			text : {
				type: 'string',
				default: 'click here'
			},

			href : {
				type: 'string',
				default: ''
			},
			target: {
				type: 'string',
				default: ''
			},
			dataShortcode : {
				type : 'string',
				default: ''
			},

			openNewTab: {
				type: 'boolean',
				default: false
			},
			dataSource : {
				type: 'string',
				default: ''
			},

			btnSize : {
				type: 'string',
				default: 'default'
			},
			btnType : {
				type: 'string',
				default: 'primary'
			},
			btnFunction : {
				type: 'string',
				default: 'modal_trigger'
			},
			postObj : {
				type: 'object',
				default: ''
			},
			permalink : {
				type: 'string',
				default: ''
			},

			popupHeading : {
				type: 'string',
				default: 'CWB button Popup'
			},
			popupSize : {
				type: 'string',
				default: ''
			},

			hoverScale: {
				type: 'boolean',
				default: ''
			}
		},
		example: {
			attributes: {
				// title: __( 'Chocolate Chip Cookies', 'gutenberg-examples' ),
				// mediaID: 1,
				// mediaURL:
				// 	'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/2ChocolateChipCookies.jpg/320px-2ChocolateChipCookies.jpg',
			},
		},


		// Block FUNCTIONS
		//-----------------------------

		edit: function( props ) {
			let attributes = props.attributes;
			let newCssClasses = [...attributes.cssClasses];
			let blockStyle = {
				// background: attributes.bgColorOutput,
				// border: attributes.borderColor ? attributes.borderWidth+'px solid '+attributes.borderColor : null,
				// color: attributes.textColor ? attributes.textColor : null,


				// // https://stackoverflow.com/questions/28365233/inline-css-styles-in-react-how-to-implement-ahover
				// "&:hover": {
				// 	background: attributes.hoverBgColorOutput,
				// 	border: attributes.hoverBorderColor ? attributes.borderWidth+'px solid '+attributes.hoverBorderColor : null,
				// 	color: attributes.hoverTextColor ? attributes.hoverTextColor : null,
				// },

				// width: '100%',
				// margin: '0px auto',
				// padding: '0px'
			};
			let block_el_tag = 'button';

			let blockProps = {
				className: attributes.cssClasses,
				style: blockStyle,
				['type']: 'button',
			};





			// Set Slider ID upon Block instantiation
			if (!attributes.sliderId){
				props.setAttributes({
					sliderId: Math.floor(Math.random() * 999999)
				});
				attributes = props.attributes
			}
			if (attributes.grid === ''){
				props.setAttributes({
					grid: {
						...defaultScreenSizeObj, base: 'width-2'
					}
				});
				attributes = props.attributes
			}


			let gridCssClasses = genResponsiveClasses(attributes.grid);

			// let gridCssClasses = 'width-2';

			if(attributes.tileGap === '') props.setAttributes({
				tileGap: {...defaultScreenSizeObj, base: '1'},
			});
			let tileGap = genTileGapCss(attributes.tileGap);
			let gapMarginCss = tileGap.gapMarginCss;
			let gapPaddingCss = tileGap.gapPaddingCss;

			// console.log('gridCssClasses', gridCssClasses);











			// Conditional Block Properties
			// - based on attributes.btnFunction value
			//-----------------------------------------

			// basic link button conditionals
			if(attributes.btnFunction === 'link') {
				blockProps._href = attributes.href !== '' ? attributes.href : null;
				blockProps['type'] = null;
				block_el_tag = 'a';
				blockProps._target = attributes.openNewTab ? '_blank' : null;
			}

			// WP link button conditionals
			if(attributes.btnFunction === 'wp_link') {
				blockProps._href = attributes.postObj !== '' ? attributes.postObj.url : null;
				blockProps['type'] = null;
				block_el_tag = 'a';
				if(typeof attributes.postObj !== ''){
					let opensInNewTab = attributes.postObj.opensInNewTab;
					opensInNewTab = typeof opensInNewTab !== 'undefined' ? opensInNewTab : false;
					blockProps._target = opensInNewTab ? '_blank' : null;
				};
			}

			// modal trigger (common) conditionals
			if(	attributes.btnFunction === 'modal_trigger' ||
				attributes.btnFunction === 'shortcode_modal_trigger' ) {

				blockProps['data-toggle'] = 'modal';
				// below requires ez-core v3.0.12
				blockProps['data-modal-size'] = attributes.popupSize !== '' ? attributes.popupSize : null;
				blockProps['data-modal-heading'] = attributes.popupHeading !== '' ? attributes.popupHeading : null;
			}

			// iframe modal trigger conditionals
			if(attributes.btnFunction === 'modal_trigger'){
				blockProps['data-target'] = '#iframe-modal';
				blockProps['data-src'] = attributes.dataSource !== '' ? attributes.dataSource : null;
			}
			// shortcode modal conditionals
			if(attributes.btnFunction === 'shortcode_modal_trigger'){
				blockProps['data-target'] = '#shortcode-ajax-modal';
				// below requires ez-core v3.0.12
				blockProps['data-shortcode'] = attributes.dataShortcode !== '' ? JSON.stringify(attributes.dataShortcode) : null;
			}

			// console.log('blockProps', blockProps.className, useBlockProps( blockProps ).className);

			// console.log('init', attributes.dataShortcode);

			// RETURN: Layout-Block (BACK-end)
			//================================
			return el( 'div', {
					className: 'd-inline-block',
					style: {
						margin: '0 2px'
					} },

				// The Button
				//-----------------------------
				el( block_el_tag, useBlockProps( blockProps ),
					attributes.text
				),




				// InspectorControls
				//================================
				el( InspectorControls, {
						key: 'controls' },

					el(CwbButtonEditor,{
						allowedBtnTypes  : [
							// 'link',
							// 'wp_link',
							'modal_trigger',
							// 'iframe',
							// 'shortcode_modal_trigger'
						],
						btnObj : {
							text			: attributes.text,
							// url 			: attributes.href,
							btnFunction 	: attributes.btnFunction,
							target			: attributes.target, // todo should be dataTarget because thats how CwbButton uses it
							// newTab 			: attributes.openNewTab,
							// // dataToggle 	: attributes.dataToggle,
							// dataShortcode 	: attributes.dataShortcode,
							dataModalHeading: attributes.popupHeading,
							dataModalSize 	: attributes.popupSize,
							// dataSrc 		: attributes.dataSource,
							// postObj 		: attributes.postObj,
							btnType 		: attributes.btnType,
							btnSize 		: attributes.btnSize,
							hoverScale 		: attributes.hoverScale,

							// permalink 		: attributes.permalink, // not part of default btnObj
						},
						updateText : function(value){
							props.setAttributes( {
								text: value
							} );
							attributes = props.attribute
						},
						updateUrl : function(value){
							props.setAttributes( {
								href: value
							} );
							attributes = props.attribute
						},
						updateNewTab : function(value){
							props.setAttributes( {
								openNewTab: value
							} );
							attributes = props.attribute
						},
						setButtonFunction : function(value){
							props.setAttributes( {
								btnFunction: value
							} );
							attributes = props.attribute;
						},
						updateDataSrc : function(value){
							props.setAttributes( {
								dataSource: value
							} );
							attributes = props.attributes;
						},
						updateDataTarget : function(value){
							props.setAttributes( {
								target: value
							} );
							attributes = props.attributes;
						},
						updateDataToggle : function(value){
							// props.setAttributes( {
							// 	dataToggle: value
							// } );
							// attributes = props.attributes;
						},
						updateShortcode : function(value){
							props.setAttributes( {
								dataShortcode: value
							} );
							attributes = props.attributes;
						},
						updateModalHeading : function(value){
							props.setAttributes( {
								popupHeading: value
							} );
							attributes = props.attributes;
						},
						updateModalSize : function(value){
							props.setAttributes( {
								popupSize: value
							} );
							attributes = props.attributes;
						},
						updatePostObj : function(value){
							props.setAttributes( {
								postObj: value
							} );
							attributes = props.attributes;
							// console.log('LinkControl', attributes.postObj);
						},




						// todo  btn styles
						updateBtnType : function(value){
							let classArr = newCssClasses.join(' ').split(' ');

							arrRemoveStrings(classArr, [
								'btn-primary',
								'btn-secondary',
								'btn-tertiary',
								'btn-alt',
								'btn-alt-lite']);
							if(value !== 'default'){
								classArr.push('btn-'+value);
							}

							props.setAttributes( {
								cssClasses: classArr,
								btnType: value
							} );
							attributes = props.attributes;
						},
						updateBtnSize : function(value){
							let classArr = newCssClasses.join(' ').split(' ');

							arrRemoveStrings(classArr, [
								'btn-sm',
								'btn-lg'
							]);
							if(value !== 'default'){
								classArr.push('btn-'+value);
							}

							props.setAttributes( {
								cssClasses: classArr,
								btnSize: value
							} );
							attributes = props.attributes;
						},
						updateHoverScale : function(value){
							let classArr = newCssClasses.join(' ').split(' ');

							arrRemoveStrings(classArr, [
								'btn-hover-scale'
							]);
							if(!attributes.hoverScale === true){
								classArr.push('btn-hover-scale');
							}

							props.setAttributes( {
								cssClasses: classArr,
								hoverScale: value//!attributes.hoverScale,
							} );
							attributes = props.attributes;
						},
					}),

					el('div', {className: 'py-2'},

						el('p', {className: 'h4', style: {padding: '0 20px'}}, 'Gallery Slider Images'),

						// Gallery Editor Block
						//================================
						el(CwGalleryEditor, {
							images: attributes.images,
							gridCssClasses 	: gridCssClasses,
							gapMarginCss 	: gapMarginCss,
							gapPaddingCss 	: gapPaddingCss,
							onSelect: function (images) {
								// console.log(images);
								props.setAttributes({ images : images });
								attributes = props.attributes;
							},
						})
					),
				)
			);
		},

		save: function( props ) {
			let block_el_tag = 'button';
			let attributes = props.attributes;
			let blockStyle = {
				// background: attributes.bgColorOutput,
				// border: attributes.borderWidth ? attributes.borderWidth+'px solid '+attributes.borderColor : null,
				// color: attributes.textColor ? attributes.textColor : null,

				// margin: '0px',
				// padding: '0px',
				// width: '100%',
			};
			let modalId = 'image-slider-'+ attributes.sliderId;

			// Conditional Block Properties
			// - based on attributes.btnFunction value
			let blockProps = {
				className: attributes.cssClasses.join(' '),
				style: blockStyle,
				// ['data-bez']: 'bez', // toggle this to force a block markup refresh
				['type']: 'button',
			};
			let btnObj = {
				text			: attributes.text,
				url 			: attributes.href,
				btnFunction 	: attributes.btnFunction,
				target			: "#"+modalId,
				newTab 			: attributes.openNewTab,
				// dataToggle 	: attributes.dataToggle,
				dataShortcode 	: attributes.dataShortcode,
				dataModalHeading: attributes.popupHeading,
				dataModalSize 	: attributes.popupSize,
				dataSrc 		: attributes.dataSource,
				postObj 		: attributes.postObj,
				btnType 		: attributes.btnType,
				btnSize 		: attributes.btnSize,
				hoverScale 		: attributes.hoverScale,

				dataId			: '1',

				// permalink 		: attributes.permalink, // not part of default btnObj
			};


			// console.log(attributes.cssClasses);

			//
			// blockProps = useBlockProps.save( {
			// 	className: attributes.cssClasses.join(' '),
			// 	// style: blockStyle,
			// });



			// Parse Header Object from data-modal-heading attribute // TODO DRY (1)
			//--------------------------------------------
			let headerObj = {
				'cssClass':'',
				'content': attributes.popupHeading
			};
			let allowedModClasses = [
				'f-black',
				'f-white',
				'no-heading',
				'text-white',
			];
			//input: {header-text}//{extra-modal-css-classes}
			let inputArr = attributes.popupHeading.split('//');
			if(inputArr.length === 2){
				let filteredClasses = [];
				let cssClasses = inputArr[1].split(' ');
				for(let x = 0; x < cssClasses.length; x++){
					if(allowedModClasses.includes(cssClasses[x])){
						filteredClasses.push(cssClasses[x]);
					}
				}
				headerObj.cssClass = filteredClasses.join(' ');
				headerObj.content = inputArr[0];
			}

			// console.log(attributes.popupHeading, inputArr, headerObj.cssClass);

			// // Modify modal according to headerObj // TODO DRY (2)
			// //--------------------------------------------
			// if(typeof attributes.popupHeading !== 'undefined') {
			// 	// modal_dialog.addClass(headerObj.cssClass);
			// 	// $("#wp-content-modal .modal-title").html(headerObj.content);
			// }

			return el('div', { className: 'd-inline-block'} ,
					el( CwbButton, {
					btnObj : btnObj,
					extraBtnCss : setBtnCssClasses(btnObj, blockProps.className),
					btnType : ''
				}),



				// JQuery Image Slider Modal
				//-----------------------------
				el(BootstrapModal, {
						modalId: modalId,
						modalHeading: (() => {
							return el('span', {className: headerObj.cssClass}, headerObj.content)
						})(),
						modalSize: 'fs'
					},
					el(CwImageSlider, {
						images: attributes.images,
						sliderId: typeof attributes.sliderId !== 'undefined' ?
							attributes.sliderId : null,
						lazyLoad: true,
						slideCaptions: true,
						sliderCaptionMarkup: function(item){
							return el( 'div', {
									className: 'slider-caption',
									['data-id']: item.key+1
								},	el('p', {},)
							)
						}
					})
				)
			)
		},
	} );
}(
	window.wp.blocks,
	window.wp.editor,
	window.wp.i18n,
	window.wp.element,
	window.wp.components,
	window._,
	window.wp.blockEditor,
	window.cwb
) );