/*
 * name: CWB Globals
 * handle: 'cwb-globals'
 *
 * Must Run Before 'cwb-components' && 'cwb'
 */


// console.log(editorGradientPresets[0]);

$palette_extras = [

	{ name: 'grey-90', color: '#1a1a1a' },
	{ name: 'grey-75', color: '#404040' },
	// { name: 'grey-60', color: '#575757' },
	{ name: 'grey-50', color: '#808080' },
	// { name: 'grey-40', color: '#9c9c9c' },
	{ name: 'grey-25', color: '#c0c0c0' },
	{ name: 'grey-10', color: '#e5e5e5' },

	{ name: 'black-90', color: 'rgba(0,0,0,0.9)' },
	{ name: 'black-75', color: 'rgba(0,0,0,0.75)' },
	// { name: 'black-60', color: 'rgba(0,0,0,0.6)' },
	{ name: 'black-50', color: 'rgba(0,0,0,0.5)' },
	// { name: 'black-40', color: 'rgba(0,0,0,0.4)' },
	{ name: 'black-25', color: 'rgba(0,0,0,0.25)' },
	{ name: 'black-10', color: 'rgba(0,0,0,0.1)' },

	{ name: 'white-90', color: 'rgba(255,255,255,0.9)' },
	{ name: 'white-75', color: 'rgba(255,255,255,0.75)' },
	// { name: 'white-60', color: 'rgba(255,255,255,0.6)' },
	{ name: 'white-50', color: 'rgba(255,255,255,0.5)' },
	// { name: 'white-40', color: 'rgba(255,255,255,0.4)' },
	{ name: 'white-25', color: 'rgba(255,255,255,0.25)' },
	{ name: 'white-10', color: 'rgba(255,255,255,0.1)' },
];


window.cwbGlobals = {
	screenSizeTabs : [
		{ name: 'base', className: 'tab-base', title: 'Base' },
		// { name: 'xs', title: 'XS', className: 'tab-xs' },
		{ name: 'sm', className: 'tab-sm', title: 'sm' },
		{ name: 'md', className: 'tab-md', title: 'md ' },
		{ name: 'lg', className: 'tab-lg', title: 'lg' },
		{ name: 'xl', className: 'tab-xl', title: 'xl' }
	],
	defaultScreenSizeObj : {
		// xs: false,
		sm: '',
		md: '',
		lg: '',
		xl: '',
	},
	innerPanelClasses : 'bg-grey-2 b-grey-10 b-solid border-1',
	// customColorPalette : editorColorPalette[0].concat($palette_extras),

	// requires (php) : add_theme_support( 'editor-color-palette', $editor_color_palette );
	// - can be found in wp-gutenberg-mods.php in ez-core child theme, ez-blank-333-wp-ez-core (since v1.1.8)
	customColorPalette : editorColorPalette !== null ?  editorColorPalette[0] : null,

	// Configured in this file
	colorPaletteExtras : $palette_extras,

	// requires (php) : add_theme_support( 'editor-gradient-palette', $gradients_arr );
	// - can be found in wp-gutenberg-mods.php in ez-core child theme, ez-blank-333-wp-ez-core (since v1.1.8)
	customGradientPresets : editorGradientPresets !== null ?  editorGradientPresets[0] : null
};


// // customize existing/native wp-blocks:
// //-----------------------------------------
// // * https://developer.wordpress.org/block-editor/reference-guides/filters/block-filters/
// // ~ https://www.liip.ch/en/blog/how-to-extend-existing-gutenberg-blocks-in-wordpress
// const { createHigherOrderComponent } = wp.compose;
// const { Fragment } = wp.element;
// const { InspectorControls } = wp.editor;
// const { PanelBody, SelectControl } = wp.components;
// const el = wp.element.createElement;
//
// const withInspectorControls = createHigherOrderComponent( function (
//     BlockEdit
//     ) {
//         return function ( props ) {
//             return el( Fragment,  {},
//                 el( BlockEdit, props ),
//                     el( InspectorControls, {},
//                         el( PanelBody, {}, 'My custom control' )
//                 )
//             );
//         };
//     },
//     'withInspectorControls' );
//
// wp.hooks.addFilter(
//     'editor.BlockEdit',
//     // 'my-plugin/with-inspector-controls',
//     'cwb/content-wrap',
//     withInspectorControls
// );


// now try custom components


