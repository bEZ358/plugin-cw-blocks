( function( blocks, editor, i18n, element, components, _, blockEditor, cwb ) {
	const __ = i18n.__;
	const el = element.createElement;
	const InnerBlocks = blockEditor.InnerBlocks;
	const useState = element.useState;
	const useBlockProps = blockEditor.useBlockProps;
	const InspectorControls = blockEditor.InspectorControls;
	const LinkControl = blockEditor.__experimentalLinkControl;

	const useSelect = wp.data.useSelect;

	const {
		Dashicon,
		Toolbar,
		ToolbarButton,
		// ToggleControl,
		// Panel,
		// PanelBody,
		// PanelRow
	} = components;

	const {
		cwbGlobals,
		cwbComponents
	} = cwb;

	const {
		screenSizeTabs,
		defaultScreenSizeObj
	} = cwbGlobals;

	const {
		// RepeaterBtn,
		// RepeaterSliderEditBlock,
		// RepeaterBtnEditor,
		// RepeaterControls,
		// RepeaterToolbar,
		// Repeater,
		cwbAttributes
	} = cwbComponents;



	// @ref links below
	// https://mediaron.com/how-to-create-a-repeater-field6-in-gutenberg/
	// https://reactnative.dev/docs/state

	blocks.registerBlockType( 'cwb/iframe-modal-image-block', {


		// Block ASSETS
		//-----------------------------

		title: __( 'iFrame Modal Image Block', 'CWB' ),
		icon: 'embed-photo',
		category: 'common',
		attributes: {
			cssClasses: {
				type: 'array',
				default: ['my-0', 'iframe-trigger', 'cursor-pointer'],//['w-100', 'd-flex', 'm-0', 'align-items-center', 'bg-cover']
			},
			dataSrc: {
				type: 'string',
				default: '#'
			},
			img: {
				type: 'object',
				default: ''
			},
			caption: {
				type: 'string',
				default: ''
			},

		},
		example: {
			attributes: {
				// title: __( 'Chocolate Chip Cookies', 'gutenberg-examples' ),
				// mediaID: 1,
				// mediaURL:
				// 	'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/2ChocolateChipCookies.jpg/320px-2ChocolateChipCookies.jpg',
			},
		},



		// Block FUNCTIONS
		//-----------------------------

		edit: function( props ) {
			let attributes = props.attributes;
			let blockStyle = {
				width: '100%',
				margin: '0px auto',
				padding: '0px',
			};

			if( attributes.img === '' ) attributes.img = false;



			// RETURN: Slider Block Editor
			//================================
			return el( 'div', useBlockProps({
					className: attributes.cssClasses,
					style: blockStyle,
				}),
				el( 'img', {
						style: {
							width: '100%',
							margin: '0px auto',
							padding: '0px',
						},
						src: attributes.img.url,
						// dataToggle: 'modal',
						// dataTarget: '#iframe-modal',
						// dataSrc: ''
					},
				),

				attributes.caption !== '' ?
					el('p', { //className: 'mt-0',
						style: {fontStyle: 'italic'}}, attributes.caption) : null,


				// // RepeaterSliderEditBlock
				// //================================
				// el('img', props,),



				// InspectorControls
				//================================
				el( InspectorControls, {
					key: 'controls' },


					// Slide Content
					//--------------------------------------
					el( PanelBody, {
							title: 'Iframe Modal Image Trigger',
							initialOpen: true//!0,
						},


						// Slide Image
						//--------------------------------------
						el( MediaUpload, {
								onSelect: function(media){
									props.setAttributes({
										img : media
									})
								},
								allowedTypes: 'image',
								value: attributes.img,
								render: function( obj ) {
									// console.log('debug: slide MediaUpload obj', obj);
									return ! attributes.img ?

										// UI: Choose Image (conditional)
										//-----------------------------
										el( 'div', null,
											el( components.Button, {
													className: 'editor-post-featured-image__toggle',
													style: {margin:'10px 0'},
													onClick: obj.open,
												},
												__( 'Choose Image', 'gutenberg-examples' )
											)
										) :

										// UI: Manage BG Image (conditional)
										//-----------------------------
										el( 'div', null,

											// UI: Preview / Replace Image
											//-----------------------------
											el( 'div', {
													className: 'image-button cursor-pointer',
													style: {margin:'10px 0'},
													onClick: obj.open,
												},
												el( 'img', {
													src: attributes.img.url }
												)
											),

											// UI: Remove Image
											//-----------------------------
											el( components.Button, {
													className: 'button-large',
													style: {margin:'0 0 20px'},
													onClick: function(){
														props.setAttributes({
															img : false
														})
													},
													isTertiary: !0,
													// isLink: !0,
													isDestructive: !0,
												},
												__( 'Remove Image', 'gutenberg-examples' )
											),
										)
								}
							}
						),


						// Embed Url
						//--------------------------------------
						el(InspectorText, {...props,
							label: 'Embed Url',
							attName: 'dataSrc'
						}),


						// Caption
						//--------------------------------------
						el(InspectorText, {...props,
							label: 'Caption',
							attName: 'caption'
						}),
					),
				)
			);
		},

		save: function( props ) {
			let attributes = props.attributes;
			let blockStyle = {
				// background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : attributes.bgColorOutput,
				margin: '0px',
				padding: '0px',
				width: '100%',
				// color: attributes.textColor ? attributes.textColor : null,
			};
			let alt = getImageAlt(attributes.img);

			// RETURN: Layout-Block (FRONT-end)
			//================================
			return el( 'div', useBlockProps.save( {
					className: attributes.cssClasses.join(' '),
					style: blockStyle,
					['data-toggle']: 'modal',
					['data-target']: '#iframe-modal',
					['data-src']: attributes.dataSrc
				}),
				el( 'img', {
					// className: attributes.cssClasses.join(' '),
					// style: blockStyle,
					src: attributes.img.url,
					alt: alt ? alt : null
				})
			);
		},
	} );
}(
	window.wp.blocks,
	window.wp.editor,
	window.wp.i18n,
	window.wp.element,
	window.wp.components,
	window._,
	window.wp.blockEditor,
	window.cwb
) );
