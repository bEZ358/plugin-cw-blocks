( function( blocks, editor, i18n, element, components, _, blockEditor, cwb ) {
	const __ = i18n.__;
	const el = element.createElement;
	const useState = element.useState;
	const useEffect = element.useEffect;
	const useBlockProps = blockEditor.useBlockProps;
	const InspectorControls = blockEditor.InspectorControls;
	const LinkControl = blockEditor.__experimentalLinkControl;
	const useEntityProp =  wp.coreData.useEntityProp;
	const useSelect = wp.data.useSelect;


	const {
		Dashicon,
		Toolbar,
		ToolbarButton,
		// ToggleControl,
		// Panel,
		// PanelBody,
		// PanelRow
	} = components;

	const {
		cwbGlobals,
		cwbComponents
	} = cwb;

	const {
		screenSizeTabs,
		defaultScreenSizeObj,
	} = cwbGlobals;

	const {
		// RepeaterBtn,
		// RepeaterSliderEditBlock,
		// RepeaterBtnEditor,
		// RepeaterControls,
		// RepeaterToolbar,
		// Repeater,
		cwbAttributes
	} = cwbComponents;

	const QUERY_DEFAULTS = {
		// context: false,
		// page: false,
		per_page: -1,
		// search: false,
		// after: false,
		// author: false,
		// author_exclude: false,
		// before: false,
		exclude: '0',
		// include: '0', // ~ TODO: this one breaks things for some reason BUT would be useful if it worked
		// offset: false,
		order: 'desc',
		orderby: 'published',
		// slug: false,
		// status: false,
		// categories: false,
		// categories_exclude: false,
		// tags: false,
		// tags_exclude: false,
		// sticky: false,
	};


	// @ref links below
	// https://mediaron.com/how-to-create-a-repeater-field6-in-gutenberg/
	// https://reactnative.dev/docs/state

	blocks.registerBlockType( 'cwb/posts-block', {


		// Block ASSETS
		//-----------------------------

		title: __( 'CWB: Posts Block', 'CWB' ),
		icon: 'slides', // see: /wp-includes/css/dashicons.css
		category: 'common',
		attributes: {
			cssClasses: {
				type: 'array',
				default: ['my-0'],//['w-100', 'd-flex', 'm-0', 'align-items-center', 'bg-cover']
			},

			// // Common Attribute Sets
			// //-----------------------------
			// ...cwbAttributes.__blockColors,
			// ...cwbAttributes.__bgImage,
			// // ...cwbAttributes.__blockBorder,

			// Wrap Settings
			//-----------------------------
			blockHeight: cwbAttributes.blockHeight,
			// vAlign: cwbAttributes.vAlign,


			query: {
				type: 'object',
				default: ''
			},

			postType: {
				type: 'string',
				default: 'post'
			},

			// tileWidthCss: {
			// 	type: 'string',
			// 	default: 'width-3'
			// },

			records: {
				type: 'array',
				default: ''
			},

			grid: {
				type: 'object',

				// Set Default as empty string (due to bug in gutenberg)
				// - @ref: https://github.com/WordPress/gutenberg/issues/37967
				default: '',
				// - Use this when bug is fixed:
				// default: JSON.parse(JSON.stringify(defaultScreenSizeObj)), // copies default obj
			},

			tileGap: {
				type: 'object',

				// Set Default as empty string (due to bug in gutenberg)
				// - @ref: https://github.com/WordPress/gutenberg/issues/37967
				default: '',
				// - Use this when bug is fixed:
				// default: JSON.parse(JSON.stringify(defaultScreenSizeObj)), // copies default obj
			},

			tileClassName: {
				type: 'string',
				default: 'post'
			},

			contentClassName: {
				type: 'string',
				default: 'post'
			},


		},
		example: {
			attributes: {
				// title: __( 'Chocolate Chip Cookies', 'gutenberg-examples' ),
				// mediaID: 1,
				// mediaURL:
				// 	'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/2ChocolateChipCookies.jpg/320px-2ChocolateChipCookies.jpg',
			},
		},

		// Block FUNCTIONS
		//-----------------------------

		edit: function( props ) {

			let attributes = props.attributes;
			let blockStyle = {
				width: '100%',
				margin: '0px auto',
				padding: '0px',
			};

			// Grid Layout
			//--------------------------------------
			const [gridTab, setGridTab] = useState(false);

			if(attributes.grid === '') props.setAttributes({
				grid: {...defaultScreenSizeObj, base: 'width-2', md: 'width-md-3'},
			});
			let gridCssClasses = genResponsiveClasses(attributes.grid);



			if(attributes.tileGap === '') props.setAttributes({
				tileGap: {...defaultScreenSizeObj, base: '2', md: '3'},
			});
			let tileGap = genTileGapCss(attributes.tileGap);
			let gapMarginCss = tileGap.gapMarginCss;
			let gapPaddingCss = tileGap.gapPaddingCss;




			// WP Query
			//--------------------------------------
			function updateQuery(value, queryArg){

				let newQuery = {...attributes.query};

				if(typeof newQuery[queryArg] !== 'undefined')
					newQuery[queryArg] = value;

				newQuery.order = 'desc';
				newQuery.orderby = 'date';


				props.setAttributes({
					query: newQuery
				});

				attributes = props.attributes;
			}
			if(attributes.query === '') props.setAttributes({
				query: QUERY_DEFAULTS,
			});



			// Dynamic WP Content
			//--------------------------------------
			const records = useSelect((select) => {
				return wp.data.select('core').getEntityRecords('postType', attributes.postType, attributes.query);
			});

			useEffect(() => {
				if(records){
					props.setAttributes({
						records: records
					});
					attributes = props.attributes;
				}
			}, [records]);




			// RETURN: Posts Block Editor
			//================================
			return el( 'div', useBlockProps( {
					className: attributes.cssClasses,
					style: blockStyle
				} ),


				el('div',{ className: 'container px-0'},
					el('div',{ className: 'row '+gapMarginCss},
						el('div', { className: 'col py-3 justify-content-center d-flex flex-wrap'},

							records ? records.map((post) => {

								let imgObj = typeof post.featured_media_obj !== 'undefined' ? post.featured_media_obj : false;
								let img_src = imgObj ? getImageSource(imgObj) : false;
								let read_more_text = typeof post.acf.read_more_text !== 'undefined' ? post.acf.read_more_text : false;
								let external_link = typeof post.acf.external_link !== 'undefined' ? post.acf.external_link : false;
								let excerpt = typeof post.excerpt !== 'undefined' ?
									post.excerpt.raw !== '' ? post.excerpt.raw : removeTags(post.excerpt.rendered) :
									false;
								let linkProps = {
									href: external_link ? external_link : post.link,
									target: external_link ? '_blank' : null,
								};





								return el( 'div',{ className: 'd-inline-block '+gapPaddingCss+' '+ gridCssClasses},
									el('div', {className: 'link-style d-inline-block text-left '+ attributes.tileClassName,
											...linkProps
										},

										img_src ?
											el('img',{src:img_src}) :
											el('p',{}, post.featured_media !== 'undefined' && post.featured_media !== 0 ?
												'Loading image...' : 'No image source'),

										el('div', {className: 'item-content '+ attributes.contentClassName},
											el('div', { className: 'h4 w-100',
											}, post.title.raw),

											excerpt ? el('div', { className: 'w-100 mt-3 f-black d-inline-block'},
												excerpt
											) : null,

											read_more_text ? el('div', {className: 'w-100 mt-3 text-right'},
												el('div', {
														className: 'read-more d-inline-block',
													},
													read_more_text
												)
											) : null
										)
									)
								);
							}) : el('p',{ className: 'mt-5'}, 'waiting for query results; post-type: '+ attributes.postType )
						),
					),
				),




				// InspectorControls
				//================================
				el( InspectorControls, {
						key: 'controls' },
					el( 'div', {},
						el( PanelBody, {
								style: {
									padding: '0 20px'
								},
								title: 'Query',
								initialOpen: false//!0,
							},


							// Posts Per Page
							//-------------------------
							el(NumberControl, {
								label: 'Posts Per Page',
								className: 'mt-0 mb-2 f-sz-14',
								value: attributes.query.per_page,
								// isShiftStepEnabled: true,
								// shiftStep : 1,
								max: 12,
								min: -1,
								onChange: function (value) {
									updateQuery(value, 'per_page');
								},
							}),



							// order: 'asc',
							// - select
							// orderby: 'title',
							// - select


							// Post Type
							//-----------------------------
							el( SelectControl, {
								key: 'multiple',
								label: __('Post Type'),
								value: attributes.postType,
								className: ['mt-3'],
								onChange: function(value){
									props.setAttributes({
										postType: value,
									});
									attributes = props.attributes;
								},
								// TODO: query available post types instead (move to child theme; disable in cw-blocks for now)
								// 	- try to query in index.php and save (localize?) as js variable
								options: [
									{ value: 'post', label: 'Post'},
									{ value: 'patient_story', label: 'Patient Story'},
									// { value: '', label: ''},
								]
							}),



							// // Include Posts
							// // TODO this breaks this BUT would NICE if it worked
							// //-------------------------
							// el(InspectorTextInput, {...props,
							// 	label: __('Include posts'),
							// 	currentValue: attributes.query.include ? attributes.query.include : '',
							// 	onChange: function(value){
							// 		updateQuery(value, 'include');
							// 	},
							// }),


							// Exclude Posts
							//-------------------------
							el(InspectorTextInput, {...props,
								label: __('Exclude posts'),
								currentValue: attributes.query.exclude ? attributes.query.exclude : '',
								onChange: function(value){
									updateQuery(value, 'exclude');
								},
							}),
						)
					),

					el( 'div', {},

						// Grid Layout Configs
						//-------------------------
						el( PanelBody, {
								style: {
									padding: '0 20px'
								},
								title: 'Grid Configs',
								initialOpen: false//!0,
							},

							el(ResponsiveTabs, props,
								(gridTab) => {
									setGridTab(gridTab);
									props = {
										tab: {...gridTab},
										...props,
										gridCssClasses: gridCssClasses,
										onUpdateLayout: function(attrStr, responsiveCssObj){
											// console.log('gridTab', attrStr, responsiveCssObj);
										}
									};
									return el(GridSettingsTab, props);
								}
							),
							// tileGap
						),

						// Css Classes
						//-------------------------
						el( PanelBody, {
								style: {
									padding: '0 20px'
								},
								title: 'Css Classes',
								initialOpen: false//!0,
							},

							el(InspectorTextInput, {...props,
								label: __('Tile Css Classes'),
								currentValue: attributes.tileClassName,
								onChange: function(value){
									props.setAttributes({
										tileClassName: value
									});
									attributes = props.attributes;
								},
							}),

							el(InspectorTextInput, {...props,
								label: __('Content Wrap Css Classes'),
								currentValue: attributes.contentClassName,
								onChange: function(value){
									props.setAttributes({
										contentClassName: value
									});
									attributes = props.attributes;
								},
							}),
						)
					),
				)
			);
		},

		save: function( props ) {
			let self = this;
			let attributes = props.attributes;
			let blockStyle = {
				// background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : attributes.bgColorOutput,
				margin: '0px',
				padding: '0px',
				width: '100%',
				// color: attributes.textColor ? attributes.textColor : null,
			};
			let gridCssClasses = genResponsiveClasses(attributes.grid);
			let tileGap = genTileGapCss(attributes.tileGap);
			let gapMarginCss = tileGap.gapMarginCss;
			let gapPaddingCss = tileGap.gapPaddingCss;


			// RETURN: Layout-Block (FRONT-end)
			//================================
			return el( 'div', useBlockProps.save( {
					className: attributes.cssClasses.join(' '),
					style: blockStyle,
					// ['data-last-update']: 230503
				}),


				el('div',{ className: 'container px-0'},
					el('div',{ className: 'row '+gapMarginCss},
						el('div', { className: 'col py-3 justify-content-center text-center d-flex flex-wrap'},

							attributes.records.length ? attributes.records.map((post) => {

								let imgObj = typeof post.featured_media_obj !== 'undefined' ? post.featured_media_obj : false;
								let img_src = imgObj ? getImageSource(imgObj) : false;
								let alt = getImageAlt(imgObj);
								let read_more_text = typeof post.acf.read_more_text !== 'undefined' ? post.acf.read_more_text : false;
								let external_link = typeof post.acf.external_link !== 'undefined' ? post.acf.external_link : false;
								let excerpt = typeof post.excerpt !== 'undefined' ?
									post.excerpt.raw !== '' ? post.excerpt.raw : removeTags(post.excerpt.rendered) :
									false;
								let linkProps = {
									href: external_link ? external_link : post.link,
									target: external_link ? '_blank' : null,
									rel: external_link ? 'noopener' : null,
									['aria-label']: external_link ? 'open in a new tab: '+post.title.raw : null
								};

								return el( 'article',{ className: 'd-inline-block p-2 '+gapPaddingCss+' '+gridCssClasses},
									el('a', {className: 'd-inline-block text-left '+ attributes.tileClassName,
											...linkProps},


										img_src ?
											el('img',{
												src:img_src,
												alt: alt ? alt : null
											}) :
											el('p',{}, post.featured_media !== 'undefined' && post.featured_media !== 0 ?
												'Loading image...' : 'No image source'),

										el('div', {className: 'p-3 item-content '+ attributes.contentClassName},
											el('div', { className: 'h4 w-100',
											}, post.title.raw),

											excerpt ? el('div', { className: 'w-100 mt-3 f-black d-inline-block'},
												excerpt
											) : null,

											read_more_text ? el('div', {className: 'w-100 mt-3 text-right'},
												el('div', {
														className: 'read-more clickable d-inline-block',
														...linkProps
													},
													read_more_text
												)
											) : null
										)
									)
								);
							}) : null
						),
					),
				),
			);
		},
	} );
}(
	window.wp.blocks,
	window.wp.editor,
	window.wp.i18n,
	window.wp.element,
	window.wp.components,
	window._,
	window.wp.blockEditor,
	window.cwb
) );
