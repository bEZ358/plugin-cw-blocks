( function( blocks, editor, i18n, element, components, _, blockEditor, cwb ) {
	const __ = i18n.__;
	const el = element.createElement;
	const InnerBlocks = blockEditor.InnerBlocks;
	const useBlockProps = blockEditor.useBlockProps;
	const RichText 			= blockEditor.RichText;
	const InspectorControls = blockEditor.InspectorControls;
	const RawHTML = element.RawHTML;

	const {
		Dashicon,
		Toolbar,
		ToolbarButton,
		ToolbarDropdownMenu,
		// ToggleControl,
		// Panel,
		// PanelBody,
		// PanelRow
	} = components;

	const {
		cwbGlobals,
		cwbComponents
	} = cwb;

	const {
		// screenSizeTabs,
		// defaultScreenSizeObj
	} = cwbGlobals;

	const {
		RepeaterControls,
		RepeaterToolbar,
		Repeater,
		// BgImagePanel,
		// BlockColors,
		// cwbAttributes
	} = cwbComponents;

	const defaultItemObj = {
		key: 0,
		itemText: 'defaultItemObj',
		subItems: [
			{ tagName: false, content: 'subItem', className: false  }
		]
	};


	// @ref links below
	// https://mediaron.com/how-to-create-a-repeater-field6-in-gutenberg/
	// https://reactnative.dev/docs/state

	blocks.registerBlockType( 'cwb/accordion-block', {


		// Block ASSETS
		//-----------------------------

		title: __( 'Accordion Block', 'CWB' ),
		icon: 'admin-page', // see: /wp-includes/css/dashicons.css
		category: 'common',
		attributes: {
			cssClasses: {
				type: 'array',
				default: ['p-3', 'my-0'],//['w-100', 'd-flex', 'm-0', 'align-items-center', 'bg-cover']
			},

			// // Common Attribute Sets
			// //-----------------------------
			// ...cwbAttributes.__blockColors,
			// ...cwbAttributes.__bgImage,
			// // ...cwbAttributes.__blockBorder,


			// // Repeater Block Settings
			// //-----------------------------
			// fullWidth: {
			// 	type: 'boolean' // for ToggleControl we need boolean type
			// }
			items: {
				type : 'array',
				default: [
					{ ...defaultItemObj, key: 0, itemText: 'item 1'},
					{ ...defaultItemObj, key: 1, itemText: 'item 2'},
					{ ...defaultItemObj, key: 2, itemText: 'item three'},
				]
			},
			focusItem: {
				type: 'integer',
				default: null
			},
			openItem: {
				type: 'integer',
				default: null
			},
			headerTag: {
				type: 'text',
				default: 'p'
			}
		},
		example: {
			attributes: {
				// title: __( 'Chocolate Chip Cookies', 'gutenberg-examples' ),
				// mediaID: 1,
				// mediaURL:
				// 	'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/2ChocolateChipCookies.jpg/320px-2ChocolateChipCookies.jpg',
			},
		},



		// Block FUNCTIONS
		//-----------------------------

		edit: function( props ) {
			let attributes = props.attributes;
			let blockStyle = {
				width: '100%',
				margin: '0px auto',
				padding: '0px',
			};

			// Options
			let ops = [];
			attributes.items.map((item) =>
			{
				ops.push({
					value : item.key,
					label : 'item '+item.key+': '+item.itemText
				});
			});

			// Block DRY Functions
			//--------------------------------------

			const updateRepeaterItems = function(newArr){
				props.setAttributes({
					items : newArr
				});
				attributes = props.attributes;
			};

			const setItemAttr = function(value, focusItem, property){

				let newArr = [...attributes.items];

				// update item
				newArr[focusItem.key][property] = value;

				//
				// update block
				updateRepeaterItems(newArr);
			};



			// used for WYSIWYG edits rather than edits via inspectorPanel
			// - called when updating text todo see if there's a way to pull this from RepeaterComponent
			const updateFocus = function(item){
				props.setAttributes({
					focusItem : item//.key.toString()
				});
				attributes = props.attributes;
			};



			// Content Modifiers
			//--------------------------------------

			const updateText = function(value, item){
				setItemAttr(value, item, 'itemText');
			};

			// TODO headerTag
			// TODO allowedTags



			const updateRepeater = function(newArr, focusItem) {

				// reset item keys
				for(let i = 0; i < newArr.length; i++){
					newArr[i].key = i;
				}
				// update block
				props.setAttributes({
					items : newArr,
					focusItem : focusItem.toString()
				});
				attributes = props.attributes;
			};



			let focusItem = attributes.items[ attributes.focusItem !== null ? attributes.focusItem : 0 ];
			focusItem = fixFocusItem(focusItem, attributes);
			let subitemKey = -1;


			// RETURN Markup
			//================================
			return el( 'div', useBlockProps( {
					className: attributes.cssClasses,
					style: blockStyle
				} ),

				// Container Wrap
				//-----------------------------
				el( 'div', {
						className: attributes.fullWidth ? '' : 'container px-0 accordion-block'
					},

					el(SimpleRepeaterBlock, {
							// ...props,
							blockProps: props,
							itemObjModel : defaultItemObj,
						},
						(item, toolbar) => {

							let theSubItems = attributes.items[item.key].subItems;


							// ACCORDION
							//-----------------------
							return  el('div', { className: 'row',
								onClick: function(){
									updateFocus(item.key.toString());
								}},
								el('div', { className: 'col-12 accordion'+(item.key === focusItem.key ? ' -open' : '') },


									// Accordion HEADER
									//-----------------------
									el('div', { className: 'row accordion-header'},
										el('div', { className: 'col _col-12 p-3'},
											el('div', {
													style: {
														position: 'relative'
													}
												},
												el( RichText,{
														style: {
															fontWeight: '500'
														},
														tagName: 'p', // TODO headerTag
														value: item.itemText,
														onChange: function(value){
															updateText(value, item);
														},
														onFocus: function(){
															updateFocus(item.key.toString());
														}
													}
												)
											),
											toolbar
										),
										el('div', { className: 'col max-w-50 p-3'},
											el('div', { className: 'accordion-icon'},)
										)
									),

									// Accordion WRAPPER, CONTENT
									//-----------------------------
									item.key === focusItem.key ? el('div', { className: 'row accordion-wrapper'},
										el('div', { className: 'col col-12 accordion-content'},

											typeof theSubItems !== 'undefined' ? theSubItems.map((subItem) => {

												subitemKey++;

												let subKey = subitemKey;


												return el('div', { className: 'position-relative w-100'},

													// Item Toolbar
													//---------------------------------------------
													el( Toolbar,{
															className: 'd-flex-on-parent-hover' ,
															style: {
																padding: 0,
																border: '1px solid #bbb',
																position: 'absolute',
																top: '-50px',
																right: '0',
															}
														},


														// List Context Editor : move/delete items
														//------------------------------------------
														el( ToolbarButton,{
																onClick: function(){

																	// console.log('wtf', attributes.items[item.key]);
																	let subItems = [...attributes.items[item.key].subItems];

																	subKey = parseInt(subKey);
																	let pulled = subItems.splice(subKey, 1);
																	let targetIndex = subKey - 1 >= 0 ? subKey - 1 : 0;


																	// console.log('up', targetIndex, subItems, pulled[0]);

																	subItems.splice(targetIndex, 0, pulled[0]);
																	setItemAttr(subItems, item,'subItems');
																}
															}, el( Dashicon, { icon : 'arrow-up-alt2'} ),
														),
														el( ToolbarButton,{
																onClick: function(){
																	// console.log('wtf', subKey);

																	let subItems = [...attributes.items[item.key].subItems];

																	subKey = parseInt(subKey);
																	let pulled = subItems.splice(subKey, 1);
																	let targetIndex = subKey + 1 <= subItems.length ? subKey + 1 : subItems.length;

																	// console.log('down', targetIndex, subItems, pulled[0]);

																	subItems.splice(targetIndex, 0, pulled[0]);
																	setItemAttr(subItems, item,'subItems');
																}
															}, el( Dashicon, { icon : 'arrow-down-alt2'} ),
														),
														el( ToolbarButton,{
																onClick	: function(){

																	// self.removeItem(focusItemKey);

																	let subItems = [...item.subItems];
																	console.log('splice::'+subKey, subItems, item.subItems);
																	subItems.splice(subKey, 1);

																	console.log('splice::'+subKey, subItems, item.subItems);

																	setItemAttr(subItems, item,'subItems');
																}
															}, el( Dashicon, { icon : 'trash'} )
														),



														// Item Markup Editor : tagName & className
														//---------------------------------------------
														el( ToolbarButton,{
																className: 'position-relative',
															},
															el(SelectControl,{
																value: subItem.tagName ? subItem.tagName : 'p',
																options: [
																	{value: 'p', label: 'p'},

																	// todo: allowedTags
																	{value: 'h1', label: 'H1'},
																	{value: 'h2', label: 'H2'},
																	{value: 'h3', label: 'H3'},
																	{value: 'h4', label: 'H4'},
																	{value: 'h5', label: 'H5'},
																	{value: 'h6', label: 'H6'},
																],
																onChange: function(value){

																	let subItems = [...item.subItems];
																	subItems[subKey].tagName = value;
																	console.log(subItems[subKey]);

																	setItemAttr(subItems, item,'subItems');
																}
															}),

															el('label', {className: 'mx-3'}, 'css:'),

															el( TextControl,
																{
																	// label: 'CSS Classes:',
																	value: subItem.className ? subItem.className : '',
																	placeholder: 'CSS Classes',
																	onChange: function(value){

																		let subItems = [...item.subItems];
																		subItems[subKey].className = value;
																		console.log(subItems[subKey]);

																		setItemAttr(subItems, item,'subItems');
																	}
																}
															),
														),

													),


													// Item Content Editor : the text
													//---------------------------------------------
													el(RichText,{
														className: subItem.className ? subItem.className+' mb-3' : 'mb-3',
														tagName: subItem.tagName ? subItem.tagName : 'p',
														value: subItem.content,
														onChange: function(value){

															let subItems = [...item.subItems];
															subItems[subKey].content = value;

															setItemAttr(subItems, item,'subItems');

														},
														onFocus: function(){
															updateFocus(item.key.toString());
														}
													}),
												)

											}) : null,


											el('div', { className: 'p-2 cursor-pointer d-inline-block',
													onClick: function(){

														let subItems = [...item.subItems];
														let newItem = { tagName: false, content: 'subItem', className: false };
														subItems.push(newItem);

														setItemAttr(subItems, item,'subItems');
													}
												},
												el( Dashicon, {icon : 'plus'} )
												// '+ sub-item'
											),
										),
									): null,
								)
							)
						}
					)
				),




				// InspectorControls
				//================================
				el( InspectorControls, {
						key: 'controls' },

					el('div', {
							style: {
								padding: '0 20px'
							},
						},
						el(SelectControl,{
							label: 'accordion header tag',
							value: attributes.headerTag ? attributes.headerTag : 'p',
							options: [
								{value: 'p', label: 'p'},

								// todo: allowedTags
								// {value: 'h1', label: 'H1'},
								{value: 'h2', label: 'H2'},
								{value: 'h3', label: 'H3'},
								{value: 'h4', label: 'H4'},
								{value: 'h5', label: 'H5'},
								{value: 'h6', label: 'H6'},
							],
							onChange: function(value){

								props.setAttributes({
									headerTag : value
								});
								attributes = props.attributes;
							}
						}),
					),

					el(RepeaterControls, {
							// ...props,
							blockProps: props,
							itemObjModel : defaultItemObj,
						},

						el( 'div', { style: {
									padding: '0 20px'
								}},
							// Item Controls:
							el( TextControl,
								{
									label: 'item text',
									value: focusItem.itemText,
									onChange: function(){
										updateText(item);
									}
								}
							),


							// Open on load
							/*--------------------------------------*/
							el( ToggleControl, {
									label: 'Open this item on page load (all other items will be closed).',
									onChange: function(value){

										if(value)
											props.setAttributes({
												openItem : focusItem.key
											})
									},
									checked: attributes.openItem === focusItem.key,
								}
							),
						)

					),
				)
			);
		},

		save: function( props ) {
			let attributes = props.attributes;
			let blockStyle = {
				// background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : attributes.bgColorOutput,
				margin: '0px',
				padding: '0px',
				width: '100%',
				// color: attributes.textColor ? attributes.textColor : null,
			};

			// RETURN: Layout-Block (FRONT-end)
			//================================
			return el( 'div', useBlockProps.save( {
					className: attributes.cssClasses.join(' '),
					style: blockStyle
				} ),

				// Container Wrap
				//-----------------------------
				el( 'div', {
						['data-updated']: '230821', // toggle this to force a block markup refresh
						className: 'container px-0 accordion-block',
						role: 'tablist',
						['aria-multiselectable']:false
					},


					attributes.items.map((item) => {

						let theSubItems = attributes.items[item.key].subItems,
							openFlag = attributes.openItem === item.key ? ' -open' : '';

						// ACCORDION
						//-----------------------------
						return  el('div', { className: 'row', role: 'tab'},
							el('div', { className: 'col-12 accordion'+openFlag},



								// Accordion HEADER
								//-----------------------------
								el('div', { className: 'row accordion-header-wrap'},
									el('button', { className: 'row accordion-header no-btn-style w-100 text-left mx-0', tabindex: '0'},
										el('div', { className: 'col _col-12 p-3'},
											el('div', {
													style: {
														position: 'relative'
													}
												},
												el( attributes.headerTag,{
														style: {
															fontWeight: '500'
														},
													}, item.itemText
												)
											)
										),
										el('div', { className: 'col max-w-50 p-3'},
											el('div', { className: 'accordion-icon'},)
										)
									),
								),




								// Accordion WRAPPER, CONTENT
								//-----------------------------
								el('div', { className: 'row accordion-wrapper'},
									el('div', { className: '_col-12 p-3 py-4 accordion-content'}, // .col-* breaks accordion resize

										typeof theSubItems !== 'undefined' ? theSubItems.map((subItem) => {

											let tag = subItem.tagName ? subItem.tagName : 'p';
											return el('div', { className: 'position-relative w-100'},

												el( tag ,{
													className: subItem.className ? subItem.className+' mb-3' : 'mb-3',
												},
													el(RawHTML, {}, subItem.content)
												),
											)
										}) : null,

									),
								)
							)
						)}
					)
				)
			);
		},
	} );
}(
	window.wp.blocks,
	window.wp.editor,
	window.wp.i18n,
	window.wp.element,
	window.wp.components,
	window._,
	window.wp.blockEditor,
	window.cwb
) );
