<?php

/**
 * Plugin Name: CW Blocks
 * Version: 1.1.0
 *
 * @package cw-blocks
 *
 * @deps:
 * - bootstrap
 * - WP theme: ez-core-wp v3.0.15
 * - WP child-theme: ez-blank-333-wp-ez-core v1.1.8
 */

defined( 'ABSPATH' ) || exit;

class CWB {
    // required to test if this plugin is active
}

include 'cwb-sandbox/index.php';

include 'cwb-layout-block/index.php';
include 'cwb-content-wrap/index.php';
include 'cwb-button/index.php';

include 'cwb-repeater/index.php';
include 'cwb-repeater-item--basic/index.php';
include 'cwb-slider/index.php';
include 'cwb-repeater-item--slide/index.php';
include 'cwb-repeater-item--video-slide/index.php';
include 'cwb-repeater-item--modal-video/index.php';
include 'cwb-repeater-item--modal-iframe/index.php';
include 'cwb-repeater-item--magic-label-slide/index.php';
include 'cwb-repeater-item--itb/index.php';

include 'cwb-repeater-block/index.php';
include 'cwb-slider-gallery/index.php';
include 'cwb-asymmetric-gallery/index.php';
include 'cwb-grid-gallery/index.php';
include 'cwb-slider-gallery-button/index.php';
include 'cwb-slider-block/index.php';
include 'cwb-iframe-modal-img-block/index.php';
include 'cwb-video/index.php';
include 'cwb-video-background/index.php';
include 'cwb-posts-block/index.php';
include 'cwb-content-grid/index.php';
include 'cwb-testimonial-grid/index.php';
include 'cwb-accordion-block/index.php';
include 'cwb-quiz-question/index.php';
include 'cwb-quiz/index.php';
include 'cwb-magic-column/index.php';
include 'cwb-magic-columns/index.php';
include 'cwb-tab/index.php';
include 'cwb-tabs/index.php';
//include '01-basic/index.php';
//include '01-basic-esnext/index.php';
//include '02-stylesheets/index.php';
//include '03-editable/index.php';
//include 'format-api/index.php';
//include 'plugin-sidebar/plugin-sidebar.php';
//include 'meta-block/meta-block.php'; <-- todo try this one


// todo: this (below) works but need to keep it inside LH project for now
//  - copy block folder to any child theme and modify as needed
//include 'cwb-posts-offset-grid/index.php';


// CWB Enqueues
// https://wp.zacgordon.com//12/26/how-to-add-javascript-and-css-to-gutenberg-blocks-the-right-way-in-plugins-and-themes/
//==========================

function enqueue_cwb_globals_js() {

    $version = null;
    $file_inFooter = true;
    $dependencies = [  'wp-blocks', 'wp-element', 'wp-components', 'wp-i18n' ];//['jquery', 'flexspinner', 'browser-detector'];

    wp_register_script( 'cwb-globals', plugin_dir_url(__FILE__).'cwb-globals.js', $dependencies, $version, $file_inFooter );
    wp_register_script( 'cwb-components', plugin_dir_url(__FILE__).'cwb-components.js', $dependencies, $version, $file_inFooter );
    wp_register_script( 'cwb', plugin_dir_url(__FILE__).'cwb.js', $dependencies, $version, $file_inFooter );

    wp_enqueue_script('cwb-globals');
    wp_enqueue_script('cwb-components');
    wp_enqueue_script('cwb');

    // WP-AJAX global ajaxurl var
    global $_wp_theme_features;
    wp_localize_script( 'cwb-globals', 'editorColorPalette', $_wp_theme_features['editor-color-palette'] );
    wp_localize_script( 'cwb-globals', 'editorGradientPresets', $_wp_theme_features['editor-gradient-presets'] );
    wp_localize_script('cwb-globals', 'WPURLS', array(
        'siteurl' => get_option('siteurl'),
        'child_theme_uri' => get_stylesheet_directory_uri()));

}
add_action( 'enqueue_block_editor_assets', 'enqueue_cwb_globals_js',1 );


function enqueue_cwb_editor_styles() {

    wp_register_style( 'cwb-block-editor-mods', plugin_dir_url(__FILE__).'cwb-block-editor-mods.css', [] );
    wp_enqueue_style('cwb-block-editor-mods');
}
add_action( 'admin_enqueue_scripts', 'enqueue_cwb_editor_styles' );






