( function( blocks, editor, i18n, element, components, _, blockEditor, cwb ) {
	const __ = i18n.__;
	const el = element.createElement;
	const InnerBlocks = blockEditor.InnerBlocks;
	const useBlockProps = blockEditor.useBlockProps;
	const InspectorControls = blockEditor.InspectorControls;

	const {
	} = components;

	const {
		cwbGlobals,
		cwbComponents
	} = cwb;

	const {
	} = cwbGlobals;

	const {
		cwbAttributes
	} = cwbComponents;


	// @ref links below
	// https://mediaron.com/how-to-create-a-repeater-field6-in-gutenberg/
	// https://reactnative.dev/docs/state

	blocks.registerBlockType( 'cwb/repeater-item--modal-video', {


		// Block ASSETS
		//-----------------------------

		title: __( 'Repeater Item (Modal Video)', 'CWB' ),
		icon: 'admin-page', // see: /wp-includes/css/dashicons.css
		category: 'common',
		attributes: {
			cssClasses: {
				type: 'array',
				default: ['slide'],//['w-100', 'd-flex', 'm-0', 'align-items-center', 'bg-cover']
			},

			videoId: {
				type: 'id',
				default: ''
			},
			vidSrc: {
				type: 'object',
				default: ''
			},

			// triggerImage: {
			// 	type: 'string',
			// 	default: ''
			// },
			// triggerImageID: {
			// 	type: 'number',
			// },

			triggerImgObj: {
				type: 'object',
				default: false
			},



			gridCssClasses: {
				type : 'string',
				default: ''
			},

			gapPaddingCss: {
				type : 'string',
				default: ''
			},

			// Common Attribute Sets
			//-----------------------------
			// ...cwbAttributes.__blockColors,
			// ...cwbAttributes.__bgImage,
			// ...cwbAttributes.__blockBorder,
			...cwbAttributes.__bgOverlayColors,
			// overlayBlur: {
			// 	type: 'integer',
			// 	default: 0,
			// },

		},
		example: {
			attributes: {
				// title: __( 'Chocolate Chip Cookies', 'gutenberg-examples' ),
				// mediaID: 1,
				// mediaURL:
				// 	'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/2ChocolateChipCookies.jpg/320px-2ChocolateChipCookies.jpg',
			},
		},



		// Block FUNCTIONS
		//-----------------------------

		edit: function( props ) {
			let attributes = props.attributes;
			// let vAlignCss = [...attributes.vAlignCss];
			// let hAlignCss = [...attributes.hAlignCss];
			let blockStyle = {
				width: '100%',
				margin: '0px auto',
				padding: '0px',
				border: '1px dotted rgba(0,0,0,0.2)',
				color: attributes.textColor ? attributes.textColor : null,
				background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : attributes.bgColorOutput,
				position: 'relative'
			};

			attributes.vidSrc = attributes.vidSrc === '' ? false : attributes.vidSrc;




			// Get & Set Dynamic Parent Block Attributes
			//-----------------------------
			let parentAtts = {...cwbAttributes.globalBlockAtts[props.clientId]};
			props.setAttributes({
				gridCssClasses : parentAtts.gridCssClasses,
				gapPaddingCss : parentAtts.gapPaddingCss
			});
			attributes = props.attributes;




			// Set Grid & TileGap Classes
			//-----------------------------
			let gridCssClasses = __get(parentAtts.gridCssClasses, attributes.gridCssClasses);
			let gapPaddingCss = __get(parentAtts.gapPaddingCss, attributes.gapPaddingCss);
			let itemCss = [
				gridCssClasses,
				gapPaddingCss,
				'cwb-editor-outline',
			].join(' ');



			const setContainerClasses = function( value ) {
				props.setAttributes( { containerClasses: value } );
				attributes = props.attributes;
			};


			// // for ResponsiveTabs
			// //-----------------------------
			// const [layoutTab, setLayoutTab] = useState(false);





			// RETURN: Repeater Item
			//================================
			return el( 'div', {
					className: itemCss,
					style: blockStyle
				} , el( 'div', useBlockProps( {
					className: attributes.cssClasses,
					// style: blockStyle
				} ),



				// attributes.bgEffect ?
				el(CwbBgEffect, {
					bgOverlay : attributes.overlayColorOutput,
					backdropFilter: attributes.overlayBlur ?
						'blur('+attributes.overlayBlur+'px)' : null
				}),
				// : null,


				el('div', {
						className: [
							attributes.containerClasses,
							// ...attributes.vAlignCss,
							// 'd-flex',
							// 'slide-flex'
						].join(' ')
					},
					el('div', {
							className: [
								// ...attributes.hAlignCss,
								'slide-content w-100'
							].join(' ')
						},

						el('div', { className: 'position-relative'},

							//SliderModalTriggerEditor onEditClick()
							attributes.triggerImgObj ?
								el('div', {
										// className: 'd-inline-block'
									},
									el(BlockMediaEditor, {
										label: 'Select a Video Image',
										imageSize: 'gallery',
										image: attributes.triggerImgObj,
										onSelect: function (value) {

											console.log('ImgOBJ', value);

											props.setAttributes({
												triggerImgObj: value
											})
										},
										onRemove: function () {
											props.setAttributes({
												triggerImgObj: false
											})
										}
									})
								) : el('div', {className: 'trace-green p-4'})



						)
					),
				),


				// InspectorControls
				//================================
				el( InspectorControls, {
						key: 'controls' },


					// Panel : Wrap Settings
					//-----------------------------
					el( PanelBody, {
							title: 'Video Item Settings',
							initialOpen: false//!0
						},


						el( MediaUpload, {
							onSelect: function(media){
								// console.log(media); // todo
								props.setAttributes({
									vidSrc : media
								});
								attributes = props.attributes
							},
							allowedTypes: 'video',
							value: attributes.vidSrc,
							render: function( obj ) {
								return el('div', { className: '_text-white',
										style: {
											// border: "1px solid rgba(255,255,255,0.5)",
											// background: "rgba(0,0,0,0.5)",
											marginBottom: '20px'
										}},

									el('div', {},

										el(TextControl, {
											label : 'Video File URL',
											help : '',
											value : attributes.vidSrc.url,
											onChange :function(value){
												let filename = attributes.vidSrc.filename;

												if(value !== '') {
													props.setAttributes({
														vidSrc: {
															url: value,
															filename: value !== filename ?
																value !== '' ? 'external link' : 'none'
																: filename
														}
													});
													attributes = props.attributes
												}
												else {
													props.setAttributes({
														vidSrc: false
													});
													attributes = props.attributes
												}
											}
										}),

										attributes.vidSrc ?
											el('p', { className: 'mb-0'}, 'file: ',
												el('strong', {}, attributes.vidSrc.filename)
											) : null

									),

									attributes.vidSrc ?
										el(components.Button,{
												className: 'components-button is-tertiary',
												onClick: function(){
													props.setAttributes({
														vidSrc : false,
													});
													attributes = props.attributes
												},
											},
											el( Icon, { icon: 'trash', style: {
													// color: 'white'
												} }),
										)
										: el(components.Button,{
											onClick: obj.open,
											className: 'components-button is-tertiary'
										},
										'Video Library ',
										el( Icon, { icon: 'format-video', style: {
												marginLeft: '10px'
												// color: 'white'
											} }),
										),
								)
							}
						}),
					),


					el(InspectorMediaInput,{
						label: 'Choose Video Image',
						currentValue: attributes.triggerImgObj,
						onSelect: function(value){
							props.setAttributes({
								triggerImgObj : value
							})
						},
						onRemove: function(){
							props.setAttributes({
								triggerImgObj : false
							})
						},
					})


					// // Panel : Block Colors
					// //-----------------------------
					// el( BlockColors, props),
					//
					// // Panel : Background Image
					// //-----------------------------
					// el( BgImagePanel, {
					// 	props : props,
					// 	blockStyle : blockStyle
					// }),
					//
					// // Panel : BgEffects
					// //-----------------------------
					// el( BgEffects, props),
				)
				)
			);
		},

		save: function( props ) {
			let attributes = props.attributes;
			// let includeAltText = attributes.includeAriaLabel && attributes.mediaAlt.length;
			let blockStyle = {
				// background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : attributes.bgColorOutput,
				margin: '0px',
				padding: '0px',
				width: '100%',
				// color: attributes.textColor ? attributes.textColor : null,
				// background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : attributes.bgColorOutput,
				position:'relative'
				// color: attributes.textColor ? attributes.textColor : null,
			};



			// Set Grid & TileGap CSS
			//-----------------------------
			let itemCss = [
				attributes.gridCssClasses,
				attributes.gapPaddingCss,
			];


			// Repeater Item CSS
			//-----------------------------
			let tileCss = [
				...attributes.cssClasses,
				'container',
				'position-relative'
			];


			// RETURN: Layout-Block (FRONT-end)
			//================================
			return el( 'div',
				// {
				// 	className: itemCss.join(' '),
				// 	style: blockStyle
				// } ,
				// el( 'div',
						useBlockProps.save( {
							className: tileCss.join(' ') + ' test',
							style: blockStyle,
							// role: includeAltText ? 'img' : null,
							// ['aria-label']: includeAltText ? attributes.mediaAlt : null,
						} ),


					// attributes.bgEffect ?
					el(CwbBgEffect, {
						bgOverlay : attributes.overlayColorOutput,
						// backdropFilter: attributes.overlayBlur ?
						// 	'blur('+attributes.overlayBlur+'px)' : null
					}),
					// : null,

					el('div', {
							className: [
								// attributes.containerClasses,
								// ...attributes.vAlignCss,
								'd-flex',
								'slide-flex'
							].join(' ')
						},

						el(CwVideoTrigger, {
							image : attributes.triggerImgObj,
							vidSrc : attributes.vidSrc,
							modalId : 'video-modal',
							imageIndex : 0,
							singleImage : false,
							captionOnHover : false,
							lazyLoad : false,
							// id: typeof attributes.anchor === 'string' ? attributes.anchor : null,
							// vidSrc : attributes.vidSrc.url,
							// loop: false
						})
						// )
					)
				// )
			);
		},
	} );
}(
	window.wp.blocks,
	window.wp.editor,
	window.wp.i18n,
	window.wp.element,
	window.wp.components,
	window._,
	window.wp.blockEditor,
	window.cwb
) );
