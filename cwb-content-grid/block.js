( function( blocks, editor, i18n, element, components, _, blockEditor, cwb ) {
	const __ = i18n.__;
	const el = element.createElement;
	const InnerBlocks = blockEditor.InnerBlocks;
	const useBlockProps = blockEditor.useBlockProps;
	const InspectorControls = blockEditor.InspectorControls;
	const RawHTML = element.RawHTML;

	const {
		TextareaControl,
	} = components;

	const {
		cwbGlobals,
		cwbComponents
	} = cwb;

	const {
		// screenSizeTabs,
		// defaultScreenSizeObj
	} = cwbGlobals;

	const {
		RepeaterControls,
	} = cwbComponents;
	
	
	const GridItemObjModel = {
		key: 0,
		imgURL: false,
		imgID: false,
		imgObj: false,

		itemText: 'item 1', // (!) cannot change param name "itemText" becaase it break repeater
		itemSubHead: false,
		headTag: 'h3',
		subHeadTag: 'h4',

		// postObj: false,
		// wpContentPopover: false,
		btn: false,
		btnObj: {
			text: 'Button Text',
			url: '',
			target: null,
			newTab: false,
			dataToggle: null,
			dataShortcode: null,
			dataModalSize: null,
			dataModalHeading: null
		},
		btnPopover: false, // todo depreciated
		// testProp: true
	};


	// @ref links below
	// https://mediaron.com/how-to-create-a-repeater-field6-in-gutenberg/
	// https://reactnative.dev/docs/state

	blocks.registerBlockType( 'cwb/content-grid', {


		// Block ASSETS
		//-----------------------------

		title: __( 'Content Grid', 'CWB' ),
		icon: 'grid-view', // see: /wp-includes/css/dashicons.css
		category: 'common',
		attributes: {
			cssClasses: {
				type: 'array',
				default: ['py-3', 'my-0'],//['w-100', 'd-flex', 'm-0', 'align-items-center', 'bg-cover']
			},

			// // Common Attribute Sets
			// //-----------------------------
			// ...cwbAttributes.__blockColors,
			// ...cwbAttributes.__bgImage,
			// // ...cwbAttributes.__blockBorder,


			// // Repeater Block Settings
			// //-----------------------------
			// fullWidth: {
			// 	type: 'boolean' // for ToggleControl we need boolean type
			// }
			items: {
				type : 'array',
				default: [
					{ ...GridItemObjModel, key: 0, itemText: 'item 1'},
					{ ...GridItemObjModel, key: 1, itemText: 'item 2'},
					{ ...GridItemObjModel, key: 2, itemText: 'item three'},
				]
			},
			focusItem: {
				type: 'integer',
				default: null
			},

			grid: {
				type: 'object',

				// Set Default as empty string (due to bug in gutenberg)
				// - @ref: https://github.com/WordPress/gutenberg/issues/37967
				default: '',
				// - Use this when bug is fixed:
				// default: JSON.parse(JSON.stringify(defaultScreenSizeObj)), // copies default obj
			},

			tileGap: {
				type: 'object',

				// Set Default as empty string (due to bug in gutenberg)
				// - @ref: https://github.com/WordPress/gutenberg/issues/37967
				default: '',
				// - Use this when bug is fixed:
				// default: JSON.parse(JSON.stringify(defaultScreenSizeObj)), // copies default obj
			},
			itemLayout: {
				type: 'text',
				default: 'img-top'
			}
		},
		example: {
			attributes: {
				// title: __( 'Chocolate Chip Cookies', 'gutenberg-examples' ),
				// mediaID: 1,
				// mediaURL:
				// 	'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/2ChocolateChipCookies.jpg/320px-2ChocolateChipCookies.jpg',
			},
		},



		// Block FUNCTIONS
		//-----------------------------

		edit: function( props ) {
			let attributes = props.attributes;
			let blockStyle = {
				width: '100%',
				margin: '0px auto',
				padding: '0px',
			};

			// Options
			let ops = [];
			attributes.items.map((item) =>
			{
				ops.push({
					value : item.key,
					label : 'item '+item.key+': '+item.itemText
				});
			});


			// Grid Layout
			//--------------------------------------
			const [gridTab, setGridTab] = useState(false);

			if(attributes.grid === '') props.setAttributes({
				grid: {...defaultScreenSizeObj, base: 'width-2', md: 'width-md-3'},
			});
			let gridCssClasses = genResponsiveClasses(attributes.grid);



			if(attributes.tileGap === '') props.setAttributes({
				tileGap: {...defaultScreenSizeObj, base: '2', md: '3'},
			});
			let tileGap = genTileGapCss(attributes.tileGap);
			let gapMarginCss = tileGap.gapMarginCss;
			let gapPaddingCss = tileGap.gapPaddingCss;



			// Block DRY Functions
			//--------------------------------------

			const updateRepeaterItems = function(newArr){
				props.setAttributes({
					items : newArr
				});
				attributes = props.attributes;
			};

			const setItemAttr = function(value, focusItem, property){

				let newArr = [...attributes.items];

				// update item
				newArr[focusItem.key][property] = value;

				//
				// update block
				updateRepeaterItems(newArr);
			};



			// used for WYSIWYG edits rather than edits via inspectorPanel
			// - called when updating text todo see if there's a way to pull this from RepeaterComponent
			const updateFocus = function(item){
				props.setAttributes({
					focusItem : item//.key.toString()
				});
				attributes = props.attributes;
			};



			// Content Modifiers
			//--------------------------------------

			const updateText = function(value, item){
				setItemAttr(value, item, 'itemText');
			};




			let focusItem = attributes.items[ attributes.focusItem !== null ? attributes.focusItem : 0 ];
			focusItem = fixFocusItem(focusItem, attributes);


			// RETURN: Simple-Repeater-Block (BACK-end)
			//================================
			return el( 'div', useBlockProps( {
					className: attributes.cssClasses,
					style: blockStyle
				} ),

				// Container Wrap
				//-----------------------------
				el( 'div', {
						className: attributes.fullWidth ? 'cwb-content-grid' : 'cwb-content-grid container px-0'
					},


						el(SimpleRepeaterBlock, {
								// ...props,
								blockProps: props,
								className: 'row '+gapMarginCss,
								itemObjModel : GridItemObjModel
							},
							(item, toolbar) => {
								return el('div', {
										className: 'cwb-content-grid-item d-inline-block position-relative '+gridCssClasses+' '+gapPaddingCss,
									},

									// attributes.itemLayout === 'img-top' ?


									el('div', { className: attributes.itemLayout === 'img-side' ?
											'row item-inner' : ' item-inner'},

										el('div', { className: attributes.itemLayout === 'img-side' ?
												'col-3 item-img-wrap' : ' item-img-wrap'},

											// Content Image
											//--------------------------------------
											item.imgObj ?

												el('div', {
													// className: 'd-inline-block'
													},
													el(BlockMediaEditor,{
														label : 'Select a Top Image',
														imageSize: 'large',
														image : item.imgObj,
														onClick: function () {
															updateFocus(item.key.toString())
														},
														onSelect: function(value){
															setItemAttr(value, item, 'imgObj')
														},
														onRemove: function(){
															setItemAttr(false, item, 'imgObj')
														},
													})
												)
												: null,
										),

										el('div', { className: attributes.itemLayout === 'img-side' ?
												'col-8 item-content-wrap' : ' item-content-wrap'},


											el(item.headTag, {
													className: 'item-head',
													style: {
														position: 'relative'
													}
												},
												el(RichTextBlockInput, {
													currentValue : item.itemText,
													onChange : function(value){
														updateText(value, item);
													},
													onClick: function(){
														updateFocus(item.key.toString());
													},
													multiLine: false
												}),
											),



											item.itemSubHead ?
												el(item.subHeadTag, {
														className: 'item-subhead',
														style: {
															position: 'relative'
														}
													},
													el(RichTextBlockInput, {
															currentValue :item.itemSubHead,
															multiLine: false,
															onChange: function(value){
																setItemAttr(value, item, 'itemSubHead')
															},
															onClick: function(){
																updateFocus(item.key.toString());
															}
														}
													)
												) : null,


											item.itemBody ?
												el('p', {
														className: 'item-body',
														style: {
															position: 'relative'
														}
													},
													el(RichTextBlockInput, {
															currentValue: item.itemBody,
															onChange: function(value){
																setItemAttr(value, item, 'itemBody')
															},
															onClick: function(){
																updateFocus(item.key.toString());
															}
														}
													)
												) : null,
											toolbar
										)
									)
								)
							}
						)
				),




				// InspectorControls
				//================================
				el( InspectorControls, {
					key: 'controls' },

					el( 'div', {},

						// Grid Layout Configs
						//-------------------------
						el( PanelBody, {
								style: {
									padding: '0 20px'
								},
								title: 'Grid Configs',
								initialOpen: false//!0,
							},

							el(ResponsiveTabs, props,
								(gridTab) => {
									setGridTab(gridTab);
									props = {
										tab: {...gridTab},
										...props,
										gridCssClasses: gridCssClasses,
										onUpdateLayout: function(attrStr, responsiveCssObj){
											// console.log('gridTab', attrStr, responsiveCssObj);
										}
									};
									return el(GridSettingsTab, props);
								}
							),

							el( SelectControl, {
								key: 'multiple',
								label: __('Item Layout'),
								className: ['mt-3'],
								value: attributes.itemLayout,
								options: [
									{ value: 'img-top', label: 'Image Top' },
									{ value: 'img-side', label: 'Image Side' },
								],
								onChange: function(value){
									props.setAttributes({
										itemLayout: value
									});
									attributes = props.attributes;
								}
							}),
						)
					),

					el(RepeaterControls, {
							// ...props,
							blockProps: props,
							itemObjModel : GridItemObjModel,
						},

						el( 'div', {
								style: {
									padding: '0 20px'
								},
							},
							// Item Controls:
							el( RichTextBlockInput,
								{
									label: 'item head',
									style: {
										border: '1px solid black',
										padding: '5px 8px',
										marginBottom: '10px',
										background: 'white'
									},
									currentValue: focusItem.itemText,
									onChange: function(value){
										updateText(value, focusItem);
									},
									multiLine: false
								},
							),

							el(SelectControl,{
								label: 'item head tag',
								value: focusItem.headTag ? focusItem.headTag : 'p',
								options: [
									{value: 'p', label: 'p'},

									// todo: allowedTags
									// {value: 'h1', label: 'H1'},
									{value: 'h2', label: 'H2'},
									{value: 'h3', label: 'H3'},
									{value: 'h4', label: 'H4'},
									{value: 'h5', label: 'H5'},
									{value: 'h6', label: 'H6'},
								],
								onChange: function(value){

									setItemAttr(value, focusItem,'headTag');
								}
							}),


							el( RichTextBlockInput,
								{
									label: 'item subhead',
									style: {
										border: '1px solid black',
										padding: '5px 8px',
										marginBottom: '10px',
										background: 'white'
									},
									currentValue: focusItem.itemSubHead,
									onChange: function(value){
										setItemAttr(value, focusItem, 'itemSubHead')
									},
									multiLine: false
								},
							),

							el(SelectControl,{
								label: 'item subhead tag',
								value: focusItem.subHeadTag ? focusItem.subHeadTag : 'p',
								options: [
									{value: 'p', label: 'p'},

									// todo: allowedTags
									// {value: 'h1', label: 'H1'},
									{value: 'h2', label: 'H2'},
									{value: 'h3', label: 'H3'},
									{value: 'h4', label: 'H4'},
									{value: 'h5', label: 'H5'},
									{value: 'h6', label: 'H6'},
								],
								onChange: function(value){

									setItemAttr(value, focusItem,'subHeadTag');
								}
							}),

							el( TextareaControl,
								{
									label: 'item body',
									value: focusItem.itemBody,
									onChange: function(value){
										setItemAttr(value, focusItem, 'itemBody')
									}
								}
							),

							el(InspectorMediaInput,{
								currentValue: focusItem.imgObj,
								onSelect: function(value){
									setItemAttr(value, focusItem, 'imgObj')
								},
								onRemove: function(){
									setItemAttr(false, focusItem, 'imgObj')
								},
							})
						)
					),



					// Dev Utils
					// TODO make conditional & compare item model vs current item
					// --------------------------------------
					el( 'div', {
							style: {
								padding: '0 20px 20px',
								// margin: '30px 0 0'
							},
						},
						el( components.Button, {
							className: 'button button-large cursor-pointer',
							onClick: function(){


								let newArr = [...attributes.items];
								let newSlide = updateItemModel(focusItem, GridItemObjModel );

								console.log( newSlide);


								// update item
								newArr[focusItem.key] = newSlide;
								//
								// update block
								props.setAttributes({
									items : newArr
								});
								attributes = props.attributes;

							}
						}, 'Update Grid Item Model' ),
					),

					// // Dev Utils
					// //--------------------------------------
					// el( 'div', {
					// 		style: {
					// 			padding: '0 25px 20px',
					// 			// margin: '30px 0 0'
					// 		},
					// 	},
					// 	el( components.Button, {
					// 		className: 'button button-large cursor-pointer',
					// 		onClick: function(){
					// 			let newArr = [...attributes.items];
					// 			for( let x = 0; x < attributes.items.length; x++ ){
					// 				let newItem = updateItemModel(attributes.items[x], GridItemObjModel );
					//
					// 				// update item
					// 				newArr[attributes.items[x].key] = newItem;
					// 				//
					// 			}
					//
					// 			// update block
					// 			props.setAttributes({
					// 				items : newArr
					// 			});
					// 			attributes = props.attributes;
					//
					// 		}
					// 	}, 'Update Item Models' ),
					// )
				)
			);
		},

		save: function( props ) {
			let attributes = props.attributes;
			let blockStyle = {
				// background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : attributes.bgColorOutput,
				margin: '0px',
				padding: '0px',
				width: '100%',
				// color: attributes.textColor ? attributes.textColor : null,
			};
			let gridCssClasses = genResponsiveClasses(attributes.grid);
			let tileGap = genTileGapCss(attributes.tileGap);
			let gapMarginCss = tileGap.gapMarginCss;
			let gapPaddingCss = tileGap.gapPaddingCss;

			let imageSize = 'gallery';//item.imgObj && typeof props.imageSize !== 'undefined' ? props.imageSize : false;



			// RETURN: Layout-Block (FRONT-end)
			//================================
			return el( 'div', useBlockProps.save( {
					className: attributes.cssClasses.join(' '),
					style: blockStyle
				} ),

				// Container Wrap
				//-----------------------------
				el( 'div', {
						['data-update']: '230505-00',
						className: attributes.fullWidth ? '' : 'cwb-content-grid container px-0'
					},

					el('div', {className: 'd-flex flex-wrap row '+gapMarginCss},
						attributes.items.map((item) => {


							let alt = getImageAlt(item.imgObj);


							// todo comment this, maybe DRY it
							// todo getImageSource()?
							let imageUrl = item.imgObj && typeof item.imgObj.sizes.medium !== 'undefined' ?
								item.imgObj.sizes.medium.url : item.imgObj ? item.imgObj.url : '';
							let imageWidth = item.imgObj && typeof item.imgObj.sizes.medium !== 'undefined' ?
								item.imgObj.sizes.medium.width : item.imgObj ? item.imgObj.width : '';
							let imageHeight = item.imgObj && typeof item.imgObj.sizes.medium !== 'undefined' ?
								item.imgObj.sizes.medium.height : item.imgObj ? item.imgObj.height : '';


							// todo comment this, maybe DRY it
							imageUrl = imageSize && typeof item.imgObj.sizes !== 'undefined' && typeof item.imgObj.sizes[imageSize] !== 'undefined' ?
								item.imgObj.sizes[imageSize].url : imageUrl;
							imageWidth = imageSize && typeof item.imgObj.sizes !== 'undefined' && typeof item.imgObj.sizes[imageSize] !== 'undefined' ?
								item.imgObj.sizes[imageSize].width : imageWidth;
							imageHeight = imageSize && typeof item.imgObj.sizes !== 'undefined' && typeof item.imgObj.sizes[imageSize] !== 'undefined' ?
								item.imgObj.sizes[imageSize].height : imageHeight;


							// console.log('item.imgObj.sizes', item.imgObj.sizes);

							// console.log(item.headTag);

							return el('div', {
									className: 'cwb-content-grid-item d-inline-block p-1 '+gridCssClasses+' '+gapPaddingCss,
								},


								el('div', { className: attributes.itemLayout === 'img-side' ?
										'row item-inner' : ' item-inner'},

									el('div', { className: attributes.itemLayout === 'img-side' ?
											'col-3 item-img-wrap col-stack-max-xs' : ' item-img-wrap'},

										// Content Image
										//--------------------------------------
										item.imgObj ?

											el('div', {
													className: 'mb-3'
												},
												el('img',{
													src: imageUrl,
													alt: alt ? alt : null,
													width: imageWidth ? imageWidth : null,
													height: imageHeight ? imageHeight : null
												})
											)
											: null,
									),

									el('div', { className: attributes.itemLayout === 'img-side' ?
											'col-8 item-content-wrap col-stack-max-xs' : ' item-content-wrap'},


										el(item.headTag, {
												className: 'item-head',
												style: {
													position: 'relative'
												}
											}, el(RawHTML,{},
												allowTagsSimpleText( item.itemText )
											)
										),


										item.itemSubHead ?
											el(item.subHeadTag, {
													className: 'item-subhead',
													style: {
														position: 'relative'
													}
												}, el(RawHTML,{},
													allowTagsSimpleText( item.itemSubHead )
												)
											) : null,


										item.itemBody ?
											el('p', {
													className: 'item-body',
													style: {
														position: 'relative'
													}
												}, item.itemBody
											) : null,
									)
								)
							)
						})
					)
				)
			);
		},
	} );
}(
	window.wp.blocks,
	window.wp.editor,
	window.wp.i18n,
	window.wp.element,
	window.wp.components,
	window._,
	window.wp.blockEditor,
	window.cwb
) );
