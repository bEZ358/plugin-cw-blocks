( function( blocks, editor, i18n, element, components, _, blockEditor, cwb ) {
	const __ = i18n.__;
	const el = element.createElement;
	const useBlockProps = blockEditor.useBlockProps;
	// const BlockControls = blockEditor.BlockControls;
	const InspectorControls = blockEditor.InspectorControls;
	const LinkControl = blockEditor.__experimentalLinkControl;
	const LinkControlSearchInput = blockEditor.__experimentalLinkControlSearchInput;

	const {
		RawHTML // use this for shortcodes later?
	} = el;

	// const {
	// // 	SelectControl,
	// // 	// TextControl,
	// // 	PanelBody,
	// // } = components;
	const CheckboxControl = components.CheckboxControl;
	const ComboboxControl = components.ComboboxControl;
	const {
		cwbGlobals,
		cwbComponents
	} = cwb;
	//
	const {
		// screenSizeTabs,
		// defaultScreenSizeObj
	} = cwbGlobals;
	//
	const {
		// BlockBorderPanel,
		cwbAttributes,
		// BlockColors,
		// HoverColors
	} = cwbComponents;


	blocks.registerBlockType( 'cwb/button', {


		// Block ASSETS
		//-----------------------------

		title: __( 'CWB: Button', 'CWB' ),
		icon: 'button',
		category: 'common',
		// attributes: atts,
		attributes: {
			cssClasses: {
				type: 'array',
				default: ['btn btn-primary cursor-pointer'],//['p-0', 'my-0'],
			},

			// Common Attributes
			//-----------------------------
			...cwbAttributes.__blockColors,
			...cwbAttributes.__blockBorder,
			...cwbAttributes.__hoverColors,


			// Button Meta
			//-----------------------------
			text : {
				type: 'string',
				default: 'click here'
			},

			href : {
				type: 'string',
				default: ''
			},
			target: {
				type: 'string',
				default: ''
			},
			dataShortcode : {
				type : 'string',
				default: ''
			},

			openNewTab: {
				type: 'boolean',
				default: false
			},
			dataSource : {
				type: 'string',
				default: ''
			},

			btnSize : {
				type: 'string',
				default: 'default'
			},
			btnType : {
				type: 'string',
				default: 'primary'
			},
			btnFunction : {
				type: 'string',
				default: 'link'
			},
			postObj : {
				type: 'object',
				default: ''
			},
			permalink : {
				type: 'string',
				default: ''
			},

			popupHeading : {
				type: 'string',
				default: 'CWB button Popup'
			},
			popupSize : {
				type: 'string',
				default: ''
			},

			hoverScale: {
				type: 'boolean',
				default: ''
			},


			// used for local dev environment
			// to ensure that "/*" urls use the correct domain path
			siteUrl: {
				type: 'string',
				default: 'post'
			},
		},
		example: {
			attributes: {
				// title: __( 'Chocolate Chip Cookies', 'gutenberg-examples' ),
				// mediaID: 1,
				// mediaURL:
				// 	'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/2ChocolateChipCookies.jpg/320px-2ChocolateChipCookies.jpg',
			},
		},


		// Block FUNCTIONS
		//-----------------------------

		edit: function( props ) {
			let attributes = props.attributes;
			let newCssClasses = [...attributes.cssClasses];
			let blockStyle = {
				// background: attributes.bgColorOutput,
				// border: attributes.borderColor ? attributes.borderWidth+'px solid '+attributes.borderColor : null,
				// color: attributes.textColor ? attributes.textColor : null,


				// // https://stackoverflow.com/questions/28365233/inline-css-styles-in-react-how-to-implement-ahover
				// "&:hover": {
				// 	background: attributes.hoverBgColorOutput,
				// 	border: attributes.hoverBorderColor ? attributes.borderWidth+'px solid '+attributes.hoverBorderColor : null,
				// 	color: attributes.hoverTextColor ? attributes.hoverTextColor : null,
				// },

				// width: '100%',
				// margin: '0px auto',
				// padding: '0px'
			};
			let block_el_tag = 'button';
			let allowedBtnTypes  = [
				'link',
				'wp_link',
				'modal_trigger',
				'iframe',
				'shortcode_modal_trigger',
				// 'popup_cpt'
			];
			// if() allowedBtnTypes.push('popup_cpt');

			let blockProps = {
				className: attributes.cssClasses,
				style: blockStyle,
				['type']: 'button',
			};

			// - get siteUrl
			const siteUrl = useSelect((select) => {
				return wp.data.select( 'core' ).getSite();
			});
			if(siteUrl && attributes.siteUrl !== siteUrl){
				props.setAttributes({
					siteUrl: siteUrl.url
				});
				attributes = props.attributes;
			}



			// Conditional Block Properties
			// - based on attributes.btnFunction value
			//-----------------------------------------

			// basic link button conditionals
			if(attributes.btnFunction === 'link') {
				blockProps._href = attributes.href !== '' ? attributes.href : null;
				blockProps['type'] = null;
				block_el_tag = 'a';
				blockProps._target = attributes.openNewTab ? '_blank' : null;
			}

			// WP link button conditionals
			if(attributes.btnFunction === 'wp_link') {
				blockProps._href = attributes.postObj !== '' ? attributes.postObj.url : null;
				blockProps['type'] = null;
				block_el_tag = 'a';
				if(typeof attributes.postObj !== ''){
					let opensInNewTab = attributes.postObj.opensInNewTab;
					opensInNewTab = typeof opensInNewTab !== 'undefined' ? opensInNewTab : false;
					blockProps._target = opensInNewTab ? '_blank' : null;
				};
			}

			// modal trigger (common) conditionals
			if(	attributes.btnFunction === 'modal_trigger' ||
				attributes.btnFunction === 'shortcode_modal_trigger' ) {

				blockProps['data-toggle'] = 'modal';
				// below requires ez-core v3.0.12
				blockProps['data-modal-size'] = attributes.popupSize !== '' ? attributes.popupSize : null;
				blockProps['data-modal-heading'] = attributes.popupHeading !== '' ? attributes.popupHeading : null;
			}

			// iframe modal trigger conditionals
			if(attributes.btnFunction === 'iframe'){
				blockProps['data-target'] = '#iframe-modal';
				blockProps['data-src'] = attributes.dataSource !== '' ? attributes.dataSource : null;
			}
			// shortcode modal conditionals
			if(attributes.btnFunction === 'shortcode_modal_trigger'){
				blockProps['data-target'] = '#shortcode-ajax-modal';
				// below requires ez-core v3.0.12
				blockProps['data-shortcode'] = attributes.dataShortcode !== '' ? JSON.stringify(attributes.dataShortcode) : null;
			}



			// Add popup_cpt btn-function if CPT exists
			//--------------------------------------
			const selectedPost = useSelect((select) => {
				return wp.data.select('core').getEntityRecords('postType','popup');
			});
			if(selectedPost !== null){
				allowedBtnTypes.push('popup_cpt');
			}


			// RETURN: Layout-Block (BACK-end)
			//================================
			return el( 'div', {
				className: 'd-inline-block',
				style: {
					margin: '0 2px'
				} },

				// The Button
				//-----------------------------
				el( block_el_tag, useBlockProps( blockProps ),
					attributes.text
				),




				// InspectorControls
				//================================
				el( InspectorControls, {
					key: 'controls' },

					el(CwbButtonEditor,{
						allowedBtnTypes  : allowedBtnTypes,
						btnObj : {
							text			: attributes.text,
							url 			: attributes.href,
							btnFunction 	: attributes.btnFunction,
							target			: attributes.target, // todo should be dataTarget because thats how CwbButton uses it
							newTab 			: attributes.openNewTab,
							// dataToggle 	: attributes.dataToggle,
							dataShortcode 	: attributes.dataShortcode,
							dataModalHeading: attributes.popupHeading,
							dataModalSize 	: attributes.popupSize,
							dataSrc 		: attributes.dataSource,
							postObj 		: attributes.postObj,
							btnType 		: attributes.btnType,
							btnSize 		: attributes.btnSize,
							hoverScale 		: attributes.hoverScale,

							// permalink 		: attributes.permalink, // not part of default btnObj
						},
						updateText : function(value){
							props.setAttributes( {
								text: value
							} );
							attributes = props.attribute
						},
						updateUrl : function(value){
							props.setAttributes( {
								href: value
							} );
							attributes = props.attribute
						},
						updateNewTab : function(value){
							props.setAttributes( {
								openNewTab: value
							} );
							attributes = props.attribute
						},
						setButtonFunction : function(value){
							props.setAttributes( {
								btnFunction: value
							} );
							attributes = props.attribute;
						},
						updateDataSrc : function(value){
							props.setAttributes( {
								dataSource: value
							} );
							attributes = props.attributes;
						},
						updateDataTarget : function(value){
							props.setAttributes( {
								target: value
							} );
							attributes = props.attributes;
						},
						updateDataToggle : function(value){
							// props.setAttributes( {
							// 	dataToggle: value
							// } );
							// attributes = props.attributes;
						},
						updateShortcode : function(value){
							props.setAttributes( {
								dataShortcode: value
							} );
							attributes = props.attributes;
						},
						updateModalHeading : function(value){
							props.setAttributes( {
								popupHeading: value
							} );
							attributes = props.attributes;
						},
						updateModalSize : function(value){
							props.setAttributes( {
								popupSize: value
							} );
							attributes = props.attributes;
						},
						updatePostObj : function(value){
							props.setAttributes( {
								postObj: value
							} );
							attributes = props.attributes;
							// console.log('LinkControl', attributes.postObj);
						},




						// todo  btn styles
						updateBtnType : function(value){
							let classArr = newCssClasses.join(' ').split(' ');

							arrRemoveStrings(classArr, [
								'btn-primary',
								'btn-secondary',
								'btn-tertiary',
								'btn-alt',
								'btn-alt-lite']);
							if(value !== 'default'){
								classArr.push('btn-'+value);
							}

							props.setAttributes( {
								cssClasses: classArr,
								btnType: value
							} );
							attributes = props.attributes;
						},
						updateBtnSize : function(value){
							let classArr = newCssClasses.join(' ').split(' ');

							arrRemoveStrings(classArr, [
								'btn-sm',
								'btn-lg'
							]);
							if(value !== 'default'){
								classArr.push('btn-'+value);
							}

							props.setAttributes( {
								cssClasses: classArr,
								btnSize: value
							} );
							attributes = props.attributes;
						},
						updateHoverScale : function(value){
							let classArr = newCssClasses.join(' ').split(' ');

							arrRemoveStrings(classArr, [
								'btn-hover-scale'
							]);
							if(!attributes.hoverScale === true){
								classArr.push('btn-hover-scale');
							}

							props.setAttributes( {
								cssClasses: classArr,
								hoverScale: value//!attributes.hoverScale,
							} );
							attributes = props.attributes;
						},
					}),




					// Button Editor Panels
					//===============================================
					// - disabled for now because hover states
					//   are problematic
					// - using only button styles for now
					// ≈ NTH: button editor to override basic styles

					// // Panel : Block Colors
					// //-----------------------------
					// el( BlockColors, props),

					// // Panel : Border Color
					// //-----------------------------
					// el( BlockBorderPanel, props),

					// // Panel : Hover Color
					// //-----------------------------
					// el( HoverColors, props),













					// TODO : below is depreciated;
					//  	- keep for reference
					//  	- delete if nothing breaks by (april 2024)

					// //
					// //
					// //
					// // Panel : Button Style
					// //-----------------------------
					// el( PanelBody, {
					// 		title: 'Button Styles',
					// 		initialOpen: false//!0
					// 	},
					//
					// 	el( SelectControl, {
					// 		key: 'multiple',
					// 		label: __('Button Type'),
					// 		value: attributes.btnType,
					// 		className: ['mt-3'],
					// 		onChange: function(value){
					//
					// 			let classArr = newCssClasses.join(' ').split(' ');
					//
					// 			arrRemoveStrings(classArr, [
					// 				'btn-primary',
					// 				'btn-secondary',
					// 				'btn-tertiary',
					// 				'btn-alt',
					// 				'btn-alt-lite']);
					// 			if(value !== 'default'){
					// 				classArr.push('btn-'+value);
					// 			}
					//
					// 			props.setAttributes( {
					// 				cssClasses: classArr,
					// 				btnType: value
					// 			} );
					// 			attributes = props.attributes;
					// 		},
					// 		options: [
					// 			// { value: '', label: tab.name === 'base' ? 'default' : 'inherit from smaller' },
					// 			{ value: 'primary', label: 'primary' },// 			{ value: 'default', label: 'Default' },
					// 			{ value: 'secondary', label: 'secondary' },
					// 			{ value: 'tertiary', label: 'tertiary' },
					// 			{ value: 'alt', label: 'alt' },
					// 			{ value: 'alt-lite', label: 'alt-lite' },
					// 			// { value: 'alt-secondary', label: 'alt-secondary' }, <--- TODO
					// 		]
					// 	}),
					//
					// 	el( SelectControl, {
					// 		key: 'multiple',
					// 		label: __('Button Size'),
					// 		value: attributes.btnSize,
					// 		className: ['mt-3'],
					// 		onChange: function(value){
					//
					// 			let classArr = newCssClasses.join(' ').split(' ');
					//
					// 			arrRemoveStrings(classArr, [
					// 				'btn-sm',
					// 				'btn-lg'
					// 			]);
					// 			if(value !== 'default'){
					// 				classArr.push('btn-'+value);
					// 			}
					//
					// 			props.setAttributes( {
					// 				cssClasses: classArr,
					// 				btnSize: value
					// 			} );
					// 			attributes = props.attributes;
					// 		},
					// 		options: [
					// 			// { value: '', label: tab.name === 'base' ? 'default' : 'inherit from smaller' },
					// 			{ value: 'sm', label: 'Small' },
					// 			{ value: 'default', label: 'Default' },
					// 			{ value: 'lg', label: 'Large' },
					// 		]
					// 	}),
					//
					//
					// 	el('p',{}, 'Button Animations'),
					// 	el( CheckboxControl, {
					// 	// 	key: 'multiple',
					// 		label: __('Grow on Hover'),
					// 		checked: attributes.hoverScale,
					// 		onChange: function(){
					//
					// 			let classArr = newCssClasses.join(' ').split(' ');
					//
					// 			arrRemoveStrings(classArr, [
					// 				'btn-hover-scale'
					// 			]);
					// 			if(!attributes.hoverScale === true){
					// 				classArr.push('btn-hover-scale');
					// 			}
					//
					// 			props.setAttributes( {
					// 				cssClasses: classArr,
					// 				hoverScale: !attributes.hoverScale,
					// 			} );
					// 			attributes = props.attributes;
					// 		},
					// 	}),
					// ),
					// // Panel : Button Meta
					// //-----------------------------
					// el( PanelBody, {
					// 		title: 'Button Meta',
					// 		initialOpen: false//!0
					// 		},
					// 	el( SelectControl, {
					// 		key: 'multiple',
					// 		label: __( 'Button Function'),
					// 		value: attributes.btnFunction,
					// 		onChange: setButtonFunction,
					// 		options: [
					// 			{ value: 'link', label: 'Link' },
					// 			{ value: 'modal_trigger', label: 'Modal Trigger' }, // todo iframe modal vs target so we can open global modals
					// 			{ value: 'shortcode_modal_trigger', label: 'Shortcode Modal Trigger' }, // <-- requires ez-core v3.0.12
					// 			{ value: 'wp_link', label: 'WP Link' } // disabled until object saving is resolved
					// 		]
					// 	}),
					//
					//
					//
					// 	el( 'div', { style: {
					// 				padding: '20px 10px',
					// 				background: '#f8f8f8'
					// 			}
					// 		},
					// 		el(InspectorText, {...props,
					// 			label: __('Button Text'),
					// 			attName: 'text'
					// 		}),
					//
					//
					// 		// Basic Link Button Meta
					// 		//-----------------------------
					// 		attributes.btnFunction === 'link' ?
					// 			el( 'div', {},
					// 				el(InspectorText, {...props,
					// 					label: __('Button Attribute: href'),
					// 					attName: 'href'
					// 				}),
					// 				el( ToggleControl,
					// 					{
					// 						label: 'Open in new tab',
					// 						onChange: function(value){
					// 							props.setAttributes( {
					// 								openNewTab : value
					// 							} );
					// 							attributes = props.attributes;
					// 						},
					// 						checked: attributes.openNewTab,
					// 					}
					// 				)
					// 			): null,
					//
					//
					//
					//
					// 		// Modal Button Meta
					// 		//-----------------------------
					// 		attributes.btnFunction === 'modal_trigger' ||
					// 		attributes.btnFunction === 'shortcode_modal_trigger' ?
					// 			el( 'div', {},
					// 				el(InspectorText, {...props,
					// 					label: __('Popup Heading'),
					// 					attName: 'popupHeading'
					// 				}),
					// 				el( SelectControl, {
					// 					key: 'multiple',
					// 					label: __('Modal Size'),
					// 					value: attributes.popupSize,
					// 					className: ['mt-3'],
					// 					onChange: function(value){
					// 						props.setAttributes( {
					// 							// cssClasses: classArr,
					// 							popupSize: value
					// 						} );
					// 						attributes = props.attributes;
					// 					},
					// 					options: [
					// 						// { value: '', label: tab.name === 'base' ? 'default' : 'inherit from smaller' },
					// 						{ value: 'default', label: 'Default' },
					// 						{ value: 'lg', label: 'Large' },
					// 						{ value: 'xl', label: 'Extra Large' },
					// 						{ value: 'fs', label: 'Full Screen' },
					// 					]
					// 				}),
					// 			): null,
					//
					//
					//
					// 		// iframe Modal Trigger Meta
					// 		//-----------------------------
					// 		attributes.btnFunction === 'modal_trigger' ?
					// 			el( 'div', {},
					// 				el(InspectorText, {...props,
					// 					label: __('iFrame Source (data-src)'),
					// 					attName: 'dataSource'
					// 				}),
					// 			): null,
					//
					//
					//
					// 		// Shortcode Modal Trigger Meta
					// 		//-----------------------------
					// 		attributes.btnFunction === 'shortcode_modal_trigger' ?
					// 			el( 'div', {},
					// 				el(InspectorText, {...props,
					// 					label: __('WP Shortcode'),
					// 					attName: 'dataShortcode'
					// 				}),
					// 			): null,
					//
					//
					//
					//
					// 		// WP Link Meta
					// 		//-----------------------------
					// 		attributes.btnFunction === 'wp_link' ?
					//
					// 			el( 'div', {
					// 					className: 'cwb-input',
					// 					style: {
					// 						padding: '5px 0 0',
					// 						background: '#f8f8f8'
					// 					}
					// 				},
					//
					// 				el( LinkControl, {
					// 					searchInputPlaceholder : "Search here...",
					// 					value: attributes.postObj,
					//
					// 					onChange: function(value){
					// 						// todo: this is not saving; tried setting default to '', no luck
					// 						// ~ try stripped down version; inquire about in forum?
					// 						// ~ or wait for WP to refine the control?
					// 						props.setAttributes({
					// 							postObj : value
					// 						});
					// 						attributes = props.attributes;
					// 						console.log('LinkControl', attributes.postObj);
					// 					},
					// 					// suggestionsQuery : null
					// 					// withCreateSuggestion : true // https://wordpress.stackexchange.com/questions/399076/gutenberg-linkcontrol-suggestionsquery-not-working
					// 				})
					//
					// 			) : null,
					// 	),
					// ),
				)
			);
		},

		save: function( props ) {
			let block_el_tag = 'button';
			let attributes = props.attributes;
			let blockStyle = {
				// background: attributes.bgColorOutput,
				// border: attributes.borderWidth ? attributes.borderWidth+'px solid '+attributes.borderColor : null,
				// color: attributes.textColor ? attributes.textColor : null,

				// margin: '0px',
				// padding: '0px',
				// width: '100%',
			};

			// Conditional Block Properties
			// - based on attributes.btnFunction value
			let blockProps = {
				className: attributes.cssClasses.join(' '),
				style: blockStyle,
				// ['data-bez']: 'bez', // toggle this to force a block markup refresh
				['type']: 'button',
			};
			let btnObj = {
				text			: attributes.text,
				url 			: attributes.href,
				btnFunction 	: attributes.btnFunction,
				target			: attributes.target, // todo should be dataTarget because thats how CwbButton uses it
				newTab 			: attributes.openNewTab,
				// dataToggle 	: attributes.dataToggle,
				dataShortcode 	: attributes.dataShortcode,
				dataModalHeading: attributes.popupHeading,
				dataModalSize 	: attributes.popupSize,
				dataSrc 		: attributes.dataSource,
				postObj 		: attributes.postObj,
				btnType 		: attributes.btnType,
				btnSize 		: attributes.btnSize,
				hoverScale 		: attributes.hoverScale,

				// permalink 		: attributes.permalink, // not part of default btnObj
			};



			// - get siteUrl
			if(typeof attributes.href !== 'undefined' && attributes.href.startsWith('/')){

				let fullUrl = attributes.siteUrl+attributes.href;
				btnObj.href = fullUrl;
			}


			blockProps = useBlockProps.save( {
				className: attributes.cssClasses.join(' '),
				// style: blockStyle,
			});



			return el( CwbButton, {
				btnObj : btnObj,
				extraBtnCss : setBtnCssClasses(btnObj, blockProps.className),
				btnType : ''
			})









			// TODO : below is depreciated;
			//  	- keep for reference
			//  	- delete if nothing breaks by (april 2024)

			// // Conditional Block Properties
			// // - based on attributes.btnFunction value
			// //-----------------------------------------
			//
			// // basic link button conditionals
			// if(attributes.btnFunction === 'link') {
			// 	blockProps.href = attributes.href !== '' ? attributes.href : null;
			// 	block_el_tag = 'a';
			// 	blockProps.target = attributes.openNewTab ? '_blank' : null;
			// 	// need to add rel="noopener" below otherwise breaks button block in editor
			// 	blockProps.rel = attributes.openNewTab ? 'noopener' : null;
			// }
			//
			// // WP link button conditionals
			// if(attributes.btnFunction === 'wp_link') {
			// 	blockProps.href = attributes.postObj !== '' ? attributes.postObj.url : null;
			// 	block_el_tag = 'a';
			// 	if(typeof attributes.postObj !== 'undefined'){
			// 		let opensInNewTab = attributes.postObj.opensInNewTab;
			// 		opensInNewTab = typeof opensInNewTab !== 'undefined' ? opensInNewTab : false;
			// 		blockProps.target = opensInNewTab ? '_blank' : null;
			// 		// need to add rel="noopener" below otherwise breaks button block in editor
			// 		blockProps.rel = opensInNewTab ? 'noopener' : null;
			// 	};
			// }
			//
			// // modal trigger (common) conditionals
			// if(	attributes.btnFunction === 'modal_trigger' ||
			// 	attributes.btnFunction === 'shortcode_modal_trigger' ) {
			//
			// 	blockProps['data-toggle'] = 'modal';
			// 	blockProps['data-modal-size'] = attributes.popupSize !== '' ? attributes.popupSize : null;
			// 	blockProps['data-modal-heading'] = attributes.popupHeading !== '' ? attributes.popupHeading : null;
			// }
			//
			// // iframe modal trigger conditionals
			// if(attributes.btnFunction === 'modal_trigger'){
			// 	blockProps['data-target'] = '#iframe-modal';
			// 	blockProps['data-src'] = attributes.dataSource !== '' ? attributes.dataSource : null;
			// }
			// // shortcode modal conditionals
			// if(attributes.btnFunction === 'shortcode_modal_trigger'){
			// 	blockProps['data-target'] = '#shortcode-ajax-modal';
			// 	blockProps['data-shortcode'] = attributes.dataShortcode !== '' ? encodeURI(attributes.dataShortcode) : null;
			// }
			//
			// // RETURN: Button-Block (FRONT-end)
			// //================================
			// return el( block_el_tag, useBlockProps.save( blockProps ),
			// 		attributes.text,
			//
			// 	);
		},
	} );
}(
	window.wp.blocks,
	window.wp.editor,
	window.wp.i18n,
	window.wp.element,
	window.wp.components,
	window._,
	window.wp.blockEditor,
	window.cwb
) );
