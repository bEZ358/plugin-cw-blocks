( function( blocks, editor, i18n, element, components, _, blockEditor, cwb ) {
	const __ = i18n.__;
	const el = element.createElement;
	const InnerBlocks = blockEditor.InnerBlocks;
	const useBlockProps = blockEditor.useBlockProps;
	const InspectorControls = blockEditor.InspectorControls;
	const select = wp.data.select;

	const {
	} = components;

	const {
		cwbGlobals,
		cwbComponents
	} = cwb;

	const {
	} = cwbGlobals;

	const {
		cwbAttributes,
	} = cwbComponents;


	// @ref links below
	// https://mediaron.com/how-to-create-a-repeater-field6-in-gutenberg/
	// https://reactnative.dev/docs/state

	blocks.registerBlockType( 'cwb/repeater-item--basic', {


		// Block ASSETS
		//-----------------------------

		title: __( 'Simple Repeater Item', 'CWB' ),
		icon: 'admin-page', // see: /wp-includes/css/dashicons.css
		category: 'common',
		attributes: {
			cssClasses: {
				type: 'array',
				default: ['my-0'],//['w-100', 'd-flex', 'm-0', 'align-items-center', 'bg-cover']
			},


			// // Repeater Item Settings
			// //-----------------------------
			heading: {
				type : 'string',
				default: 'Default Heading',
				// selector: 'h2'
			},

			body: {
				type : 'string',
				default: 'Default Body, lorem ipsum...'
			},


			gridCssClasses: {
				type : 'string',
				default: ''
			},

			gapMarginCss: {
				type : 'string',
				default: ''
			},
		},
		example: {
			attributes: {
				// title: __( 'Chocolate Chip Cookies', 'gutenberg-examples' ),
				// mediaID: 1,
				// mediaURL:
				// 	'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/2ChocolateChipCookies.jpg/320px-2ChocolateChipCookies.jpg',
			},
		},



		// Block FUNCTIONS
		//-----------------------------

		edit: function( props ) {
			let attributes = props.attributes;
			let blockStyle = {
				width: '100%',
				margin: '0px',
				padding: '0px',
				border: '1px dotted rgba(0,0,0,0.2)'
			};



			let parentAtts = {...cwbAttributes.globalBlockAtts[props.clientId]};
			let gridCssClasses = __get(parentAtts.gridCssClasses, attributes.gridCssClasses);
			let gapPaddingCss = __get(parentAtts.gapPaddingCss, attributes.gapPaddingCss);

			// console.log(gapPaddingCss);
			props.setAttributes({
				gridCssClasses : parentAtts.gridCssClasses,
				gapMarginCss : parentAtts.gapMarginCss
			});
			attributes = props.attributes;


			let itemCss = [
				gridCssClasses,
				gapPaddingCss,
				...attributes.cssClasses
			];






			// RETURN: Simple-Repeater-Block (BACK-end)
			//================================
			return el( 'div', useBlockProps( {
					className: itemCss,
					style: blockStyle
				} ),
				el('div', { className: 'container px-0' },


					el('h2', {
							className: 'item-head',
							style: {
								position: 'relative'
							}
						},
						el(RichTextBlockInput, {
							currentValue : attributes.heading,
							onChange : function(value){

								props.setAttributes({
									heading : value
								});
								attributes = props.attributes;
							},
							multiLine: false
						}),
					),

					el('p', {
							className: 'item-body',
							style: {
								position: 'relative'
							}
						},
						el(RichTextBlockInput, {
							currentValue : attributes.body,
							onChange : function(value){

								props.setAttributes({
									body : value
								});
								attributes = props.attributes;
							},
							multiLine: false
						}),
					),

					el(InnerBlocks)
				),




				// InspectorControls
				//================================
				el( InspectorControls, {
					key: 'controls' },
				)
			);
		},

		save: function( props ) {
			let attributes = props.attributes;
			let blockStyle = {
				// background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : attributes.bgColorOutput,
				margin: '0px',
				padding: '0px',
				width: '100%',
				// color: attributes.textColor ? attributes.textColor : null,
			};

			// console.log('save', attributes.items);

			// RETURN: Layout-Block (FRONT-end)
			//================================
			return el( 'div', useBlockProps.save( {
					className: attributes.cssClasses.join(' '),
					style: blockStyle
				} ),
				el('div', { className: 'container' },

					el('h2', {
							className: 'item-head',
							style: {
								position: 'relative'
							}
						},
						el(RawHTML,{},
							allowTagsSimpleText( attributes.heading )
						)
					),

					el('p', {
							className: 'item-body',
							style: {
								position: 'relative'
							}
						},
						el(RawHTML,{},
							allowTagsSimpleText( attributes.body )
						)
					),
				)
			);
		},
	} );
}(
	window.wp.blocks,
	window.wp.editor,
	window.wp.i18n,
	window.wp.element,
	window.wp.components,
	window._,
	window.wp.blockEditor,
	window.cwb
) );
