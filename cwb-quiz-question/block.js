( function( blocks, editor, i18n, element, components, _, blockEditor, cwb ) {
	const __ = i18n.__;
	const el = element.createElement;
	const InnerBlocks = blockEditor.InnerBlocks;
	const useBlockProps = blockEditor.useBlockProps;
	const InspectorControls = blockEditor.InspectorControls;

	const {
		// ToggleControl,
	} = components;

	const {
		cwbGlobals,
		cwbComponents
	} = cwb;

	const {
		// screenSizeTabs,
		// defaultScreenSizeObj
	} = cwbGlobals;

	const {
		RepeaterControls,
		RepeaterToolbar,
		Repeater,
		// BgImagePanel,
		// BlockColors,
		// cwbAttributes
	} = cwbComponents;

	const defaultItemObj = {
		key: 0,
		itemText: 'defaultItemObj',
		isCorrect: false,
		imgObj: false
	};


	// @ref links below
	// https://mediaron.com/how-to-create-a-repeater-field6-in-gutenberg/
	// https://reactnative.dev/docs/state

	blocks.registerBlockType( 'cwb/quiz-question', {


		// Block ASSETS
		//-----------------------------

		title: __( 'CWB: Quiz-Question', 'CWB' ),
		icon: 'media-code', // see: /wp-includes/css/dashicons.css
		category: 'common',
		attributes: {
			questionId: {
				type: 'string',
				default: false
			},
			cssClasses: {
				type: 'array',
				default: ['p-0', 'my-0', 'quiz-question'],//['w-100', 'd-flex', 'm-0', 'align-items-center', 'bg-cover']
			},

			questionText: {
				type: 'string',
				default: false
			},

			questionType: {
				type: 'radio', // radio | select | checkbox
				default: false
			},
			questionImg: {
				type: 'boolean',
				default: false
			},
			questionImgObj: {
				type: 'string',
				default: false
			},


			// // Repeater Block Settings
			// //-----------------------------
			// fullWidth: {
			// 	type: 'boolean' // for ToggleControl we need boolean type
			// }
			items: {
				type : 'array',
				default: [
					{ ...defaultItemObj, key: 0, itemText: 'item 1'},
					{ ...defaultItemObj, key: 1, itemText: 'item 2'},
					{ ...defaultItemObj, key: 2, itemText: 'item three'},
				]
			},
			focusItem: {
				type: 'integer',
				default: null
			}
		},
		// example: {
		// 	attributes: {
		// 		// title: __( 'Chocolate Chip Cookies', 'gutenberg-examples' ),
		// 		// mediaID: 1,
		// 		// mediaURL:
		// 		// 	'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/2ChocolateChipCookies.jpg/320px-2ChocolateChipCookies.jpg',
		// 	},
		// },



		// Block FUNCTIONS
		/*--------------------------------------*/

		edit: function( props ) {
			let attributes = props.attributes;
			let blockStyle = {
				width: '100%',
				margin: '0px auto',
				padding: '0px',
			};
			if (!attributes.questionId){
				props.setAttributes({
					questionId: 'q'+Math.floor(Math.random() * 999999)
				});
				attributes = props.attributes
			}



			// Block DRY Functions
			/*--------------------------------------*/

			const updateRepeaterItems = function(newArr){
				props.setAttributes({
					items : newArr
				});
				attributes = props.attributes;
			};

			const setItemAttr = function(value, focusItem, property){

				let newArr = [...attributes.items];

				// update item
				newArr[focusItem.key][property] = value;

				// update block
				updateRepeaterItems(newArr);
			};



			// used for WYSIWYG edits rather than edits via inspectorPanel
			// - called when updating text todo see if there's a way to pull this from RepeaterComponent
			const updateFocus = function(item){
				props.setAttributes({
					focusItem : item//.key.toString()
				});
				attributes = props.attributes;
			};



			// Content Modifiers
			/*--------------------------------------*/

			const updateText = function(value, item){
				setItemAttr(value, item, 'itemText');
			};

			const onSelectImage = function( media, item ) {

				let newArr = [...attributes.items];

				// update item
				newArr[item.key].imgID = media.id;
				newArr[item.key].imgURL = media.url;

				// update block
				updateRepeaterItems(newArr);
			};

			const removeMedia = function( item ) {
				let newArr = [...attributes.items];

				// update item
				// newArr[item.key].bgID = false;
				newArr[item.key].imgURL = false;

				// update block
				updateRepeaterItems(newArr);
			};


			// const updateRepeater = function(newArr, focusItem) {
			//
			// 	// reset item keys
			// 	for(let i = 0; i < newArr.length; i++){
			// 		newArr[i].key = i;
			// 	}
			// 	// update block
			// 	props.setAttributes({
			// 		items : newArr,
			// 		focusItem : focusItem.toString()
			// 	});
			// 	attributes = props.attributes;
			// };

			const [selectOpen, setSelectOpen] = useState(attributes.questionType !== 'select');



			let focusItem = attributes.items[ attributes.focusItem !== null ? attributes.focusItem : 0 ];
			focusItem = fixFocusItem(focusItem, attributes);


			// RETURN: Quiz-Question-Block editor (BACK-end)
			/*--------------------------------------*/
			return el( 'div', useBlockProps( {
					className: attributes.cssClasses,
					style: blockStyle } ),



				// Content Image
				//--------------------------------------
				attributes.questionImg ?

					el('div', {
							// className: 'd-inline-block'
						},
						el(BlockMediaEditor, {
							label: 'Select a Question Image',
							imageSize: 'large',
							image: attributes.questionImgObj,
							onSelect: function (value) {
								props.setAttributes({
									questionImgObj: value
								})
							},
							onRemove: function () {
								props.setAttributes({
									questionImgObj: false
								})
							}
						})
					)
					: null,


				// Quiz-Question Text todo
				/*--------------------------------------*/
				el('p', {}, 'quiz question?'),





				// Answer Options
				/*--------------------------------------*/
				el('p', { className: 'cursor-pointer',
					onClick: function(){
						console.log(attributes.questionType)
						if(attributes.questionType === 'select')
							setSelectOpen(!selectOpen);
					}
				}, 'Choose correct answer'),
				selectOpen ? el('div', {className: 'options'},
					el(SimpleRepeaterBlock, {
							// ...props,
							blockProps: props,
							// itemObjModel : itemObjModel,
						},


						// Answer Option
						/*--------------------------------------*/
						(item, toolbar) => {

							return el('div', {className: 'option',
									style: {
										position: 'relative'
									}},
								el('input', {type: 'radio',
									className: 'pointer-events-none'
								}),
								el('label', {className: ''},

									// Option Content Editor : the text
									/*--------------------------------------*/
									el(RichText,{
										// className: subItem.className ? subItem.className+' mb-3' : 'mb-3',
										tagName: 'p',//subItem.tagName ? subItem.tagName : 'label',
										value: item.itemText,
										onChange: function(value){
											updateText(value, item);
										},
										onFocus: function(){
											updateFocus(item.key.toString());
										},
										onClick: function(){
											updateFocus(item.key.toString());
										}
									}),
								),
								toolbar
							);
						}
					)
				): null,




				// InspectorControls
				/*--------------------------------------*/
				el( InspectorControls, {
						key: 'controls' },

					// Question Type
					/*--------------------------------------*/
					el( SelectControl, {
						className: ['mt-3'],
						key: 'multiple',
						label: __('Question Type'),
						currentValue: attributes.questionType,
						onChange: function(value){
							props.setAttributes({
								questionType : value
							})
						},
						options: [
							// { value: '', label: tab.name === 'base' ? 'default' : 'inherit from smaller' },
							{ value: 'radio', label: 'Radio (one correct answer)' },
							{ value: 'select', label: 'Select (one correct answer)' },
							{ value: 'checkbox', label: 'Checkbox (many correct answers)' },
						]
					}),

					// Question Text (Rich Text)
					/*--------------------------------------*/
					// TODO


					// Quiz Image (Conditional)
					/*--------------------------------------*/
					// TODO
					el( ToggleControl,
						{
							label: 'Include image With Question',
							onChange: function(value){
								props.setAttributes({
									questionImg : value
								})
							},
							checked: attributes.questionImg,
						}
					),
					attributes.questionImg ?

						el(InspectorMediaInput,{
							label: 'Choose Question Image',
							currentValue: attributes.questionImgObj,
							onSelect: function(value){
								props.setAttributes({
									questionImgObj : value
								})
							},
							onRemove: function(){
								props.setAttributes({
									questionImgObj : false
								})
							},
						}): null,


					// Answer Options
					/*--------------------------------------*/
					el(RepeaterControls, {
							// ...props,
							blockProps: props,
							// itemObjModel : itemObjModel,
						},


						attributes.questionImg ? el(InspectorMediaInput,{
							label: 'Choose Answer Option Image',
							currentValue: focusItem.imgObj,
							onSelect: function(value){
								setItemAttr(value, focusItem, 'imgObj')
							},
							onRemove: function(){
								setItemAttr(false, focusItem, 'imgObj')
							},
						}): null,


						// Option Text
						/*--------------------------------------*/
						el( TextControl,
							{
								label: 'item text',
								value: focusItem.itemText,
								onChange: function(value){
									updateText(value, focusItem);
								}
							}
						),


						// Option is Correct
						/*--------------------------------------*/
						el( ToggleControl,
							{
								label: 'This is the correct answer',
								onChange: function(value){

									// TODO:  if(onlyOneCorrect) resetItemsCorrect()
									setItemAttr(value, focusItem, 'isCorrect')
								},
								checked: focusItem.isCorrect,
							}
						)
					),


					//
					// // SELECT INPUT TODO
					// /*--------------------------------------*/
					//
					// // Answer Header
					// /*--------------------------------------*/
					// el(RichText,{
					// 	className: 'cursor-pointer h4 mt-0',
					// 	tagName: 'p',//subItem.tagName ? subItem.tagName : 'label',
					// 	value: attributes.answersHeader,
					// 	onChange: function(value){
					// 		updateText(value, item);
					// 	},
					// 	onClick: function(){
					// 		setSelectOpen(!selectOpen);
					// 	}
					// }),
					//
					//
					// // Answer Options
					// /*--------------------------------------*/
					// selectOpen ? el('div', {className: 'options'},
					// 	el(SimpleRepeaterBlock, { blockProps: props },
					//
					//
					// 		// Answer Option
					// 		/*--------------------------------------*/
					// 		(item, toolbar) => {
					// 			return el('div', {className: 'option',
					// 					style: {
					// 						position: 'relative'
					// 					}},
					//
					// 				// Option Content Editor : the text
					// 				/*--------------------------------------*/
					// 				el(RichText,{
					// 					// className: subItem.className ? subItem.className+' mb-3' : 'mb-3',
					// 					tagName: 'p',//subItem.tagName ? subItem.tagName : 'label',
					// 					value: item.itemText,
					// 					onChange: function(value){
					// 						updateText(value, item);
					// 					},
					// 					onFocus: function(){
					// 						updateFocus(item.key.toString());
					// 					},
					// 					onClick: function(){
					// 						updateFocus(item.key.toString());
					// 					}
					// 				}),
					// 				toolbar
					// 			);
					// 		}
					// 	)
					// ): null,
				)
			);
		},

		save: function( props ) {

			let attributes = props.attributes;
			let blockStyle = {
				background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : attributes.bgColorOutput,
				margin: '0px',
				padding: '0px',
				width: '100%',
				color: attributes.textColor ? attributes.textColor : null,
			};

			let options = attributes.items;


			// RETURN: Quiz-Question-Block (FRONT-end)
			/*--------------------------------------*/
			return el( 'div', useBlockProps.save( {
					id: attributes.questionId,
					className: attributes.cssClasses.join(' '),
					style: blockStyle
				} ),


				// Quiz Image (Conditional) todo
				/*--------------------------------------*/


				// Quiz Question todo
				/*--------------------------------------*/
				el('p', {}, 'quiz question?'),


				// Answer Options
				/*--------------------------------------*/
				el('div', {className: 'options'},

					// Answer Option
					/*--------------------------------------*/
					options.map((option) => {

						return el('div', {className: 'option',
								style: {
									position: 'relative'
								}},
							el('input', {type: 'radio',
								name: attributes.questionId,
								id: attributes.questionId+'-'+option.key,
								value: +option.isCorrect,
							}),
							el('label', {className: '',
									for: attributes.questionId+'-'+option.key
								}, option.itemText,
							)
						);
					})
				),

				//
				// // Answer Options Select Input TODO
				// /*--------------------------------------*/
				// el('div', { className: 'answer-options h4'},
				// 	el('select', {
				// 			name: attributes.questionId,
				// 			// https://stackoverflow.com/questions/16344583/style-select-element-based-on-selected-option
				// 			onChange: ' this.dataset.chosen = this.value; ',
				// 			['data-chosen']: 'none-selected'
				// 		},
				//
				// 		// Answer Header (prompt / default value)
				// 		/*--------------------------------------*/
				// 		el('option', {
				// 				className: 'p',
				// 				value: 'none-selected',
				// 				selected: true,
				// 				disabled: true,
				// 			},
				// 			el(RawHTML, {}, attributes.answersHeader)
				// 		),
				//
				// 		// LOOP Answer Options
				// 		/*--------------------------------------*/
				// 		options.map((option) => {
				//
				// 			// Answer Option
				// 			/*--------------------------------------*/
				// 			let oImgURL = option.imgObj ?
				// 				option.imgObj.sizes ?
				// 					option.imgObj.sizes.medium ?
				// 						option.imgObj.sizes.medium.url :
				// 						option.imgObj.sizes.full.url
				// 					: null
				// 				: null;
				//
				// 			return el('option', {
				// 				value: +option.isCorrect,
				// 				['data-img']: oImgURL
				// 			}, option.itemText)
				// 		})
				// 	)
				//
				// )

			);
		},
	} );
}(
	window.wp.blocks,
	window.wp.editor,
	window.wp.i18n,
	window.wp.element,
	window.wp.components,
	window._,
	window.wp.blockEditor,
	window.cwb
) );