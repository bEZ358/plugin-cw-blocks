( function( blocks, editor, i18n, element, components, _, blockEditor, cwb ) {
	const __ = i18n.__;
	const el = element.createElement;
	const InnerBlocks = blockEditor.InnerBlocks;
	const useBlockProps = blockEditor.useBlockProps;
	const InspectorControls = blockEditor.InspectorControls;

	const {
	} = components;

	const {
		cwbGlobals,
		cwbComponents
	} = cwb;

	const {
	} = cwbGlobals;

	const {
		cwbAttributes
	} = cwbComponents;


	// @ref links below
	// https://mediaron.com/how-to-create-a-repeater-field6-in-gutenberg/
	// https://reactnative.dev/docs/state

	blocks.registerBlockType( 'cwb/repeater-item--slide', {


		// Block ASSETS
		//-----------------------------

		title: __( 'Repeater Item (Slide)', 'CWB' ),
		icon: 'admin-page', // see: /wp-includes/css/dashicons.css
		category: 'common',
		attributes: {
			cssClasses: {
				type: 'array',
				default: ['slide', 'p-3'],//['w-100', 'd-flex', 'm-0', 'align-items-center', 'bg-cover']
			},

			// Common Attribute Sets
			//-----------------------------
			...cwbAttributes.__blockColors,
			...cwbAttributes.__bgImage,
			// ...cwbAttributes.__blockBorder,
			...cwbAttributes.__bgOverlayColors,
			overlayBlur: {
				type: 'integer',
				default: 0,
			},


			vAlign: cwbAttributes.vAlign,
			vAlignCss: {
				type : 'array',
				default: []
			},

			hAlign: cwbAttributes.hAlign,
			hAlignCss: {
				type : 'array',
				default: []
			},


			// // Repeater Item Settings
			// //-----------------------------
			heading: {
				type : 'string',
				default: 'Default Heading',
				// selector: 'h2'
			},

			body: {
				type : 'string',
				default: 'Default Body, lorem ipsum...'
			},


		},
		example: {
			attributes: {
				// title: __( 'Chocolate Chip Cookies', 'gutenberg-examples' ),
				// mediaID: 1,
				// mediaURL:
				// 	'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/2ChocolateChipCookies.jpg/320px-2ChocolateChipCookies.jpg',
			},
		},



		// Block FUNCTIONS
		//-----------------------------

		edit: function( props ) {
			let attributes = props.attributes;
			let vAlignCss = [...attributes.vAlignCss];
			let hAlignCss = [...attributes.hAlignCss];
			let blockStyle = {
				width: '100%',
				// margin: '0px auto',
				padding: '0px',
				border: '1px dotted rgba(0,0,0,0.2)',
				color: attributes.textColor ? attributes.textColor : null,
				background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : attributes.bgColorOutput,
				position: 'relative'
			};




			// Layout Mod Functions
			//-----------------------------
			const setVAlign = function(value) {
				genResponsiveCss(value, 'vAlign','align-items-', vAlignCss, attributes, props );


				props.setAttributes({ vAlignCss : vAlignCss });
				attributes = props.attributes;

				// console.log('vAlignCss', vAlignCss);
			};
			const setHAlign = function(value) {
				genResponsiveCss(value, 'hAlign','text-', hAlignCss, attributes, props );

				props.setAttributes({ hAlignCss : hAlignCss });
				attributes = props.attributes;

				// console.log('hAlignCss', hAlignCss);
			};
			const onUpdateLayout = function(data, attrStr){

				console.log('onUpdateLayout', attrStr, data);

				switch(attrStr){
					case 'vAlign':
						setVAlign(data);
						break;
					case 'hAlign':
						setHAlign(data);
						break;
					default:
						break;
				}
			};
			const setContainerClasses = function( value ) {
				props.setAttributes( { containerClasses: value } );
				attributes = props.attributes;
			};


			// for ResponsiveTabs
			//-----------------------------
			const [layoutTab, setLayoutTab] = useState(false);





			// RETURN: Simple-Repeater-Block (BACK-end)
			//================================
			return el( 'div', useBlockProps( {
					className: attributes.cssClasses,
					style: blockStyle
				} ),



				// attributes.bgEffect ?
					el(CwbBgEffect, {
						bgOverlay : attributes.overlayColorOutput,
						backdropFilter: attributes.overlayBlur ?
							'blur('+attributes.overlayBlur+'px)' : null
					}),
					// : null,


				el('div', {
						className: [
							attributes.containerClasses,
							...attributes.vAlignCss,
							'd-flex',
							'slide-flex'
						].join(' ')
					},
					el('div', {
							className: [
								...attributes.hAlignCss,
								'slide-content w-100'
							].join(' ')
						},
						el('div', { className: 'container' },



							el('h2', {
									className: 'item-head',
									style: {
										position: 'relative'
									}
								},
								el(RichTextBlockInput, {
									currentValue : attributes.heading,
									onChange : function(value){

										props.setAttributes({
											heading : value
										});
										attributes = props.attributes;
									},
									multiLine: false
								}),
							),

							el('p', {
									className: 'item-body',
									style: {
										position: 'relative'
									}
								},
								el(RichTextBlockInput, {
									currentValue : attributes.body,
									onChange : function(value){

										props.setAttributes({
											body : value
										});
										attributes = props.attributes;
									},
									multiLine: false
								}),
							),

							el(InnerBlocks, {
								allowedBlocks : [
									'cwb/button',
								],
								// template : [
								// 	[ 'cwb/'+attributes.repeaterItemType, { heading: 'Slide 1' } ],
								// 	[ 'cwb/'+attributes.repeaterItemType, { heading: 'Slide 2' } ],
								// ],
								// renderAppender : allowMoreItems ? null : false
							}),
						)
					),
				),




				// InspectorControls
				//================================
				el( InspectorControls, {
					key: 'controls' },


					// Panel : Wrap Settings
					//-----------------------------
					el( PanelBody, {
							title: 'Layout Settings',
							initialOpen: false//!0
						},

						el( TextControl, {
							label: __( 'Container CSS Class(es)'),
							help: 'Separate multiple classes with spaces.',
							value: attributes.containerClasses,
							type: 'text',
							onChange: setContainerClasses,
						}),


						el(ResponsiveTabs, props,
							(tab) => {
								setLayoutTab(tab);
								// ^ fixes something (not sure what yet) to allow below to work
								props = {
									// tab:{...layoutTab},
									// ^ this would make more sense (maybe?) but somehow below gets fixed after
									//   setLayoutTab(tab) is called... don't understand why
									tab: {...tab},
									...props,
									// enabled: [
									// 	'vAlign',
									// 	//'hAlign'
									// ],
								};
								// console.log('props.tab.name', props.tab.name);
								return el(BlockLayoutSettingsTab, {
									...props,
									onUpdateLayout: function(data, attrStr){
										onUpdateLayout(data, attrStr)
									}
								});
							}
						)
					),


			  		// Panel : Block Colors
					//-----------------------------
					el( BlockColors, props),

					// Panel : Background Image
					//-----------------------------
					el( BgImagePanel, {
						props : props,
						blockStyle : blockStyle
					}),

					// Panel : BgEffects
					//-----------------------------
					el( BgEffects, props),
				)
			);
		},

		save: function( props ) {
			let attributes = props.attributes;
			let includeAltText = attributes.includeAriaLabel && attributes.mediaAlt.length;
			let blockStyle = {
				// background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : attributes.bgColorOutput,
				margin: '0px',
				padding: '0px',
				width: '100%',
				color: attributes.textColor ? attributes.textColor : null,
				background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : attributes.bgColorOutput,
				position:'relative'
				// color: attributes.textColor ? attributes.textColor : null,
			};


			// RETURN: Layout-Block (FRONT-end)
			//================================
			return el( 'div', useBlockProps.save( {
					className: attributes.cssClasses.join(' '),
					style: blockStyle,
					role: includeAltText ? 'img' : null,
					['aria-label']: includeAltText ? attributes.mediaAlt : null,
				} ),


				// attributes.bgEffect ?
				el(CwbBgEffect, {
					bgOverlay : attributes.overlayColorOutput,
					backdropFilter: attributes.overlayBlur ?
						'blur('+attributes.overlayBlur+'px)' : null
				}),
				// : null,

				el('div', {
						className: [
							attributes.containerClasses,
							...attributes.vAlignCss,
							'd-flex',
							'slide-flex'
						].join(' ')
					},

					el('div', {
							className: [
								...attributes.hAlignCss,
								'slide-content w-100 sticky-buffer-child'
							].join(' ')
						},

						el('div', { className: 'container' },

							el('h2', {
									className: 'item-head',
									style: {
										position: 'relative'
									}
								},
								el(RawHTML,{},
									allowTagsSimpleText( attributes.heading )
								)
							),

							el('p', {
									className: 'item-head',
									style: {
										position: 'relative'
									}
								},
								el(RawHTML,{},
									allowTagsSimpleText( attributes.body )
								)
							),

							el( InnerBlocks.Content )
						)
					)
				)
			);
		},
	} );
}(
	window.wp.blocks,
	window.wp.editor,
	window.wp.i18n,
	window.wp.element,
	window.wp.components,
	window._,
	window.wp.blockEditor,
	window.cwb
) );
