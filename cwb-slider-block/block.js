( function( blocks, editor, i18n, element, components, _, blockEditor, cwb ) {
	const __ = i18n.__;
	const el = element.createElement;
	const InnerBlocks = blockEditor.InnerBlocks;
	const useState = element.useState;
	const useBlockProps = blockEditor.useBlockProps;
	const InspectorControls = blockEditor.InspectorControls;
	const LinkControl = blockEditor.__experimentalLinkControl;
	const RichText = blockEditor.RichText;
	const RawHTML = element.RawHTML;
	const useSelect = wp.data.useSelect;

	const {
		Dashicon,
		Toolbar,
		ToolbarButton,
		// ToggleControl,
		// Panel,
		// PanelBody,
		// PanelRow
	} = components;

	const {
		cwbGlobals,
		cwbComponents
	} = cwb;

	const {
		screenSizeTabs,
		defaultScreenSizeObj,
	} = cwbGlobals;

	const {
		// RepeaterBtn,
		// RepeaterSliderEditBlock,
		// RepeaterBtnEditor,
		// RepeaterControls,
		// RepeaterToolbar,
		// Repeater,
		cwbAttributes
	} = cwbComponents;



	const slideObjModel = {
		key: 0,
		itemText: 'item 1',
		itemText2: 'subheading text',
		bgCol: '#5db96e',
		bgURL: false,
		bgID: false,
		bgTint: 0,
		bgObj: false,
		bgAltText: false,
		postObj: false,
		btn: false,
		btnObj: {...cwbAttributes.defaultButtonObj},
		wpContentPopover: false,
		// vAlign: cwbAttributes.vAlign,
		vAlign: {...defaultScreenSizeObj, base: 'align-items-center'},
		hAlign: {...defaultScreenSizeObj, base: 'text-center'},
		cssClasses: []
		// testProp: true
	};

	function slideTint( value ) {
		return el( 'div' , {
			style : {
				background: value ? 'rgba(0,0,0,'+ value/10 +')' : null,
				top: 0,
				left: 0,
				width: '100%',
				height: '100%',
				position: 'absolute'
			}
		})
	}


	// @ref links below
	// https://mediaron.com/how-to-create-a-repeater-field6-in-gutenberg/
	// https://reactnative.dev/docs/state

	// todo: depreciate, used in:
	// 	- DP&O



	blocks.registerBlockType( 'cwb/slider-block', {


		// Block ASSETS
		//-----------------------------

		title: __( 'CWB: Slider Block', 'CWB' ),
		icon: 'slides', // see: /wp-includes/css/dashicons.css
		category: 'common',
		attributes: {
			cssClasses: {
				type: 'array',
				default: ['my-0'],//['w-100', 'd-flex', 'm-0', 'align-items-center', 'bg-cover']
			},

			// // Common Attribute Sets
			// //-----------------------------
			// ...cwbAttributes.__blockColors,
			// ...cwbAttributes.__bgImage,
			// // ...cwbAttributes.__blockBorder,

			// Wrap Settings
			//-----------------------------
			blockHeight: cwbAttributes.blockHeight,
			// vAlign: cwbAttributes.vAlign,
			includeAriaLabel: {
				type: 'boolean', // for ToggleControl we need boolean type
				default: false,
			},


			// Repeater Block Settings
			//-----------------------------
			items: {
				type : 'array',
				default: [
					{ ...slideObjModel, key: 0, bgCol: '#5db96e',itemText: 'item 1'},
					{ ...slideObjModel, key: 1, bgCol: '#e1b681',itemText: 'item 2'},
					{ ...slideObjModel, key: 2, bgCol: '#5790cb',itemText: 'item three'},
				]
			},
			focusItem: {
				type: 'string',
				default: '0'
			},
			useStickyBuffer: {
				type: 'boolean',
				default: false
			},
			// headerIsOpaque: {
			// 	type: 'boolean',
			// 	default: false
			// },
		},
		example: {
			attributes: {
				// title: __( 'Chocolate Chip Cookies', 'gutenberg-examples' ),
				// mediaID: 1,
				// mediaURL:
				// 	'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/2ChocolateChipCookies.jpg/320px-2ChocolateChipCookies.jpg',
			},
		},



		// Block FUNCTIONS
		//-----------------------------

		edit: function( props ) {

			let attributes = props.attributes;
			let blockStyle = {
				width: '100%',
				margin: '0px auto',
				padding: '0px',
			};

			setDefaultScreenSizeObj(props, 'blockHeight');


			// Block Editor Functions
			//--------------------------------------

			const updateRepeaterItems = function(newArr){
				props.setAttributes({
					items : newArr
				});
				attributes = props.attributes;
			};

			const setItemAttr = function(value, focusItem, property){

				let newArr = [...attributes.items];

				// update item
				newArr[focusItem.key][property] = value;

				//
				// update block
				updateRepeaterItems(newArr);
			};






			// Content Modifiers
			//--------------------------------------

			const onSelectImage = function( media, item ) {

				let newArr = [...attributes.items];

				// update item
				newArr[item.key].bgID = media.id;
				newArr[item.key].bgURL = media.url;
				if(typeof newArr[item.key].bgAltText !== 'undefined')
					newArr[item.key].bgAltText = typeof media.alt !== 'undefined' && media.alt.length ?
						media.alt : false;



				// update block
				updateRepeaterItems(newArr);
			};

			const removeMedia = function( item ) {
				let newArr = [...attributes.items];

				// update item
				newArr[item.key].bgID = false;
				newArr[item.key].bgURL = false;

				// update block
				updateRepeaterItems(newArr);
			};

			const setItemBtnAtts = function(value, focusItem, btnProperty){

				let newArr = [...attributes.items];

				// update item
				newArr[focusItem.key].btnObj[btnProperty] = value;
				//
				// update block
				updateRepeaterItems(newArr);
			};






			// Block States
			//--------------------------------------

			const [layoutTab, setLayoutTab] = useState(false);
			const [sliderHeightTab, setSliderHeightTab] = useState(false);

			const [wpContentPopover, setWpContentPopover] = useState(false);
			const toggleWpContentPopover = function(){

				/**
				 * managing from block level because popover also needs to
				 * get toggled by other UI elements in the block editor
				 */
				setWpContentPopover( !wpContentPopover )
			};



			const toggleAriaLabel = function( value ) {
				props.setAttributes( { includeAriaLabel: value } );
				attributes = props.attributes;
			};






			// Focus Item
			//--------------------------------------

			// // todo: trying to sync block state focusItemState with RepeaterComponent state focusItemKey
			// // ...not quite there yet...
			// const [focusItemState, setfocusItemState] = useState(0);
			// props = {
			// 	focusItemState: focusItemState,
			// 	...props
			// };
			// let focusItem = focusItemState;

			let focusItem = attributes.items[ attributes.focusItem !== null ? attributes.focusItem : 0 ];






			// Dynamic WP Content
			//--------------------------------------
			// * these cannot be inside functions, event handlers, or conditional statements
			// 	- ref: https://reactjs.org/docs/hooks-rules.html
			//  - dynamic content gets loaded into FOCUS item once select is called
			//  - use selectors to get that information from the store

			// get Post title & thumbnail id
			const selectedPost = useSelect((select) => {
				return wp.data.select('core').getEntityRecord('postType','post', focusItem.postObj.id);
			});
			if(selectedPost){
				focusItem.postObj = {
					...focusItem.postObj,
					title: selectedPost.title.raw,
					featured_media: selectedPost.featured_media,
					link: selectedPost.link
				};
			}

			// get post thumbnail
			const slideImage = useSelect((select) => {
				return wp.data.select('core').getEntityRecord('postType','attachment', focusItem.postObj.featured_media);
			});
			if(slideImage){
				focusItem.postObj = {
					...focusItem.postObj,
					image: slideImage
				};
			}








			// RETURN: Slider Block Editor
			//================================
			return el( 'div', useBlockProps( {
					className: attributes.cssClasses,
					style: blockStyle
				} ),

				// RepeaterSliderEditBlock
				//================================
				el(RepeaterSliderEditBlock, {
						// ...props,
						blockProps: props,
						itemObjModel : slideObjModel,
					},

					// Slide Editor
					// - Custom Slide markup with a RepeaterSliderEditBlock
					//-----------------------------------------
					(slide) => {

						let item = slide;
						let vAlignCssClasses = genResponsiveClasses(item.vAlign);
						let hAlignCssClasses = genResponsiveClasses(item.hAlign);
						let sliderHCssClasses = genResponsiveClasses(attributes.blockHeight);
						let class_name = typeof item.cssClasses !== 'undefined' ? ' '+item.cssClasses : '';

						// console.log('props', props);

						return el( 'div', {
								className: 'slide w-100 p-5 bg-cover '+class_name,
								style: {
									background: item.bgURL ? "url('"+item.bgURL+"')" : item.bgCol,
									display: 'block'
								},
								dataId: item.key+1},


							// Slide Tint (conditional)
							//-------------------------
							item.bgTint ? el(CwbSlideTint, { tintValue : item.bgTint }) : null,



							// Slide Main
							//-----------------------------------------
							el( 'div', { className: 'container text-white p-3 p-md-4 p-xl-5  '+sliderHCssClasses+' d-flex '+vAlignCssClasses+' _justify-content-center'},


								// Slide Content
								//-----------------------------------------
								el( 'div', { className: 'slide-content w-100  '+hAlignCssClasses},

									el( 'div', {},

										// Slide SubHeadline (conditional)
										//-------------------------
										item.itemText2 !== '' ? el('h4', { className: 'mb-0 position-relative'},
											// itemText2El,

											el(RichTextBlockInput, {
												currentValue : focusItem.itemText2,
												onChange : function(value){
													setItemAttr(value,focusItem,'itemText2')
												},
											})
										) : null,


										// Slide Headline
										//-------------------------
										el('h1', { className: 'mt-0 position-relative'},

											el(RichTextBlockInput, {
												currentValue : focusItem.itemText,
												onChange : function(value){
													setItemAttr(value,focusItem,'itemText')
												},
											})
										),

										// Slide Button (optional)
										//-------------------------
										focusItem.btn ? el(RepeaterBtn, {
											blockProps: props,
											// ...props,
											item: item,
										}) : null,
									)
								),



								// Slide-Specific UI Repeater Toolbar
								//-----------------------------------------
								props.isSelected ? el( 'div',{
										style: {
											padding: 0,
											border: '1px solid #bbb',
											position: 'absolute',
											top: '-20px',
											zIndex: 1000
										}
									},
									el(RepeaterToolbar, props),
								): null,




								// Experimental - Get / Use WP Content (popover)
								//--------------------------------------
								wpContentPopover ?
									el (RepeaterWpContentPopover, {
										...props,
										focusItem : focusItem,
										selectedPost : selectedPost,
										togglePopover: toggleWpContentPopover,
									}) : null,
							),

						)
					}),






				// InspectorControls
				//================================
				el( InspectorControls, {
						key: 'controls' },


					el( 'div', {
							style: {
								margin: '0 0 20px'
							},
						},
						el( PanelBody, {
								title: 'Slider Height',
								initialOpen: false//!0,
							},
							el(ResponsiveTabs, props,
								(tab) => {
									setSliderHeightTab(tab);
									// ^ fixes something (not sure what yet) to allow below to work
									props = {
										// tab:{...sliderHeightTab},
										// ^ this would make more sense (maybe?) but somehow below gets fixed after
										//   setLayoutTab(tab) is called... don't understand why
										tab:{...tab},
										...props
									};
									// console.log('BlockHeightTab.ResponsiveTabs | tab', tab);

									// console.log('props.tab.name', props.tab.name);
									return el(BlockHeightTab, props);
								}
							),

							el('div', { style: { margin: '30px 0 0'},},
								el( ToggleControl,
									{
										label: 'Use Sticky Buffer',
										onChange: function(value){
											props.setAttributes({
												useStickyBuffer : value
											});
											attributes = props.attributes
										},
										checked: props.attributes.useStickyBuffer,
									}
								)
							),

							// attributes.useStickyBuffer ?
							// 	el('div', { style: { margin: '30px 0 0'},},
							// 		el( ToggleControl,
							// 			{
							// 				label: 'Header is Opaque',
							// 				onChange: function(value){
							// 					props.setAttributes({
							// 						headerIsOpaque : value
							// 					});
							// 					attributes = props.attributes
							// 				},
							// 				checked: props.attributes.headerIsOpaque,
							// 			}
							// 		)
							// 	) : null
						)
					),


					el(RepeaterControls, {
							// ...props,
							blockProps: props,
							itemObjModel : slideObjModel,
						}, //(focusItemKey) => {
						// // todo: trying to sync block state focusItemState with RepeaterComponent state focusItemKey
						// 	console.log('Block', 'focusItemKey:',  focusItemKey, 'focusItemState:', focusItemState);


						// RepeaterControls : first child props.children[0]
						//------------------------------------------------------
						el('div', {
								style: {
									padding: '10px 0 0'
								}
							},

							el('h5', {
									style: {
										padding: '0 20px 10px'
									}
								},
								'Slide ' + (focusItem.key + 1) + ': ',
								el('strong', {}, focusItem.itemText.replace(/<[^>]+>/g, ''))),

							el('div', {
									style: {
										padding: '0 10px 0',
										// margin: '30px 0 0'
									},
								},


								// Slide Content
								//--------------------------------------
								el(PanelBody, {
										title: 'Slide Content',
										initialOpen: false//!0,
									},


									// Slide Image
									//--------------------------------------
									el(MediaUpload, {
											// extra links: https://www.liip.ch/en/blog/add-an-image-selector-to-a-gutenberg-block
											onSelect: function (media) {
												onSelectImage(media, focusItem);
											},
											allowedTypes: 'image',
											value: focusItem.bgURL,
											render: function (obj) {
												return !focusItem.bgURL ?

													// UI: Choose Image (conditional)
													//-----------------------------
													el('div', null,
														el(components.Button, {
																className: 'editor-post-featured-image__toggle',
																style: {margin: '10px 0'},
																onClick: obj.open,
															},
															__('Choose Image', 'gutenberg-examples')
														)
													) :

													// UI: Manage BG Image (conditional)
													//-----------------------------
													el('div', null,

														// UI: Preview / Replace Image
														//-----------------------------
														el('div', {
																className: 'image-button cursor-pointer',
																style: {margin: '10px 0'},
																onClick: obj.open,
															},
															el('img', {
																	src: focusItem.bgURL
																}
															)
														),

														// UI: Remove Image
														//-----------------------------
														el(components.Button, {
																className: 'button-large',
																style: {margin: '0 0 20px'},
																onClick: function () {
																	removeMedia(focusItem)
																},
																isTertiary: !0,
																// isLink: !0,
																isDestructive: !0,
															},
															__('Remove Image', 'gutenberg-examples')
														),
													)
											}
										}
									),

									el(NumberControl, {
										label: 'Image Tint',
										className: 'mt-0 mb-2 f-sz-14',
										value: focusItem.bgTint,
										// isShiftStepEnabled: true,
										// shiftStep : 1,
										max: 10,
										min: 0,
										onChange: function (value) {
											setItemAttr(value, focusItem, 'bgTint');
										},
									}),


									// Slide Heading
									//--------------------------------------
									el(RichTextInput, {
										label : 'Slide Heading',
										currentValue : focusItem.itemText,
										onChange : function(value){
											setItemAttr(value,focusItem,'itemText')
										},
									}),

									// Slide Subheading
									//--------------------------------------
									el(RichTextInput, {
										label : 'Slide Subheading',
										currentValue : focusItem.itemText2,
										onChange : function(value){
											setItemAttr(value,focusItem,'itemText2')
										},
									}),


									// Slide Button Toggle
									//--------------------------------------
									el('div', { className: 'my-3'},//style: {margin: ' 0 0 30px'}},
										el(ToggleControl,
											{
												label: 'include a button',
												onChange: function (value) {
													setItemAttr(value, focusItem, 'btn');
												},
												checked: focusItem.btn,
												help: focusItem.btn ? 'Click the slide button to edit it' : null,
											}
										),
									),


									// Experimental - Get / Use WP Content (trigger)
									// (still buggy)
									//--------------------------------------
									el( components.Button, {
										className: 'button button-large cursor-pointer mb-3',
										onClick: toggleWpContentPopover
									}, 'Get / Use WP Content' ),



									el(InspectorTextInput, {...props,
										label: __('Slide Css Classes'),
										currentValue: focusItem.cssClasses,
										onChange: function(value){
											setItemAttr(value, focusItem, 'cssClasses');
										},
									}),
								),
							),


							// Slide Layout
							//--------------------------------------
							el('div', {
									style: {
										padding: '0 10px 0',
										margin: '0 0 20px'
									},
								},
								el(PanelBody, {
										title: 'Slide Layout',
										initialOpen: false//!0,
									},


									el(ResponsiveTabs, props,
										(tab) => {
											setLayoutTab(tab);
											// ^ fixes something (not sure what yet) to allow below to work
											props = {
												// tab:{...layoutTab},
												// ^ this would make more sense (maybe?) but somehow below gets fixed after
												//   setLayoutTab(tab) is called... don't understand why
												tab: {...tab},
												...props
											};
											// console.log('props.tab.name', props.tab.name);
											return el(RepeaterLayoutSettingsTab, props);
										}
									),
								)
							),


							// Dev Utils
							// TODO make conditional & compare item model vs current item
							// --------------------------------------
							el( 'div', {
									style: {
										padding: '0 25px 20px',
										// margin: '30px 0 0'
									},
								},
								el( components.Button, {
									className: 'button button-large cursor-pointer',
									onClick: function(){


										let newArr = [...attributes.items];
										let newSlide = updateItemModel(focusItem, slideObjModel );

										// console.log( newSlide);


										// update item
										newArr[focusItem.key] = newSlide;
										//
										// update block
										props.setAttributes({
											items : newArr
										});
										attributes = props.attributes;

									}
								}, 'Update Slide Model' ),
							),
						),

						el( 'div', {
								style: {
									padding: '0 20px'
								},
							},
							el( ToggleControl,
								{
									label: 'Include ARIA Labels for Background Images',
									onChange: toggleAriaLabel,
									checked: props.attributes.includeAriaLabel,
								}
							)
						)

						// // Dev Utils
						// //--------------------------------------
						// el( 'div', {
						// 		style: {
						// 			padding: '0 25px 20px',
						// 			// margin: '30px 0 0'
						// 		},
						// 	},
						// 	el( components.Button, {
						// 		className: 'button button-large cursor-pointer',
						// 		onClick: function(){
						// 			let newArr = [...attributes.items];
						// 			for( let x = 0; x < attributes.items.length; x++ ){
						// 				let newItem = updateItemModel(attributes.items[x], itemObjModel );
						//
						// 				// update item
						// 				newArr[attributes.items[x].key] = newItem;
						// 				//
						// 			}
						//
						// 			// update block
						// 			props.setAttributes({
						// 				items : newArr
						// 			});
						// 			attributes = props.attributes;
						//
						// 		}
						// 	}, 'Update Item Models' ),
						// }
					),
				)
			);
		},

		save: function( props ) {
			let self = this;
			let attributes = props.attributes;
			let blockStyle = {
				// background: attributes.mediaURL !== '' ? `url(${ attributes.mediaURL })` : attributes.bgColorOutput,
				margin: '0px',
				padding: '0px',
				width: '100%',
				// color: attributes.textColor ? attributes.textColor : null,
			};


			// RETURN: Layout-Block (FRONT-end)
			//================================
			return el( 'div', useBlockProps.save( {
					className: attributes.cssClasses.join(' '),
					style: blockStyle,
					// ['bez']: true
				}),

				// el('div', {className:'sticky-buffer-here'}),

				// The Slider
				//-----------------
				el(CwbSlider, {
					useStickyBuffer : attributes.useStickyBuffer,
					// headerIsOpaque : attributes.headerIsOpaque,
					...props
				}, (item) => {


					if(typeof item.bgObj !== 'undefined'){
						item.bgObj = item.bgURL? {
							type: 'image',
							value: item.bgURL,
							lazyLoad: true,
							alt : attributes.includeAriaLabel && typeof item.bgAltText !== 'undefined' ? item.bgAltText : false,
						} : false;
					} else {
						console.log('"Update Slide Model" required for item:', item.itemText);
					}


					// The SLIDE
					//-----------------
					return el( CwbSlide, {
							item : item,
							slideHeight : attributes.blockHeight,
							useStickyBuffer : attributes.useStickyBuffer,
						},
						el( 'div', {},
							item.itemText2 !== ''? el('p', { className: 'h2 clickable mb-0',
									style: {
										position: 'relative'
									},
								},
								el(RawHTML, {}, item.itemText2)
							) : null,
							el('h1', { className: 'clickable mt-0',
									style: {
										position: 'relative'
									},
								},
								el(RawHTML, {}, item.itemText)
							),

							item.btn ?
								el( CwbButton, {
									btnObj : item.btnObj,
									btnType : item.btnObj.btnType,
									extraBtnCss : ['btn-hover-scale']
								}): null,

							// item.btn && item.btn2 ?
							// 	el( CwbButton, {
							// 		btnObj : item.btn2Obj,
							// 		extraBtnCss : ['btn-hover-scale','m-1'],
							// 		btnType : 'btn-alt-lite'
							// 	}): null,
						)
					)
				})
			);
		},
	} );
}(
	window.wp.blocks,
	window.wp.editor,
	window.wp.i18n,
	window.wp.element,
	window.wp.components,
	window._,
	window.wp.blockEditor,
	window.cwb
) );
